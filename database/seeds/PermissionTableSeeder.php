<?php


use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;


class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $permissions = [
           'plant-index',
           'plant-list',
           'plant-create',
           'plant-edit',
           'plant-show',
           'plant-delete',

           'plant-admin-index',
           'plant-admin-list',
           'plant-admin-create',
           'plant-admin-edit',
           'plant-admin-show',
           'plant-admin-delete',


           'deviation-index',
           'deviation-list',

           'report-index',
           'analytics-index',
           'manual-data-entry-index',
           'configuration-index',

           'role-index',
           'role-list',
           'role-create',
           'role-edit',
           'role-show',
           'role-delete',
           
           'instrument-index',
           'instrument-list',
           'instrument-create',
           'instrument-show',
           'instrument-edit',
           'instrument-delete',

           'user-index',
           'user-list',
           'user-create',
           'user-show',
           'user-edit',
           'user-delete',

           'permission-index',
           'permission-list',
           'permission-create',
           'permission-delete',


           'department-index',
           'department-list',
           'department-create',
           'department-show',
           'department-edit',
           'department-delete',

           'parameter-index',
           'parameter-list',
           'parameter-create',
           'parameter-show',
           'parameter-edit',
           'parameter-delete',

           'qrcode-index',
           'qrcode-list',
           'qrcode-create',
           'qrcode-show',
           'qrcode-edit',
           'qrcode-delete',

           'route-index',
           'route-list',
           'route-create',
           'route-show',
           'route-edit',
           'route-delete',

           'alarm-index',
           'alarm-list',
           'usermanagement-index',

           'skipreason-index',
           'skipreason-list',
           'skipreason-create',
           'skipreason-show',
           'skipreason-edit',
           'skipreason-delete',

           'unit-index',
           'unit-list',
           'unit-create',
           'unit-show',
           'unit-edit',
           'unit-delete',

           'line-index',
           'line-list',
           'line-create',
           'line-show',
           'line-edit',
           'line-delete',

           'asset-index',
           'asset-list',
           'asset-create',
           'asset-show',
           'asset-edit',
           'asset-delete',

           'workorder-index',
           'workorder-list',
           'workorder-create',
           'workorder-show',
           'workorder-edit',
           'workorder-delete',

           'workorder-history-index',
           
           'instrument-workorder-index',
           'instrument-workorder-list',
           'instrument-workorder-create',
           'instrument-workorder-show',
           'instrument-workorder-edit',
           'instrument-workorder-delete',

           'report-workorder-index',

           'tool-index',
           'tool-list',
           'tool-create',
           'tool-show',
           'tool-edit',
           'tool-delete',


           'statuses-index',
           'status-index',
           'status-list',
           'status-create',
           'status-show',
           'status-edit',
           'status-delete',

           'unitstatus-index',
           'unitstatus-list',
           'unitstatus-create',
           'unitstatus-show',
           'unitstatus-edit',
           'unitstatus-delete',

           'plantstatus-index',
           'plantstatus-list',
           'plantstatus-create',
           'plantstatus-show',
           'plantstatus-edit',
           'plantstatus-delete',

           'departmentstatus-index',
           'departmentstatus-list',
           'departmentstatus-create',
           'departmentstatus-show',
           'departmentstatus-edit',
           'departmentstatus-delete',

           'linestatus-index',
           'linestatus-list',
           'linestatus-create',
           'linestatus-show',
           'linestatus-edit',
           'linestatus-delete',

           'assetstatus-index',
           'assetstatus-list',
           'assetstatus-create',
           'assetstatus-show',
           'assetstatus-edit',
           'assetstatus-delete',

           'instrumentstatus-index',
           'instrumentstatus-list',
           'instrumentstatus-create',
           'instrumentstatus-show',
           'instrumentstatus-edit',
           'instrumentstatus-delete', 
           'parametervalue-index',
           'toolstatus-index',
           'toolstatus-list',
           'toolstatus-create',
           'toolstatus-show',
           'toolstatus-edit',
           'toolstatus-delete',

           'plant_admin_summary_dashboard-index',
           'tree_dashboard-index',
           'alarm_dashboard-index',
           'dasboard-index',
           'workorderdashboard-index',
           'plant_admin_alarm_dashboard-index',

           'alarm-create',
           'alarm-edit',
           'alarm-delete', 
        ];


        foreach ($permissions as $permission) {
             Permission::create(['name' => $permission]);
        }
    }
}