<?php 
  
use Illuminate\Database\Seeder;
use App\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
  
class CreateAdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

  
        $superAdmin = User::create([
            'name' => 'Super Admin', 
            'email' => 'super_admin@icssww.com',
            'password' => bcrypt('12345678'),
            'role' => 'super_admin',
        ]);

        $superAdminRole = Role::create(['name' => 'super_admin']);
        $plantAdminRole = Role::create(['name' => 'plant_admin']);

        $plantAdminPermissions = Permission::whereNotIn('id',[1,2,3,4,5,6,7,8,9,10,11,12,19,20,21,22,23,24,37,38,39,40,67])
            ->pluck('id');  

        $superAdminPermissions = Permission::whereIn('id',[1,2,3,4,5,6,7,8,9,10,11,12,32,33,34,35,36,19,20,21,22,23,24,37,38,39,40,67])
            ->pluck('id');             

        $plantAdminRole->syncPermissions($plantAdminPermissions);
        $superAdminRole->syncPermissions($superAdminPermissions);
   
        $superAdmin->assignRole([$superAdminRole->id]);

    }
}