<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Workorders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
     {
        Schema::create('workorders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('description');
            $table->integer('instrument_id')->nullable();
            $table->string('type');
            $table->string('priority');
            $table->boolean('send_email');
            $table->boolean('send_sms');
            $table->integer('expected_time');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('workorders');
    }
}
