<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RouteInstrumentSkipped extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('route_instrument_skipped', function (Blueprint $table) {
            
            $table->bigIncrements('id');
            $table->unsignedBigInteger('route_history_id');
            $table->foreign('route_history_id')->references('id')->on('route_history');
            $table->unsignedBigInteger('instrument_id');
            $table->foreign('instrument_id')->references('id')->on('instruments');
            $table->unsignedBigInteger('skipped_id');
            $table->foreign('skipped_id')->references('id')->on('skip_reasons');
            $table->string('remarks')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('route_instrument_skipped');
    }
}
