<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlarmCurrentState extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
     {
        Schema::create('alarm_current_state', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('alarm_id');
            $table->foreign('alarm_id')->references('id')->on('alarms');
          
            $table->string('alarm_message');
            $table->string('new_value');
            $table->string('state');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alarm_current_state');
    }
}
