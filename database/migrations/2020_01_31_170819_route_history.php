<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RouteHistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('route_history', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('route_id');
            $table->foreign('route_id')->references('id')->on('routes');
            $table->unsignedBigInteger('operator_id');
            $table->foreign('operator_id')->references('id')->on('users');
            $table->string('route_state');
            $table->dateTime('start_time');
            $table->dateTime('end_time');
            $table->integer('duration_min');   
            $table->integer('instrument_skiped');
            $table->integer('parameter_skiped');
            $table->integer('sessions');         
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
