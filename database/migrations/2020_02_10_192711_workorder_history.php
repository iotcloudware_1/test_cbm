<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class WorkOrderHistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('workorder_history', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('current_workorder_id');
            $table->foreign('current_workorder_id')->references('id')->on('current_workorder');
            $table->string('action');
            $table->unsignedBigInteger('action_taken_by');
            $table->foreign('action_taken_by')->references('id')->on('users');
            $table->timestamps();
        });    
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('workorder_history');
    }
}
