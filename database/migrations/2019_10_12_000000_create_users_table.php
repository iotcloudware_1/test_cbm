<?php


use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */ 
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('plant_id')->nullable();
            $table->foreign('plant_id')->references('id')->on('plants');
            

            $table->string('name');           
            $table->string('email');
            $table->string('password');

            $table->string('api_token', 80)
            ->unique()
            ->nullable()
            ->default(null);
            
            $table->rememberToken();
            
            $table->string('address')->nullable();
            $table->string('role')->nullable();
            $table->bigInteger('phone1')->nullable();
            $table->bigInteger('phone2')->nullable();
            $table->string('created_by')->nullable();
            $table->string('scope')->nullable();
            $table->bigInteger('dept_limit')->nullable();
            $table->bigInteger('instrument_limit')->nullable();
            $table->bigInteger('parameter_limit')->nullable();
            $table->bigInteger('user_limit')->nullable();
            $table->bigInteger('alarm_limit')->nullable();
            $table->bigInteger('routes_limit')->nullable();
            $table->bigInteger('unit_limit')->nullable();
            $table->bigInteger('line_limit')->nullable();
            $table->bigInteger('asset_limit')->nullable();
            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}