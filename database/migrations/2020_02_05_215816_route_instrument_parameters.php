<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RouteInstrumentParameters extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('route_inspection_parameters', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('route_inspection_point_id');
            $table->foreign('route_inspection_point_id')->references('id')->on('route_inspection_point');

            $table->unsignedBigInteger('parameter_id');
            $table->foreign('parameter_id')->references('id')->on('parameters');

            $table->integer('order_no')->nullable();
            $table->integer('inspection_time_min');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('route_inspection_parameters');
    }
}
