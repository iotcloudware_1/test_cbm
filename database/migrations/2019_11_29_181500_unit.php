<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Unit extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('units', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('plant_id');
            $table->foreign('plant_id')->references('id')->on('plants');
            $table->string('name');
            $table->string('description')->nullable();
            $table->integer('spare_int_1')->nullable();
            $table->integer('spare_int_2')->nullable();
            $table->integer('spare_int_3')->nullable();
            $table->integer('spare_int_4')->nullable();
            $table->integer('spare_int_5')->nullable();
            $table->boolean('spare_bool_1')->nullable();
            $table->boolean('spare_bool_2')->nullable();
            $table->boolean('spare_bool_3')->nullable();
            $table->boolean('spare_bool_4')->nullable();
            $table->boolean('spare_bool_5')->nullable();
            $table->string('spare_string_1')->nullable();
            $table->string('spare_string_2')->nullable();
            $table->string('spare_string_3')->nullable();
            $table->string('spare_string_4')->nullable();
            $table->string('spare_string_5')->nullable();         
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('units');
    }
}
