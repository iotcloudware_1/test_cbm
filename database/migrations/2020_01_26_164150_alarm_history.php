<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlarmHistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
   {
        Schema::create('alarm_history', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('alarm_current_state_id');
            $table->foreign('alarm_current_state_id')->references('id')->on('alarm_current_state');
            $table->string('old_state');
            $table->string('new_state');
            $table->integer('new_value');
            $table->string('state_ack');
            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users');

          
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alarm_history');
    }
}
