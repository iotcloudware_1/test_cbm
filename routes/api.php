<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('users', 'UserController@phoneLogin');


///////////////////////Work Order Api for Mobile Dashobard Start ////////////////////////

Route::get('getcountdashboard', 'WorkOrderController@getcountdashboard');
Route::get('workorderhistory', 'WorkOrderController@WorkOrderHistory');
Route::get('alarmhistory', 'WorkOrderController@AlarmHistory');
Route::post('setlocation', 'WorkOrderController@setLocation');
Route::post('getinstrumentdefaultlocation', 'WorkOrderController@getInstrumentDefaultLocation');
Route::post('rating', 'WorkOrderController@rating');
Route::post('basicemail','WorkOrderController@basicEmail');

Route::post('forgetpassword', 'WorkOrderController@forgetPassword');

Route::get('recoveryaccount', 'WorkOrderController@recoveryAccount');

Route::post('updatepassword', 'WorkOrderController@updatePassword');

///////////////////////Work Order Api for Mobile Dashobard End ////////////////////////////////////

//Route::get('instrument', 'InstrumentController@getinstrument');

//Route::get('getInstruments', 'RouteInspectionController@getInstruments');
//Route::get('getParameters', 'RouteInspectionController@getParameters');
Route::get('getassetdetaildatamodal','ParameterController@getAssetDetailDataModal');
Route::get('getunits','UnitController@getUnits');
Route::get('getdepartment','DepartmentController@getDepartment');
Route::get('getline','LineController@getLines');
Route::get('getasset','AssetController@getAssets');
Route::get('getParameters','ParameterController@getParameters');
Route::get('getTools','ToolController@getTools');
Route::get('showinspectionpointparam','RouteInspectionController@showInspectionPointParam');
Route::get('getInstruments','InstrumentController@getInstruments');


Route::post('paramvalues','ParameterController@postParameters');
Route::get('getparamdatatype','ParameterController@getParamDataType');
Route::post('postparametersphone','ParameterController@postParametersPhone');

Route::get('getCurrentAlarm','AlarmController@getCurrentAlarm')->name('getCurrentAlarm');

Route::post('submitmanualentrydata','ParameterController@manualEntryData')->name('manualEntryData');

Route::get('getinspectionrandom','ParameterController@getInspectionRandomPoint');

Route::get('getskipreason','SkipReasonController@getSkipReasons')->name('skipreason');

Route::get('getparametervaluesforchart','ParameterController@getParameterValuesForChart'); 
//For Multiple

Route::get('getparamidfordetailedanalytics','ParameterController@getParamIdForDetailedAnalytics');

Route::get('getsingleparameteranalyticsforchart','ParameterController@getParameterAnalyticsForChart');
Route::get('getsetpointforsingleanalytics','ParameterController@getSetpointForSingleAnalytics'); //For Single Analytics 

///////////////////////////////////////Work Order Api Start ////////////////////////////////////

Route::get('getworkorders','WorkOrderController@getWorkOrders');
Route::get('getopenworkorderlist','WorkOrderController@getOpenWorkOrderLists');
Route::post('postworkorders','WorkOrderController@postWorkOrders'); // for pictures also audio
Route::post('postworkorderresolved','WorkOrderController@postworkOrderResolved');
// Route::post('postfaultsacknowledge','FaultController@postFaultsAcknowledge');

Route::get('currentworkorder/checkacknowledgement','WorkOrderController@checkAcknowledgement');

/////////////////////////////////////////Work Order Api End/////////////////////////////////////


////////////////////////////////////////Routes Api Start////////////////////////////////////////

Route::get('getroutes', 'RouteController@getRoutes');
Route::get('getroutesdetails', 'RouteController@getRoutesDetails');
Route::get('getinspectionpoints','RouteController@getInspectionPoints'); // request me route_id operator_id user token 
Route::get('getrouteinspectionpointparam','RouteController@getRouteInspectionPointParams');
Route::get('getroutetools','RouteController@getRouteTools');
Route::post('postfinishroute','RouteController@postFinishRoutes');

/////////////////////////////////////////Routes Api End////////////////////////////////////////

/////////////////////////////////////////User Api Start////////////////////////////////////////

Route::get('getoperators','UserController@getOperators');

/////////////////////////////////////////User Api End//////////////////////////////////////////

////////////////Parameter Api Start///////////////
Route::get('getsingleanalyticsparameterdata','ParameterController@getSingleAnalyticsParameterData');
///////////////Parametet Api End////////////////// 

////////////////Alarm Parameter For Chart Start ///////////////
Route::get('getalarmhistory','AlarmController@getalarmHistory');

Route::get('getalarmhistorydatatable','AlarmController@getalarmHistoryDatatable');

////////////////Alarm Parameter For Chart End//////////////////
Route::get('currentalarm/checkacknowledgement','AlarmController@checkAcknowledgement');
// Route::get('currentalarm/checkack','AlarmController@checkAck');

Route::post('createroute','RouteController@store');
Route::post('paraminpectionpointstore','RouteController@paramInpectionPointStore');

///////////////// API for DB Sync //////////////////////////////////////////////////////
Route::get('phonedbroute','RouteController@getAllRoutes');
Route::get('phonedbroutetools','RouteController@getAllRouteTools');
Route::get('phonedbinstrumentlist','InstrumentController@getInstruments');
Route::get('phonedbinspectionpoints','RouteInspectionController@getAllInspectionPoints');
Route::get('phonedbparameters','ParameterController@getAllparameters');
Route::get('phonedbskipreason','SkipReasonController@getAllSkipReasons');
Route::get('phonedbworkorders','WorkOrderController@getAllWorkOrders');
Route::get('phonedbopenworkorderlist','CurrentWorkOrderController@getAllCurrentWorkOrders');
Route::post('getinspectionskipreasons','SkipReasonController@getInspectionSkipReasons');

Route::get('inspectionpointinstrument','RouteInspectionController@getInstrumentId');

Route::post('updateinspectionpointorder','RouteInspectionController@updateInspectionPointOrder');


Route::get('getallusers','UserController@getAllUsers');
Route::post('addusersingroup','UserController@addUsersInGroup');

//Report
Route::get('generate-report','ReportController@generateReport');
Route::get('generate-yesturday-report','ReportController@generateYesturdayReport');

//generate_route_dely
Route::post('storeregisterroute','RouteController@storeRegisterRoute');
