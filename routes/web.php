<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// // analytics route
// Route::get('/analytics-single','SingleParameterController@create');

Route::get('singleparameter','AnalyticsController@singleParameter');
// Route::get('getroutetools', 'RouteController@getRouteTools');
Route::get('getroutetoolsdata', 'RouteController@getRouteToolsData');

// Route::get('test', 'WorkOrderController@test');
Route::get('/assetmodal',function()
{
    return view('dashboardtree.asset');
});


// Route::get('/findparameter', 'SingleParameterController@findparameter');
// Route::get('/intrumentParameter', 'SingleParameterController@intrumentParameter');
// Route::get("analytics/analytics-single-parameter", 'SingleParameterController@index')->name("analytics.analytics-single-parameter");



Auth::routes();


Route::group(['middleware' => ['auth']], function() 
{
	Route::get('logout','Auth\LoginController@logout')->name('logout');
	Route::resource('roles','RoleController');	
	Route::resource('plants','PlantController');
	Route::resource('departments','DepartmentController');
	Route::resource('lines','LineController');
	Route::resource('assets','AssetController');
	Route::resource('assetstatus','AssetStatusController');
	
	Route::resource('status','StatusController');
	Route::get('status/delete/{id}', 'StatusController@destroy');
	
	Route::resource('instruments','InstrumentController');
	Route::resource('tools','ToolController');
	Route::resource('plantstatus','PlantStatusController');
	Route::resource('unitstatus','UnitStatusController');
	Route::resource('departmentstatus','DepartmentStatusController');
	Route::resource('instrumentstatus','InstrumentStatusController');
	Route::resource('toolstatus','ToolStatusController');
	Route::resource('instrumentworkorder','InstrumentWorkOrderController');
	Route::resource('linestatus','LineStatusController');
	Route::resource('parameters','ParameterController');
	Route::resource('qrcodes','QrcodeController');
	Route::resource('deviations','DeviationController');
	Route::resource('permissions','PermissionController');
	Route::resource('skipreasons','SkipReasonController');
	Route::resource('units','UnitController');
	Route::resource('groups','GroupController');  
	Route::resource('registerroute','RegisterRouteController');

	Route::get('routes/show-skipreasonsinstrument-point/{id}','RouteInstrumentController@index');
	
	//clicking on add icon in inspection points
	Route::get('addparamtoinspectionpoint/{id}','RouteController@addParamCreate')->name('addparamtoinspectionpoint');

	Route::resource('plantadmin','PlantAdminController');
	Route::get('/findparameter', 'RouteController@findparameter');
	Route::get('instrumentworkorder/delete/{id}', 'InstrumentWorkOrderController@destroy');
	Route::get('users/delete/{id}', 'UserController@destroy');
	Route::get('roles/delete/{id}', 'RoleController@destroy');
	Route::get('permissions/delete/{id}', 'PermissionController@destroy');
	Route::get('plants/delete/{id}', 'PlantController@destroy');
	Route::get('skipreasons/delete/{id}', 'SkipReasonController@destroy');
	Route::get('routes/delete/{id}', 'RouteController@destroy');
	Route::get('departments/delete/{id}', 'DepartmentController@destroy');
	Route::get('instruments/delete/{id}', 'InstrumentController@destroy');
	Route::get('units/delete/{id}', 'UnitController@destroy');
	Route::get('plantstatus/delete/{id}', 'PlantStatusController@destroy');
	Route::get('linestatus/delete/{id}', 'LineStatusController@destroy');
	Route::get('lines/delete/{id}', 'LineController@destroy');
	Route::get('assetstatus/delete/{id}', 'AssetStatusController@destroy');
	Route::get('tools/delete/{id}', 'ToolController@destroy');
	Route::get('assets/delete/{id}', 'AssetController@destroy');
	Route::get('parameters/delete/{id}', 'ParameterController@destroy');
	Route::get('deviations/delete/{id}', 'DeviationController@destroy');
	Route::get('departmentdashboard', 'DepartmentController@getDepartmentDashoard');
	Route::get('linedashboard', 'LineController@getLineDashoard');
	Route::get('assetdashboard', 'AssetController@getAssetDashoard');
	Route::get('instrumentdashboard', 'InstrumentController@getInstrumentDashoard');
	Route::get('parameterdashboard', 'ParameterController@getParameterDashoard');
	Route::get('plantadmin/delete/{id}', 'PlantAdminController@destroy');
	Route::get('unitstatus/delete/{id}', 'UnitStatusController@destroy');
	Route::get('departmentstatus/delete/{id}', 'DepartmentStatusController@destroy');
	Route::get('instrumentstatus/delete/{id}', 'InstrumentStatusController@destroy');
	Route::get('toolstatus/delete/{id}', 'ToolStatusController@destroy');
	Route::get('calculateworkorder','WorkOrderController@calculateWorkOrderPercentages');
	Route::get('opendetailworkorder','DashboardController2@openDetail')->name('workorders.opendetail');
	Route::get('closedetailworkorder','DashboardController2@closeDetail')->name('workorders.closedetail');
	Route::get('openackworkorder','DashboardController2@ackDetail')->name('workorders.openack');

	Route::get('openunackworkorder','DashboardController2@unAckDetail')->name('workorders.openunack');
	Route::get('routeinprogress','DashboardController@detailInProgressRoute')->name('routes.inprogress');
	Route::get('routecomplete','DashboardController@detailRouteComplete')->name('routes.complete');
	Route::get('routecancle','DashboardController@detailRouteCancle')->name('routes.cancle');
	Route::get('routedelay','DashboardController@detailRouteDelayed')->name('routes.delay');
	Route::get('totalalarms','DashboardController@detailTotalAlarm')->name('alarms.total');
	Route::get('currentalarms','DashboardController@detailCurrentAlarm')->name('alarms.current');
	Route::get('ackalarm','DashboardController@detailAckAlarm')->name('alarms.ackalarm');
	Route::get('unackalarm','DashboardController@detailUnAckAlarm')->name('alarms.unackalarm');

	Route::get('/authenticationerror',function()
	{
	    return view('authentication_error');
	});
	//All qrcodes print 
	Route::get('qr-code', 'InstrumentController@selectInstrument')->name('qr-code.selectinstrument');
	Route::post('printallqr', 'QrcodeController@printAllQr');
	
	Route::get('admin/pages',function()
	{
	    return view('undercontruction.index');
	});
	Route::get('/parametervalues', 'ParameterController@parameterValues')->name('parametervalues.index');

	Route::get('/munualdataentry',function()
	{
	    return view('manualdataentry.index');
	});

	Route::post('/parameter/manualdataentrystore', 'ParameterController@manualdataentry')->name('manualdataentrystore');
	Route::get('/instrumentworkorderform', 'WorkOrderController@instrumentWorkOrder');
	Route::post('/storeinstrumentworkorder', 'WorkOrderController@storeinstrumentWorkOrder')->name('workorders.storeinstrumentworkorder');

	Route::get('/showinspectionpoints/align/{id}', 'RouteController@RouteAlignCreate')->name('showinspectionpoints.align');

	Route::get('/slip', 'QrcodeController@slip');
	Route::get('/slip2', 'QrcodeController@slip2');
	Route::get('/slip3', 'QrcodeController@slip3');
	Route::get('/slip4', 'QrcodeController@slip4');
	Route::get('/slip5', 'QrcodeController@slip5');
	Route::get('multipleanalytics',function()
	{
	    return view('analytics.multiple-analytics');
	});
	Route::get('singleanalytics',function()
	{
	    return view('analytics.single-analytics');
	});
	
	Route::get('/slip', 'QrcodeController@slip');
	Route::get('/slip2', 'QrcodeController@slip2');
	Route::get('/slip3', 'QrcodeController@slip3');
	Route::get('/slip4', 'QrcodeController@slip4');
	Route::get('/slip5', 'QrcodeController@slip5');
	Route::get('multipleanalytics',function()
	{
	    return view('analytics.multiple-analytics');
	});
	Route::get('routeskippedinstruments/{id}','RouteHistoryController@getSkippedInstrument'); 
	
	Route::get('showinspectionpoint/{id}','RouteController@showInspectionPoint');
	Route::get('showinspectionperemeter/{id}','RouteController@showInspectionParameter')->name('showinspectionperemeter'); 

	Route::get('showunitdepartment/{id}','DepartmentController@showUnitDepartment')->name('showunitdepartment');
	Route::get('showdepartmentline/{id}','LineController@showDepartmentLine')->name('showdepartmentline');
	Route::get('showlineasset/{id}','AssetController@showLineAsset')->name('showlineasset');
	Route::get('showassetinstrument/{id}','InstrumentController@showAssetInstrument')->name('showassetinstrument');
	Route::get('showinstrumentparameter/{id}','ParameterController@showInstrumentParameter')->name('showinstrumentparameter');


	Route::get('acknowledgeworkorder','WorkOrderController@acknowledgeWorkOrder');
	Route::get('getspecificunitdepartment/departments',function()
	{
	    return view('departments.index');
	});
	Route::get('singleanalytics',function()
	{
	    return view('analytics.single-analytics');
	});
	Route::get('/home', 'DashboardController@index')->name('home');
	Route::get('/', 'DashboardController@index');
	Route::get('dashboardview', 'DashboardController@index');
	Route::get('/showparameteranalytics/{id}', 'ParameterController@showSelectedParameterAnalytics')->name('showanalytics');

	Route::group(['middleware' => ['can:tree_dashboard-index']], function () {
	    		
		    Route::get('unitdashboard', 'UnitController@getUnitDashoard');
		    Route::get('getspecificunitdepartment/{id}','DepartmentController@getDepartmentDashoard')->name('get_unit_department');

			Route::get('getspecificdepartmentline/{id}','LineController@getLineDashoard')->name('get_department_line');
			Route::get('getspecificlineasset/{id}','AssetController@getAssetDashoard')->name('get_line_asset');
			Route::get('getspecificassetinstrument/{id}','InstrumentController@getInstrumentDashoard')->name('get_asset_instrument');
			// Route::get('getspecificparameter/{id}','InstrumentController@getInstrumentDashoard')->name('get_asset_instrument');

			Route::get('getspecificinstrumentparameter/{id}','ParameterController@getParameterDashoard')->name('get_instrument_parameter'); 
			Route::get('getunitdetaildata/{id}','ParameterController@getUnitDetailData')->name('get_unit_detail_data');
			Route::get('getdepartmentdetaildata/{id}','ParameterController@getDepartmentDetailData')->name('get_department_detail_data');
			Route::get('getlinedetaildata/{id}','ParameterController@getLineDetailData')->name('get_line_detail_data');
			Route::get('getassetdetaildata/{id}','ParameterController@getAssetDetailData')->name('get_asset_detail_data');
			Route::get('getinstrumentdetaildata/{id}','ParameterController@getInstrumentDetailData')->name('get_instrument_detail_data');
			Route::post('/storeinstrumentworkorder', 'WorkOrderController@storeinstrumentWorkOrder')->name('workorders.storeinstrumentworkorder'); 
				});
		Route::group(['middleware' => ['can:workorderdashboard-index']], function () {
    		Route::get('workorderdashboard', 'DashboardController2@index');
			Route::resource('workorderhistory','WorkOrderHistoryController');
			Route::resource('currentworkorder','CurrentWorkOrderController');
			Route::get('workorders/delete/{id}', 'WorkOrderController@destroy');
			Route::get('currentworkorder/check_acknowledgement','WorkOrderController@checkAcknowledgement')->name('check_acknowledgement');
			Route::resource('workorders','WorkOrderController');
	    	Route::resource('users','UserController');

			Route::get('workorderdetails', 'DashboardController2@workOrderDetail')->name('workorderdetail');
	    	Route::resource('routes','RouteController');
	    	Route::resource('routehistory','RouteHistoryController');
	    	Route::get('/currentworkorder/ackwork/{id}', 'WorkOrderController@ackIndividualWorkorder');
			});

		Route::group(['middleware' => ['can:plant_admin_alarm_dashboard-index']], function () {

		Route::get('alarmdashboard', 'DashboardController2@alarmDashboard');
		Route::get('allunits', 'UnitController@allUnits')->name('allunits');
		Route::get('alldepartments', 'DepartmentController@allDepartments')->name('alldepartments');
		Route::get('alllines', 'LineController@allLines')->name('alllines');
		Route::get('allassets', 'AssetController@allAssets')->name('allassets');
		Route::get('allinstruments', 'InstrumentController@allInstruments')->name('allinstruments');
		Route::get('allparameters', 'ParameterController@allParameters')->name('allparameters');
	    Route::resource('alarms','AlarmController'); 
	    Route::get('alarms/delete/{id}', 'AlarmController@destroy'); 
	    Route::get('/currentalarmstate', 'AlarmController@currentAlarmState')->name('alarms.currentAlarmState');
		Route::get('/alarmhistory', 'AlarmController@alarmHistory')->name('alarms.alarmHistory');
		Route::get('/acknowledge', 'AlarmController@acknowledge');
		Route::get('/acknowledgecurrentalarms', 'AlarmController@acknowledgeCurrentAlarms');
		Route::get('/currentalarmstate/ack/{id}', 'AlarmController@ackIndividualAlarm');
		Route::get('/groups/add/{id}', 'GroupController@addUser');
		// Route::get('contacts/delete/{id}', 'ContactController@destroy');
		//Report
		Route::get('report',function(){
			return view('report.index');
		});
	}); 

});