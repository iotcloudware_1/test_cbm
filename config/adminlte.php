<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Title
    |--------------------------------------------------------------------------
    |
    | Here you can change the default title of your admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#61-title
    |
    */

    'title' => 'CBM',
    'title_prefix' => '',
    'title_postfix' => '',

    /*
    |--------------------------------------------------------------------------
    | Logo
    |--------------------------------------------------------------------------
    |
    | Here you can change the logo of your admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#62-logo
    |
    */

    'logo' => '<b>CBM</b>',
    'logo_img' => 'vendor/adminlte/dist/img/CBM_logo.png', 
    'logo_img_class' => 'brand-image-xl',
    'logo_img_xl' => null,
    'logo_img_xl_class' => 'brand-image-xs',
    'logo_img_alt' => 'CBM',

    /*
    |--------------------------------------------------------------------------
    | Layout
    |--------------------------------------------------------------------------
    |
    | Here we change the layout of your admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#63-layout
    |
    */

    'layout_topnav' => null,
    'layout_boxed' => null,
    'layout_fixed_sidebar' => true,
    'layout_fixed_navbar' => true,
    'layout_fixed_footer' => null,

    /*
    |--------------------------------------------------------------------------
    | Extra Classes
    |--------------------------------------------------------------------------
    |
    | Here you can change the look and behavior of the admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#64-classes
    |
    */

    'classes_body' => '',
    'classes_brand' => '',
    'classes_brand_text' => '',
    'classes_content_header' => 'container-fluid',
    'classes_content' => 'container-fluid',
    'classes_sidebar' => 'sidebar-dark-primary elevation-4',
    'classes_sidebar_nav' => '',
    'classes_topnav' => 'navbar-white navbar-light',
    'classes_topnav_nav' => 'navbar-expand-md',
    'classes_topnav_container' => 'container',

    /*
    |--------------------------------------------------------------------------
    | Sidebar
    |--------------------------------------------------------------------------
    |
    | Here we can modify the sidebar of the admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#65-sidebar
    |
    */

    'sidebar_mini' => true,
    'sidebar_collapse' => true,
    'sidebar_collapse_auto_size' => true,
    'sidebar_collapse_remember' => true,
    'sidebar_collapse_remember_no_transition' => true,
    'sidebar_scrollbar_theme' => 'os-theme-light',
    'sidebar_scrollbar_auto_hide' => 'l',
    'sidebar_nav_accordion' => true,
    'sidebar_nav_animation_speed' => 300,

    /*
    |--------------------------------------------------------------------------
    | Control Sidebar (Right Sidebar)
    |--------------------------------------------------------------------------
    |
    | Here we can modify the right sidebar aka control sidebar of the admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#66-control-sidebar-right-sidebar
    |
    */

    'right_sidebar' => false,
    'right_sidebar_icon' => 'fas fa-cogs',
    'right_sidebar_theme' => 'dark',
    'right_sidebar_slide' => true,
    'right_sidebar_push' => true,
    'right_sidebar_scrollbar_theme' => 'os-theme-light',
    'right_sidebar_scrollbar_auto_hide' => 'l',

    /*
    |--------------------------------------------------------------------------
    | URLs
    |--------------------------------------------------------------------------
    |
    | Here we can modify the url settings of the admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#67-urls
    |
    */

    'use_route_url' => false,

    'dashboard_url' => '/',

    'logout_url' => 'logout',

    'login_url' => 'login',

    'register_url' => 'register',

    'password_reset_url' => 'password/reset',

    'password_email_url' => 'password/email',

    /*
    |--------------------------------------------------------------------------
    | Laravel Mix
    |--------------------------------------------------------------------------
    |
    | Here we can enable the Laravel Mix option for the admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#68-laravel-mix
    |
    */

    'enabled_laravel_mix' => false,

    /*
    |--------------------------------------------------------------------------
    | Menu Items
    |--------------------------------------------------------------------------
    |
    | Here we can modify the sidebar/top navigation of the admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#69-menu
    |
    */

    'menu' => [
        [
            'text' => 'Super Actions',
            'icon' => 'fas fa-fw fa-user-cog',
            'can' => 'plant-admin-index',
            'submenu' => [
                [
                    'text' => 'Plant Admin',
                    'url'  => 'plantadmin',
                    'icon' => 'fas fa-user',
                    'can'  => 'plant-admin-index',
                   
                ],                         
            ],
        ],
        [
            'text' => 'Summary',
            'icon' => 'fas fa-balance-scale-left',
            'url' => 'dashboardview',
            'can' => 'plant_admin_summary_dashboard-index',

        ],
        [
           'text' => 'Dashboards',
            'icon' => 'fas fa-chalkboard-teacher',
            'can' => 'plant_admin_summary_dashboard-index',

            'submenu' => [
                [
                    'text' => 'Alarms',
                    'icon' => 'fas fa-fw fa-bell',
                    'url' => 'alarmdashboard',
                    'can' => 'plant_admin_summary_dashboard-index',
                ],
                [
                    'text' => 'Workorders',
                    'icon' => 'fas fa-pencil-ruler',
                    'url' => 'workorderdashboard',
                    'can' => 'workorder-index',
                ],
                [
                    'text' => 'Assets',
                    'url' => 'unitdashboard',
                    'icon' => 'fab fa-linode',
                    'can' => 'plant_admin_summary_dashboard-index',
                ],
                [
                    'text' => 'Users',
                    'url' => 'users',
                    'icon' => 'fas fa-street-view',
                    'can' => 'user-index'
                ],
                [
                    'text' => 'Routes',
                    'icon' => 'fas fa-route',
                    'url'  => 'routes',
                    'can'=>'route-index',     
                ],
            ],
        ],
        [
            'text' => 'Plant',
            'url' => 'plants',
            'icon' => 'fas fa-fw fa-industry',
            'can' => 'plant-index',
         
        ],
        // [
        //     'text' => 'Reports',
        //     'icon' => 'fas fa-fw  fa-clipboard',
        //     'can' => 'report-index',
        //     'submenu' => [
        //         [
        //             'text' => 'Hourly',
        //             'url'  => 'admin/pages',
        //             'icon' => 'far fa-hourglass',
        //         ],
        //         [
        //             'text' => 'Shift',
        //             'url'  => 'admin/pages',
        //             'icon' => 'fas fa-hourglass-half',
        //         ],
        //         [
        //             'text' => 'Daily',
        //             'url'  => 'admin/pages',
        //             'icon' => 'fas fa-history',
        //         ],
        //         [
        //             'text' => 'Weekly',
        //             'url'  => 'admin/pages',
        //             'icon' => 'fas fa-calendar-week',
        //         ],  
        //         [
        //             'text' => 'Custom',
        //             'url'  => 'admin/pages',
        //             'icon' => 'fas fa-calendar-alt',
        //         ],             
        //     ],
        // ],
        [
            'text' => 'Analytics',
            'icon' => 'fas fa-fw fa-chart-pie',
            'can'  => 'analytics-index',
       
            'submenu' => [
                // [
                //     'text' => 'Single',
                //     'url'  => 'singleanalytics',
                //     'icon' => 'fas fa-chart-line',
                // ],
                [
                    'text' => 'Multiple',
                    'url' => 'multipleanalytics',
                    'icon' => 'fas fa-fw fa-chart-area',
                ],
                             
            ],
        ],    
        [
            'text' => 'Parameter Value',
            'url' => 'parametervalues',
            'icon' => 'far fa-eye',
            'can' => 'parametervalue-index'    
        ],
        [
            'text' => 'User Managment',
            'icon' => 'fas fa-user-circle',
            'can' => 'usermanagement-index',
        
            'submenu' => [
                
                [
                    'text' => 'Roles',
                    'url'  => 'roles',
                    'icon' => 'fas fa-user-tag',
                    'can' => 'role-index',
                    
                ],
                [
                    'text' => 'Permissions',
                    'url' => 'permissions',
                    'icon' => 'fas fa-users-cog',
                    'can' => 'permission-index',
                    
                ],             
            ],
        ],
        [
            'text'=> 'Data Entry',
            'url' => 'munualdataentry',
            'icon'=> 'fas fa-user-edit',
            'can' => 'manual-data-entry-index',
            
        ],
        [
            'text' => 'Configuration',
            'icon' => 'fas fa-wrench',
            'can' => 'configuration-index',
            
            'submenu' => [
                
                [
                    'text' => 'Tools',
                    'url'  => 'tools',
                    'icon' => 'fas fa-tools',
                    'can' =>  'tool-index',
                ],                 
                [
                    'text' => 'Skip Reasons',
                    'url'  => 'skipreasons',
                    'icon' => 'fas fa-clipboard-check',
                    'can'  => 'skipreason-index',
                ],                                     
                [
                    'text' => 'Print QR Codes',
                    'url'  => 'qr-code',
                    'icon' => 'fas fa-qrcode',
                    'can'  => 'qrcode-index',
                   
                ],
                [
                    'text' => 'Status',
                    'url'  => 'status',
                    'icon' => 'far fa-eye',
                    'can'  => 'status-index',
                ],
                [
                    'text' => 'Plant Status',
                    'url'  => 'plantstatus',
                    'icon' => 'fas fa-tachometer-alt',
                    'can'  => 'plantstatus-index',
                  
                ],             
            ],
        ],
        // [
        //     'text' => 'Documentation',
        //     'url' => '#',
        //     'icon' => 'fas fa-question-circle',
        //     'can' => 'parametervalue-index'    
        // ],
    ],






/** end new manu */


    /*
    |--------------------------------------------------------------------------
    | Menu Filters
    |--------------------------------------------------------------------------
    |
    | Here we can modify the menu filters of the admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#610-menu-filters
    |
    */

    'filters' => [
        JeroenNoten\LaravelAdminLte\Menu\Filters\HrefFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\SearchFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\ActiveFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\SubmenuFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\ClassesFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\GateFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\LangFilter::class,
    ],

    /*
    |--------------------------------------------------------------------------
    | Plugins Initialization
    |--------------------------------------------------------------------------
    |
    | Here we can modify the plugins used inside the admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#611-plugins
    |
    */

    'plugins' => [
        [
            'name' => 'Datatables',
            'active' => true,
            'files' => [
                [
                    'type' => 'css',
                    'asset' => true,
                    'location' => '/vendor/datatables/css/dataTables.bootstrap4.min.css',
                ],
            ],
        ],
        [
            'name' => 'Select2',
            'active' => true,
            'files' => [
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js',
                ],
                [
                    'type' => 'css',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.css',
                ],
            ],
        ],
        [
            'name' => 'Chartjs',
            'active' => true,
            'files' => [
                [
                    'type' => 'js',
                    'asset' => true,
                    'location' => 'bower_components/chart.js/dist/Chart.bundle.min.js',
                ],
                [
                    'type' => 'js',
                    'asset' => true,
                    'location' => 'bower_components/chart.js/dist/Chart.min.js',
                ],
                [
                    'type' => 'css',
                    'asset' => true,
                    'location' => 'bower_components/chart.js/dist/Chart.min.css',
                ],                
            ],
        ],
        [
            'name' => 'Sweetalert2',
            'active' => false,
            'files' => [
                [
                    'type' => 'js',
                    'asset' => true,
                    'location' => '//cdn.jsdelivr.net/npm/sweetalert2@8',
                ],
            ],
        ],
        [
            'name' => 'Pace',
            'active' => true,
            'files' => [
                [
                    'type' => 'css',
                    'asset' => true,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/themes/blue/pace-theme-center-radar.min.css',
                ],
                [
                    'type' => 'js',
                    'asset' => true,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/pace.min.js',
                ],
            ],
        ],
    ],
];
