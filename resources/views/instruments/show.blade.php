{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')
 
@section('title', 'Dashboard')
@section('content_header')
    <div style="display: inline-block; width: 100%">
     <h1 style="margin-bottom:-10px"><b>Show Instrument</b></h1>
    <button onclick="printQr()" class="btn btn-success" style="float: right; background-color: #1a1c33; margin-bottom: 13px;margin-top: -35px"><i class="fas fa-qrcode" style="color: white"></i> &nbsp;Print QRCode</button>
</div>
<hr class="divider" style="margin-top: 0px">
@stop 

@section('content')
<div class="centered-container">
    <div class="row">
    <div class="col-md-6 no-print">
    <div class="container-fluid">
        <table  class="table table-bordered table-striped">
           <tr >
                <th>ID</th>
                <td>{{ $instrument->id }}</td>
            </tr>
            <tr>
                <th>Asset Name</th>
                <td>{{ $instrument->asset_id }}</td>
            </tr>
             <tr>
                <th>Name</th>
                <td>{{ $instrument->name }}</td>
            </tr>

            <tr>
                <th>Description</th>
                <td>{{ $instrument->description }}</td>        
            </tr>
             <tr>
                <th>Type</th>
                <td>{{ $instrument->type }}</td>        
            </tr>
             <tr>
                <th>Location</th>
                <td>{{ $instrument->location }}</td>        
            </tr>
            <tr>
                <th>Created at</th>
                <td>{{ $instrument->created_at }}</td>
            </tr>
        </table> 
    </div>
    </div>
    <div class="col-md-6">
        <img src="data:image/png;base64, {!! base64_encode(QrCode::format('png')->size(500)->generate($modified_json)) !!} ">
<!-- {{$modified_json}} -->
    </div>


</div>
@stop


@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
     <style type="text/css">
        @media print
        {    
            .no-print, .no-print *
            {
                display: none !important;
            }
        }
    </style>
@stop

@section('js')
     <script>
        function printQr()
        {
           window.print();
        }
    </script>
@stop 