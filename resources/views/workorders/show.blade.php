{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
     <h1 style="margin-bottom:-10px"><b>Show Workorder</b></h1>
      <hr class="divider" >
@stop

@section('content')
  

<div class="centered-container">
<div class="row">
    <div class="container-fluid">
        <table class="table table-bordered table-striped">
            <tr>
                <th>Description</th>
                <td>{{ $workorder->description }}</td>        
            </tr>
             <tr>
                <th>Type</th>
                <td>{{ $workorder->type }}</td>
            </tr>
             <tr>
                <th>Priority</th>
                <td>{{ $workorder->priority }}</td>
            </tr>
            <tr>
                <th>Send Email</th>
                <td>{{ $workorder->send_email }}</td>
            </tr>
            <tr>
                <th>Send SMS</th>
                <td>{{ $workorder->send_sms }}</td>
            </tr>
            <tr>
                <th>Created at</th>
                <td>{{ $workorder->created_at }}</td>
            </tr>
        </table>
    </div>
<!-- <div class="col-xs-12 col-sm-12 col-md-12 text-center">
        <a class="btn btn-primary" href="{{ route('departments.index') }}"> Back</a>
    </div>-->
    </div> 
</div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop