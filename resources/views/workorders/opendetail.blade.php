@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1 style="margin-bottom:-10px"><b>Detail Open Workorders</b></h1>
     <hr class="divider" >
@stop

@section('content')
    
<div class="row">
  <div class="col-md-12">
    <table id="dtBasicExample" class="table table-striped table-bordered table-xl" cellspacing="0">   
      <thead>
        <tr>
            <th>Instrument</th>
            <th>Workorder </th>
            <th>Priority</th>
            <th>Assigned By</th>
            <th>Assigned To</th>
            <th>Status</th>
            <th>Acknowledgement</th>
            <th>Timpstamp</th>
        </tr>
      </thead>  
      <tbody>
        @foreach($data as $show_insp)
        <tr>
          <td>{{$show_insp['instrument_id']}}</td>
      	  <td>{{$show_insp['remarks']}}</td>
          <td>{{$show_insp['priority']}}</td>
          <td>{{$show_insp['assigned_by']}}</td>
          <td>{{$show_insp['assigned_to']}}</td>
          <td>{{$show_insp['status']}}</td>
          <td>{{$show_insp['acknowledgement']}}</td>
          <td>{{$show_insp['created_at']}}</td>
        </tr>
        @endforeach
      </tbody>  
    </table>
  </div>
</div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <style type="text/css">
        tr th
        {
            text-align: center;
        }
    </style>
@stop

@section('js')
<script type="text/javascript" src="{{ asset('vendor/datatables/js/dataTables.bootstrap4.min.js') }}"></script>
 <script type="text/javascript">
   $(document).ready(function () {
  $('#dtBasicExample').DataTable( {
        "order": [[ 2, "asc" ]]
    } );
  $('.dataTables_length').addClass('bs-select');
});

 </script>

@stop
