{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
  <h1 style="margin-bottom:-10px"><b>Edit Workorder</b></h1>
     <hr class="divider" >
@stop

@section('content')
 <div class="centered-container">   
@if (count($errors) > 0)
  <div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
       @foreach ($errors->all() as $error)
         <li>{{ $error }}</li>
       @endforeach
    </ul>
  </div>
@endif
{!! Form::model($workorder, ['method' => 'PATCH','route' => ['workorders.update', $workorder->id]]) !!}
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <strong><i class="fas fa-cash-register" style="color: #6c757d"></i>&nbsp Instrument Name:</strong>
            {!! Form::select('instrument_id', $instrument, null, ['class'=>'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <strong><i class="fas fa-user-tie" style="color: #6c757d"></i>&nbsp Description </strong>
            {!! Form::text('description', null, array('placeholder' => 'Description','class' => 'form-control','maxlength'=>50)) !!}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <strong><i class="fas fa-user-tie" style="color: #6c757d"></i>&nbsp Type </strong>
            {!! Form::text('type', null, array('placeholder' => 'type','class' => 'form-control','maxlength'=>50)) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <strong><i class="fas fa-user-tie" style="color: #6c757d"></i>&nbsp Proirity </strong>
            {!! Form::select('priority', ['high'=>'high','medium'=>'medium','low'=>'low'], null, ['class' => 'form-control']) !!}
        </div>
    </div>
    
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <strong><i class="fas fa-user-tie" style="color: #6c757d"></i>&nbsp Send SMS </strong>
            {!! Form::select('send_sms', ['1'=>'Yes','0'=>'No'], null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
       <div class="form-group">
            <strong><i class="fas fa-user-tie" style="color: #6c757d"></i>&nbsp Send Email </strong>
            {!! Form::select('send_email', ['1'=>'Yes','0'=>'No'], null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>  
<br>
    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
        <button type="submit" class="btn btn-secondary"> <i class="fas fa-pen-nib"></i>&nbsp; Submit</button>
    </div>
    <!-- <div class="col-xs-12 col-sm-12 col-md-12 text-center">
        <a class="btn btn-primary" style="font:center" href="{{ route('plants.index') }}" style="float:left"> Back</a>
</div>
 -->
    {!! Form::close() !!}
</div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    
@stop