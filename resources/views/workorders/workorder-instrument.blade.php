{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
     <h1 style="margin-bottom:-10px"><b>Create Instrument Workorder</b></h1>
 
     <hr class="divider" >
@stop

@section('content')
<div class="centered-container">   
  @if (count($errors) > 0)
    <div class="alert alert-danger">
      <strong>Whoops!</strong> There were some problems with your input.<br><br>
      <ul>
         @foreach ($errors->all() as $error)
           <li>{{ $error }}</li>
         @endforeach
      </ul>
    </div>
  @endif
  {!! Form::open(array('route' => 'faults.storeinstrumentfault','method'=>'POST')) !!}
  <input type="hidden" name="_token" value="{{ csrf_token() }}">
  <div class="row">
      <div class="col-md-6">
          <div class="form-group">
          <strong><i class="fas fa-industry" style="color: #6c757d"></i>&nbsp  Workorder Name </strong>
            {!! Form::select('fault_id', $fault_id_options, null,['class' => 'form-control']) !!}          
          </div>
      </div>
      <div class="col-md-6">
          <div class="form-group">
          <strong><i class="fas fa-industry" style="color: #6c757d"></i>&nbsp  Instrument Name </strong>
            {!! Form::select('instrument_id', $instrument_id_options, null,['class' => 'form-control']) !!}          
          </div>
      </div>
  </div>
  <div class="row">
      <div class="col-md-6">
          <div class="form-group">
          <strong><i class="fas fa-industry" style="color: #6c757d"></i>&nbsp  Reported Time </strong>
            {!! Form::text('reported_time', null, array('placeholder' => 'Reported Time','class' => 'form-control')) !!}          
          </div>
      </div>
      <div class="col-md-6">
          <div class="form-group">
          <strong><i class="fas fa-industry" style="color: #6c757d"></i>&nbsp  Priority </strong>
            {!! Form::select('priority', ['high'=>'high','medium'=>'medium','low'=>'low'], null, ['class' => 'form-control']) !!}         
          </div>
      </div>
  </div>
  <div class="row">
      <div class="col-md-6">
          <div class="form-group">
          <strong><i class="fas fa-industry" style="color: #6c757d"></i>&nbsp  Remarks </strong>
            {!! Form::text('remarks', null, array('placeholder' => 'Remarks','class' => 'form-control')) !!}          
          </div>
      </div>
  </div>

    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
        <button type="submit" class="btn btn-secondary">Submit</button>
    </div>
    <!-- <div class="col-xs-12 col-sm-12 col-md-12 text-center">
        <a class="btn btn-primary" style="font:center" href="{{ route('plants.index') }}" style="float:left"> Back</a>
</div>
 -->
</div>
{!! Form::close() !!}

@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    
@stop