{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1 style="margin-bottom:-10px"><b> Show Role</b></h1>
    <hr class="divider" >
@stop

@section('content') 
 
<div class="row">
    <div class="container-fluid">
        <table class="table table-bordered table-striped">
            <tr>
                <th>Name</th>
                <td>{{ $role->name }}</td>
            </tr>
            <tr>
                <th>Permissions</th>
                  @if(!empty($rolePermissions))
                    @foreach($rolePermissions as $v)
                        <td>{{$v->name }}</td>
                    @endforeach
                @endif        
            </tr>
        </table>
    </div>
<div class="col-xs-12 col-sm-12 col-md-12 text-center">
        <a class="btn btn-primary" href="{{ route('roles.index') }}"> Back</a>
</div>
</div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
