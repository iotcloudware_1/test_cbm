@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header') 
  <div class="row">  
    <div class="col-xs-8 col-sm-8 col-md-9">
      <ul class="breadcrumb">
        <li class="breadcrumb-item active">
          <a  class="active">
             <img src="/images/depart.png" alt="Smiley face" height="27" width="27"> 
            <span class="text" style="color: white">Departments</span>
          </a>
        </li>
        <li>
          <a href="/unitdashboard">
            <img src="/images/unit.png" alt="Smiley face" height="27" width="27"> 
            <span class="text" style="color: black">{{$unit_name}}</span>
          </a>
        </li>            
        <li>
          <a href="/dashboardview">
            <i class="fas fa-warehouse" style="color:#272d47" aria-hidden="true"></i>
          </a>
        </li>
      </ul>      
    </div>
    <div class="col-xs-4 col-sm-4 col-md-3">
     <div class="pull-right" style="float: right;">
      <label class="dropdown">
        <div class="dd-button" >
          Manage
        </div>
        <input type="checkbox" class="dd-input" id="test">
          <ul class="dd-menu">
           <a class="btn btn-success" style="width: 100%; padding: 2%; background-color: #1a1c33;"href="/departmentstatus"><li style="color: white"><i class="fas fa-eye" style="color: white"></i>&nbsp; Status </li></a> 
           <li class="divide"></li>

           <a class="btn btn-success" style="width: 100%; padding: 2%; background-color: #1a1c33;"href="{{route('alldepartments')}}"><li style="color: white"><i class="fas fa-eye" style="color: white"></i>&nbsp; View All </li></a> 
           <li class="divide"></li>           

           <a class="btn btn-success" style="width: 100%;padding: 2%; background-color: #1a1c33;" href="/departments"><li style="color: white"><i class="fas fa-cog"></i> &nbsp; Setting </li></a>       
           <li class="divide"></li>
        </ul>
      </label>                     
      </div>
      </div>     
  </div>

  <div class="row">    
    <div class="col-md-6">
      <h1 style="margin-bottom:-10px"><b>Departments</b></h1>    
    </div>           
  </div>                                                                                                              
  <hr class="divider">
@stop

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="table-responsive">
      <table  id="dtBasicExample" class="table table-striped" cellspacing="0" width="100%">   
        <thead>
          <tr>
              <th>Unit Name</th>
              <th>Department Name</th>
              <th>Description</th>
              <th>Alarms</th>     
              <th>Healthy</th>                   
              <th>TimeStamp</th>
              <th>Action</th>
          </tr>
        </thead>  
        <tbody>
          @foreach($show_unit_department as $show_insp)
          <tr>
             <td>{{$show_insp['unit_id']}}</td>
             <td>{{$show_insp['name']}}</td>
             <td>{{$show_insp['description']}}</td>
             @if($show_insp->alarm_count>0)
             <td style="background-image: linear-gradient(to top, #ff0844 0%, #ffb199 100%);color: white; box-shadow:3px 5px 2px #ffa0a0; border-radius: 6px;">{{$show_insp['alarm_count']}}</td>
             <td>{{$show_insp['healthy_count']}}</td>
             @else
             <td>{{$show_insp['alarm_count']}}</td>
             <td  style="background-color: #1dab72; box-shadow: 3px 5px 2px  lightgreen; border-radius: 6px;color: white;">{{$show_insp['healthy_count']}}</td>           
             @endif     
             <td>{{$show_insp['created_at']}}</td>
             <td>
              <a href="{{route('get_department_detail_data',$show_insp['id'])}}"> <i class="fas fa-info-circle" style="color: #272d47"> </i> </a> &nbsp;
              <a href="{{route('showdepartmentline',$show_insp['id'])}}"> <i class="fas fa-arrow-alt-circle-right" style="color: #1a1c33;"> </i> </a>
             </td>
          </tr>
          @endforeach
        </tbody>  
      </table>
    </div>
  </div>
</div>
@stop

@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
<style type="text/css">
table.dataTable thead .sorting:after,
table.dataTable thead .sorting:before,
table.dataTable thead .sorting_asc:after,
table.dataTable thead .sorting_asc:before,
table.dataTable thead .sorting_asc_disabled:after,
table.dataTable thead .sorting_asc_disabled:before,
table.dataTable thead .sorting_desc:after,
table.dataTable thead .sorting_desc:before,
table.dataTable thead .sorting_desc_disabled:after,
table.dataTable thead .sorting_desc_disabled:before {
  bottom: .5em;
}

tr th
{
    text-align: center;
}
.fa-arrow-alt-circle-right{
  font-size: 20px;
}
.fa-info-circle{
  font-size:20px;
}

* {
  -moz-box-sizing: border-box;
  -webkit-box-sizing: border-box;
  box-sizing: border-box;
  margin: 0;
  padding: 0;
}

ul.breadcrumb {
  margin-left: 50px;
  display: inline-block;
  list-style: none;
}
ul.breadcrumb li {
  float: right;
  padding: 5px;
  background-color: #272d47;
  -moz-border-radius: 50px;
  -webkit-border-radius: 50px;
  border-radius: 50px;
  position: relative;
  margin-left: -50px;
  -moz-transition: all 0.2s;
  -o-transition: all 0.2s;
  -webkit-transition: all 0.2s;
  transition: all 0.2s;
  margin-top: 3px;
}
ul.breadcrumb li a {
  overflow: hidden;
  -moz-border-radius: 50px;
  -webkit-border-radius: 50px;
  border-radius: 50px;
  -moz-transition: all 0.2s;
  -o-transition: all 0.2s;
  -webkit-transition: all 0.2s;
  transition: all 0.2s;
  text-decoration: none;
  height: 50px;
  color: #509378;
  background-color: white;
  text-align: center;
  min-width: 50px;
  display: block;
  line-height: 50px;
  padding-left: 52px;
  padding-right: 33.33333px;
  width: 50px;
}
ul.breadcrumb li a .icon {
  display: inline-block;
}
ul.breadcrumb li a .text {
  display: none;
  opacity: 0;
}
ul.breadcrumb li a:hover {
  width: 280px;
  background-color: #aabee0;
}
ul.breadcrumb li a:hover .text {
  display: inline-block;
  opacity: 1;
}
ul.breadcrumb li:last-child a {
  padding: 0;
}
ul.breadcrumb li:last-child:hover {
  padding: 3px;
  margin-top: 0;
}
ul.breadcrumb li:last-child:hover a {
  width: 60px;
  height: 60px;
  line-height: 60px;
}

ul.breadcrumb .active{
  background-color:#262c45;
}

ul.breadcrumb li .active:hover
{ 
  background-color: #008000;
}
a:hover {
  color: red;
}

/* Dropdown */

.dropdown {
  display: inline-block;
  position: relative;
  z-index: 1;
}

.dd-button {
  display: inline-block;
  border: 1px solid gray;
  border-radius: 4px;
  padding: 10px 30px 10px 20px;
  background-color: #1a1c33;
  cursor: pointer;
  white-space: nowrap;
  color: white;
}

.dd-button:after {
  content: '';
  position: absolute;
  top: 50%;
  right: 15px;
  transform: translateY(-50%);
  width: 0; 
  height: 0; 
  border-left: 5px solid transparent;
  border-right: 5px solid transparent;
  border-top: 5px solid white;
  z-index: 1;
}

.dd-button:hover {
  background-color: #6d728a;
}


.dd-input {
  display: none;
}

.dd-menu {
  position: absolute;
  top: 100%;
  border: 1px solid #ccc;
  border-radius: 4px;
  padding: 0;
  margin: 2px 0 0 0;
  box-shadow: 0 0 6px 0 rgba(0,0,0,0.1);
  background-color: pink;
  list-style-type: none;
  color: white;
  z-index: 1;
}

.dd-input + .dd-menu {
  display: none;
  z-index: 1;
} 

.dd-input:checked + .dd-menu {
  display: block;
} 

.dd-menu li {
  padding: 10px 20px;
  cursor: pointer;
  white-space: nowrap;
  z-index: 1;
}
.dd-button::after{
  border-top: 5px solid white;
}

.dd-menu li:hover {
  background-color: #007bff;
}

.dd-menu li a {
  display: block;
  margin: -10px -20px;
  padding: 10px 20px;
  z-index: 1;
}

.dd-menu li.divide{
  padding: 0;
  border-bottom: 1px solid #fcf9fe;
}    
</style>
@stop

@section('js')
<script type="text/javascript" src="{{ asset('vendor/datatables/js/dataTables.bootstrap4.min.js') }}"></script>
 <script type="text/javascript">

$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();   
});
$(document).ready(function () {
$('#dtBasicExample').DataTable();
$('.dataTables_length').addClass('bs-select');
});

</script>

@stop




