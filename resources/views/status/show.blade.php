{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
     <h1 style="margin-bottom:-10px"><b>Show Status</b></h1>
     <hr class="divider" >
@stop

@section('content')
    
<div class="row">
    <div class="container-fluid">
        <table class="table table-bordered table-striped">
            <tr>
                <th>Status ID</th>
                <td>{{ $status->id }}</td>
            </tr>
            <tr>
                <th>Description</th>
                <td>{{ $status->description }}</td>        
            </tr>
        </table>
    </div>
<div class="col-xs-12 col-sm-12 col-md-12 text-center">
        <a class="btn btn-primary" href="{{ route('status.index') }}" style="float:left"> Back</a>
</div>
</div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop