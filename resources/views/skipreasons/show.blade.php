{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
     <h1 style="margin-bottom:-10px"><b>Show Skip Reason</b></h1>
     <hr class="divider" >
@stop

@section('content')
    
<div class="row">
    <div class="container-fluid">
        <table class="table table-bordered table-striped">
            <tr>
                <th>Name</th>
                <td>{{ $skipreason->name }}</td>
            </tr>
            <tr>
                <th>Description</th>
                <td>{{ $skipreason->description }}</td>        
            </tr>
             <tr>
                <th>Priority</th>
                <td>{{ $skipreason->priority }}</td>        
            </tr>
             <tr>
                <th>Type</th>
                <td>{{ $skipreason->type }}</td>        
            </tr>
        </table>
    </div>
</div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop