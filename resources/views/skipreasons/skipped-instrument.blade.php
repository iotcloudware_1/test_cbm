@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
     <h1 style="margin-bottom:-10px"><b>Instrument Skipped</b></h1>
     <hr class="divider" >
@stop

@section('content')
    
<div class="row">
  <div class="col-md-12">
    <table id="dtBasicExample" class="table table-striped table-bordered table-xl" cellspacing="0">   
      <thead>
        <tr>
          <th>Route Description</th>
          <th>Instrument</th>
          <th>Skipped Reason</th>
          <th>Remarks</th>
          <th>TimeStamp</th>
        </tr>
        </thead>  
      <tbody>
      @foreach($skipped_instrument as $skip_inst)
      <tr>
         <td>{{$skip_inst['route_history_id']}}</td>
         <td>{{$skip_inst['instrument_id']}}</td>
         <td>{{$skip_inst['skipped_id']}}</td>
         <td>{{$skip_inst['remarks']}}</td>
         <td>{{$skip_inst['created_at']}}</td>
      </tr>
      @endforeach
    </tbody>  
    </table>
  </div>
</div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <style type="text/css">
        tr th
        {
            text-align: center;
        }
    </style>
@stop

@section('js')
<script type="text/javascript" src="{{ asset('vendor/datatables/js/dataTables.bootstrap4.min.js') }}"></script>
 <script type="text/javascript">
   $(document).ready(function () {
  $('#dtBasicExample').DataTable();
  $('.dataTables_length').addClass('bs-select');
});

 </script>
@stop