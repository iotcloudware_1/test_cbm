{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1 style="margin-bottom:-10px"><b>Create Skip Reason</b></h1>
     <hr class="divider" >
@stop

@section('content')
    
@if (count($errors) > 0)
  <div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
       @foreach ($errors->all() as $error)
         <li>{{ $error }}</li>
       @endforeach
    </ul>
  </div>
@endif
{!! Form::open(array('route' => 'skipreasons.store','method'=>'POST')) !!}
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
        <strong><i class="fas fa-industry" style="color: #6c757d"></i>&nbsp Name </strong>
            {!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}         
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <strong><i class="fas fa-user-tie" style="color: #6c757d"></i>&nbsp Description </strong>
            {!! Form::text('description', null, array('placeholder' => 'Description','class' => 'form-control','maxlength'=>50)) !!}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
        <strong><i class="fas fa-industry" style="color: #6c757d"></i>&nbsp Priority </strong>
            {!! Form::select('priority', ['high'=>'high','medium'=>'medium','low'=>'low'], null, ['class' => 'form-control']) !!}      
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <strong><i class="fas fa-user-tie" style="color: #6c757d"></i>&nbsp Type </strong>
            {!! Form::text('type', null, array('placeholder' => 'Type','class' => 'form-control')) !!}
        </div>
    </div>
</div>

    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
        <button type="submit" class="btn btn-secondary"><i class="fas fa-pen-nib"></i> &nbsp; Submit</button>
    </div>
    <!-- <div class="col-xs-12 col-sm-12 col-md-12 text-center">
        <a class="btn btn-primary" style="font:center" href="{{ route('plants.index') }}" style="float:left"> Back</a>
</div>
 -->
{!! Form::close() !!}

@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    
@stop