{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
     <h1> Show Unit Status</h1>
     <hr class="divider" >
@stop

@section('content')
  


<div class="row">
    <div class="container-fluid">
        <table class="table table-bordered table-striped">
            <tr>
                <th>Unit Name</th>
                <td>{{ $unitstatus->unit_id }}</td>
            </tr>
             <tr>
                <th>Status Description</th>
                <td>{{ $unitstatus->status_id }}</td>
            </tr>
        </table>
    </div>

@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop