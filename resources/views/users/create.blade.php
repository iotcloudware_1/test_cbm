{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
 <h1 style="margin-bottom:-10px"><b>Create User</b></h1>
     <hr class="divider" >
@stop

@section('content')



@if (count($errors) > 0)
  <div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
       @foreach ($errors->all() as $error)
         <li>{{ $error }}</li>
       @endforeach
    </ul>
  </div>
@endif



{!! Form::open(array('route' => 'users.store','method'=>'POST')) !!}
<div class="row">
   <div class="col-md-6">
        <div class="form-group">
            <strong><i class="fas fa-file-signature" style="color: #6c757d"></i> &nbsp Name:</strong>
            {!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}
        </div>

        <div class="form-group">
            <strong><i class="fas fa-envelope" style="color: #6c757d"></i>&nbsp Email:</strong>
            {!! Form::text('email', null, array('placeholder' => 'Email','class' => 'form-control')) !!}
        </div>
   
        <div class="form-gruop">
            <strong><i class="fas fa-key"  style="color: #6c757d"></i>&nbsp Password:</strong>
            {!! Form::password('password', array('placeholder' => 'Password','class' => 'form-control')) !!}
        </div>
   </div>

    <div class="col-md-6">
        <div class="form-group">
            <strong><i class="fas fa-key" style="color: #6c757d"></i>&nbsp Confirm Password:</strong>
            {!! Form::password('confirm-password', array('placeholder' => 'Confirm Password','class' => 'form-control')) !!}
        </div>
    
        <div class="form-group">
            <strong><i class="fas fa-phone-alt" style="color: #6c757d"></i>&nbsp Phone 1:</strong>
            {!! Form::text('phone1', null, array('placeholder' => 'Number','class' => 'form-control')) !!}
        </div>
    
        <div class="form-group">
            <strong><i class="fas fa-phone-alt" style="color: #6c757d"></i>&nbsp Phone 2:</strong>
            {!! Form::text('phone2', null, array('placeholder' => 'Number','class' => 'form-control')) !!}
        </div>
    </div>
<!--         <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Created By</strong>
            {!! Form::text('created_by', null, array('placeholder' => 'Created By','class' => 'form-control')) !!}
        </div>
    </div> -->
</div>
     <br>
<div class="row">
     <input type="hidden" name="roles" value="operator" />
    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
         <button type="submit" class="btn btn-secondary"><i class="fas fa-pen-nib"></i> &nbsp; Submit</button>
    </div>
</div>
    
  <!--   <div class="col-xs-12 col-sm-12 col-md-12 text-center">
        <a class="btn btn-primary" href="{{ route('users.index') }}" > Back</a>
    </div>
     -->

{!! Form::close() !!}

@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop