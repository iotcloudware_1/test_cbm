{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
 <h1 style="margin-bottom:-10px"><b>User Management</b></h1>
     
  <div class="pull-right">
    <a class="btn btn-secondary" style="float: right;margin-top: -32px;" href="{{ route('users.create') }}"> <i class="fas fa-plus"></i>&nbsp; Create </a>
  </div>
  <hr class="divider" >
@stop

@section('content')
  <div class="row">
    <div class="col-md-10">
      
    </div>
    <div class="col-md-2"> 

    </div>
  </div>

  @if ($message = Session::get('success'))
  <div class="alert alert-success">
    <p>{{ $message }}</p>
  </div>
  @endif

@yield('datatable', View::make('datatables.user-datatable'))

@stop



@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('plugins.Datatables', true)
@section('js')
  
@stop