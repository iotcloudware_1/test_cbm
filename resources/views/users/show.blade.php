{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
     <h1 style="margin-bottom:-10px"><b>Show User</b></h1>  
     <hr class="divider" >
@stop

@section('content')
<div class="row">
    <table class="table table-bordered table-striped">
        <tr>
            <th>Name</th>
            <td>{{ $user->name }}</td>
        </tr>
        <tr>
            <th>Email</th>
            <td>{{ $user->email }}</td>
        </tr>
        <tr>
            <th>Roles</th>
            @if(!empty($user->getRoleNames()))
                @foreach($user->getRoleNames() as $v)
                    <td>{{ $v }}</td>
                @endforeach
            @endif        
        </tr>
    </table>
</div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop