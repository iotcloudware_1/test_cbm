{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1 style="margin-bottom:-10px"><b>Create Tool</b></h1>
     <hr class="divider" >
@stop

@section('content')

{!! Form::open(array('route' => 'tools.store','method'=>'POST')) !!}
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <strong><i class="fas fa-barcode" style="color: #6c757d"></i>&nbsp Name:</strong>
            {!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}
        </div>
        <div class="form-group">
            <strong><i class="fas fa-barcode" style="color: #6c757d"></i>&nbsp Serial Number:</strong> 
                {!! Form::text('serial_number', null, array('placeholder' => 'Serial Number','class' => 'form-control')) !!}
        </div>
        <div class="form-group">
            <strong><i class="fas fa-calendar-day" style="color: #6c757d"></i> &nbsp Manufacturer:</strong>
                {!! Form::text('manufacturer', null, array('placeholder' => 'Manufacturer','class' => 'form-control')) !!}
        </div>
    </div>

    <div class="col-md-6">    
        <div class="form-group">
            <strong><i class="fas fa-barcode" style="color: #6c757d"></i>&nbsp Model:</strong>
                {!! Form::text('model', null, array('placeholder' => 'Model','class' => 'form-control')) !!}         
        </div>
    </div>
   
      

    <!--  <div class="col-xs-12 col-sm-12 col-md-12 text-center">
        <a class="btn btn-primary" href="{{ route('tools.index') }}" style=""> Back</a>
	</div> -->
</div> <br>
<div class="row">
     <div class="col-xs-12 col-sm-12 col-md-12 text-center">
        <div class="form-group">
            <button type="submit" class="btn btn-secondary"><i class="fas fa-pen-nib"></i> &nbsp;Submit</button>
        </div>
    </div>
</div>
{!! Form::close() !!}
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop