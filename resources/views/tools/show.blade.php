{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')
 
@section('title', 'Dashboard')
@section('content_header')
    <h1 style="margin-bottom:-10px"><b> Show Tool</b></h1>
     <hr class="divider" >
@stop 

@section('content')
<div class="row">
    <div class="col-md-6 no-print">
    <div class="container-fluid">
        <table  class="table table-bordered table-striped">
           <tr >
                <th>ID</th>
                <td>{{ $tool->id }}</td>
            </tr>
            <tr>
                <th>Serial Number</th>
                <td>{{ $tool->serial_number }}</td>
            </tr>
           
             <tr>
                <th>Manufacturer</th>
                <td>{{ $tool->manufacturer }}</td>
            </tr>

            <tr>
                <th>Model</th>
                <td>{{ $tool->model }}</td>        
            </tr>          
            <tr>
                <th>Created at</th>
                <td>{{ $tool->created_at }}</td>
            </tr>
        </table>
    </div>    
</div>
    
</div>

<div class="row no-print">
    <div class="col-lg-12  margin-tb">
       
       
<!--         <div class="pull-right text-center">
            <a class="btn btn-primary "  href="{{ route('tools.index') }}"> Back</a>
        </div> -->

       
    </div>
@stop


@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
     <style type="text/css">
        @media print
        {    
            .no-print, .no-print *
            {
                display: none !important;
            }
        }
    </style>
@stop

@section('js')
     <script>
        function printQr()
        {
           window.print();
        }
    </script>
@stop 