{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
<h1 style="margin-bottom:-10px"><b>Tool Management</b></h1>
  <hr class="divider" >
  <div class="pull-right">
    <a class="btn btn-secondary" style="float: right;margin-top: 10px;" href="{{ route('tools.create') }}">  <i class="fas fa-plus"></i> &nbsp; Create </a>
    <a class="btn btn-secondary" style="float: right;margin-top: 10px;margin-right: 3px" href='/toolstatus'>  <i class="fas fa-eye"> </i> &nbsp; Status </a>
  </div>
  
@stop
 
@section('content') 
<div class="row">
    <div class="col-md-9">
      
    </div>
    <div class="col-md-3">

    </div>
  </div> 
  @if ($message = Session::get('success'))
  <div class="alert alert-success">
    <p>{{ $message }}</p>
  </div>
  @endif

@yield('datatable', View::make('datatables.tools-datatable'))

@stop



@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('plugins.Datatables', true)
@section('js')
  
@stop