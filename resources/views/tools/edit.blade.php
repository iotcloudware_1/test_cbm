 {{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1 style="margin-bottom:-10px"><b>Edit Tools</b></h1>
    <hr class="divider" >
@stop
  
@section('content')
   

@if (count($errors) > 0)
  <div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
       @foreach ($errors->all() as $error)
         <li>{{ $error }}</li>
       @endforeach
    </ul>
  </div>
@endif


{!! Form::model($tool, ['method' => 'PATCH','route' => ['tools.update', $tool->id]]) !!}
<div class="row">
    <div class="col-md-6">
            <div class="form-group">
                <strong><i class="fas fa-barcode" style="color: #6c757d"></i>&nbsp Name:</strong>
                
                {!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}
            </div>
            <div class="form-group">
                <strong><i class="fas fa-barcode" style="color: #6c757d"></i>&nbsp Serial Number:</strong>
                
                {!! Form::text('serial_number', null, array('placeholder' => 'serial_number','class' => 'form-control')) !!}
            </div>
        
            <div class="form-group">
                <strong><i class="fas fa-calendar-day" style="color: #6c757d"></i> &nbsp Manufacturer:</strong>
                {!! Form::text('manufacturer', null, array('placeholder' => 'manufacturer','class' => 'form-control')) !!}
            </div>
    
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <strong><i class="fas fa-barcode" style="color: #6c757d"></i>&nbsp Model:</strong>
          
            {!! Form::text('model', null, array('placeholder' => 'model','class' => 'form-control')) !!}
            
        </div>
    </div>
</div> <br>
<div class="row">
     <div class="col-xs-12 col-sm-12 col-md-12 text-center">
        <div class="form-group">
             <strong></strong>
             <button type="submit" class="btn btn-secondary"><i class="fas fa-pen-nib"></i> &nbsp;Submit</button>
    </div>
</div>
{!! Form::close() !!}
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop