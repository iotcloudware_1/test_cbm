{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
      <h1 style="margin-bottom:-10px"><b>Show Groups</b></h1>
         <hr class="divider" >
@stop

@section('content')
  
<div class="row">
    <div class="container-fluid">
        <div class="container-fluid">
        <table class="table table-bordered table-striped">
            <tr>
                <th>Name</th>
                <td>{{ $group->name }}</td>
            </tr>
             <tr>
                <th>Description</th>
                <td>{{ $group->description }}</td>
            </tr>
        </table>


        <table class="table table-bordered">
            <tr>
                <th>User</th>
            </tr>
            @foreach($group_users as $gu)
            <tr>
                <td> {{app\Helper\GeneralHelperFunctions\Helper::userIdToName($gu['user_id'])}}</td>
            </tr>
            @endforeach
        </table>
    </div>
</div>
</div>
    
<!-- <div class="col-xs-12 col-sm-12 col-md-12 text-center">
        <a class="btn btn-primary" href="{{ route('departments.index') }}"> Back</a>
    </div>
</div> -->
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop