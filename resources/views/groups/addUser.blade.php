@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')

@stop
 
@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
      <add-contact :group_id={{$input}}> </add-contact>
    </div>
  </div>
</div>

@stop



@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop


@section('js')
  
@stop

