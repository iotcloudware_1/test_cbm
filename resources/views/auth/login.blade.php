<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">


<style>
body {font-family: Arial, Helvetica, sans-serif;}
form { color:white;}

button {
  background-color: #1a1c33;
  color: white;
  padding: 14px 20px;
  margin: 8px 0;
  border: none;
  cursor: pointer;
  width: 30%;
  position:center;
}
  
button:hover {
  opacity: 0.8;
}

.cancelbtn {
  width: auto;
  padding: 10px 18px;
  background-color: #f44336;
}

.imgcontainer {
  text-align: center;
  margin: 24px 0 12px 0;
}

img.cbm {
  width: 24%;
  border-radius: 0%;
}

.container {
  padding: 16px;
}
.background-image
{
    background-opacity:0.5; 
}
span.psw {
  float: right;
  padding-top: 16px;
}

/* Change styles for span and cancel button on extra small screens */
@media screen and (max-width: 300px) {
  span.psw {
     display: block;
     float: none;
  }
  .cancelbtn {
     width: 100%;
  }
}
</style>
<link rel="stylesheet" href="{{ asset('css/app.css') }}">
</head>
<body style="background-image: url('images/loginbackground.png');background-size: 100%;background-repeat: no-repeat">
<div class="row" style="margin-left: 550px;margin-right: 550px;margin-top:150px;">
    <div style="background-color:#ffffff17;border-radius:10px;height: 451px;box-shadow: 5px 5px 15px #232c46;">
        <form method="POST" action="{{ route('login') }}">
            @csrf
        <div class="imgcontainer">
        <br>
            <img src="images/uptime_logo.png"alt="Avatar" class="cbm">
        </div>
        <br>
        <div class="container">
        <div class="md-form">
        <label for="uname"><b></b></label>
        
         <input  type="email"  class="w3-input" name="email" style="width: 350px; margin-left: 3px;margin-right: 6px;height:40px;box-shadow: 5px 5px 3px #232b46;" placeholder="E-mail Address" value="{{ old('email') }}" required autocomplete="email" autofocus>

        @error('email')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
        </div>
        <br>
        <label for="psw"><b></b></label>
        <input id="password" type="password" style="width: 350px;margin-left: 3px;margin-right: 6px;margin-top:8px;box-shadow: 5px 5px 3px #232b46; height:40px" name="password" placeholder="Password" required autocomplete="current-password">
        @error('password')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
        <br><br><br>
        <b>
<!--         <label style="text-shadow: 2px 2px 2px #181848;">
          <input style="margin-left: 30px;" type="checkbox" checked="checked" name="remember"> Remember me
        </label> -->
        </b>
        <button type="submit" style="margin-left: 67px;border-radius: 10px; height:40px;padding: initial;box-shadow: 5px 5px 7px #172040de;width:220px"><b>
        {{ __('Login') }}</b>
        </button>
        <br>
        <br>
          </div>
         
        </form> 
    </div>  

</div>

</body>
</html>
