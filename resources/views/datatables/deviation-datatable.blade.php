@section('datatable')
<center>
<div class="alert alert-success" style="display:none; width: 70%; text-align: center;" id="confirm_delete" role="alert"  >Deviation successfully deleted</div>

<div class="alert alert-warning" style="display:none; width: 70%; text-align: center;" id="foriegn_key_constraint" role="alert"  >Can not delete deviation.</div>

</center>
<table  class="table table-bordered table-striped table-condensed deviations-table" style="width: 100%">
    <thead>
          <tr>
            <th>User ID</th>
            <th>Deviation</th>
            <th>Actual</th>
            <th>Record</th>
            <th>Timestamp</th>
            
            <th style="width: 9%">Action</th>
        </tr>
    </thead>
    <tbody>
    </tbody>
</table>
@show

@section('css')

<link rel="stylesheet" type="text/css" href="{{ asset('vendor/datatables/css/dataTables.bootstrap4.min.css') }}">

@stop

@section('js')

<script type="text/javascript" src="{{ asset('vendor/datatables/js/dataTables.bootstrap4.min.js') }}"></script>

<script>
    var table = $('.deviations-table').DataTable({
        processing: true,
        serverSide: true,
        scrollX: false,
        order: [ [4, 'desc'] ],
        ajax: "{{ route('deviations.index') }}",
        columns: [
            {data: 'user_id', name: 'user_id'},
            {data: 'deviation', name: 'deviation'},
            {data: 'actual', name: 'actual'},
            {data: 'record', name: 'record'},
            {data: 'created_at', name: 'created_at'},            
            {data: 'action', name: 'action',  orderable: false, searchable: false},
        ]
    });


     var SITEURL = window.location+"/";
     
     $(document).ready( function () {
        $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });


        $('body').on('click', '#delete', function () {
            var deviation_id = $(this).data("id");
           
           if (confirm('Are you sure you want to delete this deviation into the database?')) {
            $.ajax({
                type: "get",
                url: SITEURL + "delete/"+deviation_id,
                success: function (data) {
                var oTable = $('.deviations-table').dataTable(); 
                oTable.fnDraw(false);
                $('#confirm_delete').show();
                $('#confirm_delete').fadeOut(5000,"swing");

                },
                error: function (data) {
                    console.log('Error:', data);
                        $('#foriegn_key_constraint').show();
                        $('#foriegn_key_constraint').fadeOut(5000,"swing");
                }
            });
        } else {
            
            //alert("Pagal hai phr delete pe click kue kiya tha ");
        }

     
           
        });

    });

     function showFunction(id)
    {
        console.log("click : "+id)
        var url = SITEURL+id;
        window.open(url,'_self');
    }
</script>
@stop