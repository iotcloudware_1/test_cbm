@section('datatable')
<center>
<div class="alert alert-success" style="display:none; width: 70%; text-align: center;" id="confirm_delete" role="alert"  >Route History successfully deleted</div>

<div class="alert alert-warning" style="display:none; width: 70%; text-align: center;" id="foriegn_key_constraint" role="alert"  >Can not delete Route History.</div>

</center>
<table  class="table table-bordered table-striped table-condensed routehistory-table" style="width: 100%">
    <thead>
          <tr>
            <th>Route</th>
            <th>Operator ID</th>
            <th>Route State</th>
            <th>Start Time</th>
            <th>End Time</th>
            <th>Duration Min</th>
            <th>Instrument Skipped</th>
            <th style="width: 9%">Show Skipped</th>
            <th>Sessions</th>
            <th>Timestamp</th>
        </tr>
    </thead>
</table>
@show

@section('css')

<link rel="stylesheet" type="text/css" href="{{ asset('vendor/datatables/css/dataTables.bootstrap4.min.css') }}">
<style type="text/css">
    .scrollStyle
{
overflow-x:auto;
}
</style>
@stop

@section('js')

<script type="text/javascript" src="{{ asset('vendor/datatables/js/dataTables.bootstrap4.min.js') }}"></script>

<script>
    var table = $('.routehistory-table').DataTable({
        processing: true,
        serverSide: true,
        scrollX: false,
        order: [ [9, 'desc'] ],
        ajax: "{{ route('routehistory.index') }}",
        columns: [
            {data: 'route_id', name: 'route_id'},
            {data: 'operator_id', name: 'operator_id'},
            {data: 'route_state', name: 'route_state'},
            {data: 'start_time', name: 'start_time'},
            {data: 'end_time', name: 'end_time'},
            {data: 'duration_min', name: 'duration_min'},
            {data: 'instrument_skiped', name: 'instrument_skiped'},
            {data: 'action', name: 'action',  orderable: false, searchable: false},
            {data: 'sessions', name: 'sessions'},
            {data: 'created_at', name: 'created_at'},
            
 
 
        ]
    });


     var SITEURL = window.location+"/";
     
     $(document).ready( function () {
        $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });


        $('body').on('click', '#delete', function () {
            var routehistory_id = $(this).data("id");
           
           if (confirm('Are you sure you want to delete this Route History into the database?')) {
            $.ajax({
                type: "get",
                url: SITEURL + "delete/"+routehistory_id,
                success: function (data) {
                var oTable = $('.routehistory-table').dataTable(); 
                oTable.fnDraw(false);
                $('#confirm_delete').show();
                $('#confirm_delete').fadeOut(5000,"swing");

                },
                error: function (data) {
                    console.log('Error:', data);
                        $('#foriegn_key_constraint').show();
                        $('#foriegn_key_constraint').fadeOut(5000,"swing");
                }
            });
        } else {
            
            //alert("Pagal hai phr delete pe click kue kiya tha ");
        }

     
           
        });

    });

    function showSkippedInstrument(id)
    {   
        var url= "/routeskippedinstruments/";
        console.log("click : "+id)
        var url = url+id;
        window.open(url,'_self');
    }
</script>
<script type="text/javascript">
oTable = $('#myDataTable').dataTable({
"aaSorting": [[2, "asc"]],
"sPaginationType": "full_numbers",
"bJQueryUI": true,
"aoColumnDefs": [
{ "bSortable": false, "bSearchable": false, "aTargets": [0] },
{ "bVisible": false, "bSortable": false, "bSearchable": false, "aTargets": [1] }
],
});

$(window).bind('resize', function () {
oTable.fnAdjustColumnSizing();
});
jQuery('.dataTable').wrap('');
</script>
@stop
