@section('datatable')
<center>
<div class="alert alert-success" style="display:none; width: 70%; text-align: center;" id="c-onfirm_delete" role="alert"  >Current Workorder successfully deleted</div>

<div class="alert alert-warning" style="display:none; width: 70%; text-align: center;" id="foriegn_key_constraint" role="alert"  >Can not delete current workorder.</div>

</center>
<table  class="table table-bordered table-condensed currentworkorder-table" style="width: 100%">
    <thead>
          <tr>
            <th>Instrument</th>
            <th>WorkOrder</th>
            <th>Priority</th>
            <th>Remarks</th>
            <th>Reported By</th>
            <th>Assigned To</th>
            <th>Ack Status</th>
            <th>Timestamp</th>
            <th>Action</th>


        </tr>
    </thead>
    <tbody>
    </tbody>
</table>
@show

@section('css')

<link rel="stylesheet" type="text/css" href="{{ asset('vendor/datatables/css/dataTables.bootstrap4.min.css') }}">

@stop

@section('js')

<script type="text/javascript" src="{{ asset('vendor/datatables/js/dataTables.bootstrap4.min.js') }}"></script>

<script type="text/javascript">
    $.ajax({
        type: "get",
        url:'api/currentworkorder/checkacknowledgement',
        success: function (data) {
            data=parseInt(data);
            if(data==1)
            {   
               $('#isActive').attr('disabled', true);  
            }
            if(data==0)
            {  
                $('#isActive').attr('disabled', false);
            }
            else
            {

            }  

        },
        error: function (data) {
            console.log('Error:', data);
        }
    });
</script>

<script>
    var table = $('.currentworkorder-table').DataTable({
        processing: true,
        serverSide: true,
        scrollX: false,
        order: [ [7, 'desc'] ],
        ajax: "{{ route('currentworkorder.index') }}",
        columns: [
            {data: 'instrument_name', name: 'instrument_name'},
            {data: 'workorder_id', name: 'workorder_id'},
            {data: 'priority', name: 'priority'},
            {data: 'remarks', name: 'remarks'},           
            {data: 'assigned_by', name: 'assigned_by'},
            {data: 'assigned_to', name: 'assigned_to'},
            {data: 'acknowledgement', name: 'acknowledgement'},
            {data: 'created_at', name: 'created_at'},
            {data: 'action', name: 'action'}, 
        ]
    });


     var SITEURL = window.location+"/";
     
     $(document).ready( function () {
        $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });


        $('body').on('click', '#delete', function () {
            var currentworkorder_id = $(this).data("id");
           
           if (confirm('Are you sure you want to delete this workorder againt instrument')) {
            $.ajax({
                type: "get",
                url: SITEURL + "delete/"+currentworkorder_id,
                success: function (data) {
                var oTable = $('.currentworkorder-table').dataTable(); 
                oTable.fnDraw(false);
                $('#confirm_delete').show();
                $('#confirm_delete').fadeOut(5000,"swing");

                },
                error: function (data) {
                    console.log('Error:', data);
                        $('#foriegn_key_constraint').show();
                        $('#foriegn_key_constraint').fadeOut(5000,"swing");
                }
            });
        } else {
            
            //alert("Pagal hai phr delete pe click kue kiya tha ");
        }

     
           
        });

    });

    function showFunction(id)
    {
        console.log("click : "+id)
        var url = SITEURL+id;
        window.open(url,'_self');
    }
    function ackIndividualWorkorder(id)
    {
        console.log(id)
        var url = SITEURL+"ackwork/"+id;
        window.open(url,'_self');        
    }

</script>
@stop