@section('datatable')
<center>
<div class="alert alert-success" style="display:none; width: 70%; text-align: center;" id="confirm_delete" role="alert"  >Permission successfully deleted</div>

<div class="alert alert-warning" style="display:none; width: 70%; text-align: center;" id="index_key_constraint" role="alert">Can not delete Permission. This Permission is already in use</div>
</center>
<table  class="table table-bordered table-striped table-condensed  permission-table" style="width: 100%">
        <thead>
              <tr>
                <th>Name</th>
                <th>Timestamp</th>
                <th style="width: 9%">Action</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
@show

@section('css')

<link rel="stylesheet" type="text/css" href="{{ asset('vendor/datatables/css/dataTables.bootstrap4.min.css') }}">

@stop

@section('js')

<script type="text/javascript" src="{{ asset('vendor/datatables/js/dataTables.bootstrap4.min.js') }}"></script>

<script>
    var table = $('.permission-table').DataTable({
        processing: true,
        serverSide: true,
        scrollX: false,
        order: [ [1, 'desc'] ],
        ajax: "{{ route('permissions.index') }}",
        columns: [
            {data: 'name', name: 'name'},
            {data: 'created_at', name: 'created_at'},
            {data: 'action', name: 'action'},
        ]
    });

   //var SITEURL = '{{URL::to("http://localhost/cbm/public/")}}';
     var SITEURL = window.location+"/";
     $(document).ready( function () {
        $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
    });

        $('body').on('click', '#delete', function () {
            var permission_id = $(this).data("id");
            if (confirm('Are you sure you want to delete this Permission into the database?')) {
     $.ajax({
                type: "get",
                url: SITEURL +"delete/"+permission_id,
                success: function (data) {
                var oTable = $('.permission-table').dataTable(); 
                oTable.fnDraw(false);
                $('#confirm_delete').show();
                $('#confirm_delete').fadeOut(5000,"swing");
                },
                error: function (data) {
                    console.log('Error:', data);
                    $('#index_key_constraint').show();
                    $('#index_key_constraint').fadeOut(5000,"swing");
                }
            });
        } else {
            
            alert("Pagal hai phr delete pe click kue kiya tha ");
        }

     
           
        });

    });
   function showFunction(id)
    {
        //console.log("click : "+id)
        var url = SITEURL+id;
        window.open(url,'_self');
    }
  function editFunction(id)
    {
        //console.log("click : "+id)
        var url = SITEURL+id;
        window.open(url+"/edit",'_self');
    }
  </script>
@stop