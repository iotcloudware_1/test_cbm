<div class="row">
	<div class="col-md-6">
		<a href="javascript:void(0)" data-toggle="tooltip"  data-id="{{ $id }}" data-original-title="show" class="show"  onclick="showFunction('{{$id}}');">
		   <i class="far fa-eye" style="color: #3d3482"></i>
		</a>
	</div>
	<div class="col-md-6">
<a href="javascript:void(0);" id="delete" data-toggle="tooltip" data-original-title="Delete" data-id="{{ $id }}" class="delete">
   <i class="fas fa-trash-alt" style="color: #c80909"></i>
</a>	
	</div>
</div>

 