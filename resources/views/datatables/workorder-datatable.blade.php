    @section('datatable')
<div class="alert alert-success" style="display:none; width: 70%; text-align: center;" id="confirm_delete" role="alert">Workorder successfully deleted</div>

<div class="alert alert-warning" style="display:none; width: 70%; text-align: center;" id="foriegn_key_constraint" role="alert"  >Can not delete workorder. This workorder is already in use</div><table  class="table table-bordered table-striped table-condensed workorders-table" style="width: 100%">
    <thead>
         <tr>
            <th>Description</th>
            <th>Instrument</th>
            <th>Type</th>
            <th>Priority</th>
            <th>Email</th>
            <th>SMS</th>
            <th>Expected Time</th>
            <th>Timestamp</th>
            <th style="width: 9%">Action</th>
        </tr>
    </thead>
    <tbody>
    </tbody>
</table>
@show

@section('css')

<link rel="stylesheet" type="text/css" href="{{ asset('vendor/datatables/css/dataTables.bootstrap4.min.css') }}">

@stop

@section('js')

<script type="text/javascript" src="{{ asset('vendor/datatables/js/dataTables.bootstrap4.min.js') }}"></script>

<script>
    var table = $('.workorders-table').DataTable({
        processing: true,
        serverSide: true,
        scrollX: false,
        order: [ [7, 'desc'] ],
        ajax: "{{ route('workorders.index') }}",
        columns: [
            {data: 'description',name: 'description'},
            {data: 'instrument_id',name: 'instrument_id'},
            {data: 'type',name: 'type'},  
            {data: 'priority', name: 'priority'}, 
            {data: 'send_email',name: 'send_email'},  
            {data: 'send_sms', name: 'send_sms'},
            {data: 'expected_time', name: 'expected_time'},    
            {data: 'created_at', name: 'created_at'},
            {data: 'action', name: 'action',  orderable: false, searchable: false},
        ]
    });
            var SITEURL = window.location+"/";

    // var SITEURL = '{{URL::to("http://localhost/cbm/public/")}}';

     $(document).ready( function () {
        $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });


        $('body').on('click', '#delete', function () {
            var workorder_id = $(this).data("id");
           if (confirm("Are You sure want to delete this workorder")) {
                $.ajax({
                type: "get",
                url: SITEURL + "delete/"+workorder_id,
                success: function (data) {
                var oTable = $('.workorder-table').dataTable(); 
                oTable.fnDraw(false);
                $('#confirm_delete').show();
                $('#confirm_delete').fadeOut(5000,"swing");
                },
                error: function (data) {
                    console.log('Error:', data);
                    $('#foriegn_key_constraint').show();
                    $('#foriegn_key_constraint').fadeOut(5000,"swing");
                                }
                });
            } else {
               //alert("Pagal hai phr delete pe click kue kiya tha ");
            }

        });
    
     });

    function showFunction(id)
    {
        //console.log("click : "+id)
        var url = SITEURL+id;
        window.open(url,'_self');
    }
    
    function editFunction(id)
    {
        //console.log("click : "+id)
        var url = SITEURL+id;
        window.open(url+"/edit",'_self');
    }
</script>
@stop