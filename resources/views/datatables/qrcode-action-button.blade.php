<div class="row">
	
	<div class="col-md-6">
		<a href="javascript:void(0)" data-toggle="tooltip"  data-id="{{ $id }}" data-original-title="show" class="show"  onclick="showFunction('{{$id}}');">
		   <i class="far fa-eye" style="color: #201c3f"></i>
		</a>
	</div>

	<div class="col-md-6">
	<a href="javascript:void(0)" data-toggle="tooltip"  data-id="{{ $id }}" data-original-title="generate" class="generate"  onclick="GenerateQrcode('{{$id}}');">
		   <i class="far fa-qrcode" style="color: #02e8e8"></i>
		</a>
	</div>
</div>
 
