<div class="row">
	<div class="col-md-3">
		<a href="javascript:void(0)" data-toggle="tooltip"  data-id="{{ $id }}" data-original-title="show-instrument-point" class="show-instrument-point"  onclick="showinstrumentpoint('{{$id}}');"><i class="fas fa-expand-arrows-alt"></i>
		   <!-- <button class="btn btn-success" style="float: left;">show</button> -->
		</a>
	</div>
	<div class="col-md-3">
		<a href="javascript:void(0)" data-toggle="tooltip"  data-id="{{ $id }}" data-original-title="add-instrument" class="add-instrument"  onclick="AddInstrument('{{$id}}');">
		   <i class="fa fa-plus-circle" ><!-- <button class="btn btn-success" style="float: left;">Add Instrument</button> --></i>
		</a>
	</div>
	<div class="col-md-3">
		<a href="javascript:void(0)" data-toggle="tooltip"  data-id="{{ $id }}" data-original-title="show" class="show"  onclick="showFunction('{{$id}}');">
		   <i class="far fa-eye" style="color: #02e8e8"></i>
		</a>
	</div>
	<div class="col-md-2">
		<a href="javascript:void(0)" data-toggle="tooltip"  data-id="{{ $id }}" data-original-title="Edit" class="edit-user" onclick="editFunction('{{$id}}');">
   		<i class="fas fa-pencil-alt" style="color: orange"></i>
		</a>
	</div>
	<div class="col-md-2">
<a href="javascript:void(0);" id="delete" data-toggle="tooltip" data-original-title="Delete" data-id="{{ $id }}" class="delete">
   <i class="fas fa-trash-alt" style="color: red"></i>
</a>	
	</div>



</div>

 