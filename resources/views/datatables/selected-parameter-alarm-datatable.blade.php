@section('datatable')
<center>
<div class="alert alert-success" style="display:none; width: 70%; text-align: center;" id="confirm_delete" role="alert"  >Instrument successfully deleted</div>

<div class="alert alert-warning" style="display:none; width: 70%; text-align: center;" id="foriegn_key_constraint" role="alert"  >Can not delete instrument. This instrument is already in use</div>

</center>
<table  class="table table-bordered table-striped table-condensed alarm-table" style="width: 100%">
    <thead>
          <tr>     
            <th>Alarm Message Name</th>
            <th>Value</th>
            <th>Timestamp</th>
        </tr>
    </thead>
    <tbody>
    </tbody>
</table>
@show

@section('css')

<link rel="stylesheet" type="text/css" href="{{ asset('vendor/datatables/css/dataTables.bootstrap4.min.css') }}">

@stop

@section('js')


@stop