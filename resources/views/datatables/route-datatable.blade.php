@section('datatable')
<table  class="table table-bordered table-striped table-condensed routes-table" style="width: 100%">
    <thead>
          <tr>
            <th>Operator Name</th>
            <th>Description</th>
            <th>Period</th>
            <th>Timestamp</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody style="text-align: center">
    </tbody>
</table>
@show

@section('css')

<link rel="stylesheet" type="text/css" href="{{ asset('vendor/datatables/css/dataTables.bootstrap4.min.css') }}">

@stop

@section('js')

<script type="text/javascript" src="{{ asset('vendor/datatables/js/dataTables.bootstrap4.min.js') }}"></script>

<script>
    var table = $('.routes-table').DataTable({
        processing: true,
        serverSide: true,
        scrollX: false,
        order: [ [3, 'desc'] ],
        ajax: "{{ route('routes.index') }}",
        columns: [
           
            {data: 'operator_id', name: 'operator_id'},
            {data: 'description', name: 'description'},  
            {data: 'period', name: 'period'},          
            {data: 'created_at', name: 'created_at'},
            {data: 'action', name: 'action',  orderable: false, searchable: false},
        ]
    });


     var SITEURL = window.location+"/";
     $(document).ready( function () {
        $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });


        $('body').on('click', '#delete', function () {
            var route_id = $(this).data("id");
           
           if (confirm('Are you sure you want to delete this route into the database?')) {
            $.ajax({
                type: "get",
                url: SITEURL + "delete/"+route_id,
                success: function (data) {
                var oTable = $('.routes-table').dataTable(); 
                oTable.fnDraw(false);
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        } else {
            
           
        }

     
           
        });

    });

     function showFunction(id)
    {
        //console.log("click : "+id)
        var url = SITEURL+id;
        window.open(url,'_self');
    }
    function editFunction(id)
    {
        //console.log("click : "+id)
        var url = SITEURL+id;
        window.open(url+"/edit",'_self');
    }
    function showinstrumentpoint(id)
    {
        //console.log("click : "+id)
        var url = SITEURL+"show-instrument-point/"+id;
        window.open(url,'_self');
    }

    function AddInstrument(id)
    {
        //console.log("click : "+id)
        var url = SITEURL+"add-instrument/"+id;
        window.open(url,'_self');
    }
    function showInspectionPoint(id)
    {
        var url= "/showinspectionpoint/";
        console.log("click : "+id)
        var url = url+id;
        window.open(url,'_self');
    }

</script>
@stop