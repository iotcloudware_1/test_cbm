@section('datatable')
<table  class="table table-bordered table-striped table-condensed qrcode-table" style="width: 100%">
    <thead>
          <tr>
            <th>Instrument ID</th>
            <th>Name</th>
            <th>Description</th>
            <th>Timestamp</th>
            <th style="width: 9%">Action</th>
        </tr>
    </thead>
    <tbody>
    </tbody>
</table>
@show

@section('css')

<link rel="stylesheet" type="text/css" href="{{ asset('vendor/datatables/css/dataTables.bootstrap4.min.css') }}">

@stop

@section('js')

<script type="text/javascript" src="{{ asset('vendor/datatables/js/dataTables.bootstrap4.min.js') }}"></script>

<script>
    var table = $('.qrcode-table').DataTable({
        processing: true,
        serverSide: true,
        scrollX: false,
        order: [ [3, 'desc'] ],
        ajax: "{{ route('qrcodes.index') }}",
        columns: [
            {data: 'department_id', name: 'department_id'},
            {data: 'name', name: 'name'},
            {data: 'description', name: 'description'},                
            {data: 'created_at', name: 'created_at'},
            {data: 'action', name: 'action',  orderable: false, searchable: false},
        ]
    });


     var SITEURL = window.location+"/";
     $(document).ready( function () {
        $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });


     //    $('body').on('click', '#delete', function () {
     //        var department_id = $(this).data("id");
           
     // //       if (confirm('Are you sure you want to delete this department into the database?')) {
     // // // $.ajax({
     // // //            type: "get",
     // // //            url: SITEURL + "delete/"+department_id,
     // // //            // success: function (data) {
     // // //            // var oTable = $('.qrcode-table').dataTable(); 
     // // //            // oTable.fnDraw(false);
     // // //            // },
     // // //            // error: function (data) {
     // // //            //     console.log('Error:', data);
     // // //            // }
     // // //        });
     // //        } 
     //        // else {
                
     //        //     alert("Pagal hai phr delete pe click kue kiya tha ");
     //        //         }
 
     
           
     //    });

    });

     function showFunction(id)
    {
        //console.log("click : "+id)
        var url = SITEURL+id;
        window.open("/qr-code",'_self');
    }
    function GenerateQrcode(id)
    {
        //console.log("click : "+id)
        var url = SITEURL+id;
        window.open(url+"/create",'_self');
    }
</script>
@stop