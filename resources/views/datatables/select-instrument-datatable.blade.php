@section('datatable')
<table  class="table table-bordered table-striped table-condensed select-instruments-table" style="width: 100%">
    <thead>
          <tr>     
            <th>Instrument Name</th> 
            <th>Description</th>
            <th style="width: 9%">Action</th>            
        </tr>
    </thead>
    <tbody>
    </tbody>
</table>
@show

@section('css')

<link rel="stylesheet" type="text/css" href="{{ asset('vendor/datatables/css/dataTables.bootstrap4.min.css') }}">
<style type="text/css">
    .dataTables_length{

    }
</style>
@stop

@section('js')

<script type="text/javascript" src="{{ asset('vendor/datatables/js/dataTables.bootstrap4.min.js') }}"></script>
<script>
    var table = $('.select-instruments-table').DataTable({
        processing: true,
        serverSide: true,
        scrollX: false,
        ajax: "{{ route('qr-code.selectinstrument') }}",
        columns: [    
            {data: 'name', name: 'name'},
            {data: 'description', name: 'description'},
            {data: 'action', name: 'action',  orderable: false, searchable: false},
            ]
    });


     var SITEURL = window.location+"/";
     
     $(document).ready( function () {
        $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });


        $('body').on('click', '#delete', function () {
            var instrument_id = $(this).data("id");
           
           if (confirm('Are you sure you want to delete this instrument into the database?')) {
            $.ajax({
                type: "get",
                url: SITEURL + "delete/"+instrument_id,
                success: function (data) {
                var oTable = $('.instruments-table').dataTable(); 
                oTable.fnDraw(false);
                $('#confirm_delete').show();
                $('#confirm_delete').fadeOut(5000,"swing");

                },
                error: function (data) {
                    console.log('Error:', data);
                        $('#foriegn_key_constraint').show();
                        $('#foriegn_key_constraint').fadeOut(5000,"swing");
                }
            });
        } else {
            
            //alert("Pagal hai phr delete pe click kue kiya tha ");
        }

     
           
        });

    });

     function selectinstrumentFunction(id)
    {
        //console.log("click : "+id)
        var url = SITEURL+id;
        window.open(url,'_self');
    }
    function editFunction(id)
    {
        //console.log("click : "+id)
        var url = SITEURL+id;
        window.open(url+"/edit",'_self');
    }
</script>
<script type="text/javascript">
      function selectAll(){

        var items=document.getElementsByName('instruments[]');
        for(var i=0; i<items.length; i++){
          if(items[i].type=='checkbox')
            items[i].checked=true;
          
        }
      }
      
      function UnSelectAll(){
        var items=document.getElementsByName('instruments[]');
        for(var i=0; i<items.length; i++){
          if(items[i].type=='checkbox')
            items[i].checked=false;
        }
      }     
    </script>
@stop

