  @section('datatable')

<table  class="table table-bordered table-striped table-condensed show-inspection-table" style="width: 100%">
    <thead>
          <tr>
              <th>Instrument Name</th>
              <th>Instrument Description</th>
              <th>Wrench Time</th>
              <th>TimeStamp</th>
              <th style="width: 9%">Action</th>
        </tr>
    </thead>
    <tbody>
    </tbody>
</table>
@show

@section('css')

<link rel="stylesheet" type="text/css" href="{{ asset('vendor/datatables/css/dataTables.bootstrap4.min.css') }}">

@stop

@section('js')

<script type="text/javascript" src="{{ asset('vendor/datatables/js/dataTables.bootstrap4.min.js') }}"></script>

<script>
    var table = $('.show-inspection-table').DataTable({
        processing: true,
        serverSide: true,
        scrollX: false,
        order: [ [4, 'desc'] ],
        ajax: "{{ route('showinspectionpoint') }}",
        columns: [
            {data: 'instrument_id', name: 'instrument_id'},
            {data: 'instrument_des', name: 'instrument_des'},
            {data: 'wrench_time', name: 'wrench_time'},
            {data: 'created_at', name: 'created_at'},
            {data: 'action', name: 'action',  orderable: false, searchable: false},
        ]
    });


     var SITEURL = window.location+"/";
     $(document).ready( function () {
        $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });


        $('body').on('click', '#delete', function () {
            var Parameter_id = $(this).data("id");
           
           if (confirm('Are you sure you want to delete this Parameter into the Database?')) {
     $.ajax({
                type: "get",
                url: SITEURL + "delete/"+parameter_id,
                success: function (data) {
                var oTable = $('.show-inspection-table').dataTable(); 
                oTable.fnDraw(false);
                $('#confirm_delete').show();
                $('#confirm_delete').fadeOut(5000,"swing");
                },
                error: function (data) {
                    console.log('Error:', data);
                    $('#foriegn_key_constraint').show();
                    $('#foriegn_key_constraint').fadeOut(5000,"swing");
                }
            });
        } else {
            
            //alert("Pagal hai phr delete pe click kue kiya tha ");
        }

     
           
        });

    });

     function showFunction(id)
    {
        //console.log("click : "+id)
        var url = SITEURL+id;
        window.open(url,'_self');
    }
    function editFunction(id)
    {
        //console.log("click : "+id)
        var url = SITEURL+id;
        window.open(url+"/edit",'_self');
    }

    // function showInspectionPoint(id)
    // {
    //     var url= "/showinspectionpoint/";
    //     console.log("click : "+id)
    //     var url = url+id;
    //     window.open(url,'_self');
    // }
</script>
@stop