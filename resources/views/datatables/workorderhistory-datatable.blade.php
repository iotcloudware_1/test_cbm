@section('datatable')
<center>
<div class="alert alert-success" style="display:none; width: 70%; text-align: center;" id="confirm_delete" role="alert"  >Deviation successfully deleted</div>

<div class="alert alert-warning" style="display:none; width: 70%; text-align: center;" id="foriegn_key_constraint" role="alert"  >Can not delete deviation.</div>

</center>
<table  class="table table-bordered table-condensed workorderhistory-table" style="width: 100%">
    <thead>
          <tr>
            <th>Instrument</th>
            <th>Workorder</th>
            <th>Remarks</th>
            <th>Priority</th>
            <th>Status</th>
            <th>Action by</th>
            <th>Timestamp</th>
        </tr>
    </thead>
    <tbody>
    </tbody>
</table>
@show

@section('css')

<link rel="stylesheet" type="text/css" href="{{ asset('vendor/datatables/css/dataTables.bootstrap4.min.css') }}">

@stop

@section('js')

<script type="text/javascript" src="{{ asset('vendor/datatables/js/dataTables.bootstrap4.min.js') }}"></script>

<script>
    var table = $('.workorderhistory-table').DataTable({
        processing: true,
        serverSide: true,
        scrollX: false,
        order: [ [6, 'desc'] ],
        ajax: "{{ route('workorderhistory.index') }}",
        columns: [
            {data: 'instrument_id', name: 'instrument_id'},
            {data: 'description', name: 'description'},
            {data: 'remarks', name: 'remarks'},
            {data: 'priority', name: 'priority'},
            {data: 'action', name: 'action'},
            {data: 'action_taken_by', name: 'action_taken_by'},
            {data: 'created_at', name: 'created_at'}, 
        ]
    });


     var SITEURL = window.location+"/";
     
     $(document).ready( function () {
        $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });


        $('body').on('click', '#delete', function () {
            var workorderhistory_id = $(this).data("id");
           
           if (confirm('Are you sure you want to delete this deviation?')) {
            $.ajax({
                type: "get",
                url: SITEURL + "delete/"+faulthistory_id,
                success: function (data) {
                var oTable = $('.workorderhistory-table').dataTable(); 
                oTable.fnDraw(false);
                $('#confirm_delete').show();
                $('#confirm_delete').fadeOut(5000,"swing");

                },
                error: function (data) {
                    console.log('Error:', data);
                        $('#foriegn_key_constraint').show();
                        $('#foriegn_key_constraint').fadeOut(5000,"swing");
                }
            });
        } else {
            
            //alert("Pagal hai phr delete pe click kue kiya tha ");
        }

     
           
        });

    });

     function showFunction(id)
    {
        console.log("click : "+id)
        var url = SITEURL+id;
        window.open(url,'_self');
    }
</script>
@stop