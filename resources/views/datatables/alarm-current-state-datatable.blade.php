@section('datatable')
<center>
<div class="alert alert-success" style="display:none; width: 70%; text-align: center;" id="confirm_delete" role="alert"  >Alarm successfully deleted</div>

<div class="alert alert-warning" style="display:none; width: 70%; text-align: center;" id="foriegn_key_constraint" role="alert">Can not delete alarm. This Alarm is already in use</div>
</center>


<table  class="table table-bordered table-striped table-condensed alarm-current-state-table" style="width: 100%">
   <thead>
        <tr>
            <th>Instrument</th>
            <th>Parameter</th>
            <th>Alarm</th>
            <th>Value</th>
            <th>Ack State</th>
            <th>Timestamp</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
    </tbody>
</table>
@show

@section('css')

<link rel="stylesheet" type="text/css" href="{{ asset('vendor/datatables/css/dataTables.bootstrap4.min.css') }}">

@stop

@section('js')
<script type="text/javascript" src="{{ asset('vendor/datatables/js/dataTables.bootstrap4.min.js') }}"></script>

<script type="text/javascript">
    $.ajax({
        type: "get",
        url:'api/currentalarm/checkacknowledgement',
        success: function (data) {
            data=parseInt(data);
            if(data==1)
            {   
               $('#isActive').attr('disabled', true);  
            }
            if(data==0)
            {  
                $('#isActive').attr('disabled', false);
            }
            else
            {

            }  

        },
        error: function (data) {
            console.log('Error:', data);
        }
    });    
</script>

<script>
    $.ajax({
      url: "{{ route('getCurrentAlarm') }}",
      success: function(data){
        console.log(data)
        if (data!='[]')
        {
            document.getElementById("isActive").disabled = false;
        }
      }
    });
    var table = $('.alarm-current-state-table').DataTable({
        processing: true,
        serverSide: true,
        scrollX: false,
        order: [ [5, 'desc'] ],
        ajax: "{{ route('alarms.currentAlarmState') }}",
        columns: [
            {data: 'instrument_name', name: 'instrument_name'},
            {data: 'parameter_id', name: 'parameter_id'},
            {data: 'alarm_message', name: 'alarm_message'},
            {data: 'new_value', name: 'new_value'},
            {data: 'ack_state', name: 'ack_state'},     
            {data: 'updated_at', name: 'updated_at'},
            {data: 'action', name: 'action'},
        ]
    });


     var SITEURL = window.location+"/";

     $(document).ready( function () {
        $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });


        $('body').on('click', '#delete', function () {
            var alarm_id = $(this).data("id");
           
           if (confirm('Are you sure you want to delete this alarm into the database?')) {
            $.ajax({
                type: "get",
                url: SITEURL + "delete/"+alarm_id,
                success: function (data) {
                var oTable = $('.alarm-current-state-table').dataTable(); 
                oTable.fnDraw(false);
                $('#confirm_delete').show();
                $('#confirm_delete').fadeOut(5000,"swing");

                },
                error: function (data) {
                    console.log('Error:', data);
                        $('#foriegn_key_constraint').show();
                        $('#foriegn_key_constraint').fadeOut(5000,"swing");
                }
            });
        }
        else {
            
            //alert("Pagal hai phr delete pe click kue kiya tha ");
        }    
           
        });

    });

    function showFunction(id)
    {
        //console.log("click : "+id)
        var url = SITEURL+id;
        window.open(url,'_self');
    }
    function editFunction(id)
    {
        //console.log("click : "+id)
        var url = SITEURL+id;
        window.open(url+"/edit",'_self');
    }
    // function ackIndividualAlarm(id)
    // {
    //     console.log("click : "+id)
    //     var url = SITEURL+id;
    //     window.open(url+"/ack",'_self');
    // }   
    //    function ackIndividualAlarm(id)
    // {
    //     //console.log("click : "+id)
    //     var url = SITEURL+"ack-individual-alarm/"+id;
    //     window.open(url,'_self');
    // }
    function ackIndividualAlarm(id)
    {
        console.log(id)
        var url = SITEURL+"ack/"+id;
        window.open(url,'_self');
    }  
</script>

@stop
