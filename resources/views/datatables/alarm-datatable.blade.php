@section('datatable')
<center>
<div class="alert alert-success" style="display:none; width: 70%; text-align: center;" id="confirm_delete" role="alert"  >Alarm successfully deleted</div>

<div class="alert alert-warning" style="display:none; width: 70%; text-align: center;" id="foriegn_key_constraint" role="alert"  >Can not delete alarm. This Alarm is already in use</div>

<table  class="table table-bordered table-striped table-condensed alarms-table" style="width: 100%">
   <thead>
          <tr>
            <th>Alarm Message</th>
            <th>Instrument Name</th>
            <th>Parameter Name</th>
            <th>Condition</th>
            <th>Set Point</th>
            <th>Priority</th>
            <th>Timestamp</th>
            <th style="width: 9%">Action</th>
        </tr>
    </thead>
    <tbody>
    </tbody>
</table>
@show

@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('vendor/datatables/css/dataTables.bootstrap4.min.css') }}">

@stop

@section('js')

<script type="text/javascript" src="{{ asset('vendor/datatables/js/dataTables.bootstrap4.min.js') }}"></script>

<script>
    var table = $('.alarms-table').DataTable({
        processing: true,
        serverSide: true,
        scrollX: false,
        order: [ [5, 'desc'] ],
        ajax: "{{ route('alarms.index') }}",
        columns: [
            {data: 'alarm_message', name: 'alarm_message'},
            {data: 'instrument_id', name: 'instrument_id'},
            {data: 'parameter_id', name: 'parameter_id'},
            {data: 'condition', name: 'condition'},
            {data: 'setpoint', name: 'setpoint'},
            {data: 'priority', name: 'priority'},           
            {data: 'created_at', name: 'created_at'},
            {data: 'action', name: 'action',  orderable: false, searchable: false},
        ]
    });


     var SITEURL = window.location+"/";

     $(document).ready( function () {
        $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });


        $('body').on('click', '#delete', function () {
            var alarm_id = $(this).data("id");
           
           if (confirm('Are you sure you want to delete this alarm into the database?')) {
            $.ajax({
                type: "get",
                url: SITEURL + "delete/"+alarm_id,
                success: function (data) {
                var oTable = $('.alarms-table').dataTable(); 
                oTable.fnDraw(false);
                $('#confirm_delete').show();
                $('#confirm_delete').fadeOut(5000,"swing");

                },
                error: function (data) {
                    console.log('Error:', data);
                        $('#foriegn_key_constraint').show();
                        $('#foriegn_key_constraint').fadeOut(5000,"swing");
                }
            });
        }
        else {
            
            //alert("Pagal hai phr delete pe click kue kiya tha ");
        }    
           
        });

    });

    function showFunction(id)
    {
        //console.log("click : "+id)
        var url = SITEURL+id;
        window.open(url,'_self');
    }
    function editFunction(id)
    {
        //console.log("click : "+id)
        var url = SITEURL+id;
        window.open(url+"/edit",'_self');
    }
</script>

@stop
