@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
     <h1 style="margin-bottom:-10px"><b>Show Inspection Parameter</b></h1>
     <hr class="divider" >
@stop

@section('content')
    
<div class="row">
    
   <div class="col-md-12">
    <table id="dtBasicExample" class="table table-striped table-bordered table-xl" cellspacing="0">   
      <thead>
      <tr>
          <th>Instrument Name</th>
          <th>Route Description</th>
          <th>Parameter Name</th>
          <th>Time Min</th>
      </tr>
      </thead>  
      <tbody>
      @foreach($show_inspection_parameter as $show_insp)
      <tr> 
         <td>{{$show_insp['instrument_name']}}</td>
         <td>{{$show_insp['route_des']}}</td>
         <td>{{$show_insp['parameter_id']}}</td>
         <td>{{$show_insp['inspection_time_min']}}</td>
      </tr>
        @endforeach
      </tbody>  
    </table>
  </div>
</div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <style type="text/css">
        tr th
        {
            text-align: center;
        }
    </style>
@stop

@section('js')
<script type="text/javascript" src="{{ asset('vendor/datatables/js/dataTables.bootstrap4.min.js') }}"></script>
 <script type="text/javascript">
   $(document).ready(function () {
  $('#dtBasicExample').DataTable();
  $('.dataTables_length').addClass('bs-select');
});

 </script>
@stop