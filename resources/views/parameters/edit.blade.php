 {{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1 style="margin-bottom:-10px"><b>Edit Parameter</b></h1>
    <hr class="divider" >
@stop
  
@section('content')
<div class="centered-container">
@if (count($errors) > 0)
  <div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
       @foreach ($errors->all() as $error)
         <li>{{ $error }}</li>
       @endforeach
    </ul>
  </div>
@endif


{!! Form::model($parameter, ['method' => 'PATCH','route' => ['parameters.update', $parameter->id]]) !!}
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <strong><i class="fas fa-cash-register" style="color: #6c757d"></i>&nbsp Instrument Name:</strong>
            {!! Form::select('instrument_id', $instrument, null, ['class'=>'form-control']) !!}

        </div>
        <div class="form-group">
            <strong><i class="fas fa-tachometer-alt" style="color: #6c757d"></i>&nbsp Parameter Name:</strong>
            
            {!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}          
        </div>
    
        <div class="form-group">
            <strong><i class="fas fa-info-circle" style="color: #6c757d"></i>&nbsp Description:</strong>
            
            {!! Form::text('description', null, array('placeholder' => 'Description','class' => 'form-control','maxlength'=>50)) !!}
        </div>
         <div class="form-group">
            <strong><i class="fas fa-user-edit" style="color: #6c757d"></i>&nbsp Entry:</strong>
           
            {!! Form::select('entry', ['automated'=>'Automated','manual'=>'Manual'], null, ['class' => 'form-control']  ) !!}
            
        </div>
        <div class="form-group">
            <strong><i class="fas fa-user-edit" style="color: #6c757d"></i>&nbsp Type:</strong>     
           {!! Form::select('type', ['physical'=>'Physical','logical'=>'Logical'], null, ['class' => 'form-control']  ) !!}
            
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <strong>Data Type:</strong>
           
            {!! Form::text('data_type', null, array('placeholder' => 'data_type','class' => 'form-control')) !!}
        </div>
        <div class="form-group">
             <strong><i class="fas fa-hashtag" style="color: #6c757d"></i>&nbsp Max Value:</strong>
            
            {!! Form::Number('max_value', null, array('placeholder' => 'Max Value','class' => 'form-control')) !!}
            
        </div>
    
        <div class="form-group">
            <strong><i class="fas fa-hashtag" style="color: #6c757d"></i>&nbsp Min Value:</strong>
           
            {!! Form::Number('min_value', null, array('placeholder' => 'Min Value','class' => 'form-control')) !!}
        </div>
        <div class="form-group">
             <strong>Engineering Unit:</strong>        
            {!! Form::text('engrineering_unit', null, array('placeholder' => 'Engineering Unit','class' => 'form-control')) !!}
           
        </div>

        <div class="form-group">
            <strong> <i class="fas fa-cannabis" style="color: #6c757d"></i>&nbsp Tool Name:</strong>          
           {!! Form::select('tool_id', $tool, null, ['class'=>'form-control']) !!}
           
        </div>
    </div>        
   
    <!--  <div class="col-xs-12 col-sm-12 col-md-12 text-center">
        <a class="btn btn-primary" href="{{ route('parameters.index') }}" style=""> Back</a>
    </div> -->
</div>
<br>
    <div class="row">
         <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <button type="submit" class="btn btn-secondary"><i class="fas fa-pen-nib"></i> &nbsp; Submit</button>
        </div>
    </div>
    {!! Form::close() !!}
</div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop