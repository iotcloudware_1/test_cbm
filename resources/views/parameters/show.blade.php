 {{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1 style="margin-bottom:-10px"><b>Show Parameter</b></h1>
    <hr class="divider" >
@stop

@section('content')
<div class="centered-container">
<div class="row">
    <table class="table table-bordered table-striped">
        <tr>
            <th>Instrument Name</th>
            <td>{{ $parameter->instrument_id }}</td>
        </tr>
        <tr>
            <th>Tool Name</th>
            <td>{{ $parameter->tool_id }}</td>
        </tr>
         <tr>
            <th>Name</th>
            <td>{{ $parameter->name }}</td>
        </tr>

        <tr>
            <th>Description</th>
            <td>{{ $parameter->description }}</td>        
        </tr>
         <tr>
            <th>Type </th>
            <td>{{ $parameter->type  }}</td>        
        </tr>
         <tr>
            <th>Entry</th>
            <td>{{ $parameter->entry }}</td>        
        </tr>
         <tr>
            <th>Data Type</th>
            <td>{{ $parameter->data_type }}</td>        
        </tr>
         <tr>
            <th>Max Value</th>
            <td>{{ $parameter->max_value }}</td>        
        </tr>
         <tr>
            <th>Min Value</th>
            <td>{{ $parameter->min_value }}</td>        
        </tr>
         <tr>
            <th>Engrineering Unit</th>
            <td>{{ $parameter->engrineering_unit }}</td>        
        </tr>
        <tr>
            <th>Created at</th>
            <td>{{ $parameter->created_at }}</td>
        </tr>
    </table>
</div>
<!--     <div class="col-xs-12 col-sm-12 col-md-12 text-center">
        <a class="btn btn-primary" href="{{ route('parameters.index') }}"> Back</a>
    </div> -->
</div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop