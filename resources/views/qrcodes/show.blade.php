  {{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')

@stop
 
@section('content')
<div style="display: inline-block; width: 100%">
    <h1 style="font-size: 29px;margin-top:0px;"> All QRCodes</h1>
    <button onclick="printQr()" class="btn btn-success" style="float: right; background-color: #1a1c33; margin-bottom: 6px;margin-top: -28px"><i class="fas fa-cog" style="color: white"></i> &nbsp;Print All QRCode</button>
</div>
<hr class="divider" style="margin-top: 0px">

<div class="row">
    @foreach(range(1, 1) as $key => $value)
        
        @if(!($key % 6) and $key > 0)
            </div>

            <div class="row" style="margin-top: 0px">

        @endif
        @foreach($allQrCodes as $key => $qrCode)
            @if($key == 36  )
            <div class="col-md-2" style="padding-bottom: 0%; margin-bottom: 130px">
            @elseif($key == 72)
            <div class="col-md-2">
            @elseif($key == 108)
            <div class="col-md-2">
            @elseif($key == 144)
            <div class="col-md-2">
            @elseif($key == 180)
            <div class="col-md-2">
            @else
            <div class="col-md-2">
            @endif
            <div style="height:180px;width:75%;border-style: solid;margin-bottom: 3%; border-radius: 10px">

                <center>
           
                <b style="font-size: 15px">
                <?php 
                    $instrument = json_decode($qrCode,true);
                    echo(($instrument[0]['instrument_name']));
                ?>
                </b>
                <br>
                <?php 
                    $instrument = json_decode($qrCode,true);
                     echo(($instrument[0]['instrument_description']));
                ?>x
                <br><img src="data:image/png;base64, {!! base64_encode(QrCode::format('png')->size(100)->generate($qrCode)) !!} ">
                <br>
                <a style="font-size: 15px">
                <?php 
                    $instrument = json_decode($qrCode,true);
                    echo(($instrument[0]['qr_code']));
                ?>
                </a>                
                </center>      
            </div>  
              <br>  <br>   
            </div>


        @endforeach
       
    @endforeach

</div>
<!-- <div class="col-xs-12 col-sm-12 col-md-12 text-center">
        <a class="btn btn-primary" href="{{ url('/qr-code') }}" style="float:left"> Back</a>
</div> -->

       
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <style type="text/css">
        @media print
        {    
            .no-print, .no-print *
            {
                display: none !important;
            }
        }
        @media print{
  .box-text {

    font-size: 27px !important; 
    color: blue !important;
    -webkit-print-color-adjust: exact !important;
  }
}
    </style>
@stop

@section('js')
    <script>
        function printQr()
        {
           window.print();
        }
    </script>
@stop 