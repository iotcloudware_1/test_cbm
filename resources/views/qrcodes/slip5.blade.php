 {{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header') 
@stop

@section('content')
<input type="button" onclick="printDiv('ele')" value="print a div!" />
<div id="ele" >
	<div class="row">
		<div class="col-md-3" style="margin-left: 5px; padding-left: 2px;padding-right: 2px; padding-bottom: 2px">
			<h4><b>Instrument Name</b></h4>
			<h4><b>Description of instr</b></h4>
			<h4><b>ument..........</b></h4>
			<p style="margin-left: 50px;padding-top: 12px"><b>11001110</b></p>
			<img src="images/qrcode.png" style="margin-top: -9px;margin-left: 29px;width:190px;height:190px;">
		</div>
		<div class="col-md-3" style="margin-left: 5px; padding-left: 2px;padding-right: 2px; padding-bottom: 2px">
			<h4><b>Instrument Name</b></h4>
			<h4><b>Description of instr</b></h4>
			<h4><b>ument..........</b></h4>
			<p style="margin-left: 50px;padding-top: 12px"><b>11001110</b></p>
			<img src="images/qrcode.png" style="margin-top: -9px;margin-left: 29px;width:190px;height:190px;">
		</div>
		<div class="col-md-3" style="margin-left: 5px; padding-left: 2px;padding-right: 2px; padding-bottom: 2px">
			<h4><b>Instrument Name</b></h4>
			<h4><b>Description of instr</b></h4>
			<h4><b>ument..........</b></h4>
			<p style="margin-left: 50px;padding-top: 12px"><b>11001110</b></p>
			<img src="images/qrcode.png" style="margin-top: -9px;margin-left: 29px;width:190px;height:190px;">
		</div>
	</div>
	<br>
<div class="row">
		<div class="col-md-3" style="margin-left: 5px; padding-left: 2px;padding-right: 2px; padding-bottom: 2px">
			<h4><b>Instrument Name</b></h4>
			<h4><b>Description of instr</b></h4>
			<h4><b>ument..........</b></h4>
			<p style="margin-left: 50px;padding-top: 12px"><b>11001110</b></p>
			<img src="images/qrcode.png" style="margin-top: -9px;margin-left: 29px;width:190px;height:190px;">
		</div>
		<div class="col-md-3" style="margin-left: 5px; padding-left: 2px;padding-right: 2px; padding-bottom: 2px">
			<h4><b>Instrument Name</b></h4>
			<h4><b>Description of instr</b></h4>
			<h4><b>ument..........</b></h4>
			<p style="margin-left: 50px;padding-top: 12px"><b>11001110</b></p>
			<img src="images/qrcode.png" style="margin-top: -9px;margin-left: 29px;width:190px;height:190px;">
		</div>
		<div class="col-md-3" style="margin-left: 5px; padding-left: 2px;padding-right: 2px; padding-bottom: 2px">
			<h4><b>Instrument Name</b></h4>
			<h4><b>Description of instr</b></h4>
			<h4><b>ument..........</b></h4>
			<p style="margin-left: 50px;padding-top: 12px"><b>11001110</b></p>
			<img src="images/qrcode.png" style="margin-top: -9px;margin-left: 29px;width:190px;height:190px;">
		</div>
	</div>
	<br>
<div class="row">
		<div class="col-md-3" style="margin-left: 5px; padding-left: 2px;padding-right: 2px; padding-bottom: 2px">
			<h4><b>Instrument Name</b></h4>
			<h4><b>Description of instr</b></h4>
			<h4><b>ument..........</b></h4>
			<p style="margin-left: 50px;padding-top: 12px"><b>11001110</b></p>
			<img src="images/qrcode.png" style="margin-top: -9px;margin-left: 29px;width:190px;height:190px;">
		</div>
		<div class="col-md-3" style="margin-left: 5px; padding-left: 2px;padding-right: 2px; padding-bottom: 2px">
			<h4><b>Instrument Name</b></h4>
			<h4><b>Description of instr</b></h4>
			<h4><b>ument..........</b></h4>
			<p style="margin-left: 50px;padding-top: 12px"><b>11001110</b></p>
			<img src="images/qrcode.png" style="margin-top: -9px;margin-left: 29px;width:190px;height:190px;">
		</div>
		<div class="col-md-3" style="margin-left: 5px; padding-left: 2px;padding-right: 2px; padding-bottom: 2px">
			<h4><b>Instrument Name</b></h4>
			<h4><b>Description of instr</b></h4>
			<h4><b>ument..........</b></h4>
			<p style="margin-left: 50px;padding-top: 12px"><b>11001110</b></p>
			<img src="images/qrcode.png" style="margin-top: -9px;margin-left: 29px;width:190px;height:190px;">
		</div>
	</div>
</div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')

<script type="text/javascript">
function printDiv(divName) {
     var printContents = document.getElementById(divName).innerHTML;
     var originalContents = document.body.innerHTML;

     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
}
</script>

@stop