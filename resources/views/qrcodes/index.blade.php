 {{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>QRCode Management</h1>
     <h1 style="margin-bottom:-10px"><b>QRCode Management</b></h1>
    <hr class="divider" >
@stop
 
@section('content')
  
    <div class="row">
        <div class="col-md-9">
          
        </div>
        <div class="col-md-3">
            <div class="pull-right">
                <a class="btn btn-secondary" style="float: right" href=""> Print All QRCodes</a>
            </div>
        </div>
      </div>




  @if ($message = Session::get('success'))
  <div class="alert alert-success">
    <p>{{ $message }}</p>
  </div>
  @endif

@yield('datatable', View::make('datatables.qrcode-datatable'))

@stop



@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('plugins.Datatables', true)
@section('js')
  
@stop 