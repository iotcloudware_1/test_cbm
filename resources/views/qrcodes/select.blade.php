{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1 style="margin-bottom:-10px"><b>Select QR</b></h1>
     <hr class="divider" >
@stop

@section('content')
  <form method="POST" action="/printallqr"> 
        @csrf
    <div class="row">
      <div class="col-md-6">
        <button type="submit" class="btn btn-secondary"><i class="fas fa-pen-nib"></i> &nbsp; Submit</button>
      </div>
      <div class="col-md-6">
          
          <a class="btn btn-secondary" style="float: right;margin-top: 0px; margin-left: 3px; color: white" onclick='selectAll()'> <i class="fas fa-hand-pointer"></i>&nbsp; Select All </a>

          <a class="btn btn-secondary" style="float: right;margin-top: 0px;color: white " onclick='UnSelectAll()'> <i class="far fa-hand-pointer"></i> &nbsp;Unselect All </a>          
      </div>
    </div>  
<br>

  <div class="row">
    <div class="col-md-9">
    </div>
    <div class="col-md-3">

    </div>
  </div>

  @if ($message = Session::get('success'))
  <div class="alert alert-success">
    <p>{{ $message }}</p>
  </div>
  @endif

  @yield('datatable', View::make('datatables.select-instrument-datatable'))
</form>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script type="text/javascript">
      function selectAll(){

        var items=document.getElementsByName('instruments[]');
        for(var i=0; i<items.length; i++){
          if(items[i].type=='checkbox')
            items[i].checked=true;
          
        }
      }
      
      function UnSelectAll(){
        var items=document.getElementsByName('instruments[]');
        for(var i=0; i<items.length; i++){
          if(items[i].type=='checkbox')
            items[i].checked=false;
        }
      }     
    </script>
@stop 