@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
        <h1 style="margin-bottom:-10px"><b>Permission</b></h1>
        <hr class="divider" >
        <div class="pull-right">
           <a class="btn btn-secondary" style="float: right;margin-top: 10px;" href="{{ route('permissions.create') }}"><i class="fas fa-plus"></i> &nbsp; Create </a>
        </div>
    
@stop

@section('content')
<div class="centered-container">

@yield('datatable', View::make('datatables.permission'))

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
</div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('plugins.Datatables', true)
@section('js')
  
@stop