{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1 style="margin-bottom:-10px"><b>Create Permission</b></h1>
     <hr class="divider" >
@stop

@section('content')
<div class="centered-container">
    @if (count($errors) > 0)
      <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
           @foreach ($errors->all() as $error)
             <li>{{ $error }}</li>
           @endforeach
        </ul>
      </div>
    @endif
{!! Form::open(array('route' => 'permissions.store','method'=>'POST')) !!}
@csrf
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-inline ">
            <strong>Permission Name:</strong> &nbsp;
            {!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}
        </div>
    </div>
     <div class="col-xs-12 col-sm-12 col-md-12 text-left">
        <center>
           <button type="submit" class="btn btn-secondary"><i class="fas fa-eye"></i> &nbsp; Submit</button>
        </center>
    </div>
    

</div>
    {!! Form::close() !!}
</div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop