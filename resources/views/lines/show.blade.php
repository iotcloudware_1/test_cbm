{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1 style="margin-bottom:-10px"><b>Show Lines</b></h1>
     <hr class="divider" >
@stop

@section('content')
  

<div class="centered-container">
<div class="row">
    <div class="container-fluid">
        <table class="table table-bordered table-striped">
            <tr>
                <th>Department Name</th>
                <td>{{ $line->department_id }}</td>
            </tr>
             <tr>
                <th>Name</th>
                <td>{{ $line->name }}</td>
            </tr>

            <tr>
                <th>Description</th>
                <td>{{ $line->description }}</td>        
            </tr>
            <tr>
                <th>Created at</th>
                <td>{{ $line->created_at }}</td>
            </tr>
        </table>
    </div>
</div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop