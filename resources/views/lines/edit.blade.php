{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1 style="margin-bottom:-10px"><b>Edit Line</b></h1>
    <hr class="divider" >
@stop
  
@section('content')
<div class="centered-container">
@if (count($errors) > 0)
  <div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
       @foreach ($errors->all() as $error)
         <li>{{ $error }}</li>
       @endforeach
    </ul>
  </div>
@endif


{!! Form::model($line, ['method' => 'PATCH','route' => ['lines.update', $line->id]]) !!}
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <strong><i class="fas fa-industry" style="color: #6c757d"></i>&nbsp Department Name:</strong>
            {!! Form::select('department_id', $department, null, ['class'=>'form-control']) !!}
        </div>   
        <div class="form-group">
            <strong><i class="fas fa-dungeon" style="color: #6c757d"></i>&nbsp Line Name:</strong>
            {!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <strong><i class="fas fa-info-circle" style="color: #6c757d"></i>&nbsp Description:</strong>
            {!! Form::text('description', null, array('placeholder' => 'Description','class' => 'form-control','maxlength'=>50)) !!}
        </div>
    </div>
   
     <!-- <div class="col-xs-12 col-sm-12 col-md-12 text-center">
        <a class="btn btn-primary" href="{{ route('departments.index') }}" style=""> Back</a>
    </div> -->
</div><br>
<div class="row">
     <div class="col-xs-12 col-sm-12 col-md-12 text-center">
          <button type="submit" class="btn btn-secondary"><i class="fas fa-pen-nib"></i> &nbsp; Submit</button>
    </div>  
</div>
  {!! Form::close() !!}
</div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop