{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')

 <h1 style="margin-bottom:-10px"><b>Parameter Values</b></h1>
  <hr class="divider" >
@stop
 
@section('content')
<div class="centered-container">
  <div class="row">
    <div class="col-md-9">
      
    </div>
    <div class="col-md-3">
        
    </div>
  </div>

  @if ($message = Session::get('success'))
  <div class="alert alert-success">
    <p>{{ $message }}</p>
  </div>
  @endif
  @yield('datatable', View::make('datatables.parameter-values-datatable'))
</div>
@stop



@section('css')
<link rel="stylesheet" href="{{ asset('vendor/adminlte/dist/css/adminlte.min.css') }}">
@stop

@section('plugins.Datatables', true)
@section('js')
  
@stop