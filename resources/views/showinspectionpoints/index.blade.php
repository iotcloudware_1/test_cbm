@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1 style="margin-bottom:-10px"><b>Show Inspection Points</b></h1>
      <div class="pull-right">
        <a class="btn btn-secondary" style="float: right;margin-bottom: 13px;margin-top: -35px;" href="{{ route('showinspectionpoints.align',$route_id) }}"><i class="fas fa-plus"></i> &nbsp; Align </a>
      </div>

     <hr class="divider" >
@stop

@section('content')
    
<div class="row">
  <div class="col-md-12">
    <table id="dtBasicExample" class="table table-striped table-bordered table-xl" cellspacing="0">   
      <thead>
        <tr>
            <th>Route Description</th>
            <th>Instrument Name</th>
            <th>Instrument Description</th>
            <th>Order No</th>
            <th>Wrench Time</th>
            <th>TimeStamp</th>
            <th>Action</th>
        </tr>
      </thead>  
      <tbody>
        @foreach($show_inspection_point as $show_insp)
        <tr>
           <td>{{$show_insp['route_id']}}</td>
           <td>{{$show_insp['instrument_id']}}</td>
           <td>{{$show_insp['instrument_des']}}</td>
           <td>{{$show_insp['order_no']}}</td>
           <td>{{$show_insp['wrench_time']}}</td>
           <td>{{$show_insp['created_at']}}</td>
           <td>
            <a href="{{route('showinspectionperemeter',$show_insp['id'])}}"> <i class="fas fa-eye" style="color: blue"> </i> </a> &nbsp;
            <a href="{{route('addparamtoinspectionpoint',$show_insp['id'])}}"> <i class="fas fa-plus" style="color: blue"> </i></a> </td>
        </tr>
        @endforeach
      </tbody>  
    </table>
  </div>
</div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <style type="text/css">
        tr th
        {
            text-align: center;
        }
    </style>
@stop

@section('js')
<script type="text/javascript" src="{{ asset('vendor/datatables/js/dataTables.bootstrap4.min.js') }}"></script>
 <script type="text/javascript">
   $(document).ready(function () {
  $('#dtBasicExample').DataTable( {
        "order": [[ 2, "asc" ]]
    } );
  $('.dataTables_length').addClass('bs-select');
});

</script>

@stop
