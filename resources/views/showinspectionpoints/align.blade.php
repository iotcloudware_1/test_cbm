@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')

@stop

@section('content')
<h1><b>Re-Order {{App\Helper\GeneralHelperFunctions\Helper::routeIdToDes($route_id)}} inspection points</b></h1>
<hr class="divider">
 
 <align-inspection :route_id="{{$route_id}}"> </align-inspection>
@stop
@section('css')
<style>
	 #sortable-5, #sortable-6,#sortable-7 { 
	    list-style-type: none; margin: 0; padding: 0;
	    width: 20%;float:left }
	 #sortable-5 li, #sortable-6 li,#sortable-7 li { 
	    margin: 0 3px 3px 3px; padding: 0.4em; 
	    padding-left: 1.5em; font-size: 17px; height: 16px; }
	 .default {
	    background: #cedc98;
	    border: 1px solid #DDDDDD;
	    color: #333333;
	 }
</style>

@stop

@section('js')
<link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css"
 rel = "stylesheet">
<script src = "https://code.jquery.com/jquery-1.10.2.js"></script>
<script src = "https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script>
 $(function() {
    $( "#sortable-5, #sortable-6" ).sortable({
       connectWith: "#sortable-5, #sortable-6"
    });
    $( "#sortable-7").sortable({
       connectWith: "#sortable-5",
       dropOnEmpty: false
    });
 });
</script>
@stop
