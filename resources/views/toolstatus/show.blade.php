{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1 style="margin-bottom:-10px"><b>Show Tool Status</b></h1>
     <hr class="divider" >
@stop

@section('content')
  


<div class="row">
    <div class="container-fluid">
        <table class="table table-bordered table-striped">
            <tr>
                <th>Tool Name</th>
                <td>{{ $toolstatus->tool_id }}</td>
            </tr>
             <tr>
                <th>Status Description</th>
                <td>{{ $toolstatus->status_id }}</td>
            </tr>
        </table>
    </div>
<!-- <div class="col-xs-12 col-sm-12 col-md-12 text-center">
        <a class="btn btn-primary" href="{{ route('departments.index') }}"> Back</a>
    </div>
</div> -->
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop