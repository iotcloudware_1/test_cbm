@extends('adminlte::page')

@section('title', 'Permission')

@section('content_header')
    <h1></h1>
@stop

@section('content')
    <h1 style="text-align: center;">You do not have permission for this operation. Please contact administrator</h1>
    <button class="btn btn-warning"></button>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
   
@stop