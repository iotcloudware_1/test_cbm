@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
@stop
 
@section('content')
<div class="centered-container">
	<manual-entry-form>
	</manual-entry-form>
</div>
@stop



@section('css')
    <link rel="stylesheet" href="/css/app.css">
@stop

@section('plugins.Datatables', true)
@section('js')
  <script>
// rename myToken as you like
window.myToken =  <?php echo json_encode([
    'csrfToken' => csrf_token(),
]); ?>
</script>
@stop