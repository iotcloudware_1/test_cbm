@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')                                                                                                
 <div class="row">
  <div class="col-xs-6 col-sm-6 col-md-9">
    <ul class="breadcrumb">
      <li class="breadcrumb-item active">
        <a  href="{{route('alldepartments')}}" class="active">
          <img src="/images/depart.png" alt="Smiley face" height="27" width="27"> 
          <span class="text" style="color:white">{{$department_name}}</span>
        </a>
      </li>      
      <li>
        <a href="/unitdashboard">
          <img src="/images/unit.png" alt="Smiley face" height="27" width="27"> 
          <span class="text" style="color:black">{{$unit_name}}</span>
        </a>
      </li>            
      <li>
        <a href="/dashboardview">
          <i class="fas fa-warehouse" style="color:#272d47" aria-hidden="true"></i>
        </a>
      </li>
    </ul>      
  </div>
  <div class="col-md-4">
    <h1 style="margin-bottom:-10px"><b>Health Card</b></h1>
  </div>  
</div>                              
                                                                                      
<hr class="divider"> 
@stop

@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
      <table  id="dtHorizontalVerticalExample" class="table table-striped" cellspacing="0" width="100%">
          <thead>
                <tr>
                  <th>Line Name</th>
                  <th>Asset Name</th>
                  <th>Instrument Name</th>     
                  <th>Parameter Name</th>
                  <th>Value</th>
                  <th>Range</th>
                  <th>Alarm State</th>
                  <th>Timestamp</th>
                </tr>  
          </thead>
          <tbody>
            @foreach($detail_data as $detail)              
            <tr>
              <td>{{$detail['line_name']}}</td>
              <td>{{$detail['asset_name']}}</td>
              <td>{{$detail['instrument_name']}}</td>
              <td>{{$detail['name']}}</td>
          @if($detail['value']=='')
          <td></td>
          @else              
           @if($detail['type']=='Bool')
              @if($detail['condition']=='equal' &&  $detail['bool'] == $detail['setpoint'] )
              <td style="background-image: linear-gradient(to top, #ff0844 0%, #ffb199 100%);"><h6 style="color: white">{{$detail['value']}}</h6></td>  
              @else
              <td style="background-color: #1dab72;"><h6 style="color: white">{{$detail['value']}}</h6></td>
              @endif
            @else  
              @if($detail['condition']=='greater' &&  $detail['value'] > $detail['setpoint'] )
              <td style="background-image: linear-gradient(to top, #ff0844 0%, #ffb199 100%);"><h6 style="color: white">{{$detail['value']}}</h6></td>
              @elseif($detail['condition']=='less' &&  $detail['value'] < $detail['setpoint'] )
              <td style="background-image: linear-gradient(to top, #ff0844 0%, #ffb199 100%);"><h6 style="color: white">{{$detail['value']}}</h6></td>            
              @else
              <td style="background-color: #1dab72"><h6 style="color: white">{{$detail['value']}}</h6></td>                
              @endif
            @endif
          @endif
            @if($detail['min'] == 0 && $detail['max'] == 1)
              <td>False/True</td>
            @else  
              <td>{{$detail['min']."-".$detail['max']}}</td>
            @endif  
            @if($detail['value']=='' && $detail['state']=='alarm' || $detail['value']=='' && $detail['state']=='normal')
             <td><h4><i style="color:grey;height:" class="fas fa-info-circle"></i></h4></td>
            @else            
              @if($detail['state']=='alarm')
              <td><a href="{{ route('showanalytics',$detail['id']) }}"><h4><i style="color:#ff144a;height:" class="fas fa-info-circle"></i></h4></a></td>
              @else
              <td><a href="{{ route('showanalytics',$detail['id']) }}"><h4><i style="color:#1dab72;height:" class="fas fa-info-circle"></i></h4></a></td>
              @endif
            @endif 
              <td>{{$detail['time']}}</td>           
            @endforeach
          </tbody>
      </table>
    </div> 
  </div>
</div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
<style type="text/css">
* {
  -moz-box-sizing: border-box;
  -webkit-box-sizing: border-box;
  box-sizing: border-box;
  margin: 0;
  padding: 0;
}

ul.breadcrumb {
  margin-left: 50px;
  display: inline-block;
  list-style: none;
}
ul.breadcrumb li {
  float: right;
  padding: 5px;
  background-color: #272d47;
  -moz-border-radius: 50px;
  -webkit-border-radius: 50px;
  border-radius: 50px;
  position: relative;
  margin-left: -50px;
  -moz-transition: all 0.2s;
  -o-transition: all 0.2s;
  -webkit-transition: all 0.2s;
  transition: all 0.2s;
  margin-top: 3px;
}
ul.breadcrumb li a {
  overflow: hidden;
  -moz-border-radius: 50px;
  -webkit-border-radius: 50px;
  border-radius: 50px;
  -moz-transition: all 0.2s;
  -o-transition: all 0.2s;
  -webkit-transition: all 0.2s;
  transition: all 0.2s;
  text-decoration: none;
  height: 50px;
  color: #509378;
  background-color: white;
  text-align: center;
  min-width: 50px;
  display: block;
  line-height: 50px;
  padding-left: 52px;
  padding-right: 33.33333px;
  width: 50px;
}
ul.breadcrumb li a .icon {
  display: inline-block;
}
ul.breadcrumb li a .text {
  display: none;
  opacity: 0;
}
ul.breadcrumb li a:hover {
  width: 280px;
  background-color: #aabee0;
}
ul.breadcrumb li a:hover .text {
  display: inline-block;
  opacity: 1;
}
ul.breadcrumb li:last-child a {
  padding: 0;
}
ul.breadcrumb li:last-child:hover {
  padding: 3px;
  margin-top: 0;
}
ul.breadcrumb li:last-child:hover a {
  width: 60px;
  height: 60px;
  line-height: 60px;
}

ul.breadcrumb .active{
  background-color:#262c45;
}

ul.breadcrumb li .active:hover
{ 
  background-color: #008000;
}  
.dtHorizontalVerticalExampleWrapper {
max-width: 600px;
margin: 0 auto;
}
#dtHorizontalVerticalExample th, td {
white-space: nowrap;
}
table.dataTable thead .sorting:after,
table.dataTable thead .sorting:before,
table.dataTable thead .sorting_asc:after,
table.dataTable thead .sorting_asc:before,
table.dataTable thead .sorting_asc_disabled:after,
table.dataTable thead .sorting_asc_disabled:before,
table.dataTable thead .sorting_desc:after,
table.dataTable thead .sorting_desc:before,
table.dataTable thead .sorting_desc_disabled:after,
table.dataTable thead .sorting_desc_disabled:before {
bottom: .5em;
}
</style>
@stop

@section('js')
 <script  type="text/javascript" src="{{ asset('bower_components/chart.js/dist/Chart.min.js') }}"></script> 
 <script type="text/javascript" src="{{ asset('vendor/datatables/js/dataTables.bootstrap4.min.js') }}"></script>  
<script>
$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();   
});

$(document).ready(function () {
$('#dtHorizontalVerticalExample').DataTable({

"scrollX": 450,
"scrollY": 450,
});
$('.dataTables_length').addClass('bs-select');
});

</script>

@stop



