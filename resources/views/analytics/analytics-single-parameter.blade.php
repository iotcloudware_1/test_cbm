{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
  <h1>Instrument and Parameters</h1>
  <hr class="divider" >
@stop

@section('content')
<div class="centered-container">
  <div class="row">
    <single-analytics-component> </single-analytics-component>   
  </div>
</div>    

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
@stop

@section('js')

   
    <script src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
    <script>
        $(document).ready(function() {
            $('.add-to-collection').on('click', function(e) {
                e.preventDefault();
                var container = $('.collection-container');

                var count = container.children().length;
                var proto = container.data('prototype').replace(/__NAME__/g, count);
                container.append(proto);
                console.log(proto);
            });
        });
    </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function(){

        $(document).on('change','.departmentname',function(){
           //console.log("its change");
            var inst_id=$(this).val();
            //console.log(inst_id);
            var div=$(this).parent();
            var op=" ";
            $.ajax({
                type:'get',
                url:'{!!URL::to('findparameter')!!}',
                data:{'id':inst_id} ,
                success:function(data){

                    op+='<option value="0" disabled="true">select Instrument</option>';
                    for(var i=0; i<data.length; i++){
                        op+='<option value="'+data[i].id+'">'
                        +data[i].name+'</option>' 
                    }
                     // console.log('success');
                     
                       console.log(data);

                    div.find('.instrumentname').html(" ");
                    div.find('.instrumentname').append(op);
                    // div.find('.parametername').html(" ");
                    // div.find('.parametername').append(op);
                },
                error:function(){

                }
            });

              });

    $(document).on('change','.instrumentname',function(){
        
        var inst1_id=$(this).val();
       //console.log(inst1_id);
        var div=$(this).parent();
        var op1=" ";
        $.ajax({
                type:'get',
                url:'{!!URL::to('intrumentParameter')!!}',
                data:{'id':inst1_id} ,

                success:function(data){
//onsole.log(data);
            op1+='<option value="0" disabled="true">select parameter</option>';
            for(var p=0; p<data.length; p++){
                        op1+='<option value="'+data[p].id+'">'
                        +data[p].name+'</option>' 
                    }  
                      console.log(data);   
                      div.find('.parametername').html(" ");
                    div.find('.parametername').append(op1);
                },
                error:function(){

                }
            });


    });


      });
    </script>

    <!-- date range filter -->
    <script>
$(document).ready(function(){
 $('.input-daterange').datepicker({
  todayBtn:'linked',
  format:'yyyy-mm-dd',
  autoclose:true
 });

 

 $('#filter').click(function(){
  var from_date = $('#from_date').val();
  var to_date = $('#to_date').val();
  if(from_date != '' &&  to_date != '')
  {
   $('#order_table').DataTable().destroy();
   load_data(from_date, to_date);
  }
  else
  {
   alert('Both Date is required');
  }
 });

 $('#refresh').click(function(){
  $('#from_date').val('');
  $('#to_date').val('');
  $('#order_table').DataTable().destroy();
  load_data();
 });

});
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js"></script>




    
@stop