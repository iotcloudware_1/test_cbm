@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
<head>


</head>
@stop
 
@section('content')
<div class="centered-container">
		<multiple-analytics-component> </multiple-analytics-component>
	</div>
@stop



@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
   
@stop

@section('plugins.Datatables', true)
@section('js')
  <script>
  	 
// rename myToken as you like
window.myToken =  <?php echo json_encode([
    'csrfToken' => csrf_token(),
]); ?>
</script>
@stop