@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')

<!------ Include the above in your HEAD tag ---------->
  <div class="row"> 
    <div class="col-xs-12 col-sm-12 col-md-12">
      <ul class="breadcrumb">
        <li class="breadcrumb-item active">
          <a class="active">
           <i class="fas fa-sliders " aria-hidden="true" style="color: white"></i>
            <span class="text" style="color: white">{{$data['param']}} Analytics</span>
          </a>
        </li>
        <li>
          <a href="{{route('allparameters')}}">
          <img src="/images/parameter.png" alt="Smiley face" height="27" width="27"> 
            <span class="text" style="color: black">Parameters</span>
          </a>
        </li>
        <li>
          <a href="{{ route('get_asset_instrument',$asset_instrument) }}">
             <img src="/images/instrument.png" alt="Smiley face" height="27" width="27"> 
            <span class="text" style="color: black" >{{$data['instrument']}}</span>
          </a>
        </li>
        <li>
          <a href="{{ route('get_line_asset',$line_asset) }}">
            <img src="/images/assets.png" alt="Smiley face" height="27" width="27">
            <span class="text" style="color: black">{{$data['asset']}}</span>
          </a>
        </li>
        <li>
          <a href="{{ route('get_department_line',$department_line) }}">
            <img src="/images/line.png" alt="Smiley face" height="27" width="27">
            <span class="text" style="color: black">{{$data['line']}}</span>
          </a>
        </li>
        <li>
          <a href="{{ route('get_unit_department',$unit_department) }}">
             <img src="/images/department.png" alt="Smiley face" height="27" width="27"> 
            <span class="text" style="color: black">{{$data['department']}}</span>
          </a>
        </li>
        <li>
          <a href="/unitdashboard">
            <img src="/images/unit.png" alt="Smiley face" height="27" width="27"> 
            <span class="text" style="color: black">{{$data['unit']}}</span>
          </a>
        </li>            
        <li>
          <a href="/dashboardview">
            <i class="fas fa-warehouse" style="color:#272d47" aria-hidden="true"></i>
          </a>
        </li>
      </ul>      
    </div>
  </div>
@stop

@section('content')

<div class="container-fluid"> 
  <h1><b>Parameter Analytics</b></h1>
  <hr class="divider" style="margin-top: 0rem">  
  <br />
  <h3>Alarms</h3>
    <div class="row">
      <div class="col-md-12">
          <table  id="dtBasicExample" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
              <thead>
                    <tr>     
                      <th>Alarm Message Name</th>
                      <th>Set point</th>
                      <th>Condition</th>
                      <th>Priority</th>
                      <th>Timestamp</th>
                  </tr>  
              </thead>
              <tbody>
                @foreach($alarms_detail as $alarm)
                  <tr>
                    <td>{{$alarm['alarm_message']}}</td>
                    <td>{{$alarm['setpoint']}}</td>
                    <td>{{$alarm['condition']}}</td>
                    <td>{{$alarm['priority']}}</td>
                    <td>{{$alarm['created_at']}}</td>
                  </tr>
                 @endforeach 
              </tbody>
          </table>        
      </div>
    </div>
    <br />
    <br />

    <div class="row">
    <div class="col-md-6">
      <div style="border: 2px solid #272d47; box-shadow: 7px 9px 5px #272d47; border-radius: 8px; height: 500px">
        @if(empty($alarm_linechart))
         <div>
           <h2 style="text-align: center; margin:25%">No Data</h2>
         </div> 
        @else
        {!! $alarm_linechart->container() !!}
        @endif
      </div>
    </div>
    <div class="col-md-6">
          <table  id="dtVerticalScrollExample" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
              <thead>
                    <tr>     
                     <th>Value</th>                      
                      <th>State</th>
                      <th>Timestamp</th>
                  </tr>  
              </thead>
              <tbody>
                @foreach($alarms_values as $alarm)
                @foreach($alarm as $a)                
                <tr>
                  <td>{{$a['new_value']}}</td>
                  <td>{{$a['state_ack']}}</td>
                  <td>{{$a['created_at']}}</td>
                </tr>
                @endforeach
                @endforeach
              </tbody>
          </table>
    </div>
  </div>
  <br />
  <h3>Parameter</h3>  
  <div class="row">
    <div class="col-md-12">
      <detailed-parmeter-analytics-component :id="{{$data['id']}}"> </detailed-parmeter-analytics-component>
    </div>
  </div>  
  <br />
    <div class="row">          
      <div class="col-md-3 sm-box">
        <a data-toggle="tooltip" title="# of open workorders"> 
        <div class="box hvr-grow hvr-shutter-out-vertical">
          @if($data_type=='Bool')
          <h3># Of Yes</h3>
          <h2 style="font-size: 4.5vw">{{$yes_bool}}</h2>            
          @else
          <h3>Maximum</h3>
          <h2 style="font-size: 4.5vw">{{$data['max']}}</h2>
          @endif
        </div>
        </a>  
      </div> 
      <div class="col-md-3 sm-box">
        <a data-toggle="tooltip" title="# of closed workorders"> 
        <div class="box hvr-grow hvr-shutter-out-vertical">
          @if($data_type=='Bool')
          <h3># Of No</h3>
          <h2 style="font-size: 4.5vw">{{$no_bool}}</h2>            
          @else
          <h3>Minimum</h3>
          <h2 style="font-size: 4.5vw">{{$data['min']}}</h2>
          @endif
        </div>
        </a>  
      </div> 
      <div class="col-md-3 sm-box">
        <a data-toggle="tooltip" title="# of open and acknowledged workorders"> 
        <div class="box hvr-grow hvr-shutter-out-vertical">
          <h3>Average</h3>
          <h2 style="font-size: 4.5vw">{{$data['avg']}}</h2>
        </div>
        </a>  
      </div> 
      <div class="col-md-3 sm-box">
        <a data-toggle="tooltip" title="# of open and unacknowledged workorders"> 
        <div class="box hvr-grow hvr-shutter-out-vertical">
          <h3>Total</h3>
          <h2 style="font-size: 4.5vw">{{$data['total']}}</h2>
        </div>
        </a>  
      </div>                                           
    </div>
      </div>    
  </div>       

@stop

@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
<link rel="stylesheet" href="bower_components/chart.js/dist/Chart.min.css">
<style type="text/css">
@media screen and (min-width: 25em){
    h3{ font-size: calc( 13px + (22 - 16) * (99vw - 400px) / (800 - 401) ) }
}

@media screen and (min-width: 50em){

    h3 { font-size: calc( 13px + (22 - 16) * (99vw - 400px) / (800 - 401) ) }
} 

table.dataTable thead .sorting:after,
table.dataTable thead .sorting:before,
table.dataTable thead .sorting_asc:after,
table.dataTable thead .sorting_asc:before,
table.dataTable thead .sorting_asc_disabled:after,
table.dataTable thead .sorting_asc_disabled:before,
table.dataTable thead .sorting_desc:after,
table.dataTable thead .sorting_desc:before,
table.dataTable thead .sorting_desc_disabled:after,
table.dataTable thead .sorting_desc_disabled:before {Show
bottom: .5em;
}

 *{
  -moz-box-sizing: border-box;
  -webkit-box-sizing: border-box;
  box-sizing: border-box;
  margin: 0;
  padding: 0;
}


ul.breadcrumb {
  margin-left: 50px;
  display: inline-block;
  list-style: none;
}
ul.breadcrumb li {
  float: right;
  padding: 5px;
  background-color: #272d47 ; /* for outlinig color #59A386 :  */
  -moz-border-radius: 50px;
  -webkit-border-radius: 50px;
  border-radius: 50px;
  position: relative;
  margin-left: -50px;
  -moz-transition: all 0.2s;
  -o-transition: all 0.2s;
  -webkit-transition: all 0.2s;
  transition: all 0.2s;
  margin-top: 3px;
}
ul.breadcrumb li a {
  overflow: hidden;
  -moz-border-radius: 50px;
  -webkit-border-radius: 50px;
  border-radius: 50px;
  -moz-transition: all 0.2s;
  -o-transition: all 0.2s;
  -webkit-transition: all 0.2s;
  transition: all 0.2s;
  text-decoration: none;
  height: 50px;
  color: #509378;
  background-color: white ;  /*inside color of breadcrum 8cc5db====#aabee0 */ 
  text-align: center;
  min-width: 50px;
  display: block;
  line-height: 50px;
  padding-left: 52px;
  padding-right: 33.33333px;
  width: 50px;
}
ul.breadcrumb li a .icon {
  display: inline-block;
}
ul.breadcrumb li a .text {
  display: none;
  opacity: 0;
}
ul.breadcrumb li a:hover {
  width: 350px;
  background-color: #aabee0;
}
ul.breadcrumb li a:hover .text {
  display: inline-block;
  opacity: 1;
}
ul.breadcrumb li:last-child a {
  padding: 0;
}
ul.breadcrumb li:last-child:hover {
  padding: 3px;
  margin-top: 0;
}
ul.breadcrumb li:last-child:hover a {
  width: 60px;
  height: 60px;
  line-height: 60px;
}

ul.breadcrumb .active{
  background-color:#262c45;
}

ul.breadcrumb li .active:hover
{ 
  background-color: #008000;
}

</style>
@stop

@section('js')


@if(empty($alarm_linechart))

@else
{!! $alarm_linechart->script() !!}
@endif
 <script  type="text/javascript" src="{{ asset('bower_components/chart.js/dist/Chart.min.js') }}"></script> 
 <script type="text/javascript" src="{{ asset('vendor/datatables/js/dataTables.bootstrap4.min.js') }}"></script>  

<script>

$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();   
});

$(document).ready(function () {
  $('#dtBasicExample').DataTable();
  $('.dataTables_length').addClass('bs-select');
});
$(document).ready(function () {
$('#dtVerticalScrollExample').DataTable({
"scrollY": "200px",
"scrollCollapse": true,
});
$('.dataTables_length').addClass('bs-select');
});

</script>




@stop 

