@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
  <div class="row">
    <div class="col-md-6">
      <h1 style="margin-bottom:0px;"><b>Alarms Management</b></h1>    
    </div>   
    <div class="col-md-6">
      <div style="float:right;">
       <a href="{{ route('alarms.create') }}"><button class="btn btn-secondary" style="height:45px; float: right;background-color: #1a1c33;margin-top:0px; margin-right: 3px"> <i class="fas fa-plus"></i>&nbsp; Create</button></a> 
      </div> 
      <div  style="margin-right: 5px;float: right;">   
        <label class="dropdown">
          <div class="dd-button">
            Manage
          </div>
          <input type="checkbox" class="dd-input" id="test">
            <ul class="dd-menu">
             <a class="btn btn-success" style="width: 100%; padding: 5%; background-color: #1a1c33;"href="/alarmdashboard"><li style="color: white"><i class="fas fa-chalkboard-teacher"></i> Dashboard </li></a> 
             <li class="divide"></li>

             <a class="btn btn-success" style="width: 100%; padding: 5%; background-color: #1a1c33;" href="{{ route('alarms.currentAlarmState') }}"><li style="color: white"><i class="fas fa-wallet"></i> &nbsp; Current </li></a>                  
             <li class="divide"></li>             

             <a class="btn btn-success" style="width: 100%; padding: 6%; background-color:#1a1c33;"href="{{ route('alarms.alarmHistory') }}"><li style="color: white"><i class="fas fa-history"></i> History </li></a> 
             <li class="divide"></li>


          </ul>
        </label>
      </div>       
    </div>
  </div>
    <hr class="divider">    
@stop
 
@section('content')
<div class="centered-container">
  <div class="row">
    <div class="col-md-9">
    </div>
    <div class="col-md-3">

    </div>
  </div>

  @if ($message = Session::get('success'))
  <div class="alert alert-success">
    <p>{{ $message }}</p>
  </div>
  @endif

  @yield('datatable', View::make('datatables.alarm-datatable'))
</div>
@stop
@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
<link rel="stylesheet" href="bower_components/chart.js/dist/Chart.min.css">

@stop


@section('plugins.Datatables', true)
<style type="text/css">
a:hover {
  color: red;
}

/* Dropdown */

.dropdown {
  display: inline-block;
  position: relative;
  z-index: 1;
}

.dd-button {
  display: inline-block;
  border: 1px solid gray;
  border-radius: 4px;
  padding: 10px 30px 10px 20px;
  background-color: #1a1c33;
  cursor: pointer;
  white-space: nowrap;
  color: white;
}

.dd-button:after {
  content: '';
  position: absolute;
  top: 50%;
  right: 15px;
  transform: translateY(-50%);
  width: 0; 
  height: 0; 
  border-left: 5px solid transparent;
  border-right: 5px solid transparent;
  border-top: 5px solid white;
  z-index: 1;
}

.dd-button:hover {
  background-color: #6d728a;
}


.dd-input {
  display: none;
}

.dd-menu {
  position: absolute;
  top: 100%;
  border: 1px solid #ccc;
  border-radius: 4px;
  padding: 0;
  margin: 2px 0 0 0;
  box-shadow: 0 0 6px 0 rgba(0,0,0,0.1);
  background-color: pink;
  list-style-type: none;
  color: white;
  z-index: 1;
}

.dd-input + .dd-menu {
  display: none;
  z-index: 1;
} 

.dd-input:checked + .dd-menu {
  display: block;
} 

.dd-menu li {
  padding: 10px 20px;
  cursor: pointer;
  white-space: nowrap;
  z-index: 1;
}

.dd-menu li:hover {
  background-color: #007bff;
}

.dd-menu li a {
  display: block;
  margin: -10px -20px;
  padding: 10px 20px;
  z-index: 1;
}

.dd-menu li.divide{
  padding: 0;
   
</style>
@section('js')
  
@stop