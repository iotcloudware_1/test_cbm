  {{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')
 
@section('title', 'Dashboard')
@section('content_header')
     <h1> Show Alaram</h1> 
     <hr class="divider" >
@stop 
 
@section('content')
<div class="centered-container">
    <div class="row">
        <div class="col-md-6 no-print">
            <div class="container-fluid">
                <table  class="table table-bordered table-striped">
                   <tr >
                        <th>ID</th>
                        <td>{{ $alarm->id }}</td>
                    </tr>
                    <tr>
                        <th>Parameter Name</th>
                        <td>{{ $alarm->parameter_id }}</td>
                    </tr>
                   
                     <tr>
                        <th>Alarm Message</th>
                        <td>{{ $alarm->alarm_message }}</td>
                    </tr>

                    <tr>
                        <th>Condition</th>
                        <td>{{ $alarm->condition }}</td>        
                    </tr>
                     <tr>
                        <th>Set Point</th>
                        <td>{{ $alarm->setpoint}}</td>        
                    </tr>
                      <tr>
                        <th>Priority</th>
                        <td>{{ $alarm->priority}}</td>        
                    </tr>
                    <tr>
                        <th>Created at</th>
                        <td>{{ $alarm->created_at }}</td>
                    </tr>
                </table>
            </div> 
        </div>
        
    </div>
</div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
     
@stop

@section('js')
     
@stop 