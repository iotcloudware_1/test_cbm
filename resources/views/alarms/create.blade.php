{{-- resources/views/admin/dashboard.blade.php --}}
 
@extends('adminlte::page')

@section('title', 'Dashboard') 

@section('content_header')
     <h1 style="margin-bottom:-10px"><b>Create Alarm</b></h1>
     <hr class="divider" >
@stop

@section('content')
<div class="centered-container">
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
        </ul>
    </div>
@endif

{!! Form::open(array('route' => 'alarms.store','method'=>'POST')) !!}
    @csrf
<div class="row">
    <config-alarm> </config-alarm> 
    <div class="col-md-6">  
        <div class="form-group">
            <strong><i class="fas fa-dungeon" style="color: #6c757d"></i>&nbsp Alarm Message:</strong>
            {!! Form::text('alarm_message', null, array('placeholder' => 'Alaram Message','class' => 'form-control')) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <strong><i class="fas fa-info-circle" style="color: #6c757d"></i>&nbsp Priority:</strong>
            {!! Form::select('priority', ['high'=>'high','medium'=>'medium','low'=>'low'], null, ['class' => 'form-control']) !!}
        </div>
    </div>
   
</div>
<br>
<div class="row">
     <div class="col-xs-12 col-sm-12 col-md-12 text-center">
        <button type="submit" class="btn btn-secondary"> <i class="fas fa-pen-nib"></i>&nbsp; Submit</button>
    </div>
    
</div>
{!! Form::close() !!}
</div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop