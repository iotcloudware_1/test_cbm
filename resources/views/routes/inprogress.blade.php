@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1 style="margin-bottom:-10px"><b>Detail Route Inprogress</b></h1>
     <hr class="divider" >
@stop

@section('content')
    
<div class="row">
  <div class="col-md-12">
    <table id="dtBasicExample" class="table table-striped table-bordered table-xl" cellspacing="0">   
      <thead>
        <tr>
            <th>Route Id </th>
            <th>Operator Id</th>
            <th>Route State</th>
            <th>Start Time</th>
            <th>End Time</th>
            <th>Duration Min</th>
            <th>Instrument Skipped</th>
            <th>Sessions</th>
            <th>Timpstamp</th>
        </tr>
      </thead>  
      <tbody>
        @foreach($in_progress_routes as $show_insp)
        <tr>
        	<td>{{$show_insp['route_id']}}</td>
            <td>{{$show_insp['operator_id']}}</td>
            <td>{{$show_insp['route_state']}}</td>
            <td>{{$show_insp['start_time']}}</td>
            <td>{{$show_insp['end_time']}}</td>
            <td>{{$show_insp['duration_min']}}</td>
            <td>{{$show_insp['instrument_skiped']}}</td>
            <td>{{$show_insp['sessions']}}</td>
            <td>{{$show_insp['created_at']}}</td>
        </tr>
        @endforeach
      </tbody>  
    </table>
  </div>
</div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <style type="text/css">
        tr th
        {
            text-align: center;
        }
    </style>
@stop

@section('js')
<script type="text/javascript" src="{{ asset('vendor/datatables/js/dataTables.bootstrap4.min.js') }}"></script>
 <script type="text/javascript">
   $(document).ready(function () {
  $('#dtBasicExample').DataTable( {
        "order": [[ 2, "asc" ]]
    } );
  $('.dataTables_length').addClass('bs-select');
});

 </script>

@stop
