@extends('adminlte::page')
@section('title', 'Dashboard')
@section('content_header')
    <h1 style="margin-bottom:-10px"><b>Create Route</b></h1>
    <hr class="divider" >
@stop
 
@section('content')
  <route-create> </route-create>
@stop



@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('plugins.Datatables', true)
@section('js')
<script>
// rename myToken as you like
window.myToken =  <?php echo json_encode([
    'csrfToken' => csrf_token(),
]); ?>
</script>
@stop