 {{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page') 

@section('title', 'Dashboard')

@section('content_header')
     <h1 style="margin-bottom:-10px"><b>Show Points</b></h1>
     <hr class="divider" >
@stop

@section('content')
    <div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
          
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('routes.index') }}"> Back</a>
        </div>
    </div>
</div>

<div class="row">
    <table class="table table-bordered table-striped">
        <tr>
            <th>route_id</th>
            <th>Instrument ID</th>
            <th>parameter_id</th>
            <th>order_no </th>
            <th>time </th>

        </tr>

    	@foreach($routeInstrument as $v)
            <tr>
                <td>{{$v->route_id }}</td>
            	<td>{{$v->instrument_id }}</td>
                <td>{{$v->parameter_id }}</td>
                <td>{{$v->order_no }}</td>
                <td>{{$v->time }}</td>

            </tr>
        @endforeach
    </table>
</div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')

@stop