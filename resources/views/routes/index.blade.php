{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
 <h1 style="margin-bottom:-10px"><b>Routes</b></h1>
     
  <div class="pull-right">
      <a class="btn btn-secondary" style="float: right;margin-top: -32px;" href="{{ route('routes.create') }}"> <i class="fas fa-plus"></i>&nbsp; Create </a>
    <a class="btn btn-secondary" style="float: right;margin-top: -32px;margin-right:5px" href="{{ route('routehistory.index') }}"> <i class="fas fa-history"></i>&nbsp; History </a>
  </div>
  <hr class="divider" >
@stop

@section('content')
<!-- <div class="centered-container">    
<div style="display: inline-block; width: 100%">
     <h1 style="margin-bottom:-10px"><b>Routes</b></h1>
    <a class="btn btn-secondary" class="btn btn-success" style="float: right; background-color: #1a1c33; margin-bottom: 6px;margin-top: -43px" href="{{ route('routehistory.index') }}"> <i class="fas fa-history"></i> History </a>
</div>
<hr class="divider" style="margin-top: 0px">
  -->

  <div class="row">
    <div class="col-md-9">
      
    </div>
    <div class="col-md-3">

    </div>
  </div>

  @if ($message = Session::get('success'))
  <div class="alert alert-success">
    <p>{{ $message }}</p>
  </div>
  @endif

@yield('datatable', View::make('datatables.route-datatable'))

@stop



@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('plugins.Datatables', true)
@section('js')
  
@stop