@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
     <h1 style="margin-bottom:-10px"><b>Add Parameter</b></h1>
     <hr class="divider" >
@stop


@section('content')
<add-param-inpectionpoint :inspection_point_id="{{$id}}" :route_id="{{$route_id}}"> </add-param-inpectionpoint>
@stop



@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
   
    
@stop