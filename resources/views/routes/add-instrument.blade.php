@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
     <h1 style="margin-bottom:-10px"><b>Create Route</b></h1>
     <hr class="divider" >
@stop


@section('content')
    <button type="button" class="add-to-collection btn btn-success">Add More Instrument</button>
    {!! form_start($form) !!}
    <div class="collection-container" data-prototype="{{ form_row($form->tags->prototype()) }}"> 
        {!! form_row($form->tags) !!}
    </div>
    {!! form_end($form) !!}

@stop



@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
   
    <script src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
    <script>
        $(document).ready(function() {
            $('.add-to-collection').on('click', function(e) {
                e.preventDefault();
                var container = $('.collection-container');

                var count = container.children().length;
                var proto = container.data('prototype').replace(/__NAME__/g, count);
                container.append(proto);
                console.log(proto);
            });
        });
    </script>
    
@stop