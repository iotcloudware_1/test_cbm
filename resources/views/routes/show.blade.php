{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
     <h1 style="margin-bottom:-10px"><b>Show Route</b></h1>
     <hr class="divider" >
@stop

@section('content')
    <div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
          
        </div>
<!--         <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('routes.index') }}"> Back</a>
        </div> -->
    </div>
</div>




<div class="row">
    <table class="table table-bordered table-striped">
        <tr>
            <th>Instrument ID</th>
            <td> @if(!empty($routeInstrument))
                @foreach($routeInstrument as $v)
                    <td>{{$v->name }}</td>
                @endforeach
            @endif  </td>
        </tr>
         <tr>
            <th>Sequence</th>
            <td>{{ $route->sequence }}</td>
        </tr>

        <tr>
            <th>Timing</th>
            <td>{{ $route->timing }}</td>        
        </tr>
        <tr>
            <th>Created at</th>
            <td>{{ $route->created_at }}</td>
        </tr>
    </table>
</div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')

@stop