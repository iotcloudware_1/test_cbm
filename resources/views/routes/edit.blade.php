{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
     <h1 style="margin-bottom:-10px"><b>Edit Route</b></h1>
    <hr class="divider" >
@stop
 
@section('content')
   <div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
       
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('routes.index') }}"> Back</a>
        </div>
    </div>
</div>


@if (count($errors) > 0)
  <div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
       @foreach ($errors->all() as $error)
         <li>{{ $error }}</li>
       @endforeach
    </ul>
  </div>
@endif
 

{!! Form::model($route, ['method' => 'PATCH','route' => ['routes.update', $route->id]]) !!}
<div class="row">
  
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Sequence:</strong>
            {!! Form::text('sequence', null, array('placeholder' => 'Sequence','class' => 'form-control')) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Timing:</strong>
            {!! Form::text('timing', null, array('placeholder' => 'Timing','class' => 'form-control')) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>User ID:</strong>
            {!! Form::text('user_id', null, array('placeholder' => 'user_id','class' => 'form-control')) !!}
        </div>
    </div>
    
    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</div>
{!! Form::close() !!}
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop