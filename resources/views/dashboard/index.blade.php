 @extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header') 
 <h1 style="margin-bottom:-10px"><b>Summary</b></h1>  
  <hr class="divider">
@stop

@section('content')
<div class="container"> 
 <!------------------------WORK ORDER PART START -------------------------------------->
    <a href="#" class="hvr-icon-buzz">
    <h3 style="font-weight:600">Work Orders</h3>
    </a>
    <hr class="divider" style="margin-top: 0rem">  

    <div class="row">
      <div class="col-md-12">
        <div style="border: 2px solid #272d47; box-shadow: 7px 9px 5px #272d47; border-radius: 8px">
          {!! $workorder_barchart->container() !!}
        </div>
      </div>
    </div>
    <br />
    <div class="row">
    </div>    
    <br />
    <div class="row">          
      <div class="col-md-3 sm-box">
        <a href="{{ route('workorders.opendetail') }}" data-toggle="tooltip" title="# of open workorders"> 
          @if($open_workorders > 0)
        <div class="box hvr-grow hvr-shutter-out-vertical"style="background:rgba(0, 0, 0, 0) radial-gradient(at center center, rgba(241, 0, 0, 0.4) 0%, rgba(255, 255, 255, 0.6) 100%) repeat scroll 0% 0%">
          <h3 style="font-weight:600">Open</h3>
          <h2 style="font-size: 4.5vw">{{$open_workorders}}</h2>
        </div>
         @else
         <div class="box hvr-grow hvr-shutter-out-vertical">
          <h3 style="font-weight:600">Open</h3>
          <h2 style="font-size: 4.5vw">{{$open_workorders}}</h2>
        </div>
        @endif  
        </a>  
      </div> 
      <div class="col-md-3 sm-box">
        <a href="{{ route('workorders.closedetail') }}" data-toggle="tooltip" title="# of closed workorders"> 
        <div class="box hvr-grow hvr-shutter-out-vertical">
          <h3 style="font-weight:600">Closed</h3>
          <h2 style="font-size: 4.5vw">{{$close_workorders}}</h2>
        </div>
        </a>  
      </div> 
      <div class="col-md-3 sm-box">
        <a href="{{ route('workorders.openack') }}" data-toggle="tooltip" title="# of open and acknowledged workorders"> 
        <div class="box hvr-grow hvr-shutter-out-vertical">
          <h3 style="font-weight:600" >Ack</h3>
          <h2 style="font-size: 4.5vw">{{$open_acknowledge_workorders}}</h2>
        </div>
        </a>  
      </div> 
      <div class="col-md-3 sm-box">
        <a href="{{ route('workorders.openunack') }}" data-toggle="tooltip" title="# of open and unacknowledged workorders"> 
          @if($open_unacknowledge_workorders > 0)
        <div class="box hvr-grow hvr-shutter-out-vertical"style="background:rgba(0, 0, 0, 0) radial-gradient(at center center, rgba(241, 0, 0, 0.4) 0%, rgba(255, 255, 255, 0.6) 100%) repeat scroll 0% 0%
">
          <h3 style="font-weight:600">Unack</h3>
          <h2 style="font-size: 4.5vw">{{$open_unacknowledge_workorders}}</h2>
        </div>
        @else
        <div class="box hvr-grow hvr-shutter-out-vertical">
          <h3 style="font-weight:600">Unack</h3>
          <h2 style="font-size: 4.5vw">{{$open_unacknowledge_workorders}}</h2>
        </div>
        @endif
        </a>  
      </div>                                           
    </div>  
 <!------------------------WORK ORDER PART END -------------------------------------->

 <!-----------------------ALARM PART START ------------------------------------------>
    <a href="#" class="hvr-icon-buzz">
    <h3 style="font-weight:600">Alarms</h3>
    </a>
    <hr class="divider" style="margin-top: 0rem">  
<!--     <div class="row">
        <div class="col-md-4">
            <h1>asdasd</h1>
        </div>
        <div class="col-md-4">
             <h1>asdasd</h1>
        </div>
        <div class="col-md-4">
             <h1>asdasd</h1>
        </div>
    </div> -->
    <br />
    <div class="row">          
        <div class="col-md-3 sm-box">
            <a href="{{ route('alarms.total') }}" data-toggle="tooltip" title="Total number of observed alarms"> 
            <div class="box hvr-grow hvr-shutter-out-vertical">
              <h3 style="font-weight:600">Total</h3>
              <h2 style="font-size: 4.5vw">{{$total_alarms}}</h2>
            </div>
            </a>  
        </div> 
        <div class="col-md-3 sm-box">
            <a href="{{ route('alarms.current') }}"data-toggle="tooltip" title="# of current unresolved alarms"> 
              @if($current_alarms > 0)
            <div class="box hvr-grow hvr-shutter-out-vertical"style="background:rgba(0, 0, 0, 0) radial-gradient(at center center, rgba(241, 0, 0, 0.4) 0%, rgba(255, 255, 255, 0.6) 100%) repeat scroll 0% 0%">
              <h3 style="font-weight:600">Current</h3>
              <h2 style="font-size: 4.5vw">{{$current_alarms}}</h2>
            </div>
            @else
            <div class="box hvr-grow hvr-shutter-out-vertical">
              <h3 style="font-weight:600">Current</h3>
              <h2 style="font-size: 4.5vw">{{$current_alarms}}</h2>
            </div>
            @endif
            </a>  
        </div> 
        <div class="col-md-3 sm-box">
            <a href="{{ route('alarms.ackalarm') }}" data-toggle="tooltip" title="# of current acknowledged alarms"> 
            <div class="box hvr-grow hvr-shutter-out-vertical">
              <h3 style="font-weight:600">Ack</h3>
              <h2 style="font-size: 4.5vw">{{$acknowledge_alarms}}</h2>
            </div>
            </a>  
        </div> 
        <div class="col-md-3 sm-box">
            <a  href="{{ route('alarms.unackalarm') }}" data-toggle="tooltip" title="# of current unacknowledged alarms"> 
              @if($unacknowledge_alarms > 0)
            <div class="box hvr-grow hvr-shutter-out-vertical" style="background:rgba(0, 0, 0, 0) radial-gradient(at center center, rgba(241, 0, 0, 0.4) 0%, rgba(255, 255, 255, 0.6) 100%) repeat scroll 0% 0%">
              <h3 style="font-weight:600">Unack</h3>
              <h2 style="font-size: 4.5vw">{{$unacknowledge_alarms}}</h2>
            </div>
            @else
            <div class="box hvr-grow hvr-shutter-out-vertical">
              <h3 style="font-weight:600">Unack</h3>
              <h2 style="font-size: 4.5vw">{{$unacknowledge_alarms}}</h2>
            </div>
            @endif
            </a>  
        </div>                                           
    </div>   
 <!-----------------------ALARM PART END -------------------------------------------->


 <!-----------------------ROUTE PART START ------------------------------------------->
    <a href="#" class="hvr-icon-buzz">
    <h3 style="font-weight:600">Routes</h3>
    </a>
    <hr class="divider" style="margin-top: 0rem">  
<!--     <div class="row">
        <div class="col-md-4">
            <h1>asdasd</h1>
        </div>
        <div class="col-md-4">
             <h1>asdasd</h1>
        </div>
        <div class="col-md-4">
             <h1>asdasd</h1>
        </div>
    </div> -->
    <br />
    <div class="row">          
        <div class="col-md-3 sm-box">
            <a  href="{{ route('routes.inprogress') }}" data-toggle="tooltip" title="# of routes currently in progress"> 
            <div class="box hvr-grow hvr-shutter-out-vertical">
              <h3 style="font-weight:600">In Progress</h3>
              <h2 style="font-size: 4.5vw">{{$in_progress_routes}}</h2>
            </div>
            </a>  
        </div> 
        <div class="col-md-3 sm-box">
            <a href="{{ route('routes.complete') }}" data-toggle="tooltip" title="# of completed routes"> 
            <div class="box hvr-grow hvr-shutter-out-vertical">
              <h3 style="font-weight:600">Completed</h3>
              <h2 style="font-size: 4.5vw">{{$finish_routes}}</h2>
            </div>
            </a>  
        </div> 
        <div class="col-md-3 sm-box">
            <a href="{{ route('routes.cancle') }}" data-toggle="tooltip" title="# of cancelled routes"> 
              @if($cancelled_routes > 0)
            <div class="box hvr-grow hvr-shutter-out-vertical" style="background:rgba(0, 0, 0, 0) radial-gradient(at center center, rgba(241, 0, 0, 0.4) 0%, rgba(255, 255, 255, 0.6) 100%) repeat scroll 0% 0%">
              <h3 style="font-weight:600">Cancelled</h3>
              <h2 style="font-size: 4.5vw">{{$cancelled_routes}}</h2>
            </div>
            @else
            <div class="box hvr-grow hvr-shutter-out-vertical">
              <h3 style="font-weight:600">Cancelled</h3>
              <h2 style="font-size: 4.5vw">{{$cancelled_routes}}</h2>
            </div>
             @endif
            </a>  
        </div> 
        <div class="col-md-3 sm-box">
            <a  href="{{ route('routes.delay') }}" data-toggle="tooltip" title="# of delayed routes">
             @if($late_routes > 0) 
            <div class="box hvr-grow hvr-shutter-out-vertical" style="background:rgba(0, 0, 0, 0) radial-gradient(at center center, rgba(241, 0, 0, 0.4) 0%, rgba(255, 255, 255, 0.6) 100%) repeat scroll 0% 0%">
              <h3 style="font-weight:600">Delayed</h3>
              <h2 style="font-size: 4.5vw">{{$late_routes}}</h2>
            </div>
            @else
            <div class="box hvr-grow hvr-shutter-out-vertical">
              <h3 style="font-weight:600">Delayed</h3>
              <h2 style="font-size: 4.5vw">{{$late_routes}}</h2>
            </div>
            @endif
            </a>  
        </div>                                           
    </div>  
<!-----------------------ROUTE PART END --------------------------------------------->


 <!-----------------------Random Inspection PART START ------------------------------------------->
<!--     <a href="#" class="hvr-icon-buzz">
    <h3 style="font-weight:600">Random Inspections</h3>
    </a>
    <hr class="divider" style="margin-top: 0rem">  
    <br />
    <div class="row">          
        <div class="col-md-3 sm-box">
            <a  href="{{ route('routes.inprogress') }}" data-toggle="tooltip" title="all time totla # of random inspections"> 
            <div class="box hvr-grow hvr-shutter-out-vertical">
              <h3 style="font-weight:600">Total</h3>
              <h2 style="font-size: 4.5vw">{{$in_progress_routes}}</h2>
            </div>
            </a>  
        </div> 
        <div class="col-md-3 sm-box">
            <a href="{{ route('routes.complete') }}" data-toggle="tooltip" title="total # of random inspections in 7 days"> 
            <div class="box hvr-grow hvr-shutter-out-vertical">
              <h3 style="font-weight:600">Weekly </h3>
              <h2 style="font-size: 4.5vw">{{$finish_routes}}</h2>
            </div>
            </a>  
        </div>
        <div class="col-md-3 sm-box">
            <a  href="{{ route('routes.inprogress') }}" data-toggle="tooltip" title="total # of random inspections in 30 days"> 
            <div class="box hvr-grow hvr-shutter-out-vertical">
              <h3 style="font-weight:600">Monthly</h3>
              <h2 style="font-size: 4.5vw">{{$in_progress_routes}}</h2>
            </div>
            </a>  
        </div> 
        <div class="col-md-3 sm-box">
            <a href="{{ route('routes.complete') }}" data-toggle="tooltip" title="total # of random inspections in 365 days"> 
            <div class="box hvr-grow hvr-shutter-out-vertical">
              <h3 style="font-weight:600">Yearly </h3>
              <h2 style="font-size: 4.5vw">{{$finish_routes}}</h2>
            </div>
            </a>  
        </div>                                                   
    </div>   -->
<!-----------------------Random Inspection PART END --------------------------------------------->
</div>
@stop

@section('css')
<link rel="stylesheet" href="/css/app.css">
<link rel="stylesheet" href="bower_components/chart.js/dist/Chart.min.css">
<style type="text/css">
@media screen and (min-width: 25em){
    h3{ font-size: calc( 13px + (22 - 16) * (99vw - 400px) / (800 - 401) ) }
}

@media screen and (min-width: 50em){

    h3 { font-size: calc( 13px + (22 - 16) * (99vw - 400px) / (800 - 401) ) }
} 

</style>
@stop

@section('js')
{!! $workorder_barchart->script() !!}
 <script  type="text/javascript" src="{{ asset('bower_components/chart.js/dist/Chart.min.js') }}"></script>   
<script>
$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();   
});


</script>

@stop 
