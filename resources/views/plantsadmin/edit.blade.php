{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
     <h1 style="margin-bottom:-10px"><b>Edit Plants Admin</b></h1>
    <hr class="divider" >
@stop
  
@section('content')
<div class="centered-container">
    @if (count($errors) > 0)
  <div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
       @foreach ($errors->all() as $error)
         <li>{{ $error }}</li>
       @endforeach
    </ul>
  </div>
@endif


{!! Form::model($plantadmin, ['method' => 'PATCH','route' => ['plantadmin.update',$plantadmin->id]]) !!}

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
        <strong><i class="fas fa-industry" style="color: #6c757d"></i> &nbspPlant Name</strong>
            {!! Form::select('plant_id', $plant_id_options, null,['class' => 'form-control']) !!}         
        </div>
        <div class="form-group">
            <strong><i class="fas fa-user-tie" style="color: #6c757d"></i> &nbsp Plant Admin Name </strong>
            {!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}
        </div>
        <div class="form-group">
            <strong><i class="far fa-envelope" style="color: #6c757d"></i>&nbsp Email </strong>
            {!! Form::text('email', null, array('placeholder' => 'Email','class' => 'form-control')) !!}
        </div>
        
        <!-- 
        <div class="form-group">
            <strong>Phone 2 &nbsp <i class="fas fa-address-book" style="color: #007bff"></i></strong>
            {!! Form::text('phone2', null, array('placeholder' => 'Number','class' => 'form-control')) !!}
        </div>     --> 
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <strong><i class="far fa-envelope" style="color: #6c757d"></i>&nbsp Address </strong>
            {!! Form::text('address', null, array('placeholder' => 'Address','class' => 'form-control')) !!}
        </div>
        <div class="form-group">
            <strong><i class="fas fa-address-book" style="color: #6c757d"></i> &nbsp Contact 1</strong>
            {!! Form::tel('phone1', null, array('placeholder' => 'Number','class' => 'form-control')) !!}
        </div>
        <div class="form-group">
            <strong><i class="fas fa-address-book" style="color: #6c757d"></i> &nbsp Contact 2</strong>
            {!! Form::tel('phone2', null, array('placeholder' => 'Number','class' => 'form-control')) !!}
        </div>   
    </div>
</div>


<div class="row"> 
    <div class="col-md-2">
        <div class="form-group">
            <strong><i class="fas fa-asterisk" style="color: #6c757d"></i> &nbsp Department Limit  </strong>
            {!! Form::number('dept_limit', null, array('placeholder' => 'Number','class' => 'form-control')) !!}
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group">
            <strong><i class="fas fa-asterisk" style="color: #6c757d"></i> &nbsp Instrument Limit</strong>
            {!! Form::number('instrument_limit', null, array('placeholder' => 'Number','class' => 'form-control')) !!}
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group">
            <strong>  <i class="fas fa-asterisk" style="color: #6c757d"></i> &nbsp Parameter Limit </strong>
            {!! Form::number('parameter_limit', null, array('placeholder' => 'Number','class' => 'form-control')) !!}
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group">
            <strong><i class="fas fa-asterisk" style="color: #6c757d"></i> &nbsp User Limit </strong>
            {!! Form::number('user_limit', null, array('placeholder' => 'Number','class' => 'form-control')) !!}
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group">
            <strong> <i class="fas fa-asterisk" style="color: #6c757d"></i> &nbsp Alarm Limit </strong>
            {!! Form::number('alarm_limit', null, array('placeholder' => 'Number','class' => 'form-control')) !!}        
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group">
            <strong><i class="fas fa-asterisk" style="color: #6c757d"></i> &nbsp Route Limit  </strong>
            {!! Form::number('routes_limit', null, array('placeholder' => 'Number','class' => 'form-control')) !!}
        </div>
    </div>
     <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            {!! Form::hidden('created_by', null, array('placeholder' => 'Created By','class' => 'form-control')) !!}
    </div>
</div>
</div>
<div class="row">
    <div class="col-md-2">
        <div class="form-group">
            <strong><i class="fas fa-asterisk" style="color: #6c757d"></i> &nbsp Unit Limit  </strong>
            {!! Form::number('unit_limit', null, array('placeholder' => 'Number','class' => 'form-control')) !!}
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group">
            <strong><i class="fas fa-asterisk" style="color: #6c757d"></i> &nbsp Line Limit  </strong>
            {!! Form::number('line_limit', null, array('placeholder' => 'Number','class' => 'form-control')) !!}
        </div>
    </div>
</div>
  <br>

    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
        <button type="submit" class="btn btn-secondary"><i class="fas fa-pen-nib"></i> &nbsp; Submit</button>
    </div>

    <!-- <div class="col-xs-12 col-sm-12 col-md-12 text-center">
        <a class="btn btn-primary" href="{{ route('plantadmin.index') }}" > Back</a>
    </div> -->
    
</div>
{!! Form::close() !!}

@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop






