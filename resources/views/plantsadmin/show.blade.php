{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1 style="margin-bottom:-10px"><b>Show Plants Admin</b></h1>
    <hr class="divider" >
@stop

@section('content')
<div class="centered-container">  
    <div class="row">
        <div class="container-fluid">
            <table class="table table-bordered table-striped">
                <tr>
                    <th>ID</th>
                    <td>{{ $plantadmin->id }}</td>
                </tr>
                <tr>
                    <th>Plant ID</th>
                    <td>{{ $plantadmin->plant_id }}</td>        
                </tr>
                <tr>
                    <th>Name</th>
                    <td>{{ $plantadmin->name }}</td>        
                </tr>
                <tr>
                    <th>Email</th>
                    <td>{{ $plantadmin->email }}</td>        
                </tr>
                <tr>
                    <th>Password</th>
                    <td>{{ $plantadmin->password }}</td>        
                </tr>
                <tr>
                    <th>Address</th>
                    <td>{{ $plantadmin->address }}</td>        
                </tr>
                <tr>
                    <th>Phone 1</th>
                    <td>{{ $plantadmin->phone1 }}</td>        
                </tr>
                <tr>
                    <th>Phone 2</th>
                    <td>{{ $plantadmin->phone2 }}</td>        
                </tr>
                <tr>
                    <th>Department Limit</th>
                    <td>{{ $plantadmin->dept_limit }}</td>        
                </tr>
                <tr>
                    <th>Instrument Limit</th>
                    <td>{{ $plantadmin->instrument_limit }}</td>        
                </tr>
                <tr>
                    <th>Parameter Limit</th>
                    <td>{{ $plantadmin->parameter_limit }}</td>        
                </tr>
                <tr>
                    <th>User Limit</th>
                    <td>{{ $plantadmin->user_limit }}</td>        
                </tr>
                <tr>
                    <th>Alarm Limit</th>
                    <td>{{ $plantadmin->alarm_limit }}</td>        
                </tr>
                <tr>
                    <th>Route Limit</th>
                    <td>{{ $plantadmin->routes_limit }}</td>        
                </tr>
                <tr>
                    <th>Unit Limit</th>
                    <td>{{ $plantadmin->unit_limit }}</td>        
                </tr>
                <tr>
                    <th>Line Limit</th>
                    <td>{{ $plantadmin->line_limit }}</td>        
                </tr>
                <tr>
                    <th>Created By</th>
                    <td>{{ $plantadmin->created_by }}</td>        
                </tr>
            </table>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <a class="btn btn-primary" href="{{ route('plantadmin.index') }}"> Back</a>
        </div>
    </div>
</div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop