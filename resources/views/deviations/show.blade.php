{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
     <h1 style="margin-bottom:-10px"><b>Show Deviations</b></h1>
     <hr class="divider" >
@stop

@section('content')
<div class="centered-container">
<div class="row">
    <div class="container-fluid">
        <table class="table table-bordered table-striped">
            <tr>
                <th>User ID</th>
                <td>{{ $deviation->user_id }}</td>
            </tr>
             <tr>
                <th>Deviation</th>
                <td>{{ $deviation->deviation}}</td>
            </tr>

            <tr>
                <th>Description</th>
                <td>{{ $deviation->actual }}</td>        
            </tr>
             <tr>
                <th>Record</th>
                <td>{{ $deviation->record }}</td>
            </tr>
            <tr>
                <th>Time Stamp</th>
                <td>{{ $deviation->created_at }}</td>
            </tr>
        </table>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <a class="btn btn-primary" href="{{ route('deviations.index') }}" style="float:left"> Back</a>
        </div>
    </div>
</div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')

@stop