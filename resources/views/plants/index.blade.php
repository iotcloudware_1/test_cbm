{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', 'Dashboard')
@section('content_header')
      <h1 style="margin-bottom:-10px"><b>Plants</b></h1>
        <div class="pull-right">
            <a class="btn btn-secondary" style="float: right;margin-bottom: 13px;margin-top: -35px;" href="{{ route('plants.create') }}"><i class="fas fa-plus"></i> &nbsp; Create </a>
        </div>
        <hr class="divider">
@stop 
@section('content')
<div class="centered-container">
   <div class="row">
      <div class="col-md-9">
        
      </div>
  </div>

  @if ($message = Session::get('success'))
  <div class="alert alert-success">
    <p>{{ $message }}</p>
  </div>
  @endif

  @yield('datatable', View::make('datatables.plant-datatable'))
</div>
@stop



@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('plugins.Datatables', true)
@section('js')
  
@stop