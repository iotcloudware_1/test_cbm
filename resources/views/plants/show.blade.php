{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1 style="margin-bottom:-10px"><b> Show Plants</b></h1>
     <hr class="divider" >
@stop

@section('content')
<div class="centered-container">    
<div class="row">
        <table class="table table-bordered table-striped">
            <tr>
                <th>Name</th>
                <td>{{ $plant->name }}</td>
            </tr>
            <tr>
                <th>Description</th>
                <td>{{ $plant->description }}</td>        
            </tr>
        </table>
    </div>
</div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop