{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
<div class="centered-container">
    <h1 style="margin-bottom:-10px"><b>Workorder Instrument</b></h1>
     <hr class="divider" >
        <div class="pull-right">
            <a class="btn btn-secondary" style="float: right;margin-top: 10px;" href="{{ route('instrumentfault.create') }}"> Create </a>
        </div>
        
@stop
 
@section('content')
 <div class="row">
    <div class="col-md-9">
      
    </div>
</div>

  @if ($message = Session::get('success'))
  <div class="alert alert-success">
    <p>{{ $message }}</p>
  </div>
  @endif

  @yield('datatable', View::make('datatables.instrument-workorder-datatable'))
</div>
@stop



@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('plugins.Datatables', true)
@section('js')
  
@stop