@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header') 

  <div class="row">
    <div class="col-md-6">
      <h1 style="margin-bottom:0px;"><b>Alarms</b></h1>    
    </div>
    <div class="col-md-6">   
      <label class="dropdown" style="float: right;">
        <div class="dd-button">
          Manage
        </div>
        <input type="checkbox" class="dd-input" id="test">
          <ul class="dd-menu">
           <a class="btn btn-success" style="width: 100%; padding: 5%; background-color: #1a1c33;"href="{{ route('alarms.currentAlarmState') }}"><li style="color: white"><i class="fas fa-wallet"></i> &nbsp;Current </li></a> 
           <li class="divide"></li>

           <a class="btn btn-success" style="width: 100%;padding: 6%; background-color:#1a1c33;"href="{{ route('alarms.alarmHistory') }}"><li style="color: white"><i class="fas fa-history"></i>&nbsp; History </li></a> 
           <li class="divide"></li>

           <a class="btn btn-success" style="width: 100%;padding: 5%; background-color: #1a1c33;" href="{{ route('alarms.index') }}"><li style="color: white"><i class="fas fa-cog"></i> &nbsp; Setting </li></a>       
           <li class="divide"></li>
        </ul>
      </label>       
    </div>
  </div>
    <hr class="divider">
@stop
@section('content')

<div class="centered-container">    
    <div class="row">
        <div class=col-md-4>
            <h3 style="font-weight:600">Alarm vs Normal</h3>
            <div style="border: 2px solid #272d47; box-shadow: 7px 9px 5px #272d47; border-radius: 8px;height:250px;">
                @if(empty($chart))
                 <p> NO Data</p>
                @else    
                {!! $chart->container() !!}
                @endif
            </div>   
        </div>   
          <div class=col-md-4>
            <h3 style="font-weight:600">Priority wise</h3>
            <div style="border: 2px solid #272d47; box-shadow: 7px 9px 5px #272d47; border-radius: 8px;height:250px;">
                @if(empty($bar_chart))
                 <p> NO Data</p>
                @else    
                {!! $bar_chart->container() !!}
                @endif
            </div>   
        </div>
       <div class=col-md-4>
            <h3 style="font-weight:600">Status wise</h3>
            <div style="border: 2px solid #272d47; box-shadow: 7px 9px 5px #272d47; border-radius: 8px;height:250px;">
                @if(empty($bar_chart1))
                 <p> NO Data</p>
                @else    
                {!! $bar_chart1->container() !!}
                @endif
            </div>   
        </div>         
    </div> 
    <br/>
    <br/>
    <div class="row">
    <div class="col-md-3 sm-box">
      <a href="{{ route('alarms.total') }}" data-toggle="tooltip" title="Total # of observed alarms"> 
        <div class="box hvr-grow hvr-shutter-out-vertical" style="padding-bottom: 10%">
          <h3 style="font-weight:600">Total</h3>
          <h2 style="font-size: 4.5vw">{{$alarm_history}}</h2>
        </div>
      </a>  
    </div>
    <div class="col-md-3 sm-box">
      <a href="{{ route('alarms.current') }}" data-toggle="tooltip" title="# of current unresolved alarms">
         @if($current_alarm > 0) 
        <div class="box hvr-grow hvr-shutter-out-vertical"style="padding-bottom: 10%;background:rgba(0, 0, 0, 0) radial-gradient(at center center, rgba(241, 0, 0, 0.4) 0%, rgba(255, 255, 255, 0.6) 100%) repeat scroll 0% 0%">
          <h3 style="font-weight:600">Current</h3>
          <h2 style="font-size: 4.5vw">{{$current_alarm}}</h2>
        </div>
        @else
        <div class="box hvr-grow hvr-shutter-out-vertical"style="padding-bottom: 10%">
          <h3 style="font-weight:600">Current</h3>
          <h2 style="font-size: 4.5vw">{{$current_alarm}}</h2>
        </div>
        @endif
      </a>  
    </div>    
    <div class="col-md-3 sm-box">
      <a href="{{ route('alarms.ackalarm') }}" data-toggle="tooltip" title="# of open acknowledged alarms"> 
        <div class="box hvr-grow hvr-shutter-out-vertical"style="padding-bottom: 10%">
          <h3 style="font-weight:600">Ack</h3>
          <h2 style="font-size: 4.5vw">{{$alarm_ack}}</h2>
        </div>
      </a>  
    </div>
    <div class="col-md-3 sm-box">
      <a href="{{ route('alarms.unackalarm') }}" data-toggle="tooltip" title="# of open unacknowledged alarms"> 
        @if($alarm_unack > 0)
        <div class="box hvr-grow hvr-shutter-out-vertical"style="padding-bottom: 10%;background:rgba(0, 0, 0, 0) radial-gradient(at center center, rgba(241, 0, 0, 0.4) 0%, rgba(255, 255, 255, 0.6) 100%) repeat scroll 0% 0%">
          <h3 style="font-weight:600">Unack</h3>
          <h2 style="font-size: 4.5vw">{{$alarm_unack}}</h2>
        </div>
        @else
        <div class="box hvr-grow hvr-shutter-out-vertical" style="padding-bottom: 10%" >
          <h3 style="font-weight:600">Unack</h3>
          <h2 style="font-size: 4.5vw">{{$alarm_unack}}</h2>
        </div>
        @endif
      </a>  
    </div> 
    </div>
    @stop
@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
<link rel="stylesheet" href="bower_components/chart.js/dist/Chart.min.css">

<style type="text/css">
.bs-example{
    margin: 20px;
  }  
.centered-container
{
    margin-left: 5%;
    margin-right: 5%;
}


a:hover {
  color: red;
}

/* Dropdown */

.dropdown {
  display: inline-block;
  position: relative;
  z-index: 1;
}

.dd-button {
  display: inline-block;
  border: 1px solid gray;
  border-radius: 4px;
  padding: 10px 30px 10px 20px;
  background-color: #1a1c33;
  cursor: pointer;
  white-space: nowrap;
  color: white;
}

.dd-button:after {
  content: '';
  position: absolute;
  top: 50%;
  right: 15px;
  transform: translateY(-50%);
  width: 0; 
  height: 0; 
  border-left: 5px solid transparent;
  border-right: 5px solid transparent;
  border-top: 5px solid white;
  z-index: 1;
}

.dd-button:hover {
  background-color: #6d728a;
}


.dd-input {
  display: none;
}

.dd-menu {
  position: absolute;
  top: 100%;
  border: 1px solid #ccc;
  border-radius: 4px;
  padding: 0;
  margin: 2px 0 0 0;
  box-shadow: 0 0 6px 0 rgba(0,0,0,0.1);
  background-color: pink;
  list-style-type: none;
  color: white;
  z-index: 1;
}

.dd-input + .dd-menu {
  display: none;
  z-index: 1;
} 

.dd-input:checked + .dd-menu {
  display: block;
} 

.dd-menu li {
  padding: 10px 20px;
  cursor: pointer;
  white-space: nowrap;
  z-index: 1;
}
.dd-button::after{
  border-top: 5px solid white;
}

.dd-menu li:hover {
  background-color: #007bff;
}

.dd-menu li a {
  display: block;
  margin: -10px -20px;
  padding: 10px 20px;
  z-index: 1;
}

.dd-menu li.divide{
  padding: 0;
  border-bottom: 1px solid #fcf9fe;
}
</style>

@stop

@section('js')

    @if(empty($chart))
     <p> NO Data</p>
    @else    
    {!! $chart->script() !!} 
    @endif

    @if(empty($bar_chart))
     <p> NO Data</p>
    @else    
    {!! $bar_chart->script() !!} 
    @endif

    @if(empty($bar_chart1))
     <p> NO Data</p>
    @else    
    {!! $bar_chart1->script() !!} 
    @endif

 <script  type="text/javascript" src="{{ asset('bower_components/chart.js/dist/Chart.min.js') }}"></script>

<script type="text/javascript">
$(document).ready(function(){
$('[data-toggle="tooltip"]').tooltip();   
});
</script>
@stop 

<style type="text/css">

.divider {
    border:3px solid #272d47; width: 100% ;
}

@media screen and (min-width: 25em){
    .i-icon { font-size: calc( 16px + (24 - 16) * (140vw - 400px) / (800 - 400) ); }
}

@media screen and (min-width: 50em){

    .i-icon { font-size: calc( 16px + (24 - 16) * (100vw - 400px) / (800 - 400) ); }
}

</style>