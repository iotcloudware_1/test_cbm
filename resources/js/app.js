/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

Vue.component('inspection-route-component', require('./components/InspectionRoute.vue').default);

Vue.component('test-route-component', require('./components/test.vue').default);

Vue.component('manual-entry-form', require('./components/ManualEntryForm.vue').default);

Vue.component('single-analytics-component', require('./components/SingleAnalyticsComponent.vue').default);

Vue.component('detailed-parmeter-analytics-component', require('./components/DetailedParameterAnalytics.vue').default);

Vue.component('multiple-analytics-component', require('./components/MultipleAnalyticsComponent.vue').default);

Vue.component('line-chart', require('./components/LineChart.vue').default); 

Vue.component('line-chart-for-detail', require('./components/LineChartForDetail.vue').default); 

Vue.component('bar-chart', require('./components/BarChart.vue').default); 

Vue.component('config-alarm', require('./components/ConfigAlarm.vue').default); 

Vue.component('unresolved-faults', require('./components/UnresolvedFaults.vue').default); 

Vue.component('fault-history', require('./components/FaultHistory.vue').default);

Vue.component('route-create', require('./components/RouteCreation.vue').default); 

Vue.component('add-param-inpectionpoint', require('./components/AddParamInpectionPoint.vue').default);

Vue.component('align-inspection', require('./components/AlignInspection.vue').default);

Vue.component('add-contact', require('./components/AddContact.vue').default);
Vue.component('report', require('./components/Report.vue').default); 

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
import Vuex from 'vuex'
import axios from 'axios'
import vSelect from 'vue-select'
import "vue-select/src/scss/vue-select.scss";
import 'es6-promise/auto';
import VueTimepicker from 'vue2-timepicker';
import 'vue2-timepicker/dist/VueTimepicker.css';
import Datepicker from 'vuejs-datepicker';
import datetime from 'vuejs-datetimepicker';
import moment from 'moment';
import VueMoment from 'vue-moment';
import VueTableDynamic from 'vue-table-dynamic';
import { vsButton, vsSelect, vsPopup, vsTabs } from 'vuesax';
import 'material-icons/iconfont/material-icons.css';
import 'vuesax/dist/vuesax.css';
import 'v-slim-dialog/dist/v-slim-dialog.css';
import SlimDialog from 'v-slim-dialog';
import VueApexCharts from 'vue-apexcharts';
import Multiselect from 'vue-multiselect';
import DateRangePicker from "@gravitano/vue-date-range-picker";
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue';
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'



import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'





Vue.component('apexchart', VueApexCharts)

Vue.use(SlimDialog);
Vue.use('vue-timepicker',VueTimepicker);
// Vue.use(VueMoment, { moment });
Vue.use(Vuex)
Vue.use(require('vue-moment'));
Vue.component('v-select', vSelect);
Vue.component('vue-timepicker', VueTimepicker);
Vue.component('vuejs-datepicker', Datepicker);
Vue.component('vuejs-datetimepicker',datetime);
Vue.use('vue-table-dynamic',VueTableDynamic);
Vue.component('vue-table-dynamic',VueTableDynamic);
Vue.component('multiselect', Multiselect);
Vue.component('date-range-picker', DateRangePicker);

Vue.use(vsButton)
Vue.use(vsSelect)
Vue.use(vsPopup)
Vue.use(vsTabs)

Vue.use(require('vue-moment'));

// Install BootstrapVue
Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)

Vue.use(Vuetify)

//Drag n Drop
import draggable from 'vuedraggable'
Vue.component('draggable',draggable);

const app = new Vue({
    el: '#app',
    vuetify: new Vuetify(),
});

