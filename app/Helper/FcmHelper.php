<?php  

namespace app\Helper;

use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;
use App\Models\UserDeviceToken;
use App\Models\NotificationMessage;


class FcmHelper
{
    /**
     * @param NotifyName   : Notification Logic Name.
     * @param UserId       : User Id For Device Token or Registrator Id.
     * @throws \Exception
     */

    public static function FcmForMultipleDevices($UserId,$NotifyName){

        // 
        \Log::info("uayyyyy");
        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60*20);

        // $tokensData = UserDeviceToken::select('language','device_token as fcm_token')->where('UserId',$UserId)->get();
        $tokensData=User::whereIn('id',[23,24])->get();
        foreach($tokensData as $key => $value){{

            // $notifyMessageData = NotificationMessage::select('english_title','english_body_text','click_action')
            // ->where('notify_name',$NotifyName)
            // ->first();

            $notificationBuilder = new PayloadNotificationBuilder($value['email']);
            $notificationBuilder->setBody($alarm_message_body)
                        ->setSound('default');

            $dataBuilder = new PayloadDataBuilder();
            $dataBuilder->addData(['phone_no' => $value['phone_no']]);

            $option         = $optionBuilder->build();
            $notification   = $notificationBuilder->build();
            $data           = $dataBuilder->build();

            $downstreamResponse = FCM::sendTo($value['email'], $option, $notification, $data);

            //logs 
            \Log::debug("downstreamResponse->numberSuccess()", [$downstreamResponse->numberSuccess()]);
            \Log::debug("downstreamResponse->numberFailure()", [$downstreamResponse->numberFailure()]);
        
            $downstreamResponse->numberSuccess();
            $downstreamResponse->numberFailure();
            $downstreamResponse->numberModification();

            // return Array - you must remove all this tokens in your database
            $downstreamResponse->tokensToDelete();

            // return Array (key : oldToken, value : new token - you must change the token in your database)
            $downstreamResponse->tokensToModify();

            // return Array - you should try to resend the message to the tokens in the array
            $downstreamResponse->tokensToRetry();

            // return Array (key:token, value:error) - in production you should remove from your database the tokens
            $downstreamResponse->tokensWithError();
        } //ending token For Looping




        // foreach($tokensData as $key => $value){
        //     if($value->language == "en"){

        //         $notifyMessageData = NotificationMessage::select('english_title','english_body_text','click_action')
        //         ->where('notify_name',$NotifyName)
        //         ->first();

        //         $notificationBuilder = new PayloadNotificationBuilder($notifyMessageData->english_title);
        //         $notificationBuilder->setBody($notifyMessageData->english_body_text)
        //                     ->setSound('default');

        //         $dataBuilder = new PayloadDataBuilder();
        //         $dataBuilder->addData(['click_action' => $notifyMessageData->click_action]);

        //     }else if($value->language == "ar"){

        //         $notifyMessageData = NotificationMessage::select('arabic_title','arabic_body_text','click_action')
        //         ->where('notify_name',$NotifyName)
        //         ->first();

        //         $notificationBuilder = new PayloadNotificationBuilder($notifyMessageData->arabic_title);
        //         $notificationBuilder->setBody($notifyMessageData->arabic_body_text)
        //                     ->setSound('default');

        //         $dataBuilder = new PayloadDataBuilder();
        //         $dataBuilder->addData(['click_action' => $notifyMessageData->click_action]);
        //     }

        //     $option         = $optionBuilder->build();
        //     $notification   = $notificationBuilder->build();
        //     $data           = $dataBuilder->build();

        //     $downstreamResponse = FCM::sendTo($value->fcm_token, $option, $notification, $data);

        //     //logs 
        //     \Log::debug("downstreamResponse->numberSuccess()", [$downstreamResponse->numberSuccess()]);
        //     \Log::debug("downstreamResponse->numberFailure()", [$downstreamResponse->numberFailure()]);
        
        //     $downstreamResponse->numberSuccess();
        //     $downstreamResponse->numberFailure();
        //     $downstreamResponse->numberModification();

        //     // return Array - you must remove all this tokens in your database
        //     $downstreamResponse->tokensToDelete();

        //     // return Array (key : oldToken, value : new token - you must change the token in your database)
        //     $downstreamResponse->tokensToModify();

        //     // return Array - you should try to resend the message to the tokens in the array
        //     $downstreamResponse->tokensToRetry();

        //     // return Array (key:token, value:error) - in production you should remove from your database the tokens
        //     $downstreamResponse->tokensWithError();
        // } //ending token For Looping
    }
}

?>