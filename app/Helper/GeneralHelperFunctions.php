<?php // Code within app\Helper\GeneralHelperFunctions.php

namespace App\Helper\GeneralHelperFunctions;
use App\Model\Department;
use App\Model\Plant;
use App\Model\Unit;
use App\Model\Instrument;
use App\Model\Parameter;
use App\Model\Alarm;
use App\Model\Line;
use App\Model\Tool;
use App\Model\WorkOrder;
use App\Model\Asset;
use App\Model\Status;
use App\Model\Route;
use App\Model\RouteHistory;
use App\Model\SkipReason;
use App\Model\CurrentWorkOrder;
use App\Model\ParameterValues;
use App\Model\RouteInspectionParameter;
use App\Model\RouteInspectionPoint;
use App\Model\AlarmCurrentState;
use App\User;

class Helper
{
    public static function plantIdToName($id)
    {
        $plantName = Plant::find($id);
        return $plantName->name;
    }
    public static function statusIdToDes($id)
    {
        $statusDes = Status::find($id);
        return $statusDes->description;
    }
    public static function assetIdToName($id)
    {
        $assetName = Asset::find($id);
        return $assetName->name;
    }
     public static function unitIdToName($id)
    {
        $unitName = Unit::find($id);
        return $unitName->name;
    }
    public static function workorderIdToRemarks($id)
    {
        $workorder_remarks = CurrentWorkOrder::find($id);
        return $workorder_remarks->remarks;
    }
    public static function WorkOrderIdToDescription($id)
    {
        $WorkOrderName = WorkOrder::find($id);
        return $WorkOrderName->description;
    }
    public static function userIdToName($id)
    {
        if($id=='')
        {
            return " ";
        }    
        else
        {
            $userName = User::find($id);
            return $userName->name;
        }
    }
    public static function departmentIdToName($id)
    {
        $departmentName = Department::find($id);
        return $departmentName->name;
    }
    public static function lineIdToName($id)
    {
        $lineName = Line::find($id);
        return $lineName->name;
    }
    public static function instrumentIdToName($id)
    {
        $instrumentName = Instrument::find($id);
        return $instrumentName->name;
    }
    public static function instrumentIdToDes($id)
    {
        $instrumentDes = Instrument::find($id);
        return $instrumentDes->description;
    } 

    public static function assetIdToInstrument($id)
    {
        $asset_instrument = Instrument::where('id',$id)->pluck('asset_id');
        $asset_name=Asset::where('id',$asset_instrument)->value('name');
        return $asset_name;
    }
    public static function lineIdToInstrument($id)
    {
        $asset_instrument = Instrument::where('id',$id)->pluck('asset_id');
        $line_id=Asset::where('id',$asset_instrument)->value('line_id');
        $line_name=Line::where('id',$line_id)->value('name');
        return $line_name;
    } 
    public static function departmentIdToInstrument($id)
    {
        $asset_instrument = Instrument::where('id',$id)->pluck('asset_id');
        $line_id=Asset::where('id',$asset_instrument)->value('line_id');
        $department_id=Line::where('id',$line_id)->pluck('department_id');   
        $department_name=Department::where('id',$department_id)->value('name');
        return $department_name;
    }         
    public static function unitIdToInstrument($id)
    {
        $asset_instrument = Instrument::where('id',$id)->pluck('asset_id');
        $line_id=Asset::where('id',$asset_instrument)->value('line_id');
        $department_id=Line::where('id',$line_id)->pluck('department_id');   
        $unit_id=Department::where('id',$department_id)->pluck('unit_id');
        $unit_name=Unit::where('id',$unit_id)->value('name');
        return $unit_name;
    }
    public static function assetIdToParameterId($id)
    {
        $instrument_id = Parameter::where('id',$id)->pluck('instrument_id');
        $asset_instrument = Instrument::where('id',$instrument_id)->pluck('asset_id');
        return $asset_instrument;
    } 
    public static function lineIdToParameterId($id)
    {
        $instrument_id = Parameter::where('id',$id)->pluck('instrument_id');
        $asset_instrument = Instrument::where('id',$instrument_id)->pluck('asset_id');
        $line_asset = Asset::where('id',$asset_instrument)->pluck('line_id');
        return $line_asset;
    } 
    public static function departmentIdToParameterId($id)
    {
        $instrument_id = Parameter::where('id',$id)->pluck('instrument_id');
        $asset_instrument = Instrument::where('id',$instrument_id)->pluck('asset_id');
        $line_asset = Asset::where('id',$asset_instrument)->pluck('line_id');
        $department_line = Line::where('id',$line_asset)->pluck('department_id');
        return $department_line;
    }
    public static function unitIdToParameterId($id)
    {
        $instrument_id = Parameter::where('id',$id)->pluck('instrument_id');
        $asset_instrument = Instrument::where('id',$instrument_id)->pluck('asset_id');
        $line_asset = Asset::where('id',$asset_instrument)->pluck('line_id');
        $department_line = Line::where('id',$line_asset)->pluck('department_id');
        $unit_department = Department::where('id',$department_line)->pluck('unit_id');
        return $unit_department;
    }

    public static function instrumentIdToParameter($id)
    {
        $instrument_id = Parameter::where('id',$id)->pluck('instrument_id');
        $instrumentName = Instrument::where('id',$instrument_id)->value('name');
        return $instrumentName;
    }

    
    public static function assetIdToParameter($id)
    {
        $instrument_id = Parameter::where('id',$id)->pluck('instrument_id');
        $asset_id = Instrument::where('id',$instrument_id)->pluck('asset_id');
        $asset_name = Asset::where('id',$asset_id)->value('name');
        return $asset_name;
    }

    public static function lineIdToParameter($id)
    {
        $instrument_id = Parameter::where('id',$id)->pluck('instrument_id');
        $asset_id = Instrument::where('id',$instrument_id)->pluck('asset_id');
        $line_id = Asset::where('id',$asset_id)->pluck('line_id');
        $line_name = Line::where('id',$line_id)->value('name');
        return $line_name;
    }
    public static function departmentIdToParameter($id)
    {
        $instrument_id = Parameter::where('id',$id)->pluck('instrument_id');
        $asset_id = Instrument::where('id',$instrument_id)->pluck('asset_id');
        $line_id = Asset::where('id',$asset_id)->pluck('line_id');
        $department_id = Line::where('id',$line_id)->pluck('department_id');
        $department_name = Department::where('id',$department_id)->value('name');
        return $department_name;
    } 
    public static function unitIdToParameter($id)
    {
        $instrument_id = Parameter::where('id',$id)->pluck('instrument_id');
        $asset_id = Instrument::where('id',$instrument_id)->pluck('asset_id');
        $line_id = Asset::where('id',$asset_id)->pluck('line_id');
        $department_id = Line::where('id',$line_id)->pluck('department_id');
        $unit_id = Department::where('id',$department_id)->pluck('unit_id');
        $unit_name = Unit::where('id',$unit_id)->value('name');
        return $unit_name;
    }           
    public static function parameterIdToName($id)
    {
        $parameterName = Parameter::find($id);
        // dd($parameterName);
        return (string)$parameterName->name;
    }
    public static function alarmIdToName($id)
    {
        $alarmName = Alarm::find($id);
        return $alarmName->alarm_message;
    }
   
    public static function toolIdToName($id)
    {
        $toolName = Tool::find($id);
        return $toolName->name;
    }
    public static function parameterIdToDatatype($id)
    {
        $parameterDatatype = Parameter::find($id);
        return $parameterDatatype->data_type;
    }
    public static function parameterIdToMax($id)
    {
        $parameterMax = Parameter::find($id);
        return (string)$parameterMax->max_value;
    }
    public static function parameterIdToMin($id)
    {
        $parameterMin = Parameter::find($id);
        return (string)$parameterMin->min_value;
    }
    public static function ParameterIdToDescription($id)
    {
        $WorkOrderName = Parameter::find($id);
        return $WorkOrderName->description;
    }
    public static function ParameterIdToEU($id)
    {
        $WorkOrderName = Parameter::find($id);
        return $WorkOrderName->engrineering_unit;
    }                
    public static function getInstrumentQrCode($id)
    {
        $instrument_code = str_pad($id, 8, "0", STR_PAD_LEFT);
        return $instrument_code;
    }   
    public static function routeIdToDes($id)
    {
        $routeDescription = Route::find($id);
        return $routeDescription->description;
    }
    public static function inspectionPointToInstrumentName($id)
    {
        
         $instrument_id= RouteInspectionPoint::where('id',$id)->value('instrument_id');
         $instrument_name= Instrument::where('id',$instrument_id)->value('name');
        return $instrument_name;
        //dd($instrument_name);    
         // return $route_desc;  
    }

    public static function inspectionPointToRouteDes($id)
    {
        $route_id= RouteInspectionPoint::where('id',$id)->value('route_id');
         $route_name= Route::where('id',$route_id)->value('description');
        return $route_name;
         // return $route_desc;  
    }

    public static function alarmCurrentStateToParameterName($id)
    {
        
         $alarm_id= AlarmCurrentState::where('id',$id)->value('alarm_id');
          $parameter_id= Alarm::where('id',$alarm_id)->value('parameter_id');
          $parameter_name= Parameter::where('id',$parameter_id)->value('name');

         return $parameter_name;  
    }
        public static function alarmIdToParameterName($id)
    {
          $parameter_id= Alarm::where('id',$id)->value('parameter_id');
          $parameter_name= Parameter::where('id',$parameter_id)->value('name');

         return $parameter_name;  
    }

    public static function alarmIdToInstrumentName($id)
    {
          $parameter_id= Alarm::where('id',$id)->value('parameter_id');
          $instrument_id= Parameter::where('id',$parameter_id)->value('instrument_id');
          $instrument_name= Instrument::where('id',$instrument_id)->value('name');

         return $instrument_name;  
    }

    public static function alarmCurrentStateToInstrumentName($id)
    {
        
         $alarm_id= AlarmCurrentState::where('id',$id)->value('alarm_id');
          $parameter_id= Alarm::where('id',$alarm_id)->value('parameter_id');
          $instrument_id= Parameter::where('id',$parameter_id)->value('instrument_id');
          $instrument_name= Instrument::where('id',$instrument_id)->value('name');

         return $instrument_name;  
    }
        public static function alarmCurrentStateToAlarmName($id)
    {
        
         $alarm_id= AlarmCurrentState::where('id',$id)->value('alarm_id');
          $alarm_message= Alarm::where('id',$alarm_id)->value('alarm_message');

         return $alarm_message;  
    }
    public static function routeHisIdToDes($id)
    {
        $route_id= RouteHistory::where('id',$id)->value('route_id');
         $route_desc= Route::where('id',$route_id)->value('description');
         return $route_desc;  
    }
    public static function skipReasonToName($id)
    {
        $skip_reason_Name = SkipReason::find($id);
        return $skip_reason_Name->name;
    }
    public static function parameterIdtoInstrumentName($id)
    {
        $instrument_name = Parameter::find($id)->instrument->value('name');
        return $instrument_name;
    } 
       
    public static function WorkOrderIdToInstrumentName($id)
    {
        $instrument_name = WorkOrder::find($id)->instrument->value('name');
        return $instrument_name;
    }    
    public static function intToBool($request)
    {
        if ($request==1) 
        {
            return "True";
        }
        elseif ($request==0) 
        {   
            return "False";
        }
        else
        {
            return $request;
        }
    }
    public static function lineIdToAsset($id)
    {
        $line_id = Asset::where('id',$id)->value('line_id');
        return $line_id;
    }
    public static function departmentIdToAsset($id)
    {
        $line_id = Asset::where('id',$id)->pluck('line_id');
        $department_id = Line::where('id',$line_id)->value('department_id');
        return $department_id;
    }

    public static function unitIdToAsset($id)
    {
        $line_id = Asset::where('id',$id)->pluck('line_id');
        $department_id = Line::where('id',$line_id)->pluck('department_id');
        $unit_id = Department::where('id',$department_id)->value('unit_id');
        return $unit_id;
    }

    public static function departmentIdToLine($id)
    {
        $department_id = Line::where('id',$id)->value('department_id');
        return $department_id;
    }
    public static function unitIdToLine($id)
    {
        $department_id = Line::where('id',$id)->value('department_id');
        $unit_id = Department::where('id',$department_id)->value('unit_id');
        return $unit_id;
    }

    public static function unitIdToDepartment($id)
    {
        $unit_id = Department::where('id',$id)->value('unit_id');
        return $unit_id;
    }
    public static function parameterIdToToolId($id)
    {
        $tool_id = Parameter::where('id',$id)->value('tool_id');
        return $tool_id;
    }  

    public static function array_sort_by_column(&$array, $column, $direction = SORT_ASC) {
        $reference_array = array();

        foreach($array as $key => $row) {
            $reference_array[$key] = $row[$column];
        }

        array_multisort($reference_array, $direction, $array);
        \Log::info("$reference_array=====================");
        \Log::info($reference_array);
    }  
}



















