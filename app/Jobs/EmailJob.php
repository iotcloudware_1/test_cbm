<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Mail;
use App\User;
use App\Model\Configuration;
class EmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    public $message;
    public $subject;
    public $status;
    public $email;


    public function __construct($message,$subject,$status,$email)
    {
        $this->message=$message;
        $this->subject=$subject;
        $this->status=$status;
        $this->email=$email;
                    
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // \Log::info($this->message);
        if ($this->status == 1)
        {

            $to_name =' ';
            $to_email = 'iotcloud@gmail.com';

            $data = array('name'=>' ', "body" => $this->message);
        
            Mail::send('email_template', $data, function($message) use ($to_name, $to_email) {
                $message->to($to_email, $to_name)
                        ->subject($this->subject);
                $message->from('cbmpepsico@gmail.com');
             }); 
        }
        elseif ($this->status == 2)
        {

            $to_name =' ';
            $to_email = $this->email;

            $data = array('name'=>' ', "body" => $this->message);
        
            Mail::send('email_template', $data, function($message) use ($to_name, $to_email) {
                $message->to($to_email, $to_name)
                        ->subject($this->subject);
                $message->from('cbmpepsico@gmail.com');
             }); 
        }
         
         elseif ($this->status == 3)
        {

            $status=Configuration::value('status');
            \Log::info($status);
            if($status==1)
            {
                \Log::info("trigger email");
                $contacts=User::whereIn('id',[23])->get();   
                foreach ($contacts as $key => $contact) 
                {
                    $to_name =' ';
                    $to_email = $contact['email'];
                    $data = array('name'=>' ', "body" => $this->message);
                
                    Mail::send('email_template', $data, function($message) use ($to_name, $to_email) {
                        $message->to($to_email, $to_name)
                                ->subject($this->subject);
                        $message->from('cbmpepsico@gmail.com');
                    }); 
                }            
            }
            else
            {

                \Log::info("no email trigger");
            }
        }   
    
    }
}
