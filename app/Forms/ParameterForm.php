<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;
use Kris\LaravelFormBuilder\Field;
use App\Model\Instrument;
use DB;

class ParameterForm extends Form
{
    public function buildForm()
    {
        //$plant = Plant::select('id')
                           //->get();
                           
    	$plant = DB::table('instruments')->get();
 		$this
			->add('instrument_id', 'entity', [
		        'class' => 'App\Model\Instrument',
		        'property' => 'name',
		        'query_builder' => function ($plant) {
		            return $plant;
		        }
		    ])
            ->add('name', Field::TEXT, [
                'rules' => 'required|min:5'
            ])
            ->add('description', Field::TEXTAREA, [
                'rules' => 'max:5000'
            ])
            ->add('type', Field::TEXT, [
                'rules' => 'max:50'
            ])
            ->add('entry', Field::TEXT, [
                'rules' => 'max:50'
            ])
            ->add('data_type', Field::TEXT, [
                'rules' => 'max:50'
            ])
            ->add('max_value', Field::TEXT, [
                'rules' => 'max:50'
            ])
            ->add('min_value', Field::TEXT, [
                'rules' => 'max:50'
            ])
            ->add('engrineering_unit', Field::TEXT, [
                'rules' => 'max:50'
            ])
            ->add('submit', 'submit', ['label' => 'Submit','attr' => ['class' => 'btn btn-secondary']]);

    } 
}
 