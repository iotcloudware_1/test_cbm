<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;
use Kris\LaravelFormBuilder\Field;
use App\Model\Plant;
use DB;

class PlantAdminForm extends Form
{
    public function buildForm()
    {
       $plant = DB::table('plants')->get();
 		$this
			->add('plant_id', 'entity', [
		        'class' => 'App\Model\Plant',
		        'property' => 'name',
		        'query_builder' => function ($plant) {
		            return $plant;
		        }
		    ])
            ->add('name', Field::TEXT, [
                'rules' => 'required|min:3'
            ])
            ->add('email', Field::EMAIL, [
                'rules' => 'required|min:5'
            ])
            ->add('password', Field::PASSWORD, [                
               'rules' => 'required|min:6',
            ])
            ->add('confirm-password', Field::PASSWORD, [                
               'rules' => 'required|min:6',
            ])
            ->add('phone1', Field::TEXT, [
                'rules' => 'required|min:11'
            ])
            ->add('phone2', Field::TEXT, [
                'rules' => 'required|min:11'
            ])
            ->add('dept_limit', Field::NUMBER, [
                'rules' => 'required|min:1'
            ])
            ->add('instrument_limit', Field::NUMBER, [
                'rules' => 'required|min:1'
            ])
            ->add('parameter_limit', Field::NUMBER, [
                'rules' => 'required|min:1'
            ])
            ->add('user_limit', Field::NUMBER, [
                'rules' => 'required|min:1'
            ])
            ->add('alarm_limit', Field::NUMBER, [
                'rules' => 'required|min:1'
            ])
            ->add('routes_limit', Field::NUMBER, [
                'rules' => 'required|min:1'
            ])
            // ->add('created_by', Field::TEXT, [
            //     'value' => 'super_admin'
            // ])
            ->add('roles', Field::HIDDEN, [
               'value' => 'plant_admin'
            ])
            ->add('submit', 'submit', ['label' => 'Submit','attr' => ['class' => 'btn btn-secondary']]);

    }
}


