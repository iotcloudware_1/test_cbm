<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;
use Kris\LaravelFormBuilder\Field;
use App\Model\Tool;
use DB;

class ToolForm extends Form
{
    public function buildForm()
    {
       $this
            
            ->add('serial_number', Field::TEXT, [
                'rules' => 'max:45'
            ])
            ->add('manufacturer', Field::TEXT, [
                'rules' => 'max:200'
            ])
            ->add('model', Field::TEXT, [
                'rules' => 'max:45'
            ])
            ->add('inspection_date', Field::TEXT, [
                'rules' => 'max:45'
            ])
             ->add('status', Field::TEXT, [
                'rules' => 'max:45'
            ])
           ->add('submit', 'submit', ['label' => 'Submit','attr' => ['class' => 'btn btn-secondary']]);
    }
}
