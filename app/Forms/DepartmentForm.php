<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;
use Kris\LaravelFormBuilder\Field;
use App\Model\Plant;
use DB;

class DepartmentForm extends Form
{
    public function buildForm()
    {
        //$plant = Plant::select('id')
                           //->get();
                           
    	$plant = DB::table('plants')->get();
 		$this
			->add('plant_id', 'entity', [
		        'class' => 'App\Model\Plant',
		        'property' => 'name',
		        'query_builder' => function ($plant) {
		            return $plant;
		        }
		    ])
            ->add('name', Field::TEXT, [
                'rules' => 'required|min:5'
            ])
            ->add('description', Field::TEXTAREA, [
                'rules' => 'max:5000'
            ])
          ->add('submit', 'submit', ['label' => 'Submit','attr' => ['class' => 'btn btn-secondary']]);

    } 
}


