<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;
use Kris\LaravelFormBuilder\Field;
use App\Model\Instrument;
use App\User;
use DB;



class RouteForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('sequence', Field::TEXT, [
                'rules' => 'required|min:5'
            ])
            ->add('timing', Field::TEXT, [
                'rules' => 'max:10'
            ])
            ->add('user_id', 'entity', [
                'class' => 'App\User',
                'property' => 'name',
                'query_builder' => function ($user) {
                    return $user;
                }
            ])
            

           ->add('submit', 'submit', ['label' => 'Submit','attr' => ['class' => 'btn btn-secondary']]);
    }
}