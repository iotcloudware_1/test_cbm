<?php 

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;
use Kris\LaravelFormBuilder\Field;
use App\Model\Department;
use DB;

class InstrumentForm extends Form
{
    public function buildForm() 
   { 
        //$plant = Plant::select('id')
                           //->get();
                            
    	$department = DB::table('departments')->get();
 		$this
			->add('department_id', 'entity', [
		        'class' => 'App\Model\Department',
		        'property' => 'name',
		        'query_builder' => function ($department) {
		            return $department;
		       }
		    ])
            
            ->add('name', Field::TEXT, [
                'rules' => 'max:45'
            ])
            ->add('description', Field::TEXT, [
                'rules' => 'max:200'
            ])
            ->add('type', Field::TEXT, [
                'rules' => 'max:45'
            ])
            ->add('location', Field::TEXT, [
                'rules' => 'max:45'
            ])
           ->add('submit', 'submit', ['label' => 'Submit','attr' => ['class' => 'btn btn-secondary']]);
    }
} 
