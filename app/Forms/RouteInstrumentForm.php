<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;
use Kris\LaravelFormBuilder\Field;
use App\Model\Instrument;
use App\User;
use DB;
 
class InstrumentFieldForm extends Form
{
    public function buildForm()
    {
                           
    	$instrument = DB::table('instruments')->get();
        $user = DB::table('users')->get();
        
 		$this
            ->add('instrument_id', 'entity', [
                'class' => 'App\Model\Instrument',
                'property' => 'name',
                'query_builder' => function ($instrument) {
                    return $instrument;
                }
            ]);
    }
} 

class RouteInstrumentForm extends Form
{
    public function buildForm()
    {
        $this
           
            ->add('tags', 'collection', [
                'label' => 'Select All instruments for this route',
                'type' => 'form',
                'options' => [
                    'class' => 'App\Forms\InstrumentFieldForm',
                    'label' => false,
                ],
                'prototype_name' => '__NAME__'
            ])

           ->add('submit', 'submit', ['label' => 'Submit','attr' => ['class' => 'btn btn-success']]);
    }
}