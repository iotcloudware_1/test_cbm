<?php
 
namespace App\Forms;
 
use Kris\LaravelFormBuilder\Form;
use Kris\LaravelFormBuilder\Field;
use App\User;
use DB;


class RegistrationForm extends Form
{
    public function buildForm()
    {
        // Add fields here...
        $register = DB::table('plants')->get();
 		$this
			->add('plant_id', 'entity', [
		        'class' => 'App\Model\Plant',
		        'property' => 'name',
		        'query_builder' => function ($register) {
		            return $register;
		       }
		    ])
            
            ->add('name', Field::TEXT, [
                'rules' => 'max:45'
            ])
            ->add('email', Field::TEXT, [
                'rules' => 'max:200'
            ])
            ->add('password', Field::PASSWORD, [
                'rules' => 'max:45'
            ])
             
            ->add('address', Field::TEXT, [
                'rules' => 'max:45'
            ])
            ->add('phone1', Field::TEXT, [
                'rules' => 'max:45'
            ])
            ->add('phone2', Field::TEXT, [
                'rules' => 'max:45'
            ])
            ->add('creadted_by', Field::TEXT, [
                'rules' => 'max:45'
            ])
            ->add('scope', Field::TEXT, [
                'rules' => 'max:45'
            ])
            ->add('dept_limit', Field::TEXT, [
                'rules' => 'max:45'
            ])
            ->add('instrument_limit', Field::TEXT, [
                'rules' => 'max:45'
            ])
            ->add('parameter_limit', Field::TEXT, [
                'rules' => 'max:45'
            ])
            ->add('user_limit', Field::TEXT, [
                'rules' => 'max:45'
            ])
            ->add('alarm_limit', Field::TEXT, [
                'rules' => 'max:45'
            ])
            ->add('routes_limit', Field::TEXT, [
                'rules' => 'max:45'
            ])
            ->add('submit', 'submit', ['label' => 'Save form']);
    }
}
