<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class WorkOrderHistory extends Model
{
	protected $table = "workorder_history";
    Protected $fillable = ['current_workorder_id','action','action_taken_by'];
}
