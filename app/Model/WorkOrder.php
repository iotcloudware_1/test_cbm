<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\Instrument;
class WorkOrder extends Model
{
    protected $table = "workorders";
    Protected $fillable = ['description', 'instrument_id','type','priority','send_email','send_sms','expected_time'];
    public function instrument()
	{
	    return $this->belongsTo('App\Model\Instrument');
	}
	public function workorderMedia()
    {
        return $this->belongsTo(WorkOrderMedia::class);
    }
}
