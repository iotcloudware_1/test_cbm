<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class RouteHistory extends Model
{
    protected $table = "route_history";
    Protected $fillable = ['route_id','operator_id','route_state','start_time','end_time','duration_min','instrument_skipped','parameter_skipped','sessions'];
}
