<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AssetStatus extends Model
{
	protected $table = "asset_status";
    Protected $fillable = ['asset_id','status_id',];
}
