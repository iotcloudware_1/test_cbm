<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ToolStatus extends Model
{
 	protected $table = "tool_status";
    Protected $fillable = ['tool_id','status_id',];
}
