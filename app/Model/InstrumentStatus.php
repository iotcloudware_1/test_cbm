<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class InstrumentStatus extends Model
{
    protected $table = "instrument_status";
    Protected $fillable = ['instrument_id','status_id',];
}
