<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class WorkorderMedia extends Model
{
    protected $table = "workorder_media";
    Protected $fillable = ['workorder_id','media_type','base_image'];
}
