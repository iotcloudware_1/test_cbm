<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use\App\Model\Line;
class Department extends Model
{
    protected $table = "departments";
    Protected $fillable = ['unit_id','name','description'];
    public function lines()
	{
	    return $this->hasMany('App\Model\Line');
	}
}
