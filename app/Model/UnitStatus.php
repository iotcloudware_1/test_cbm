<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UnitStatus extends Model
{
	protected $table = "unit_status";
    Protected $fillable = ['unit_id','status_id',];
}
