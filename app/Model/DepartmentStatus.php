<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class DepartmentStatus extends Model
{
    protected $table = "department_status";
    Protected $fillable = ['department_id','status_id',];
}
