<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\Plant;
use App\Model\Parameter;

class Instrument extends Model
{
    protected $table = "instruments";
    Protected $fillable = ['asset_id','qr_code','name','description','type','location','altitude','latitude','longitude'];

    public function getCode($id)
    {
    	$instrument_code = str_pad($id, 8, "0", STR_PAD_LEFT);
    	return $instrument_code;
    }
    public function parameters()
	{
	    return $this->hasMany('App\Model\Parameter');
	}
}
