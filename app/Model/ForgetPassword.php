<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ForgetPassword extends Model
{
    protected $table = "forget_password";
    Protected $fillable = ['user_id','email', 'token',];
}
