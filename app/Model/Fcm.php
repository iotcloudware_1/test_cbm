<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Fcm extends Model
{
    protected $fillable = [
        'user_id','token',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
