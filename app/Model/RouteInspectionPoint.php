<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class RouteInspectionPoint extends Model
{
 	protected $table = "route_inspection_point";
    Protected $fillable = ['instrument_id','parameter_id','route_id','order_no','inspection_time','distance_cover'];
}
