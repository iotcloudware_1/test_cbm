<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class RouteInspectionParameter extends Model
{
    	protected $table = "route_inspection_parameters";
    Protected $fillable = ['route_inspection_point_id','parameter_id','inspection_time_min'];
}
