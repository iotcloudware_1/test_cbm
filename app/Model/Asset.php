<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\Instrument;

class Asset extends Model
{
    protected $table = "assets";
    Protected $fillable = ['line_id','name','description'];
    
    public function instruments()
	{
	    return $this->hasMany('App\Model\Instrument');
	}
}
