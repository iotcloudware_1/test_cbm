<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Tool extends Model
{
     protected $table = "tools";
    Protected $fillable = ['name','serial_number','manufacturer','model','inspection_date'];
}
