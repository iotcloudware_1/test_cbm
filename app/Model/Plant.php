<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Plant extends Model
{
    protected $table = "plants";
    Protected $fillable = ['name','description',];
}
