<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ParameterValues extends Model
{
    protected $table = "parameter_values";
    Protected $fillable = ['parameter_id','value','altitude','latitude','longitude','distance_measured','user_id','skip_reason','type'];
}
