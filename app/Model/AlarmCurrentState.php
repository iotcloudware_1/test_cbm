<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\Alarm;

class AlarmCurrentState extends Model
{
	protected $table = "alarm_current_state";
	Protected $fillable = ['alarm_id','alarm_message','new_value','state','created_at'];

	public function alarm()
    {
        return $this->belongsTo('App\Model\Alarm');
    }
}
