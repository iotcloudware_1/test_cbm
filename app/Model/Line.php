<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use\App\Model\Asset;
class Line extends Model
{
	protected $table = "lines";
    Protected $fillable = ['department_id','name','description'];

    public function assets()
	{
	    return $this->hasMany('App\Model\Asset');
	}
}
