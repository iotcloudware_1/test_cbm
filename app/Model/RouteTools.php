<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class RouteTools extends Model
{
 	protected $table = "route_tools";
    Protected $fillable = ['tool_id','route_id'];
}
