<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class LineStatus extends Model
{
    protected $table = "line_status";
    Protected $fillable = ['line_id','status_id',];
}
