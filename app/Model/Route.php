<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Route extends Model 
{
     protected $table = "routes";
    Protected $fillable = ['operator_id','description','period','origin_time','created_at'];
}
