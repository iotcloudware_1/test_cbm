<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\Alarm;
class Parameter extends Model
{
    protected $table = "parameters";
    Protected $fillable = ['instrument_id','tool_id','name','description','type','entry','data_type','max_value','min_value','engrineering_unit'];

	public function hasAlarm()
	{
	    return $this->hasMany('App\Model\Alarm');
	}
	
	public function instrument()
 	{
    return $this->belongsTo('App\Model\Instrument');
 	}

}

