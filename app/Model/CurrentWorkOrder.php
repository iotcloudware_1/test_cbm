<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CurrentWorkOrder extends Model
{
    protected $table = "current_workorder";
    Protected $fillable = ['workorder_id','priority','remarks','base_image','assigned_by','assigned_to','status','acknowledgement'];
}
