<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\AlarmCurrentState;
class Alarm extends Model
{
    protected $table = "alarms";
    Protected $fillable = ['parameter_id','alarm_message','condition','setpoint','priority','created_at'];

	public function alarms()
	{
	    return $this->hasOne('App\Model\AlarmCurrentState');
	}
}