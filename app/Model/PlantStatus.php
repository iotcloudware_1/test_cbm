<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PlantStatus extends Model
{
    protected $table = "plant_status";
    Protected $fillable = ['plant_id','status_id',];
}
