<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Deviation extends Model
{
    protected $table = "deviations";
    Protected $fillable = ['user_id','deviation','actual','record','created_at'];
}
