<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use\App\Model\Department;
class Unit extends Model
{
    protected $table = "units";
    Protected $fillable = ['plant_id','name','description'];

    public function departments()
	{
	    return $this->hasMany('App\Model\Department');
	}
}
