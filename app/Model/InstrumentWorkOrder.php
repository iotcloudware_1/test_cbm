<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class InstrumentWorkOrder extends Model
{
    protected $table = "instrument_workorder";
    Protected $fillable = ['instrument_id','workorder_id',];
}
