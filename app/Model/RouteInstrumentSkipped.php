<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class RouteInstrumentSkipped extends Model
{
   protected $table = "route_instrument_skipped";
}
