<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class RegisterRoute extends Model
{
	protected $table = "register_route";
    Protected $fillable = ['route_id','month','date','origin_time'];

}