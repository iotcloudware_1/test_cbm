<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SkipReason extends Model
{
	protected $table = "skip_reasons";
    Protected $fillable = ['name','description','priority','type'];
}
