<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AlarmHistory extends Model
{
    protected $table = "alarm_history";
	Protected $fillable = ['alarm_current_state_id','old_state','new_state','state_ack','user_id','created_at'];
}
