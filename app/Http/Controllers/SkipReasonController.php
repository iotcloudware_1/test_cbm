<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Permission;
use App\User;
use App\Model\SkipReason;
use App\Model\RouteInstrumentSkipped; 
use DB;
use Hash;
use App\Post;
use Yajra\Datatables\Datatables;
use Response;
class SkipReasonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    function __construct()
    {
        $this->middleware('permission:skipreason-list|skipreason-create|skipreason-edit|skipreason-delete', ['only' => ['index','store']]);
         
        $this->middleware('permission:skipreason-create', ['only' => ['create','store']]);
        $this->middleware('permission:skipreason-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:skipreason-delete', ['only' => ['destroy']]);
        
        return view('permission-error');
    }

    public function index(Request $request)
    {
        if($request->ajax())
        {
            $data = SkipReason::latest()->get();
            return Datatables::of($data)
            ->addColumn('action', 'datatables.action-button')
            ->rawColumns(['action'])
            ->make(true);
        }    
        return view('skipreasons.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('skipreasons.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'description' => 'required',
            'priority' => 'required',
            'type' => 'required',
        ]);

        $input = $request->all();
        
        $skipreason = SkipReason::create($input);

        return redirect()->route('skipreasons.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show($id)
    {
        $skipreason = SkipReason::find($id);
        return view('skipreasons.show',compact('skipreason'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $skipreason = SkipReason::find($id);
        return view('skipreasons.edit',compact('skipreason'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'description' => 'required',
            'priority' => 'required',
            'type' => 'required',
        ]);

        $skipreason = SkipReason::find($id);
        $skipreason->name = $request->input('name');
        $skipreason->description = $request->input('description');
        $skipreason->priority = $request->input('priority');
        $skipreason->type = $request->input('type');
        $skipreason->save();


        return redirect()->route('skipreasons.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         DB::table("skip_reasons")->where('id',$id)->delete();
        return redirect()->route('skipreasons.index')
        ->with('success','Skip Reason deleted successfully');
    }
    public function getSkipReasons()
    {
        $skip_reasons = SkipReason::select('id','name')->get()->toArray();
        if($skip_reasons == NULL)
        {
            $response = '[{"id":0,"name":"","message":"No Skip Reason Found"}]';
            return $response;
        }
        else
        {
            $skip_reasons_array=[];
            foreach ($skip_reasons as $key => $skip_reason) {
               $skipreason = (array)$skip_reason;
               $skipreason['message'] = '';
               array_push($skip_reasons_array, $skipreason);
            }
            return(json_encode($skip_reasons_array));
        }
        
    }

    //For Phone DB Sync
    public function getAllSkipReasons()
    {
        return SkipReason::all()->toJson();
    }

    public function getInspectionSkipReasons(Request $request)
    {
            $route_instrument_skipped= new RouteInstrumentSkipped;
            $route_instrument_skipped->route_history_id= $request['route_history_id'];
            $route_instrument_skipped->instrument_id =$request['instrument_id'];
            $route_instrument_skipped->skipped_id =$request['instrument_skip_reason_id'];
            $route_instrument_skipped->save();

            $response='successfully';

            return $response;
    }
}
