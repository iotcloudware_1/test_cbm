<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\FormBuilder;
use App\Forms\DepartmentForm;
use App\Http\Controllers\Controller;
use App\Model\Line;
use App\Model\Asset;
use App\Model\Instrument;
use App\Model\Parameter;
use App\Model\AlarmCurrentState;
use App\Model\Department;
use App\Model\Unit;
use App\Post;
use App\User;
use Auth;
use Yajra\Datatables\Datatables;
use DB;
use Response;
Use Exception;
use Hash;
use Helper;

class DepartmentController extends Controller 
{
    /*
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
   
    function __construct()
    {
         $this->middleware('permission:department-list|department-create|department-edit|department-delete', ['only' => ['index','store']]);
         
         $this->middleware('permission:department-create', ['only' => ['create','store']]);
         $this->middleware('permission:department-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:department-delete', ['only' => ['destroy']]);
         return view('permission-error');
    }

    /*
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
        {

            if($request->ajax())
            {
                $data = Department::latest()->get();

                foreach ($data as $key => $value) {
                    $data[$key]['unit_id']= Helper::unitIdToName($value['unit_id']);
                }


                return Datatables::of($data)
                  ->addColumn('action', 'datatables.action-button')
                  ->rawColumns(['action'])
                  ->make(true);
            }    
             return view('departments.index');
        }


    /*
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        $unit=Unit::all()->pluck('name','id');
        return view('departments.create', compact('unit'));
    }


    /*
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $this->validate($request, [
            'unit_id' => 'required',
            'name' => 'required',

        ]);
           
            
            $input = $request->all();   

            $input['user_id'] = Auth::user()->id;
            $department_exist = Department::count();
            $department_limit = User::where('id',Auth::user()->id)->value('dept_limit');


            if($department_limit > $department_exist)
            {
                Department::create($input);
                return redirect('/departments');
            }
            else
            {
                // return redirect('/departments')->with('msg','Limit reached. Please contact Administrator for adding more department');
                echo("<script>
                        alert('Limit Reached');
                        window.location.href = 'departments'
                    </script>");
            }
            
    }


     /*
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $department = Department::find($id);
        $department->unit_id = Helper::unitIdToName($department->unit_id);
        return view('departments.show',compact('department'));
    }

 
    /*
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $unit=Unit::all()->pluck('name','id');
        $department = Department::find($id);
        return view('departments.edit',compact('department','unit'));
    }


    // *
    //  * Update the specified resource in storage.
    //  *
    //  * @param  \Illuminate\Http\Request  $request
    //  * @param  int  $id
    //  * @return \Illuminate\Http\Response
     
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'unit_id' => 'required',
            'name' => 'required',

        ]);


        $department = Department::find($id);
        $department->unit_id = $request->input('unit_id');
        $department->name = $request->input('name');
        $department->description = $request->input('description');
        $department->save();


        return redirect()->route('departments.index');
    }
    /*
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try
        {
            DB::table("departments")->where('id',$id)->delete();
            return redirect()->route('departments.index');
        }
        catch(Exception $e)
        {
            $err = json_encode($e);
            $err_array = json_decode($err,true);
            if($err_array['errorInfo'][1] == 1451)
            {
                return Response::json(array('success' => false, 'msg_string' => 'Department is in use.'), 500);
            }
          
        } 
    }

    //Function for fetching department data and send it to the api
    public function getDepartment()
    {
        $departments = Department::all()->toJson();
        return $departments;
    }

    public function getDepartmentDashoard(Request $request)
    {   
        
            $show = 'specific'; 
            $departments = Department::where('unit_id',$request['id'])->get();
            return view('dashboardtree.departments',compact('departments','show'));
        
    }
    public function showUnitDepartment(Request $request)
    {
        $unit_name= Helper::unitIdToName($request['id']);

        $show_unit_department= Department::where('unit_id',$request['id'])->get();
        $alarms_arr=[];
        $department_id;

             foreach ($show_unit_department as $key => $department) 
             {
                $department_id = $department['id'];
                $alarm_count = 0;
                $healthy_count=0; 
                $lines= Department::find($department['id'])->lines; 
                foreach ($lines as $key => $line) 
                {
                    $assets= Line::find($line->id)->assets; 

                    foreach ($assets as $key => $asset) 
                    {
                        $instruments= Asset::find($asset->id)->instruments;

                        foreach ($instruments as $instrument) 
                        {
                            $parameters= Instrument::find($instrument->id)->parameters;

                            foreach ($parameters as $parameter) 
                            {
                                $alarms= Parameter::find($parameter->id)->hasAlarm;
                                //dd($alarms);
                                if($alarms != null)
                                {
                                    foreach ($alarms as $key => $alarm)
                                    {
                                        $current_alarms= AlarmCurrentState::where('alarm_id',$alarm->id)->where('state','alarm')->count();
                                        $healthy_alarms= AlarmCurrentState::where('alarm_id',$alarm->id)->where('state','normal')->count();
                                        $alarm_count = $alarm_count + $current_alarms;
                                        $healthy_count = $healthy_count + $healthy_alarms;
                                    }
                                }
                                else
                                {
                                    $alarm_count = $alarm_count;
                                    $healthy_count = $healthy_count;

                                }    
                            }   
                        }
                    }                    
                }
                    $temp = array('departments_id' =>$department_id,'alarm_count'=>$alarm_count,'healthy_count'=>$healthy_count);
                    array_push($alarms_arr, $temp);                
             }   
                foreach ($show_unit_department as $key => $department) 
                {
                    $show_unit_department[$key]['alarm_count'] = $alarms_arr[$key]['alarm_count'];
                    $show_unit_department[$key]['healthy_count'] = $alarms_arr[$key]['healthy_count'];

                }                              

        foreach($show_unit_department as $key => $value) 
          {   
            
            $show_unit_department[$key]['unit_id']= Helper::unitIdToName($value['unit_id']);
          }  
        
        return view('dashboardtree.departments',compact('show_unit_department','unit_name'));
    } 
    public function allDepartments()
    { 
        
        $alldepartments = Department::all();
        $unit_name='Units';
        $alarms_arr=[];
        $department_id;

             foreach ($alldepartments as $key => $department) 
             {
                $department_id = $department['id'];
                $alarm_count = 0;
                $healthy_count=0; 
                $lines= Department::find($department['id'])->lines; 
                foreach ($lines as $key => $line) 
                {
                    $assets= Line::find($line->id)->assets; 

                    foreach ($assets as $key => $asset) 
                    {
                        $instruments= Asset::find($asset->id)->instruments;

                        foreach ($instruments as $instrument) 
                        {
                            $parameters= Instrument::find($instrument->id)->parameters;

                            foreach ($parameters as $parameter) 
                            {
                                $alarms= Parameter::find($parameter->id)->hasAlarm;
                                //dd($alarms);
                                if($alarms != null)
                                {
                                    foreach ($alarms as $key => $alarm)
                                    {
                                        $current_alarms= AlarmCurrentState::where('alarm_id',$alarm->id)->where('state','alarm')->count();
                                        $healthy_alarms= AlarmCurrentState::where('alarm_id',$alarm->id)->where('state','normal')->count();
                                        $alarm_count = $alarm_count + $current_alarms;
                                        $healthy_count = $healthy_count + $healthy_alarms;
                                    }
                                }
                                else
                                {
                                    $alarm_count = $alarm_count;
                                    $healthy_count = $healthy_count;

                                }    
                            }   
                        }
                    }                    
                }
                    $temp = array('departments_id' =>$department_id,'alarm_count'=>$alarm_count,'healthy_count'=>$healthy_count);
                    array_push($alarms_arr, $temp);                
             }   
                foreach ($alldepartments as $key => $department) 
                {
                    $alldepartments[$key]['alarm_count'] = $alarms_arr[$key]['alarm_count'];
                    $alldepartments[$key]['healthy_count'] = $alarms_arr[$key]['healthy_count'];

                }            
        foreach ($alldepartments as $key => $value) 
          {   
            
            $alldepartments[$key]['unit_id']= Helper::unitIdToName($value['unit_id']);
          } 
        return view('dashboardtree.alldepartments',compact('alldepartments','unit_name'));

    }

}



  