<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Model\Role;
use DB;
use Hash;
use App\Post;
use Yajra\Datatables\Datatables;
use Response;
use Illuminate\Support\Str;
use Auth;
use App\Model\UserGroup;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     function __construct()
    {
         $this->middleware('permission:user-list|user-create|user-edit|user-delete', ['only' => ['index','store']]);
         $this->middleware('permission:user-create', ['only' => ['create','store']]);
         $this->middleware('permission:user-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:user-delete', ['only' => ['destroy']]);

    }
    public function index(Request $request)
    {
        if($request->ajax())
        {
            $data = User::latest()
                    ->whereNotIn('role',['plant_admin','super_admin'])
                    ->get();
            return Datatables::of($data)
            ->addColumn('action', 'datatables.action-button')
            ->rawColumns(['action'])
            ->make(true);
        }    
        return view('users.index');
        //     ->with('i', ($request->input('page', 1) - 1) * 5);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::pluck('name','name')->all();
        return view('users.create',compact('roles'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|same:confirm-password',
            'roles' => 'required',

        ]);


        $input = $request->all();
        $input['password'] = Hash::make($input['password']);
        $input['api_token'] = Str::random(60);
        $input['role'] = $request->input('roles');
        $input['created_by'] = Auth::user()->name;
        $user = User::create($input);
        $user->assignRole($request->input('roles'));

        if($request->input('roles') == 'plant_admin')
        {
            return redirect()->route('plantadmin.index')
                        ->with('success','Plant Admin created successfully');
        }
        else
        {
            return redirect()->route('users.index')
                        ->with('success','Operator created successfully');
        }

    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        return view('users.show',compact('user'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $roles = Role::pluck('name','name')->all();
        $userRole = $user->roles->pluck('name','name')->all();


        return view('users.edit',compact('user','roles','userRole'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,'.$id,
            'password' => 'same:confirm-password',
        ]);


        $input = $request->all();
        $user = User::find($id);
        $user->update($input);
        DB::table('model_has_roles')->where('model_id',$id)->delete();


        $user->assignRole($request->input('roles'));


        return redirect()->route('users.index')
                        ->with('success','Operator updated successfully');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::where('id',$id)->delete();
     
        return Response::json($user);
    }
    
    public function phoneLogin(Request $request)
    {
        \Log::info($request);
        $users = User::all();

        foreach ($users as $key => $user) {
            if($user->email == $request['user'] && password_verify($request['password'], $user->password))
            {
                $data = array('id' => $user->id, 'name' => $user->name, 'role' => $user->role, 'token' => $user->api_token );
                return json_encode($data);
            }

        }

        $response = array('message' => 'Wrong Email or Password' );
        return json_encode($response);

    }

    public function getOperators(Request $request)
    {
        $operators= User::where('role','operator')->select('id','name','email','api_token')->get();
        if (1==2) 
        {
          return '[{"id":0,"name":"","email":"","api_token":"","message":"No operator found."}]';
        }
        else
        {
            foreach ($operators as $key => $operator)
            {
                $operators[$key]['message']='';
            }
        }
        return json_encode($operators);
    }

    public function getAllUsers()
    {
        $users  = User::all();
        return json_encode($users);
    }

    public function addUsersInGroup(Request $request)
    {
       $users= $request['users'];
       foreach($users as $user)
       {
            $user_group = UserGroup::create([
                'user_id'=> $user['user_id'],             
                'group_id' => $request['group_id']
            ]);
       }

       return response()
            ->json([
                'error'   => false, 
                'message' => 'Users added in group',
            ]);   
    }   
}

