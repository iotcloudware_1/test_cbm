<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Permission;
use App\User;
use App\Model\Instrument;
use App\Model\InstrumentStatus;
use App\Model\Status;
use DB;
use Hash;
use App\Post;
use Yajra\Datatables\Datatables;
use Response;
use Helper;


class InstrumentStatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    function __construct()
    {
         $this->middleware('permission:instrumentstatus-list|instrumentstatus-create|instrumentstatus-edit|instrumentstatus-delete', ['only' => ['index','store']]);
         
         $this->middleware('permission:instrumentstatus-create', ['only' => ['create','store']]);
         $this->middleware('permission:instrumentstatus-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:instrumentstatus-delete', ['only' => ['destroy']]);
         return view('permission-error');
    }
   public function index(Request $request)
        {

            if($request->ajax())
            {
                $data = InstrumentStatus::latest()->get();

                foreach ($data as $key => $value) 
                {
                    $data[$key]['instrument_id']= Helper::instrumentIdToName($value['instrument_id']);
                }
                foreach ($data as $key => $value) 
                {
                    $data[$key]['status_id']= Helper::statusIdToDes($value['status_id']);
                }


                return Datatables::of($data)
                  ->addColumn('action', 'datatables.action-button')
                  ->rawColumns(['action'])
                  ->make(true);
            }    
             return view('instrumentstatus.index');
        }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $instrument=Instrument::all()->pluck('name','id');
        $status=Status::all()->pluck('description','id');
        return view('instrumentstatus.create', compact('instrument','status'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
       {
        $this->validate($request, [
            'instrument_id' => 'required',
            'status_id' => 'required',
        ]);

        $input = $request->all();
        
        $instrumentstatus = InstrumentStatus::create($input);

        return redirect()->route('instrumentstatus.index');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $instrumentstatus = InstrumentStatus::find($id);
        $instrumentstatus->instrument_id = Helper::instrumentIdToName($instrumentstatus->instrument_id);
        $instrumentstatus->status_id = Helper::statusIdToDes($instrumentstatus->status_id);
        return view('instrumentstatus.show',compact('instrumentstatus'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $instrument=Instrument::all()->pluck('name','id');
        $status=Status::all()->pluck('description','id');
        $instrumentstatus = InstrumentStatus::find($id);
        return view('instrumentstatus.edit',compact('instrumentstatus','status','instrument'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'instrument_id' => 'required',
            'status_id' => 'required',

        ]);

        $instrumentstatus = InstrumentStatus::find($id);
        $instrumentstatus->instrument_id = $request->input('instrument_id');
        $instrumentstatus->status_id = $request->input('status_id');
        $instrumentstatus->save();


        return redirect()->route('instrumentstatus.index')
                        ->with('success','Instrument Status updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table("instrument_status")->where('id',$id)->delete();
    }
}
