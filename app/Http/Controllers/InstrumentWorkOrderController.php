<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Permission;
use App\User;
use App\Model\Instrument;
use App\Model\InstrumentWorkOrder;
use App\Model\WorkOrder;
use DB;
use Hash;
use App\Post;
use Yajra\Datatables\Datatables;
use Response;
use Helper;


class InstrumentWorkOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
        function __construct()
    {
         $this->middleware('permission:instrumentworkorder-list|instrumentworkorder-create|instrumentworkorder-edit|instrumentworkorder-delete', ['only' => ['index','store']]);
         
         $this->middleware('permission:instrumentworkorder-create', ['only' => ['create','store']]);
         $this->middleware('permission:instrumentworkorder-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:instrumentworkorder-delete', ['only' => ['destroy']]);
         return view('permission-error');
    }
    public function index(Request $request)
    {
        if($request->ajax())
        {
            $data = InstrumentWorkOrder::latest()->get();

            foreach ($data as $key => $value) 
            {
                $data[$key]['instrument_id']= Helper::instrumentIdToName($value['instrument_id']);
            }
            foreach ($data as $key => $value) 
            {
                $data[$key]['workorder_id']= Helper::WorkOrderIdToName($value['workorder_id']);
            }


            return Datatables::of($data)
              ->addColumn('action', 'datatables.action-button')
              ->rawColumns(['action'])
              ->make(true);
        }  

        return view('instrumentworkorder.index');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $instrument=Instrument::all()->pluck('name','id');
        $workorder=WorkOrder::all()->pluck('name','id');
        return view('instrumentworkorder.create', compact('instrument','workorder'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'instrument_id' => 'required',
            'workorder_id' => 'required',
        ]);

        $input = $request->all();
        
        $instrument_workorder = InstrumentWorkOrder::create($input);

        return redirect()->route('instrumentworkorder.index')
            ->with('success','Workorder created successfully');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $instrument_workorder = InstrumentWorkOrder::find($id);
        $instrument_workorder->instrument_id = Helper::instrumentIdToName($instrument_workorder->instrument_id);
        $instrument_workorder->workorder_id = Helper::WorkOrderIdToName($instrument_workorder->workorder_id);
        return view('instrumentworkorder.show',compact('instrumentworkorder'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
     {
        $instrument=Instrument::all()->pluck('name','id');
        $workorder=WorkOrder::all()->pluck('name','id');
        $instrument_workorder = InstrumentWorkOrder::find($id);
        return view('instrumentworkorder.edit',compact('instrument','workorder','instrument_workorder'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'instrument_id' => 'required',
            'workorder_id' => 'required',
        ]);

        $instrument_workorder = InstrumentWorkOrder::find($id);
        $instrument_workorder->instrument_id = $request->input('instrument_id');
        $instrument_workorder->workorder_id = $request->input('workorder_id');
        $instrument_workorder->save();


        return redirect()->route('instrumentworkorder.index')
                        ->with('success','Instrument Workorder updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table("instrument_workorder")->where('id',$id)->delete();
    }
}
