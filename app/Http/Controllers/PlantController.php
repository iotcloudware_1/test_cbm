<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Permission;
use App\User;
use App\Model\Plant;
use DB;
use Hash;
use App\Post;
use Yajra\Datatables\Datatables;
use Response;

class PlantController extends Controller
{
    /*
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
   
    function __construct()
    {
         $this->middleware('permission:plant-list|plant-create|plant-edit|plant-delete', ['only' => ['index','store']]);
         
         $this->middleware('permission:plant-create', ['only' => ['create','store']]);
         $this->middleware('permission:plant-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:plant-delete', ['only' => ['destroy']]);
         return view('permission-error');
    }

    /*
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
 
        if($request->ajax())
        {
            $data = Plant::latest()->get();
            return Datatables::of($data)
            ->addColumn('action', 'datatables.action-button')
            ->rawColumns(['action'])
            ->make(true);
        }    
        return view('plants.index');


        //     ->with('i', ($request->input('page', 1) - 1) * 5);
    }


    /*
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('plants.create');
    }


    /*
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);

        $input = $request->all();
        
        $plant = Plant::create($input);

        return redirect()->route('plants.index')
                        ->with('success','Plant created successfully');
    }
     /*
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $plant = Plant::find($id);
        return view('plants.show',compact('plant'));
    }


    /*
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $plant = Plant::find($id);
        return view('plants.edit',compact('plant'));
    }


    // *
    //  * Update the specified resource in storage.
    //  *
    //  * @param  \Illuminate\Http\Request  $request
    //  * @param  int  $id
    //  * @return \Illuminate\Http\Response
     
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);


        $plant = Plant::find($id);
        $plant->name = $request->input('name');
        $plant->description = $request->input('description');
        $plant->save();


        return redirect()->route('plants.index')
                        ->with('success','Plant updated successfully');
    }
    /*
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table("plants")->where('id',$id)->delete();
        return redirect()->route('plants.index')
        ->with('success','Plant deleted successfully');
    }
    public function dashboard(Request $request)
    {
        return view('Dasboard.index');
    } 
}