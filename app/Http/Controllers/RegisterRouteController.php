<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Model\RegisterRoute;
use App\Model\Route;
use Carbon\Carbon;
use App\Jobs\RouteRegister;
class RegisterRouteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $operator = User::where('role','operator')->pluck('name','id');
        return view('routes.register_route', compact('operator'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      //$route_id = $request['route_id'];
      $operator_id = $request['operator_id'];
      $description = $request['description'];
      $period = $request['period'];
      $origin_time = $request['origin_time'];

      
     // \Log::info($operator_id);
     //  \Log::info($description);
     //  \Log::info($period);
     //  \Log::info($origin_time);
      
      $route = Route::create([
        'operator_id'=>$operator_id,
        'description'=>$description,
        'period'=>$period,
        'origin_time'=>$origin_time,
        ]);
       $route_id=$route->id;
       
        
        $day=1;
        $year = Carbon::now()->year;
        $current_year = Carbon::now()->year;
        $date=$year.'-01-01 '.$origin_time;
    
      while($year==$current_year) 
      {    
        $month=Carbon::parse($date)->month;
        
        $data = array('route_id' =>$route_id ,'month' =>$month,'date'=>$date);
        RouteRegister::dispatch($data);
        $date = Carbon::parse($date)->addHour($period);
        $day=Carbon::parse($date)->day;  //date carbon 
        $current_year = Carbon::parse($date)->year;         
      }
    
  }
  
    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
