<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\WorkOrder;
use App\Model\InstrumentWorkOrder;
use App\Model\Instrument; 
use App\Model\CurrentWorkOrder;
use App\Model\WorkOrderHistory; 
use App\Post;
use App\User;
use Auth;
use Yajra\Datatables\Datatables;
use DB;
use Response;
Use Exception;
use Hash;
use Helper;

class CurrentWorkOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    // function __construct()
    // {
    //      $this->middleware('permission:reportfault-list|reportfault-create', ['only' => ['index','store']]);

    //      return view('permission-error');
    // }

    public function index (Request $request)
    {
        if($request->ajax())
        {
            $workorders = CurrentWorkOrder::latest()
                ->where('status','open')
                ->get();

            foreach ($workorders as $key => $value) {
                $workorders[$key]['instrument_name']= Helper::WorkOrderIdToInstrumentName($value['workorder_id']);
                $workorders[$key]['workorder_id']= Helper::WorkOrderIdToDescription($value['workorder_id']);
                $workorders[$key]['assigned_by']= Helper::userIdToName($value['assigned_by']);
                $workorders[$key]['assigned_to']= Helper::userIdToName($value['assigned_to']);
            } 
            return Datatables::of($workorders)
                ->addColumn('action', 'datatables.current-workorder-button')
                ->rawColumns(['action'])
                ->setRowClass('@if($priority=="high") 
                    {{ "alert-danger" }} 
                    @elseif($priority=="medium")
                    {{"alert-warning"}}
                    @endif 
                    ')
                ->editColumn('acknowledgement', function($acknowledgement){
                    if($acknowledgement->acknowledgement == 'acknowledged'){
                       return "<i style='color:black' class='fas fa-check-circle'> </i>";

                    }
                    else{
                        return "<i style='color:black' class='fas fa-exclamation-triangle'> </i>";
                    }
                })
                ->escapeColumns([])
                ->make(true);
        }
        return view('currentworkorder.index');
    }

    public function getAllCurrentWorkOrders()
    {
        return CurrentWorkOrder::all()->toJson();
    }
}
