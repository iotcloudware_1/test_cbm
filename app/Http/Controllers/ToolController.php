<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Tool;
use Auth;
Use Exception;
use Yajra\Datatables\Datatables;
use Response;
use App\User;
use DB;
use Hash;
use App\Post;
use Kris\LaravelFormBuilder\FormBuilder;
use App\Forms\ToolForm;

class ToolController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
         $this->middleware('permission:tool-list|tool-create|tool-edit|tool-delete', ['only' => ['index','store']]);
         
         $this->middleware('permission:tool-create', ['only' => ['create','store']]);
         $this->middleware('permission:tool-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:tool-delete', ['only' => ['destroy']]);
         return view('permission-error');
    }
   public function index(Request $request)
   {

        if($request->ajax())
        {
            $data = Tool::latest()->get();
            return Datatables::of($data)
            ->addColumn('action', 'datatables.action-button')
            ->rawColumns(['action'])
            ->make(true);
        }    
        return view('tools.index');
         // $data = dep::latest()->get();
         // dd($data);

        //     ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       

        return view('tools.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'serial_number' => 'required',
            'manufacturer' => 'required',
            'model' => 'required',
         
        ]);

        $input = $request->all(); 
        $input['user_id'] = Auth::user()->id;

        $tool = Tool::create($input);
 

        return redirect()->route('tools.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        $tool = Tool::find($id);
            
        

        // echo($parameterList);

        return view('tools.show',compact('tool'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $tool = Tool::find($id);
        return view('tools.edit',compact('tool'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'serial_number' => 'required', 
            'manufacturer' => 'required',
            'model' => 'required',
              

        ]);


        $tool = Tool::find($id);
        $tool->name = $request->input('name');
        $tool->serial_number = $request->input('serial_number');
        $tool->manufacturer = $request->input('manufacturer');
        $tool->model = $request->input('model');
        
        $tool->save();


        return redirect()->route('tools.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try
        {
            DB::table("tools")->where('id',$id)->delete();
            return redirect()->route('tools.index')
            ->with('success','Tool deleted successfully');
        }
        catch(Exception $e)
        {
            $err = json_encode($e);
            $err1 = json_decode($err,true);
            if($err1['errorInfo'][1] == 1451)
            {
                return Response::json(array('success' => false, 'msg_string' => 'tools is in use.'), 500);
            }
          
        } 
    }

    public function getTools()
    {
        $tools = Tool::all();

        return $tools;
    }
}
