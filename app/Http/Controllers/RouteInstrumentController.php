<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Permissio;
use App\User;
use App\Model\Route;
use App\Model\RoteInspectionPoint;
use DB;
use Hash;
use App\Post;
use Yajra\Datatables\Datatables;
use Response;
use Kris\LaravelFormBuilder\FormBuilder;
use App\Forms\RouteForm;
use Auth;


class RouteInstrumentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function index(FormBuilder $formBuilder, $id)
    // {
    //      //$form = Route::find($id);
    //      $form = $formBuilder->create('App\Forms\RouteInstrumentForm', [
    //         'method' => 'POST',
    //         'url' => route('routes.store')
    //     ]);
        
    //     return view('routes.show-instrument-point',compact('form'));
    // }

     
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $route = RoteInspectionPoint::find($id);
     
        $routeInstrument = RoteInspectionPoint::select('instrument_id','parameter_id','order_no','time','route_id')->where('route_id',$id)->get();
        //dd($routeInstrument);
       //      ->where("instrument_route.route_id",$id)
       //      ->get(['id','instruments.id']);
       // // dd($routeInstrument);
        return view('routes.show-instrument-point',compact('route','routeInstrument'));
    }

    



    
}
