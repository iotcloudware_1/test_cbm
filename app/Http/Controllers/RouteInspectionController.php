<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Permissio;
use App\User;
use App\Model\Route;
use App\Model\Instrument;
use App\Model\Parameter;
use App\Model\RouteInspectionPoint;
use App\Model\RouteInspectionParameter;
use DB;
use Hash;
use App\Post;
use Yajra\Datatables\Datatables;
use Response;
use Kris\LaravelFormBuilder\FormBuilder;
use App\Forms\RouteForm;
use Auth;


class RouteInspectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax())
        {
            $data = Route::latest()->get();
           
            return Datatables::of($data)
            ->addColumn('action', 'datatables.route-action-button-datatable')
            ->rawColumns(['action'])
            ->make(true);
        }    
        return view('routes.index');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $instrument= Instrument::all();
        $users= User::all('name');
        $route= Route::all('sequence');
    
        return view('routes.create',compact('instrument','users','parameter'));
    }
    public function findparameter(Request $request)
    {
        $data= Parameter::select('name','id')
            ->where('instrument_id', $request->id)
            ->take(100)
            ->get();
        return response()->json($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $latestroute = Route::get()->orderBy('created_at','desc')->first()->value('id');
        echo($latestroute);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getparameter(Request $request)
    {

        $parameter = Parameter::get()->toJson();
        return $parameter;
    }

    public function selectparameter()
    {
        $iparameters = Parameter::get();
        return view('routes.select',compact('parameters'));
    }


    // test api for test.vue
    public function getInstruments()
    {
        $data = DB::table('instruments')
            ->rightJoin('parameters', 'instruments.id', '=', 'parameters.instrument_id')
            ->get();
        return response()->json($data);
    }
   
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function getParameters(Request $request)
    {
        $data = Parameter::where('instrument_id', $request->instrument_id)->get();
        return response()->json($data);
    }

    public function getAllInspectionPoints()
    {
        return RouteInspectionPoint::all()->toJson();
    }
    public function getInstrumentId(Request $request)
    {

        $inspection_point_id = $request['data'];
        return RouteInspectionPoint::find($inspection_point_id)->value('instrument_id');
    }
    public function updateInspectionPointOrder(Request $request)
    {
        $inpection_points = $request['inspection_points'];

        foreach ($inpection_points as $key => $inspection_point) 
        {
            $affectedRows = RouteInspectionPoint::where('id', $inspection_point['id'])->update(array('order_no' => $inspection_point['order_no']));
        }

    }
    public function showInspectionPointParam(Request $request)
    {
        $route_inspection_parameter=RouteInspectionParameter::where('route_inspection_point_id',$request['data'])->get();
        return json_encode($route_inspection_parameter);
        // dd(json_encode(($route_inspection_parameter)));
    }


}
