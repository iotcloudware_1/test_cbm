<?php

namespace App\Http\Controllers;
 
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Model\Instrument;
use DB;
use Hash;
use App\Post;
use Yajra\Datatables\Datatables;
use Response;
use Kris\LaravelFormBuilder\FormBuilder;
use App\Forms\InstrumentForm;
use Auth;
Use Exception;
use App\Model\Parameter;
use Helper;
class QrcodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */



function __construct()
    {
         $this->middleware('permission:qrcode-list|qrcode-index|qrcode-create|qrcode-edit|qrcode-delete', ['only' => ['index','store']]);
         
         $this->middleware('permission:qrcode-create', ['only' => ['create','store']]);
         $this->middleware('permission:qrcode-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:qrcode-delete', ['only' => ['destroy']]);
         return view('permission-error');
    }
    
    public function printAllQr(Request $request)
    {
        $instruments_id = $request['instruments'];
        $instruments = Instrument::whereIn('id',$instruments_id)->get();
        $instruments_description = $request['instruments'];
        $instruments = Instrument::whereIn('id',$instruments_description)->get();
        $allQrCodes=[];

        foreach ($instruments as $key => $instrument)
        {
            $instrument_description = Instrument::where('id',$instrument->id)->value('description');

            $qrCode = array('instrument_id' => $instrument->id, 'instrument_name' => $instrument->name,'instrument_description' => $instrument_description,'qr_code' => Helper::getInstrumentQrCode($instrument->qr_code)); 
            $qrCodeJson = json_encode($qrCode); 
            array_push($allQrCodes, '['.$qrCodeJson.']');
            
        }

        return view('qrcodes.show',compact('allQrCodes'));
    }
    public function slip()
    {
         return view('qrcodes.slip');
    }
     public function slip2()
    {
         return view('qrcodes.slip2');
    }
     public function slip3()
    {
         return view('qrcodes.slip3');
    }
    public function slip4()
    {
         return view('qrcodes.slip4');
    }
    public function slip5()
    {
         return view('qrcodes.slip5');
    }
}
 