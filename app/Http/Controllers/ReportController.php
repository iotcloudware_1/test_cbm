<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\AlarmHistory;
use App\Model\AlarmCurrentState;
use App\Model\WorkOrder;
use App\Model\WorkOrderHistory;
use App\Model\ParameterValues;
use App\Model\RouteHistory;
use Carbon\Carbon;
use DB;
use Helper;

class ReportController extends Controller
{
    public function generateReport(Request $request)
    {
        \Log::info($request);
        $start_date = $request['start_date'];
        $end_date = $request['end_date'];
        $type = $request['type'];
         $today = Carbon::today();
        $alarms = [];
        $alarms['total'] = AlarmHistory::whereBetween('created_at',[$start_date,$end_date])
            ->where('new_state','alarm')
            ->count();
        
        $alarms['resolved'] = AlarmHistory::whereBetween('created_at',[$start_date,$end_date])
            ->where('new_state','normal')
            ->count();

        $alarms['unresolved'] = $alarms['total'] - $alarms['resolved'];
       
        $alarms['acknowledged'] = AlarmHistory::whereBetween('created_at',[$start_date,$end_date])
            ->where('new_state','alarm')
            ->where('state_ack','acknowledged')
            ->count();
        
        $alarms['unacknowledged'] = AlarmHistory::whereBetween('created_at',[$start_date,$end_date])
            ->where('new_state','alarm')
            ->where('state_ack','unacknowledged')
            ->count();

        $workorders = [];

        $workorders['total'] = WorkOrderHistory::whereBetween('created_at',[$start_date,$end_date])
            ->where('action','created')
            ->count();


        $workorders_ids = WorkOrderHistory::whereBetween('created_at',[$start_date,$end_date])
            ->groupBy('current_workorder_id')
            ->pluck('current_workorder_id');
        
        $workorders['closed'] = WorkOrderHistory::whereBetween('created_at',[$start_date,$end_date])
            ->whereIn('current_workorder_id',$workorders_ids)
            ->where('action','closed')
            ->count();

        $workorders['open'] = $workorders['total'] - $workorders['closed'];


        $workorders['acknowledged'] = WorkOrderHistory::whereBetween('created_at',[$start_date,$end_date])
            ->whereIn('current_workorder_id',$workorders_ids)
            ->where('action','acknowledged')
            ->count();

        $workorders['unacknowledged'] = $workorders['total'] - $workorders['acknowledged'];     

        $routes = [];
        $routes['total']  = RouteHistory::whereBetween('created_at',[$start_date,$end_date])
            ->count();

        $routes['progress']  = RouteHistory::whereBetween('created_at',[$start_date,$end_date])
            ->where('route_state','In Progress')
            ->count();

        $routes['cancelled']  = RouteHistory::whereBetween('created_at',[$start_date,$end_date])
            ->where('route_state','cancelled')
            ->count();

        $routes['completed']  = RouteHistory::whereBetween('created_at',[$start_date,$end_date])
            ->where('route_state','complete')
            ->count();

        $alarms_history_today= AlarmHistory::whereDate('created_at',$today)->get();

        //$workorder_history_today= WorkOrderHistory::whereDate('created_at',$today)->get();
         $workorder_history_today = DB::table('workorder_history')
            ->join('current_workorder', 'workorder_history.current_workorder_id', '=', 'current_workorder.id')
            ->join('workorders', 'current_workorder.workorder_id', '=', 'workorders.id')
            ->select('workorder_history.*', 'current_workorder.priority','current_workorder.remarks','current_workorder.assigned_to','current_workorder.status','current_workorder.acknowledgement','workorders.description','workorders.type' )
            ->whereDate('workorder_history.created_at',$today)
            ->get();

           
             // $workorder_history_today= json_decode(json_encode($workorder_history_today), true);
             //  \Log::info($workorder_history_today);
                foreach ($workorder_history_today as $key => $value) {
                $workorder_history_today['assigned_by']= Helper::userIdToName($value['assigned_by']);
                 $workorder_history_today['assigned_to']= Helper::userIdToName($value['assigned_to']);
            }

            $route_history_today = RouteHistory::whereDate('created_at',$today)->get();

            \Log::info($alarms_history_today);
            \Log::info($workorder_history_today);
            \Log::info($route_history_today);


        return response()->json([
            'alarms'     => $alarms, 
            'workorders' => $workorders,
            'routes'     => $routes,
            'alarms_history_today'    =>  $alarms_history_today,
            'workorder_history_today' =>  $workorder_history_today,
            'route_history_today'     =>  $route_history_today,
        ]);
    }




    public function generateYesturdayReport(Request $request)
    {
        
        $start_date = $request['start_date'];
        $end_date = $request['end_date'];
        $type = $request['type'];
        $yesterday = Carbon::yesterday();

        $alarms = [];
        $alarms['total'] = AlarmHistory::whereDate('created_at',$start_date)
            ->where('new_state','alarm')
            ->count();
        
        $alarms['resolved'] = AlarmHistory::whereDate('created_at',$start_date)
            ->where('new_state','normal')
            ->count();

        $alarms['unresolved'] = $alarms['total'] - $alarms['resolved'];
       
        $alarms['acknowledged'] = AlarmHistory::whereDate('created_at',$start_date)
            ->where('new_state','alarm')
            ->where('state_ack','acknowledged')
            ->count();
        
        $alarms['unacknowledged'] = AlarmHistory::whereDate('created_at',$start_date)
            ->where('new_state','alarm')
            ->where('state_ack','unacknowledged')
            ->count();

        $workorders = [];

        $workorders['total'] = WorkOrderHistory::whereDate('created_at',$start_date)
            ->where('action','created')
            ->count();


        $workorders_ids = WorkOrderHistory::whereDate('created_at',$start_date)
            ->groupBy('current_workorder_id')
            ->pluck('current_workorder_id');
        
        $workorders['closed'] = WorkOrderHistory::whereDate('created_at',$start_date)
            ->whereIn('current_workorder_id',$workorders_ids)
            ->where('action','closed')
            ->count();

        $workorders['open'] = $workorders['total'] - $workorders['closed'];


        $workorders['acknowledged'] = WorkOrderHistory::whereDate('created_at',$start_date)
            ->whereIn('current_workorder_id',$workorders_ids)
            ->where('action','acknowledged')
            ->count();

        $workorders['unacknowledged'] = $workorders['total'] - $workorders['acknowledged'];     

        $routes = [];
        $routes['total']  = RouteHistory::whereDate('created_at',[$start_date])
            ->count();

        $routes['progress']  = RouteHistory::whereDate('created_at',[$start_date])
            ->where('route_state','In Progress')
            ->count();

        $routes['cancelled']  = RouteHistory::whereDate('created_at',[$start_date])
            ->where('route_state','cancelled')
            ->count();

        $routes['completed']  = RouteHistory::whereDate('created_at',[$start_date])
            ->where('route_state','complete')
            ->count();
         $alarms_history_yesterday= AlarmHistory::whereDate('created_at',$yesterday)->get();

        $workorder_history_yesterday= WorkOrderHistory::whereDate('created_at',$yesterday)->get();

        $workorder_history_yesterday = DB::table('workorder_history')
      ->join('current_workorder', 'workorder_history.current_workorder_id', '=', 'current_workorder.id')
      ->join('workorders', 'current_workorder.workorder_id', '=', 'workorders.id')
      ->select('workorder_history.*', 'current_workorder.priority','current_workorder.remarks','current_workorder.assigned_to','current_workorder.status','current_workorder.acknowledgement','workorders.description','workorders.type' )->whereDate('workorder_history.created_at',$yesterday)->get();
        foreach ($workorder_history_yesterday as $key => $value) {
                $workorder_history_yesterday[$key]['assigned_by']= Helper::userIdToName($value['assigned_by']);
                 $workorder_history_yesterday[$key]['assigned_to']= Helper::userIdToName($value['assigned_to']);
            }
        

        $route_history_yesterday = RouteHistory::whereDate('created_at',$yesterday)->get();

        \Log::info($routes);

        return response()->json([
            'alarms'     => $alarms, 
            'workorders' => $workorders,
            'routes'     => $routes,
            'alarms_history_yesterday'  => $alarms_history_yesterday,
            'workorder_history_yesterday'=>$workorder_history_yesterday,
            'route_history_yesterday'    =>$route_history_yesterday,
        ]);
    }
}


