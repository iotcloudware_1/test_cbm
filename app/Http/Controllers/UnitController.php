<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\FormBuilder;
use App\Forms\DepartmentForm;
use App\Http\Controllers\Controller;
use App\Model\Line;
use App\Model\Asset;
use App\Model\Instrument;
use App\Model\Parameter;
use App\Model\AlarmCurrentState;
use App\Model\Unit;
use App\Model\Plant;
use App\Model\Department;
use App\Post;
use App\User;
use Auth;
use Yajra\Datatables\Datatables;
use DB;
use Response;
Use Exception;
use Hash;
use Helper;

use Yajra\DataTables\Services\DataTable;

class UnitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
         $this->middleware('permission:unit-list|unit-create|unit-edit|unit-delete', ['only' => ['index','store']]);
         
         $this->middleware('permission:unit-create', ['only' => ['create','store']]);
         $this->middleware('permission:unit-edit', ['only' => ['edit','updated']]);
         $this->middleware('permission:unit-delete', ['only' => ['destroy']]);
         return view('permission-error');
    }

    public function index(Request $request)
    {
        if($request->ajax())
        {
            $data = Unit::latest()->get();

            foreach ($data as $key => $value) {
                $data[$key]['plant_id']= Helper::plantIdToName($value['plant_id']);
            }

            return Datatables::of($data)
                ->addColumn('action', 'datatables.action-button')
                ->rawColumns(['action'])
                ->make(true);
        }    
        return view('units.index');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $plant=Plant::all()->pluck('name','id');
        return view('units.create', compact('plant'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'plant_id' => 'required',
            'name' => 'required',
        ]);
                       
        $input = $request->all();   

        $input['user_id'] = Auth::user()->id;
        $unit_exist = Unit::count();
        $unit_limit = User::where('id',Auth::user()->id)->value('unit_limit');


        if($unit_limit > $unit_exist)
        {
            Unit::create($input);
            return redirect('/units');
        }
        else
        {
            echo("<script>
                    alert('Limit Reached');
                    window.location.href = 'units'
                </script>");
        }    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $unit = Unit::find($id);
        $unit->plant_id = Helper::plantIdToName($unit->plant_id);
        return view('units.show',compact('unit'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
   {
        $plant=Plant::all()->pluck('name','id');
        $unit = Unit::find($id);
        return view('units.edit',compact('plant','unit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'plant_id' => 'required',
            'name' => 'required',
        ]);

        $unit = Unit::find($id);
        $unit->plant_id = $request->input('plant_id');
        $unit->name = $request->input('name');
        $unit->description = $request->input('description');
        $unit->save();

        return redirect()->route('units.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try
        {
            DB::table("units")->where('id',$id)->delete();
            return redirect()->route('units.index');
        }
        catch(Exception $e)
        {
            $err = json_encode($e);
            $err_array = json_decode($err,true);
            if($err_array['errorInfo'][1] == 1451)
            {
                return Response::json(array('success' => false, 'msg_string' => 'Unit is in use.'), 500);
            }
          
        } 
    }

    public function getUnits()
    {
        $units = Unit::all()->toJson();
        return $units;
    }

        public function getUnitDashoard()
    { 
        $units = Unit::all();
        $alarms_arr=[];
        $unit_id;
        foreach ($units as $key => $unit) 
        {   
            $units[$key]['plant_id']= Helper::plantIdToName($unit['plant_id']);
            $departments=Unit::find($unit['id'])->departments;
            $unit_id = $unit['id'];
            $alarm_count = 0;
            $healthy_count=0; 
             foreach ($departments as $key => $department) 
             {
                $lines= Department::find($department->id)->lines; 
                foreach ($lines as $key => $line) 
                {
                    $assets= Line::find($line->id)->assets; 

                    foreach ($assets as $key => $asset) 
                    {
                        $instruments= Asset::find($asset->id)->instruments;

                        foreach ($instruments as $instrument) 
                        { 
                            $parameters= Instrument::find($instrument->id)->parameters;

                            foreach ($parameters as $parameter) 
                            {
                                $alarms= Parameter::find($parameter->id)->hasAlarm;
                                //dd($alarms);
                                if($alarms != null)
                                {
                                    foreach ($alarms as $key => $alarm)
                                    {
                                        $current_alarms= AlarmCurrentState::where('alarm_id',$alarm->id)->where('state','alarm')->count();
                                        $healthy_alarms= AlarmCurrentState::where('alarm_id',$alarm->id)->where('state','normal')->count();
                                        $alarm_count = $alarm_count + $current_alarms;
                                        $healthy_count = $healthy_count + $healthy_alarms;
                                    }
                                }
                                else
                                {
                                    $alarm_count = $alarm_count;
                                    $healthy_count = $healthy_count;

                                }    
                            }   
                        }

                    }                    
                }
                         
             }
                $temp = array('unit_id' =>$unit_id,'alarm_count'=>$alarm_count,'healthy_count'=>$healthy_count);
                        array_push($alarms_arr, $temp);

          } 
                foreach ($units as $key => $unit) {
                    $units[$key]['alarm_count'] = $alarms_arr[$key]['alarm_count'];
                    $units[$key]['healthy_count'] = $alarms_arr[$key]['healthy_count'];

                } 

        return view('dashboardtree.units',compact('units'));

    }
    public function allUnits()
    { 
        $allunits = Unit::all();
        $alarms_arr=[];
         $department_id;
         foreach ($allunits as $key => $unit) 
          {   
            $allunits[$key]['plant_id']= Helper::plantIdToName($unit['plant_id']);
            $departments=Unit::find($unit['id'])->departments;
            $unit_id = $unit['id'];
            $alarm_count = 0;
            $healthy_count=0; 
             foreach ($departments as $key => $department) 
             {
                $lines= Department::find($department->id)->lines; 
                foreach ($lines as $key => $line) 
                {
                    $assets= Line::find($line->id)->assets; 

                    foreach ($assets as $key => $asset) 
                    {
                        $instruments= Asset::find($asset->id)->instruments;

                        foreach ($instruments as $instrument) 
                        {
                            $parameters= Instrument::find($instrument->id)->parameters;

                            foreach ($parameters as $parameter) 
                            {
                                $alarms= Parameter::find($parameter->id)->hasAlarm;
                                //dd($alarms);
                                if($alarms != null)
                                {
                                    foreach ($alarms as $key => $alarm)
                                    {
                                        $current_alarms= AlarmCurrentState::where('alarm_id',$alarm->id)->where('state','alarm')->count();
                                        $healthy_alarms= AlarmCurrentState::where('alarm_id',$alarm->id)->where('state','normal')->count();
                                        $alarm_count = $alarm_count + $current_alarms;
                                        $healthy_count = $healthy_count + $healthy_alarms;
                                    }
                                }
                                else
                                {
                                    $alarm_count = $alarm_count;
                                    $healthy_count = $healthy_count;

                                }    
                            }   
                        }

                    }                    
                }                          
             }
                $temp = array('unit_id' =>$unit_id,'alarm_count'=>$alarm_count,'healthy_count'=>$healthy_count);
                array_push($alarms_arr, $temp);             

          } 
                foreach ($allunits as $key => $unit) {
                    $allunits[$key]['alarm_count'] = $alarms_arr[$key]['alarm_count'];
                    $allunits[$key]['healthy_count'] = $alarms_arr[$key]['healthy_count'];

                }          
        return view('dashboardtree.allunits',compact('allunits'));

    }

}
 