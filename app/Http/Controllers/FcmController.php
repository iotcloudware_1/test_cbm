<?php

namespace App\Http\Controllers;

use App\Model\Fcm;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class FcmController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Fcm  $fcm
     * @return \Illuminate\Http\Response
     */
    public function show(Fcm $fcm)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Fcm  $fcm
     * @return \Illuminate\Http\Response
     */
    public function edit(Fcm $fcm)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Fcm  $fcm
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Fcm $fcm)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Fcm  $fcm
     * @return \Illuminate\Http\Response
     */
    public function destroy(Fcm $fcm)
    {
        //
    }

    public function updateToken(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'fcm_token' => 'required',
        ]);

        Fcm::updateOrCreate(['user_id' => Auth::id()],
            ['token' => $request->fcm_token]);

        return "Token Updated Successfully!";
    }
}
