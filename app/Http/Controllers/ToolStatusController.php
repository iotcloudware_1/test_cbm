<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Permission;
use App\User;
use App\Model\Tool;
use App\Model\ToolStatus;
use App\Model\Status;
use DB;
use Hash;
use App\Post;
use Yajra\Datatables\Datatables;
use Response;
use Helper;

class ToolStatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
function __construct()
    {
         $this->middleware('permission:toolstatus-list|toolstatus-create|toolstatus-edit|toolstatus-delete', ['only' => ['index','store']]);
         
         $this->middleware('permission:toolstatus-create', ['only' => ['create','store']]);
         $this->middleware('permission:toolstatus-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:toolstatus-delete', ['only' => ['destroy']]);
         return view('permission-error');
    }
    public function index(Request $request)
    {

        if($request->ajax())
        {
            $data = ToolStatus::latest()->get();

            foreach ($data as $key => $value) 
            {
                $data[$key]['tool_id']= Helper::toolIdToName($value['tool_id']);
            }
            foreach ($data as $key => $value) 
            {
                $data[$key]['status_id']= Helper::statusIdToDes($value['status_id']);
            }


            return Datatables::of($data)
              ->addColumn('action', 'datatables.action-button')
              ->rawColumns(['action'])
              ->make(true);
        }    
             return view('toolstatus.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
     {
        $tool=Tool::all()->pluck('name','id');
        $status=Status::all()->pluck('description','id');
        return view('toolstatus.create', compact('tool','status'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
     {
        $this->validate($request, [
            'tool_id' => 'required',
            'status_id' => 'required',
        ]);

        $input = $request->all();
        
        $toolstatus = ToolStatus::create($input);

        return redirect()->route('toolstatus.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $toolstatus = ToolStatus::find($id);
        $toolstatus->tool_id = Helper::toolIdToName($toolstatus->tool_id);
        $toolstatus->status_id = Helper::statusIdToDes($toolstatus->status_id);
        return view('toolstatus.show',compact('toolstatus'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tool=Tool::all()->pluck('name','id');
        $status=Status::all()->pluck('description','id');
        $toolstatus = ToolStatus::find($id);
        return view('toolstatus.edit',compact('toolstatus','tool','status'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'tool_id' => 'required',
            'status_id' => 'required',

        ]);

        $toolstatus = ToolStatus::find($id);
        $toolstatus->tool_id = $request->input('tool_id');
        $toolstatus->status_id = $request->input('status_id');
        $toolstatus->save();


        return redirect()->route('toolstatus.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         DB::table("tool_status")->where('id',$id)->delete();
    }
}
