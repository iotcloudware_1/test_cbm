<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\FormBuilder;
use App\Forms\ParameterForm;
use App\Model\Instrument;
use App\Model\ParameterValues;
use App\Model\Parameter;
use App\Model\Alarm;
use App\Model\Line;
use App\Model\Department;
use App\Model\Asset; 
use App\Model\Tool; 
use App\Model\AlarmHistory;
use App\Model\RoteInspectionPoint;
use Auth;
use Yajra\Datatables\Datatables;
use DB;
use Response;
use App\User;
use App\Model\AlarmCurrentState;
use Helper;
use Carbon\Carbon;
use App\Charts\DefaultChart;
use App\Providers\ChartsServiceProvider;
use App\Jobs\EmailJob;
// use app\Helper\FcmHelper;
class ParameterController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */

  function __construct()
  {
    $this->middleware('permission:parameter-list|parameter-create|parameter-edit|parameter-delete', ['only' => ['index','store']]);

    $this->middleware('permission:parametervalue-index', ['only' => ['parameterValues']]);
    $this->middleware('permission:parameter-create', ['only' => ['create','store']]);
    $this->middleware('permission:parameter-edit', ['only' => ['edit','update']]);
    $this->middleware('permission:parameter-delete', ['only' => ['destroy']]);
    
    return view('permission-error');
  }

  public function index(Request $request)
  {
    if($request->ajax())
    {
        $data = Parameter::latest()->get();
        foreach ($data as $key => $value) 
        {
            $data[$key]['instrument_id'] = Helper::instrumentIdToName($value['instrument_id']);
             $data[$key]['tool_id'] = Helper::toolIdToName($value['tool_id']);
        }
        return Datatables::of($data)
        ->addColumn('action', 'datatables.action-button')
        ->rawColumns(['action'])
        ->make(true);
    }    
    return view('parameters.index');
  }

  /** 
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    $instrument=Instrument::all()->pluck('name','id');
    $tool=Tool::all()->pluck('name','id');
    
    return view('parameters.create', compact('instrument','tool'));
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
      $this->validate($request, [
        'instrument_id' => 'required',
        'tool_id' => 'required',
        'name' => 'required',
        'description' => 'required',
        'type' => 'required',
        'entry' => 'required',
        'data_type' => 'required',
        'max_value' => 'required',
        'min_value' => 'required',
        'engrineering_unit' => 'required',
      ]);

      $input = $request->all(); 
      $input['user_id'] = Auth::user()->id;
      $parameter_exist = Parameter::count();
      $parameter_limit = User::where('id',Auth::user()->id)->value('parameter_limit');

      if($parameter_limit > $parameter_exist)
      {
          Parameter::create($input);
          return redirect('/parameters')->with('success','Parameter create successfully');;
      }
      else
      {
        // return redirect('/departments')->with('msg','Limit reached. Please contact Administrator for adding more department');
        echo("<script>
            alert('Limit Reached');
            window.location.href = 'parameters'
        </script>");
      }
  }   

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
      $parameter = Parameter::find($id);
      $parameter->instrument_id = Helper::instrumentIdToName( $parameter->instrument_id);
      $parameter->tool_id = Helper::toolIdToName( $parameter->tool_id);

      return view('parameters.show',compact('parameter'));
  }


  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $instrument=Instrument::all()->pluck('name','id');
    $tool=Tool::all()->pluck('name','id');
    $parameter = Parameter::find($id);
    
    return view('parameters.edit',compact('parameter','instrument','tool'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $this->validate($request, [
        'instrument_id' => 'required',
        'tool_id' => 'required',
        'name' => 'required',
        'description' => 'required',
        'type' => 'required',
        'entry' => 'required',
        'data_type' => 'required',
        'max_value' => 'required',
        'min_value' => 'required',
        'engrineering_unit' => 'required',

    ]);

    $parameter = Parameter::find($id);
    $parameter->instrument_id = $request->input('instrument_id');
    $parameter->tool_id = $request->input('tool_id');
    $parameter->name = $request->input('name');
    $parameter->description = $request->input('description');
    $parameter->type = $request->input('type');
    $parameter->entry = $request->input('entry');
    $parameter->data_type = $request->input('data_type');
    $parameter->max_value  = $request->input('max_value');
    $parameter->min_value = $request->input('min_value');
    $parameter->engrineering_unit = $request->input('engrineering_unit');
    $parameter->save();

    return redirect()->route('parameters.index')
      ->with('success','Parameter updated successfully');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    DB::table("parameters")->where('id',$id)->delete();
    
    return redirect()->route('parameters.index')
      ->with('success','Parameter deleted successfully');
  }

  public function getParameters()
  {
    $parameters = Parameter::all()->toJson();

    return $parameters;
  }

  public function parameterValues(Request $request)
  {   
    if($request->ajax())
    {  
      $parameter_values = DB::table('parameter_values')
      ->join('parameters', 'parameter_values.parameter_id', '=', 'parameters.id')
      ->select('parameter_values.*', 'parameters.instrument_id','parameters.data_type' );

      $parameter_value_asset = DB::table('instruments')
          ->joinSub($parameter_values, 'parameters', function ($join) {
          $join->on('parameters.instrument_id', '=', 'instruments.id');
      })->get();

      $parametervalues = [];

      // \Log::info($parameter_value_asset);
      foreach ($parameter_value_asset as $key => $parameter_value) 
      {   
          $p = (array)$parameter_value;

          if($p['data_type']=='Bool')
          {
             $p['value'] = Helper::intToBool($p['value']);
          }   
          else
          {
              $p['value'] = $p['value'];
          }    
          $p['parameter_id'] = Helper::parameterIdToName($p['parameter_id']);
          $p['asset_id'] = Helper::assetIdToName($p['asset_id']);
          $p['instrument_id'] = Helper::instrumentIdToName($p['instrument_id']);
          $p['user_id'] = Helper::userIdToName($p['user_id']);
          array_push($parametervalues, $p);
      }

      return Datatables::of($parametervalues)
      ->addColumn('action', 'datatables.action-button')
      ->rawColumns(['action'])
      ->make(true);
    }    
      
    return view('parametervalues.index');
  }

  public function postParameters(Request $request)
  {
    $input = $request->all();   
    ParameterValues::create($input);

    $normal_flag=true;
    $input= $request->all();
    $parameter_id= $input['parameter_id'];
    $parameter_value=(Int)$input['value'];
    $location_stamp= $input['location_stamp'];
    
    $user_id = $input['user_id'];
    $skip_reason= $input['skip_reason'];
    $created_at= $input['created_at'];
    $alarm='';
    $alarm= Alarm::where('parameter_id',$parameter_id)->get();
      
    foreach ($alarm as $key => $value) 
    {
      $set_point=(Int)$value['setpoint'];
      $condition=$value['condition'];

      if($condition =='less')
      {
        if($parameter_value < $set_point)
        {
          $current_alarm_old_state = AlarmCurrentState::where('alarm_id',$alarm['id'])->get();  

          $current_alarm_state = AlarmCurrentState::where('alarm_id',$alarm['id'])->update(array('state' => 'asdas','new_value'=>$parameter_value));

          $alarm_history= new AlarmHistory();
          $alarm_history->alarm_current_state_id=$current_alarm_old_state['id'];
          $alarm_history->old_state= $current_alarm_old_state['state'];
          $alarm_history->new_state= "alarm";
          $alarm_history->new_value= $parameter_value;
          $alarm_history->state_ack= "unacknowledged";
          $alarm_history->save();   
          $normal_flag=false;
        }
      }
      if($condition == 'greater' )
      {   
        if($set_point < $parameter_value)
        {  
          $current_alarm_old_state = AlarmCurrentState::where('alarm_id',$alarm['id'])->get();     

          $current_alarm_state = AlarmCurrentState::where('alarm_id',$alarm['id'])->update(array('state' => 'alarm', 'new_value'=>$parameter_value));
          $alarm_history= new AlarmHistory;  
          $alarm_history->alarm_current_state_id= $current_alarm_old_state['id'];
          $alarm_history->old_state= $current_alarm_old_state['state'];
          $alarm_history->new_state= "alarm";
          $alarm_history->new_value= $parameter_value;
          $alarm_history->state_ack= "unacknowledgedd";
          $alarm_history->save(); 
          $normal_flag=false;
        }
      }           
    }

    if($normal_flag == true)
    {
      $current_alarm_id = Alarm::where('parameter_id',$parameter_id)->pluck('id');   
      foreach ($current_alarm_id as $key => $id) 
      {
        $alarm_current_state= AlarmCurrentState::where('alarm_id',$id)->where('state','=' , "ALARM" )->get();
        foreach ($alarm_current_state as $key => $value)
        {
          $alarm_history= new AlarmHistory();
          $alarm_history->alarm_current_state_id= $alarm_current_state['id'];
          $alarm_history->old_state= "alarm";
          $alarm_history->new_state= "alaem";
          $alarm_history->new_value= $parameter_value;
          $alarm_history->save();   
        }
        $updated_state= AlarmCurrentState::where('alarm_id',$id)->where('state',"alarm" )->update(array('state' => 'alarm','new_value'=>$parameter_value)); 
      }                                                                                
      // $current_alarm_state = AlarmCurrentState::where('alarm_id',$alarm['id'])->update(array('state' => 'NORMAL'));                           
    }
     // ParameterValues::create($input);
  }

  public function manualEntryData(Request $request)
  {
    //\Log::info($request);
    $input= $request->all();
    $normal_flag=true;
    foreach ($input as $key => $value) 
    {   
      $parameter_value=$value['value'];  
      $data= new ParameterValues;
      $data->parameter_id= $value['parameter'];
      $data->value=$value['value'];
      $data->user_id=2;
      $data->location_stamp="Maunal Data";
      $data->save();
	    $alarm='';      
      $alarms= Alarm::where('parameter_id',$value['parameter'])->get();

      //----for asset and instrument name -----------//
      
      $asset_name=Helper::assetIdToParameter($value['parameter']);
      $instrument_name=Helper::instrumentIdToParameter($value['parameter']);  
      $parameter_name=Helper::parameterIdToName($value['parameter']);
      //---for asset and instrument name --------------//
      
      foreach ($alarms as $key => $alarm) 
      {
        $set_point=(Int)$alarm['setpoint'];
        $condition=$alarm['condition'];
        
        if($condition =='less')
        { 
          if($parameter_value < $set_point)
          { 
            // \Log::info($alarm); 
            $current_alarm_old_state = AlarmCurrentState::where('alarm_id',$alarm['id'])->first(); 
            //\Log::info($current_alarm_old_state);
            $current_alarm_state = AlarmCurrentState::where('alarm_id',$alarm['id'])->update(array('state' => 'alarm','new_value'=>$parameter_value));

            $alarm_message_body=" Asset : ".$asset_name." -- "." Instrument : ".$instrument_name." -- "." Parameter : ".$parameter_name." of ".$alarm['priority']." priority alarm ".$alarm['alarm_message']." has been raised ,which current value is = ".$parameter_value." but normal value should be greater than ".$set_point; 

            $subject=$alarm['priority']." ".$alarm['alarm_message']."  Asset : ".$asset_name." -- "." Instrument : ".$instrument_name." -- "." Parameter : ".$parameter_name;

            $alarm_history= new AlarmHistory();
            $alarm_history->alarm_current_state_id=$current_alarm_old_state['id'];
            $alarm_history->old_state= $current_alarm_old_state['state'];
            $alarm_history->new_state= "alarm";
            $alarm_history->new_value= $parameter_value;
            $alarm_history->state_ack= "unacknowledged";
            $alarm_history->save();   

            EmailJob::dispatch($alarm_message_body,$subject);            
          }
          else
          {
            $alarm = Alarm::where('parameter_id',$value['parameter'])
               ->where('condition',$condition)->first(); 
            $current_alarm_old_state = AlarmCurrentState::where('alarm_id',$alarm['id'])->where('state','alarm')->first();
            if($current_alarm_old_state!=NULL)
            {
              $current_alarm_state = AlarmCurrentState::where('alarm_id',$alarm['id'])->update(array('state' => 'normal','new_value'=>$parameter_value));

              $alarm_history= new AlarmHistory();
              $alarm_history->alarm_current_state_id=$current_alarm_old_state['id'];
              $alarm_history->old_state= $current_alarm_old_state['state'];
              $alarm_history->new_state= "normal";
              $alarm_history->new_value= $parameter_value;
              $alarm_history->state_ack= "unacknowledged";              
              $alarm_history->save();    
            }             
          }
        }
        else if($condition =='greater')
        { 
          if($parameter_value > $set_point)
          {            
            $current_alarm_old_state = AlarmCurrentState::where('alarm_id',$alarm['id'])->first();  
            $current_alarm_state = AlarmCurrentState::where('alarm_id',$alarm['id'])->update(array('state' => 'alarm','new_value'=>$parameter_value));

             $alarm_message_body=" Asset : ".$asset_name." -- "." Instrument : ".$instrument_name." -- "." Parameter : ".$parameter_name." of ".$alarm['priority']." priority alarm ".$alarm['alarm_message']." has been raised ,which current value is = ".$parameter_value." but normal value should be less than ".$set_point;

             $subject=$alarm['priority']." ".$alarm['alarm_message']."  Asset : ".$asset_name." -- "." Instrument : ".$instrument_name." -- "." Parameter : ".$parameter_name;
                                
            $alarm_history= new AlarmHistory();
            $alarm_history->alarm_current_state_id=$current_alarm_old_state['id'];
            $alarm_history->old_state= $current_alarm_old_state['state'];
            $alarm_history->new_state= "alarm";
            $alarm_history->new_value= $parameter_value;
            $alarm_history->state_ack= "unacknowledged";
            $alarm_history->save();   

            EmailJob::dispatch($alarm_message_body,$subject);

            \Log::info("========================NEW ALARM AYA==========================");
          }
          else
          {
            $alarm = Alarm::where('parameter_id',$value['parameter'])
               ->where('condition',$condition)->first(); 
            $current_alarm_old_state = AlarmCurrentState::where('alarm_id',$alarm['id'])->where('state','alarm')->first();
            
            if($current_alarm_old_state!=NULL)
            {
              $current_alarm_state = AlarmCurrentState::where('alarm_id',$alarm['id'])->update(array('state' => 'normal','new_value'=>$parameter_value));

              $alarm_history= new AlarmHistory();
              $alarm_history->alarm_current_state_id=$current_alarm_old_state['id'];
              $alarm_history->old_state= $current_alarm_old_state['state'];
              $alarm_history->new_state= "normal";
              $alarm_history->new_value= $parameter_value;
              $alarm_history->state_ack= "unacknowledged";
              $alarm_history->save();    
            }             
          }              
        }

        else if($condition =='equal')
        { 
          $parameter_value= (int)$parameter_value;
          $set_point=(int)$set_point;
          if($parameter_value == $set_point)
          { 
            $current_alarm_old_state = AlarmCurrentState::where('alarm_id',$alarm['id'])->first();  

            $current_alarm_state = AlarmCurrentState::where('alarm_id',$alarm['id'])->update(array('state' => 'alarm','new_value'=>$parameter_value));

            $alarm_message_body=" Asset : ".$asset_name." -- "." Instrument : ".$instrument_name." -- "." Parameter : ".$parameter_name." of ".$alarm['priority']." priority alarm ".$alarm['alarm_message']." has been raised ,which current value is = ".$parameter_value." but normal value should not be equal to ".$set_point;

            $subject=$alarm['priority']." ".$alarm['alarm_message']."  Asset : ".$asset_name." -- "." Instrument : ".$instrument_name." -- "." Parameter : ".$parameter_name;

            $alarm_history= new AlarmHistory();
            $alarm_history->alarm_current_state_id=$current_alarm_old_state['id'];
            $alarm_history->old_state= $current_alarm_old_state['state'];
            $alarm_history->new_state= "alarm";
            $alarm_history->new_value= $parameter_value;
            $alarm_history->state_ack= "unacknowledged";
            $alarm_history->save();   

            // EmailJob::dispatch($alarm_message_body,$subject);
          }
          else
          {  
            $alarm = Alarm::where('parameter_id',$value['parameter'])
               ->where('condition',$condition)->first(); 
            $current_alarm_old_state = AlarmCurrentState::where('alarm_id',$alarm['id'])->where('state','alarm')->first();

            if($current_alarm_old_state!=NULL)
            {
              $current_alarm_state = AlarmCurrentState::where('alarm_id',$alarm['id'])->update(array('state' => 'normal','new_value'=>$parameter_value));

              $alarm_history= new AlarmHistory();
              $alarm_history->alarm_current_state_id=$current_alarm_old_state['id'];
              $alarm_history->old_state= $current_alarm_old_state['state'];
              $alarm_history->new_state= "normal";
              $alarm_history->new_value= $parameter_value;
              $alarm_history->state_ack= "unacknowledged";
              $alarm_history->save();    
            }             
          }              
        }            
        else{
         
        }           
      }
    } 
  }   


public function getInspectionRandomPoint (Request $request)
{      
  $parameters= Parameter::where('instrument_id',$request['instrument_id'])->select('id','data_type')->get();
  if($parameters=='[]')
  {
    return '[{"message":"No parameters found against this instrument","id":0,"parameter_name":"","data_type":"","max":"","min":""}]';
  }
  $params=[];   
  foreach ($parameters as $key => $value) 
  {
    $parameter_name = Helper::parameterIdToName($value['id']);
    $parameter_max=Helper::parameterIdToMax($value['id']);
    $parameter_min=Helper::parameterIdToMin($value['id']);
    $parameter_data_type = Helper::parameterIdToDatatype($value['id']);
    $parameter_desc=Helper::ParameterIdToDescription($value['id']);
    $parameter_eu=Helper::ParameterIdToEU($value['id']);
      if($parameter_data_type != 'Bool')
      {
        $temp_arr = array('message'=>'','parameter_id'=>$value['id'],'parameter_name'=>$parameter_name." ".'('.$parameter_eu.')','data_type'=>$parameter_data_type,'max'=>$parameter_max,'min'=>$parameter_min);
      }
      else
      {
        $temp_arr = array('message'=>'','parameter_id'=>$value['id'],'parameter_name'=>$parameter_name,'data_type'=>$parameter_data_type,'max'=>$parameter_max,'min'=>$parameter_min);
      }
    array_push($params, $temp_arr);
  }
  return json_encode($params);
}

  public function postParametersPhone(Request $request)
  { 
    \Log::info("===============================================================================");  
    // \Log::info($request['data']);
    $arr = $request['data'];
    $data = json_decode($arr,true);  
    foreach ($data as $key => $value) 
    {
      $instrument_id=Parameter::where('id',$value['parameter_id'])->value('instrument_id');
      $instrument_location=Instrument::where('id',$instrument_id)->first();
       $theta = $instrument_location->longitude - $value['longitude'];
        $miles = (sin(deg2rad($instrument_location->latitude)) * sin(deg2rad($value['latitude']))) + (cos(deg2rad($instrument_location->latitude)) * cos(deg2rad($value['latitude'])) * cos(deg2rad($theta)));
        $miles = acos($miles);
        $miles = rad2deg($miles);
        $miles = $miles * 60 * 1.1515;
        $feet = $miles * 5280;
        $yards = $feet / 3;
        $kilometers = $miles * 1.609344;
        $meters = $kilometers * 1000;

        $meters=number_format($meters,0); 

      $value['distance_measured']=$meters;
      $postParametersPhone= ParameterValues::create($value);
      $alarm='';
      $alarms= Alarm::where('parameter_id',$value['parameter_id'])->get();
      $parameter_value=$value['value'];

      $asset_name=Helper::assetIdToParameter($value['parameter_id']);
      $instrument_name=Helper::instrumentIdToParameter($value['parameter_id']);  
      $parameter_name=Helper::parameterIdToName($value['parameter_id']);

      foreach ($alarms as $key => $alarm) 
      {
        $set_point=(Int)$alarm['setpoint'];
        $condition=$alarm['condition'];
        
        if($condition =='less')
        { 
          if($parameter_value < $set_point)
          {

            $current_alarm_old_state = AlarmCurrentState::where('alarm_id',$alarm['id'])->first(); 
            $current_alarm_state = AlarmCurrentState::where('alarm_id',$alarm['id'])->update(array('state' => 'alarm','new_value'=>$parameter_value));

            $alarm_message_body=" Asset : ".$asset_name." -- "." Instrument : ".$instrument_name." -- "." Parameter : ".$parameter_name." of ".$alarm['priority']." priority alarm ".$alarm['alarm_message']." has been raised ,which current value is = ".$parameter_value." but normal value should be greater than ".$set_point;

            $subject=$alarm['priority']." ".$alarm['alarm_message']."  Asset : ".$asset_name." -- "." Instrument : ".$instrument_name." -- "." Parameter : ".$parameter_name;

            $status=3;
            $email='';

            $alarm_history= new AlarmHistory();
            $alarm_history->alarm_current_state_id=$current_alarm_old_state['id'];
            $alarm_history->old_state= $current_alarm_old_state['state'];
            $alarm_history->new_state= "alarm";
            $alarm_history->new_value= $parameter_value;
            $alarm_history->state_ack= "unacknowledged";
            $alarm_history->save();

            EmailJob::dispatch($alarm_message_body,$subject,$status,$email); 
             // FcmHelper::FcmForMultipleDevices($request->user_id, $NotifyName);           

          }
          else
          {
            $alarm = Alarm::where('parameter_id',$value['parameter_id'])
               ->where('condition',$condition)->first(); 
            $current_alarm_old_state = AlarmCurrentState::where('alarm_id',$alarm['id'])->where('state','alarm')->first();
            if($current_alarm_old_state!=NULL)
            {
              $current_alarm_state = AlarmCurrentState::where('alarm_id',$alarm['id'])->update(array('state' => 'normal','new_value'=>$parameter_value));

              $alarm_history= new AlarmHistory();
              $alarm_history->alarm_current_state_id=$current_alarm_old_state['id'];
              $alarm_history->old_state= $current_alarm_old_state['state'];
              $alarm_history->new_state= "normal";
              $alarm_history->new_value= $parameter_value;
              $alarm_history->state_ack= "unacknowledged";
              $alarm_history->save();    
            }             
          }
        }
        else if($condition == 'greater')
        { 
          if($parameter_value > $set_point)
          { 
            $current_alarm_old_state = AlarmCurrentState::where('alarm_id',$alarm['id'])->first();  

            $current_alarm_state = AlarmCurrentState::where('alarm_id',$alarm['id'])->update(array('state' => 'alarm','new_value'=>$parameter_value));

            $alarm_message_body=" Asset : ".$asset_name." -- "." Instrument : ".$instrument_name." -- "." Parameter : ".$parameter_name." of ".$alarm['priority']." priority alarm ".$alarm['alarm_message']." has been raised ,which current value is = ".$parameter_value." but normal value should be greater than ".$set_point;

            $subject=$alarm['priority']." ".$alarm['alarm_message']."  Asset : ".$asset_name." -- "." Instrument : ".$instrument_name." -- "." Parameter : ".$parameter_name;
            $status=3;
            $email='';

            $alarm_history= new AlarmHistory();
            $alarm_history->alarm_current_state_id=$current_alarm_old_state['id'];
            $alarm_history->old_state= $current_alarm_old_state['state'];
            $alarm_history->new_state= "alarm";
            $alarm_history->new_value= $parameter_value;
            $alarm_history->state_ack= "unacknowledged";
            $alarm_history->save();   

            EmailJob::dispatch($alarm_message_body,$subject,$status,$email);
          }
          else
          {
            $alarm = Alarm::where('parameter_id',$value['parameter_id'])
               ->where('condition',$condition)->first(); 
            $current_alarm_old_state = AlarmCurrentState::where('alarm_id',$alarm['id'])->where('state','alarm')->first();
            
            if($current_alarm_old_state!=NULL)
            {
              $current_alarm_state = AlarmCurrentState::where('alarm_id',$alarm['id'])->update(array('state' => 'normal','new_value'=>$parameter_value));

              $alarm_history= new AlarmHistory();
              $alarm_history->alarm_current_state_id=$current_alarm_old_state['id'];
              $alarm_history->old_state= $current_alarm_old_state['state'];
              $alarm_history->new_state= "normal";
              $alarm_history->new_value= $parameter_value;
              $alarm_history->state_ack= "unacknowledged";
              $alarm_history->save();    
            }             
          }              
        }

        else if($condition =='equal')
        { 
          $parameter_value= (int)$parameter_value;
          $set_point=(int)$set_point;
          if($parameter_value == $set_point)
          { 
            $current_alarm_old_state = AlarmCurrentState::where('alarm_id',$alarm['id'])->first(); 

            $current_alarm_state = AlarmCurrentState::where('alarm_id',$alarm['id'])->update(array('state' => 'alarm','new_value'=>$parameter_value));

            $alarm_message_body=" Asset : ".$asset_name." -- "." Instrument : ".$instrument_name." -- "." Parameter : ".$parameter_name." of ".$alarm['priority']." priority alarm ".$alarm['alarm_message']." has been raised ,which current value is = ".$parameter_value." but normal value should not be equal to ".$set_point;

            $subject=$alarm['priority']." ".$alarm['alarm_message']."  Asset : ".$asset_name." -- "." Instrument : ".$instrument_name." -- "." Parameter : ".$parameter_name;

            $status=3;
            $email='';

            $alarm_history= new AlarmHistory();
            $alarm_history->alarm_current_state_id=$current_alarm_old_state['id'];
            $alarm_history->old_state= $current_alarm_old_state['state'];
            $alarm_history->new_state= "alarm";
            $alarm_history->new_value= $parameter_value;
            $alarm_history->state_ack= "unacknowledged";
            $alarm_history->save();  

            EmailJob::dispatch($alarm_message_body,$subject,$status,$email);
          }
          else
          {  
            $alarm = Alarm::where('parameter_id',$value['parameter_id'])
               ->where('condition',$condition)->first(); 
            $current_alarm_old_state = AlarmCurrentState::where('alarm_id',$alarm['id'])->where('state','alarm')->first();
            
            if($current_alarm_old_state!=NULL)
            {
              $current_alarm_state = AlarmCurrentState::where('alarm_id',$alarm['id'])->update(array('state' => 'normal','new_value'=>$parameter_value));

              $alarm_history= new AlarmHistory();
              $alarm_history->alarm_current_state_id=$current_alarm_old_state['id'];
              $alarm_history->old_state= $current_alarm_old_state['state'];
              $alarm_history->new_state= "normal";
              $alarm_history->new_value= $parameter_value;
              $alarm_history->state_ack= "unacknowledged";
              $alarm_history->save();    
            }             
          }              
        }            
        else{
         
        }           
      }
    }
      if($instrument_location->radius < $meters)
      {
         $response = array('message' => 'value successfully submitted');
      return json_encode($response);
      } 
      else {
         $response = array('message' => 'value successfully submitted','distance_measured'=>$meters." m",'acceptable_radius'=>$instrument_location->radius." m",'note'=>'exceeded limit of acceptable radius');
      return json_encode($response);
      }
    
     
  
  }


  public function getParametersName()
  {
    $parameters_name= Parameter::pluck('name');
    
    return $parameters_name;
  }

  public function getParameterValuesForChart(Request $request)
  {       
    $parameter_values_arr=[];
    foreach ($request['param_id'] as $key => $value)
    {
      if($request['selected_option']=='hourly')
      {
        $parameter_values=DB::table('parameter_values')
        ->whereRaw ('HOUR (`created_at`) = '.$request['selected_period'])
        ->where('parameter_id',$value)
        ->select('created_at as x','value as y')
        ->orderBy('created_at','DESC')
        ->get();
      }
      else if($request['selected_option']=='daily')
      {
          $date= Carbon::parse($request['selected_period']);
           \Log::info($request['selected_period']);
          $parameter_values=DB::table('parameter_values')
          ->whereDate('created_at',$date)
          ->where('parameter_id',$value)
          ->select('created_at as x','value as y')
          ->orderBy('created_at','DESC')
          ->get();
          \Log::info($parameter_values);
      }
      else if($request['selected_option']=='monthly')
      {
        $month=Carbon::parse($request['selected_period']);
        $month=$month->month;
        // $month++;
        $year=Carbon::parse($request['selected_period'])->format('Y');
        $parameter_values=DB::table('parameter_values')
          ->whereMonth('created_at',$month)
          ->whereYear('created_at',$year)
          ->where('parameter_id',$value)
          ->select('created_at as x','value as y')
          ->orderBy('created_at','DESC')
          ->get();
      }
      if($request['selected_option']=='yearly')
      {
        $year=Carbon::parse($request['selected_period'])->format('Y');
        $parameter_values=DB::table('parameter_values')
          ->whereYear('created_at',$year)
          ->where('parameter_id',$value)
          ->select('created_at as x','value as y')
          ->orderBy('created_at','DESC')
          ->get();
      }
      $parameter_name= Helper::parameterIdToName($value);
      $temp_arr= array('parameter_name'=>$parameter_name,'parameter_values'=> $parameter_values);
      array_push($parameter_values_arr, $temp_arr);
    }   
    $param_arr=json_encode($parameter_values_arr);
    return $param_arr;  
  }

  public function getParameterAnalyticsForChart(Request $request) 
  {
    if($request['selected_option']=='hourly')
    {
      $parameter_values=DB::table('parameter_values')
        ->whereRaw ('HOUR (`created_at`) = '.$request['selected_period'])
        ->where('parameter_id',$request['param_id'])
        ->select('created_at as x','value as y')
        ->orderBy('created_at','DESC')
        ->get();

      $max_param_value=DB::table('parameter_values')
        ->whereRaw ('HOUR (`created_at`) = '.$request['selected_period'])
        ->where('parameter_id',$request['param_id'])
        ->min('value');
    }
    else if($request['selected_option']=='daily')
    {
      $date= Carbon::parse($request['selected_period']);  
      $parameter_values=DB::table('parameter_values')
        ->whereDate('created_at',$date)
        ->where('parameter_id',$request['param_id'])
        ->select('created_at as x','value as y')
        ->orderBy('created_at','DESC')
        ->get()
        ->toArray();
        
      $param_values=[];
      foreach ($parameter_values as $key => $value) 
      {
        $temp=(array)$value;
        array_push($param_values, $temp);           
      }
    }
    else if($request['selected_option']=='monthly')
    {
      $month=Carbon::parse($request['selected_period']);
      $month=$month->month;

      $year=Carbon::parse($request['selected_period'])->format('Y');

      $parameter_values=DB::table('parameter_values')
        ->whereMonth('created_at',$month)
        ->whereYear('created_at',$year)
        ->where('parameter_id',$request['param_id'])
        ->select('created_at as x','value as y')
        ->orderBy('created_at','DESC')
        ->get()
        ->toArray();

      $param_values=[];
      foreach ($parameter_values as $key => $value) 
      {
        $temp=(array)$value;
        array_push($param_values, $temp);           
      }
    }
    if($request['selected_option']=='yearly')
    {
      $year=Carbon::parse($request['selected_period'])->format('Y');
      $parameter_values=DB::table('parameter_values')
        ->whereYear('created_at',$year)
        ->where('parameter_id',$request['param_id'])
        ->select('created_at as x','value as y')
        ->orderBy('created_at','DESC')
        ->get()
        ->toArray();

      $param_values=[];
      foreach ($parameter_values as $key => $value) 
      {
        $temp=(array)$value;
        array_push($param_values, $temp);           
      }
    }

    $parameter_name= Helper::parameterIdToName($request['param_id']);
    $temp_arr= array('parameter_name'=>$parameter_name,'parameter_values'=> $param_values);

    return $temp_arr;
  }

  public function getSetpointForSingleAnalytics(Request $request)
  {
      $setpoint= Alarm::where('parameter_id',$request['param_id'])->get()->toJson(); 

      return $setpoint;
  }

  public function getParamDataType(Request $request)
  {
    $parameter= Parameter::find($request['parameter_id']);
    
    return $parameter->data_type; 
  }

  public function getParameterDashoard(Request $request)
  {  
    $show;  
    if($request['id']== 'all')
    {
      $show = 'all';
      $parameters = Parameter::all();
      $alarms_arr=[];
      $parameter_id;
      foreach ($parameters as $parameter) 
      {   
          $alarms= Parameter::find($parameter['id'])->hasAlarm;
          $parameter_id = $parameter['id'];
          $alarm_count = 0;
          $healthy_count=0;    
          if($alarms != null)
          {
              foreach ($alarms as $key => $alarm)
              {   
                  $current_alarms= AlarmCurrentState::where('alarm_id',$alarm->id)->where('state','alarm')->count();
                  $healthy_alarms= AlarmCurrentState::where('alarm_id',$alarm->id)->where('state','normal')->count();
                  $alarm_count = $alarm_count + $current_alarms;
                  $healthy_count = $healthy_count + $healthy_alarms;
              }
          }
          else
          {
              $alarm_count = $alarm_count;
              $healthy_count = $healthy_count;
          }  
          $temp = array('parameter_id' =>$parameter_id,'alarm_count'=>$alarm_count,'healthy_count'=>$healthy_count);
          array_push($alarms_arr, $temp);                  

      } 
       
      foreach ($parameters as $key => $parameter) {
          $parameters[$key]['alarm_count'] = $alarms_arr[$key]['alarm_count'];
          $parameters[$key]['healthy_count'] = $alarms_arr[$key]['healthy_count'];

      }        
      return view('dashboardtree.parameters',compact('parameters','show'));            
    }
    else
    {
      $show = 'specific'; 
      $parameters = Parameter::where('instrument_id',$request['id'])->get();
      $alarms_arr=[];
      $parameter_id;
      foreach ($parameters as $parameter) 
      {   
          $alarms= Parameter::find($parameter['id'])->hasAlarm;
          $parameter_id = $parameter['id'];
          $alarm_count = 0;
          $healthy_count=0;    
          if($alarms != null)
          {
              foreach ($alarms as $key => $alarm)
              {   
                  $current_alarms= AlarmCurrentState::where('alarm_id',$alarm->id)->where('state','alarm')->count();
                  $healthy_alarms= AlarmCurrentState::where('alarm_id',$alarm->id)->where('state','normal')->count();
                  $alarm_count = $alarm_count + $current_alarms;
                  $healthy_count = $healthy_count + $healthy_alarms;
              }
          }
          else
          {
              $alarm_count = $alarm_count;
              $healthy_count = $healthy_count;
          }  
          $temp = array('parameter_id' =>$parameter_id,'alarm_count'=>$alarm_count,'healthy_count'=>$healthy_count);
          array_push($alarms_arr, $temp);                  
      } 
      
      foreach ($parameters as $key => $parameter) {
        $parameters[$key]['alarm_count'] = $alarms_arr[$key]['alarm_count'];
        $parameters[$key]['healthy_count'] = $alarms_arr[$key]['healthy_count'];
      }
      
      return view('dashboardtree.parameters',compact('parameters','show'));
    }
  } 

  public function getSingleAnalyticsParameterData(Request $request)
  {   

    if($request['selected_option']=='hourly')
    {   
      $data= ParameterValues::where('parameter_id',$request['param_id'])->where('parameter_id',$request['selected_period'])->pluck('value');
      if($data_type=='Bool')
      { 
        $yes_bool= ParameterValues::where('parameter_id',$request['param_id'])->where('value',1)->count();
        $no_bool= ParameterValues::where('parameter_id',$request['param_id'])->where('value',0)->count();
      }    

      $max_value=$data->max();
      $min_value=$data->min();
      $avg_value=ParameterValues::where('parameter_id',$request['param_id'])->avg('value');
      $total_count=ParameterValues::where('parameter_id',$request['param_id'])->count();
      $temp = array('max' =>$max_value ,'min' =>$min_value ,'avg' =>round($avg_value,2) ,'total' => $total_count);
    }

    if($request['selected_option']=='daily')
    {
      $data_type=Helper::parameterIdToDatatype($request['param_id']);

      $date=Carbon::parse($request['selected_period']);
      $data= ParameterValues::where('parameter_id',$request['param_id'])->whereDate('created_at',$date)->orderBy('created_at','DESC')->pluck('value');
      // $yes_bool='';
      // $no_bool='';

      // if($data_type=='Bool')
      // {
      //   $yes_bool= ParameterValues::where('parameter_id',$request['param_id'])->where('value',1)->count();
      //   $no_bool= ParameterValues::where('parameter_id',$request['param_id'])->where('value',0)->count();
      // }    

      $max_value=$data->max();
      $min_value=$data->min();
      $total=$data->count();
      $avg_value=$data->avg();

      $temp = array('max' =>$max_value ,'min' =>$min_value ,'avg' =>round($avg_value,2) ,'total' => $total,'data_type'=>$data_type);
    }
      
    if($request['selected_option']=='monthly')
    {
      $month=Carbon::parse($request['selected_period']);
      $month=$month->month;

      $year=Carbon::parse($request['selected_period'])->format('Y');
      $data= ParameterValues::where('parameter_id',$request['param_id'])->whereMonth('created_at',$month)->whereYear('created_at',$year)->orderBy('created_at','DESC')->pluck('value');
      $max_value=$data->max();
      $min_value=$data->min();
      $total=$data->count();
      $avg_value=$data->avg();
      $temp = array('max' =>$max_value ,'min' =>$min_value ,'avg' =>round($avg_value,2) ,'total' => $total);
    } 
    if($request['selected_option']=='yearly')
    {
      $year=Carbon::parse($request['selected_period'])->format('Y');            
      $data= ParameterValues::where('parameter_id',$request['param_id'])->whereYear('created_at',$year)->orderBy('created_at','DESC')->pluck('value');
      $max_value=$data->max();
      $min_value=$data->min();
      $avg_value=ParameterValues::where('parameter_id',$request['param_id'])->avg('value');
      $total_count=ParameterValues::where('parameter_id',$request['param_id'])->count();
      $temp = array('max' =>$max_value ,'min' =>$min_value ,'avg' =>round($avg_value,2) ,'total' => $total_count);
    }                     
    return $temp;
  }

  public function showSelectedParameterAnalytics($id)
  {
    $param_values=[];

    $parameter_values=DB::table('parameter_values')
      ->where('parameter_id',$id)
      ->select('created_at as x','value as y')
      ->get();

      $parameter_name=Helper::parameterIdToName($id);
      $instrument_name=Helper::instrumentIdToParameter($id);
      $asset_name=Helper::assetIdToParameter($id);
      $line_name=Helper::lineIdToParameter($id);
      $department_name=Helper::departmentIdToParameter($id);
      $unit_name=Helper::unitIdToParameter($id);


      $assets=Helper::assetIdToParameterId($id);
      $lines=Helper::lineIdToParameterId($id);
      $department=Helper::departmentIdToParameterId($id);
      $unit=Helper::unitIdToParameterId($id);
      // $line_assets=Helper::lineAssetIdToParameterId($id);

      $asset_instrument= Instrument::where('asset_id',$assets)->where('name',$instrument_name)->value('asset_id');
      $line_asset= Asset::where('line_id',$lines)->where('name',$asset_name)->value('line_id');
      $department_line= Line::where('department_id',$department)->where('name',$line_name)->value('department_id');
      $unit_department= Department::where('unit_id',$unit)->where('name',$department_name)->value('unit_id');

      $data_type=Helper::parameterIdToDatatype($id);

      if($data_type=='Bool')
      {
        $yes_bool= ParameterValues::where('parameter_id',$id)->where('value',1)->count();
        $no_bool= ParameterValues::where('parameter_id',$id)->where('value',0)->count();
      }
      // dd($yes_bool."".$no_bool);

      $max_value=DB::table('parameter_values')
                  ->where('parameter_id',$id)
                  ->max('value');
      $min_value=DB::table('parameter_values')
                  ->where('parameter_id',$id)
                  ->min('value');

      $avg_value=ParameterValues::where('parameter_id',$id)->avg('value');
      $total_count=ParameterValues::where('parameter_id',$id)->count();

       if($parameter_values=='[]')
       {    
            $parameter_linechart='';
            $alarm_linechart='';
            $alarms_values=[];

            $data = array('id'=>$id,'max' =>'No Data' ,'min' =>'No Data' ,'avg' =>'No Data' ,'total' => 'No Data','param'=>$parameter_name,'instrument'=>$instrument_name,'asset'=>$asset_name,'line'=>$line_name,'department'=>$department_name,'unit'=>$unit_name,'alarms_detail');
              
            // return view('analytics.selected-parameter-analytics',compact('parameter_linechart','data','alarm_linechart','alarms_values','asset_instrument','line_asset','department_line','unit_department','alarms_detail'));
       } 
       else 
       {
          $data = array('id'=>$id,'max' =>$max_value ,'min' =>$min_value ,'avg' =>round($avg_value,2) ,'total' => $total_count,'param'=>$parameter_name,'instrument'=>$instrument_name,'asset'=>$asset_name,'line'=>$line_name,'department'=>$department_name,'unit'=>$unit_name,);

          $labels=[];
          $values=[];
          foreach ($parameter_values as $key => $value) 
          {
            $temp=(array)$value;   
            array_push($labels, $temp['x']); 
            array_push($values, $temp['y']); 
          }
          $parameter_linechart = new DefaultChart;
          $parameter_linechart->labels($labels);
          $parameter_linechart->options([
              'tooltip' => [
                  'show' => true // or false, depending on what you want.
              ],
             'annotation'=>[ 
                'annotations'=>[
                  'type'=> 'line',
                  'mode' => 'horizontal',
                  'scaleID'=> 'y-axis-0',
                  'value' => 100,                
                ]
             ] 
          ]);
          $parameter_linechart->dataset('parameter', 'line',$values)
            ->backgroundColor(['#c2a8d5ba'])
            ->color(['#907c9f']);         
        } 
    //////////////////////////////////////////////////////////////////////////
    // $alarm_detail=Alarm::where('parameter_id',$id)->get();
    //////////////////////////////////////////////////////////////////////////
    $alarms_detail=Alarm::where('parameter_id',$id)->get();

    $alarms=Alarm::where('parameter_id',$id)->pluck('id');
    $current_alarm_ids = collect();
    foreach ($alarms as $key => $alarm) {
        $alarm_current_state_id=AlarmCurrentState::where('alarm_id',$alarm)->value('id');
        $current_alarm_ids->push($alarm_current_state_id);
    }

    $alarms_count=[];
    $alarms_label=[];
    $alarms_values=[];
    foreach ($current_alarm_ids as $key => $current_alarm_id) 
    {
       $count =AlarmHistory::where('alarm_current_state_id',$current_alarm_id)->where('new_state','alarm')->count();
       array_push($alarms_count, $count);
       
       $value =AlarmHistory::where('alarm_current_state_id',$current_alarm_id)->where('new_state','alarm')->orderBy('created_at','DESC')->paginate(10); 

       array_push($alarms_values, $value);

       $alarm_name = AlarmCurrentState::find($current_alarm_id)->alarm->alarm_message;
       array_push($alarms_label, $alarm_name);
       // dd($alarms_values);
    }   
       
          $alarm_linechart = new DefaultChart;
          $alarm_linechart->labels($alarms_label);
          $alarm_linechart->dataset('alarms', 'bar',$alarms_count)
            ->backgroundColor(['#e89ea68a','#f6d7a6a8','#cfbbdfa3'])
            ->color(['#d0000057','#f9a30057','#c2a8d5ba']); 
////////////////////////////////////////////////////////////////////// 
        return view('analytics.selected-parameter-analytics',compact('parameter_linechart','data','alarm_linechart','alarms_values','asset_instrument','line_asset','department_line','unit_department','alarms_detail','data_type','yes_bool','no_bool'));        

    }
////////////////*****************HEALTH CARD FOR HEIRARCHY START**********************************//////////////////

///////////Instrument Health Card Start/////////////////////////
  public function getInstrumentDetailData($id)
  {
    // $asset_id=Helper::assetIdToInstrument($id);
    $asset_name=Helper::assetIdToInstrument($id);
    
    // $line_id=Helper::lineIdToInstrument($id);
    $line_name=Helper::lineIdToInstrument($id);

    // $department_id=Helper::departmentIdToInstrument($id);
    $department_name=Helper::departmentIdToInstrument($id);

    // $unit_id=Helper::unitIdToInstrument($id);
    $unit_name=Helper::unitIdToInstrument($id);    


    $parameter_id= Parameter::where('instrument_id',$id)->pluck('id');
    $e=Helper::instrumentIdToName($id);
    $detail_data=[];
    $bool='';
    foreach ($parameter_id as $key => $parameter)
    {
      $latest_values=ParameterValues::where('parameter_id',$parameter)->orderBy('created_at','DESC')->first();
      $type=Helper::parameterIdToDatatype($parameter);
      if($latest_values['value']!=NULL)
      {  
        if($type == 'Bool')
        { 
          $a=Helper::intToBool($latest_values['value']);
          if($a =='True')
          {
            $bool=1;
          }
          elseif($a=='False')
          {
            $bool=0;
          }  
          else
          {

          }  
        }    
        else
        {  
          $a=$latest_values['value'];
        }
      }
      else
      {
        $a='';
      }       
      $b=$latest_values['created_at'];
      $c=Helper::parameterIdToName($parameter);
      $max=Helper::parameterIdToMax($parameter);
      $min=Helper::parameterIdToMin($parameter);
      $alarm=Alarm::where('parameter_id',$parameter)->get();
      $parameters=$parameter;
      $alarm_flag="normal";
      foreach ($alarm as $key => $alarms)
      {
        $setpoint=$alarms['setpoint'];
        $condition=$alarms['condition'];
        $d= AlarmCurrentState::where('alarm_id',$alarms['id'])->where('state','alarm')->count();
        if($d>0)
        {
          $alarm_flag="alarm";
        }
      }
      $temp_arr= array('type'=>$type,'id'=>$parameters,'max'=>$max,'min'=>$min,'value' => $a, 'time' => $b,'name' => $c, 'state' => $alarm_flag,'setpoint' => $setpoint,'condition'=>$condition,'bool'=>$bool);
      array_push($detail_data, $temp_arr);
    }
    return view('analytics.selected-instrument-detail-analytics',compact('detail_data','e','asset_name','line_name','department_name','unit_name'));  
  }
///////////Instrument Health Card End/////////////////////////  

///////////Asset Health Card Start/////////////////////////  
  public function getAssetDetailData($id)
  {
    $line_id=Helper::lineIdToAsset($id);
    $line_name=Helper::lineIdToName($line_id);

    $department_id=Helper::departmentIdToAsset($id);
    $department_name=Helper::departmentIdToName($department_id);

    $unit_id=Helper::unitIdToAsset($id);
    $unit_name=Helper::unitIdToName($unit_id);

    $instrument_id=Instrument::where('asset_id',$id)->pluck('id');
    $asset_name=Helper::assetIdToName($id);
    $detail_data=[];
    $bool='';    
    foreach ($instrument_id as $key => $instrument)
    {
      $parameter_id= Parameter::where('instrument_id',$instrument)->pluck('id');
      $instrument_name=Helper::instrumentIdToName($instrument);

      foreach ($parameter_id as $key => $parameter)
      {
        $latest_values=ParameterValues::where('parameter_id',$parameter)->orderBy('created_at','DESC')->first();
        $type=Helper::parameterIdToDatatype($parameter);
        if($latest_values['value']!=NULL)
        {  
          if($type == 'Bool')
          { 
            $a=Helper::intToBool($latest_values['value']);
            if($a =='True')
            {
              $bool=1;
            }
            elseif($a=='False')
            {
              $bool=0;
            }  
            else
            {

            }  
          }  
          else
          {  
            $a=$latest_values['value'];
          }
        }
        else
        {
          $a='';
        } 
          
        $b=$latest_values['created_at'];
        $c=Helper::parameterIdToName($parameter);
        $max=Helper::parameterIdToMax($parameter);
        $min=Helper::parameterIdToMin($parameter);
        $alarm=Alarm::where('parameter_id',$parameter)->get();
        $parameters=$parameter;
        $alarm_flag="normal";
        foreach ($alarm as $key => $alarms)
        {
          $setpoint=$alarms['setpoint'];
          $condition=$alarms['condition'];
          $d= AlarmCurrentState::where('alarm_id',$alarms['id'])->where('state','alarm')->count();
          if($d>0)
          {
            $alarm_flag="alarm";
          }
          
        }
        
        $temp_arr= array('type'=>$type,'id'=>$parameters,'max'=>$max,'min'=>$min, 'instrument_name'=>$instrument_name,'value' => $a, 'time' => $b,'name' => $c, 'state' => $alarm_flag,'setpoint' => $setpoint,'condition'=>$condition,'bool'=>$bool);
        array_push($detail_data, $temp_arr);
      }      
    }
    return view('analytics.selected-asset-detail-analytics',compact('detail_data','asset_name','line_name','department_name','unit_name'));    
  } 
///////////Asset Health Card End /////////////////////////

///////////Lines Health Card Start/////////////////////////  
  public function getLineDetailData($id)
  {
      $department_id=Helper::departmentIdToLine($id);
      $department_name=Helper::departmentIdToName($department_id);

      $unit_id=Helper::unitIdToLine($id);
      $unit_name=Helper::unitIdToName($unit_id);      

      $line_name=Helper::lineIdToName($id);
      $asset_id=Asset::where('line_id',$id)->pluck('id');
      $detail_data=[];
      $bool='';
        foreach ($asset_id as $key => $asset) 
        {
          $asset_name=Helper::assetIdToName($asset);

          $instrument_id=Instrument::where('asset_id',$asset)->pluck('id');
          
          foreach ($instrument_id as $key => $instrument)
          {
            $instrument_name=Helper::instrumentIdToName($instrument);
            $parameter_id= Parameter::where('instrument_id',$instrument)->pluck('id');

            foreach ($parameter_id as $key => $parameter)
            {
              $latest_values=ParameterValues::where('parameter_id',$parameter)->orderBy('created_at','DESC')->first();
              $type=Helper::parameterIdToDatatype($parameter);
              if($latest_values['value']!=NULL)
              {  
                if($type == 'Bool')
                { 
                  $a=Helper::intToBool($latest_values['value']);
                  if($a =='True')
                  {
                    $bool=1;
                  }
                  elseif($a=='False')
                  {
                    $bool=0;
                  }  
                  else
                  {

                  }  
                }  
                else
                {  
                  $a=$latest_values['value'];
                }
              }
              else
              {
                $a='';
              } 
                
              $b=$latest_values['created_at'];
              $c=Helper::parameterIdToName($parameter);
              $max=Helper::parameterIdToMax($parameter);
              $min=Helper::parameterIdToMin($parameter);
              $alarm=Alarm::where('parameter_id',$parameter)->get();
              $parameters=$parameter;
              $alarm_flag="normal";
              foreach ($alarm as $key => $alarms)
              {
                $setpoint=$alarms['setpoint'];
                $condition=$alarms['condition'];
                $d= AlarmCurrentState::where('alarm_id',$alarms['id'])->where('state','alarm')->count();
                if($d>0)
                {
                  $alarm_flag="alarm";
                }
                
              }
              
              $temp_arr= array('type'=>$type,'id'=>$parameters,'max'=>$max,'min'=>$min, 'instrument_name'=>$instrument_name,'value' => $a, 'time' => $b,'name' => $c, 'state' => $alarm_flag,'setpoint' => $setpoint,'condition'=>$condition,'asset_name'=>$asset_name,'bool'=>$bool);
              array_push($detail_data, $temp_arr);
            }      
          }    
        } 

    return view('analytics.selected-line-detail-analytics',compact('detail_data','line_name','unit_name','department_name'));    
  } 
///////////Lines Health Card End /////////////////////////  



///////////Department Health Card Start/////////////////////////  
  public function getDepartmentDetailData($id)
  {
      $unit_id=Helper::unitIdToDepartment($id);
      $unit_name=Helper::unitIdToName($unit_id);

      $line_id=Line::where('department_id',$id)->pluck('id');
      $department_name=Helper::departmentIdToName($id);
      $detail_data=[];
      $bool='';
      foreach ($line_id as $key => $line)
      {
        $line_name=Helper::lineIdToName($line);
        $asset_id=Asset::where('line_id',$line_id)->pluck('id');
 
        foreach ($asset_id as $key => $asset) 
        {
          $asset_name=Helper::assetIdToName($asset);

          $instrument_id=Instrument::where('asset_id',$asset)->pluck('id');
          
          foreach ($instrument_id as $key => $instrument)
          {
            $instrument_name=Helper::instrumentIdToName($instrument);
            $parameter_id= Parameter::where('instrument_id',$instrument)->pluck('id');

            foreach ($parameter_id as $key => $parameter)
            {
              $latest_values=ParameterValues::where('parameter_id',$parameter)->orderBy('created_at','DESC')->first();
              $type=Helper::parameterIdToDatatype($parameter);
              if($latest_values['value']!=NULL)
              {  
                if($type == 'Bool')
                {
                  $a=Helper::intToBool($latest_values['value']);
                  if($a =='True')
                  {
                    $bool=1;
                  }
                  elseif($a=='False')
                  {
                    $bool=0;
                  }  
                  else
                  {

                  } 
                }  
                else
                {  
                  $a=$latest_values['value'];
                }
              }
              else
              {
                $a='';
              } 
                
              $b=$latest_values['created_at'];
              $c=Helper::parameterIdToName($parameter);
              $max=Helper::parameterIdToMax($parameter);
              $min=Helper::parameterIdToMin($parameter);
              $alarm=Alarm::where('parameter_id',$parameter)->get();
              $parameters=$parameter;
              $alarm_flag="normal";
              foreach ($alarm as $key => $alarms)
              {
                $setpoint=$alarms['setpoint'];
                $condition=$alarms['condition'];
                $d= AlarmCurrentState::where('alarm_id',$alarms['id'])->where('state','alarm')->count();
                if($d>0)
                {
                  $alarm_flag="alarm";
                }
                
              }
              
              $temp_arr= array('type'=>$type,'id'=>$parameters,'max'=>$max,'min'=>$min,'instrument_name'=>$instrument_name,'value' => $a, 'time' => $b,'name' => $c, 'state' => $alarm_flag,'setpoint' => $setpoint,'condition'=>$condition,'line_name'=>$line_name,'asset_name'=>$asset_name,'bool'=>$bool);
              array_push($detail_data, $temp_arr);
            }      
          }    
        }
      } 

    return view('analytics.selected-department-detail-analytics',compact('detail_data','department_name','unit_name'));    
  } 
///////////Department Health Card End /////////////////////////  


///////////Unit Health Card Start/////////////////////////  
  public function getUnitDetailData($id)
  {
    $department_id=Department::where('unit_id',$id)->pluck('id');
    $unit_name=Helper::unitIdToName($id);
    $detail_data=[];
    $bool='';
    foreach ($department_id as $key => $department)
    {

      $department_name=Helper::departmentIdToName($department);
      $line_id=Line::where('department_id',$department)->pluck('id');

      foreach ($line_id as $key => $line)
      {
        $line_name=Helper::lineIdToName($line);
        // dd($line_name);
        $asset_id=Asset::where('line_id',$line_id)->pluck('id');
 

        foreach ($asset_id as $key => $asset) 
        {
          $asset_name=Helper::assetIdToName($asset);

          $instrument_id=Instrument::where('asset_id',$asset)->pluck('id');
          
          foreach ($instrument_id as $key => $instrument)
          {
            $instrument_name=Helper::instrumentIdToName($instrument);
            $parameter_id= Parameter::where('instrument_id',$instrument)->pluck('id');

            foreach ($parameter_id as $key => $parameter)
            {
              $latest_values=ParameterValues::where('parameter_id',$parameter)->orderBy('created_at','DESC')->first();
              $type=Helper::parameterIdToDatatype($parameter);
              
              if($latest_values['value']!=NULL)
              {  
                if($type == 'Bool')
                { 
                  $a=Helper::intToBool($latest_values['value']);
                  if($a =='True')
                  {
                    $bool=1;
                  }
                  elseif($a=='False')
                  {
                    $bool=0;
                  }  
                  else
                  {

                  }  
                }  
                else
                {  
                  $a=$latest_values['value'];
                }
              }
              else
              {
                $a='';
              } 
                
              $b=$latest_values['created_at'];
              $c=Helper::parameterIdToName($parameter);
              $max=Helper::parameterIdToMax($parameter);
              $min=Helper::parameterIdToMin($parameter);
              $alarm=Alarm::where('parameter_id',$parameter)->get();
              $parameters=$parameter;
              $alarm_flag="normal";
              foreach ($alarm as $key => $alarms)
              {
                $setpoint=$alarms['setpoint'];
                $condition=$alarms['condition'];
                $d= AlarmCurrentState::where('alarm_id',$alarms['id'])->where('state','alarm')->count();
                if($d>0)
                {
                  $alarm_flag="alarm";
                }
                
              }
              
              $temp_arr= array('type'=>$type,'id'=>$parameters,'max'=>$max,'min'=>$min, 'instrument_name'=>$instrument_name,'value' => $a, 'time' => $b,'name' => $c, 'state' => $alarm_flag,'setpoint' => $setpoint,'condition'=>$condition,'department_name'=>$department_name,'line_name'=>$line_name,'asset_name'=>$asset_name,'bool'=>$bool);
              array_push($detail_data, $temp_arr);
            }      
          }
        }
     
      }
    } 

    return view('analytics.selected-unit-detail-analytics',compact('detail_data','unit_name'));    
  } 
///////////Unit Health Card End /////////////////////////  

////////////////*****************HEALTH CARD FOR HEIRARCHY END**********************************//////////////////



  //For Phone Db Sync
  public function getAllparameters()
  {
    return Parameter::all()->toJson();
  }
  public function showInstrumentParameter(Request $request)
    {
      // $parameter_id=Parameter::where('instrument_id',$request['id'])->value('id');
      // $parameter_name=Helper::parameterIdToName($parameter_id);

      $instrument_name=Helper::instrumentIdToName($request['id']);
       
      $asset_id=Instrument::where('id',$request['id'])->value('asset_id');     
      $asset_name=Helper::assetIdToName($asset_id);

      $asset_id=Instrument::where('id',$request['id'])->pluck('asset_id');
      $line_id= Asset::where('id',$asset_id)->value('line_id');
      $line_name=Helper::lineIdToName($line_id);

      $asset_id=Instrument::where('id',$request['id'])->pluck('asset_id');
      $line_id= Asset::where('id',$asset_id)->pluck('line_id');
      $department_id= Line::where('id',$line_id)->value('department_id');
      $department_name= Helper::departmentIdToName($department_id);
      
      $asset_id=Instrument::where('id',$request['id'])->pluck('asset_id');
      $line_id= Asset::where('id',$asset_id)->pluck('line_id');
      $department_id= Line::where('id',$line_id)->pluck('department_id');
      $unit_id= Department::where('id',$department_id)->value('unit_id');
      $unit_name= Helper::unitIdToName($unit_id); 

      $show_instrument_parameter= Parameter::where('instrument_id',$request['id'])->get();
      $alarms_arr=[];
      $parameter_id;
      foreach ($show_instrument_parameter as $parameter) 
      {   
        $alarms= Parameter::find($parameter['id'])->hasAlarm;
        $parameter_id = $parameter['id'];
        $alarm_count = 0;
        $healthy_count=0;    
        if($alarms != null)
        {
          foreach ($alarms as $key => $alarm)
          {   
              $current_alarms= AlarmCurrentState::where('alarm_id',$alarm->id)->where('state','alarm')->count();
              $healthy_alarms= AlarmCurrentState::where('alarm_id',$alarm->id)->where('state','normal')->count();
              $alarm_count = $alarm_count + $current_alarms;
              $healthy_count = $healthy_count + $healthy_alarms;
          }
        }
        else
        {
          $alarm_count = $alarm_count;
          $healthy_count = $healthy_count;
        } 

        $temp = array('parameter_id' =>$parameter_id,'alarm_count'=>$alarm_count,'healthy_count'=>$healthy_count);
        array_push($alarms_arr, $temp);                  
      } 

      foreach ($show_instrument_parameter as $key => $parameter) 
      {
        $show_instrument_parameter[$key]['alarm_count'] = $alarms_arr[$key]['alarm_count'];
        $show_instrument_parameter[$key]['healthy_count'] = $alarms_arr[$key]['healthy_count'];
      }

      foreach ($show_instrument_parameter as $key => $value) 
      {   
          
          $show_instrument_parameter[$key]['instrument_id']= Helper::instrumentIdToName($value['instrument_id']);
      }  
        
        return view('dashboardtree.parameters',compact('show_instrument_parameter','department_name','line_name','asset_name','instrument_name','unit_name'));
    } 
    
    public function allParameters()
    { 
      $allparameters = Parameter::all();
      $alarms_arr=[];
      $parameter_id;
      foreach ($allparameters as $parameter) 
      {   
          $alarms= Parameter::find($parameter['id'])->hasAlarm;
          $parameter_id = $parameter['id'];
          $alarm_count = 0;
          $healthy_count=0;    
          if($alarms != null)
          {
              foreach ($alarms as $key => $alarm)
              {   
                  $current_alarms= AlarmCurrentState::where('alarm_id',$alarm->id)->where('state','alarm')->count();
                  $healthy_alarms= AlarmCurrentState::where('alarm_id',$alarm->id)->where('state','normal')->count();
                  $alarm_count = $alarm_count + $current_alarms;
                  $healthy_count = $healthy_count + $healthy_alarms;
              }
          }
          else
          {
              $alarm_count = $alarm_count;
              $healthy_count = $healthy_count;
          }  
          $temp = array('parameter_id' =>$parameter_id,'alarm_count'=>$alarm_count,'healthy_count'=>$healthy_count);
          array_push($alarms_arr, $temp);                  
      } 
      
      foreach ($allparameters as $key => $parameter) {
        $allparameters[$key]['alarm_count'] = $alarms_arr[$key]['alarm_count'];
        $allparameters[$key]['healthy_count'] = $alarms_arr[$key]['healthy_count'];
      }
       

      foreach ($allparameters as $key => $value) 
          {   
            
            $allparameters[$key]['instrument_id']= Helper::instrumentIdToName($value['instrument_id']);
          }  
        return view('dashboardtree.allparameters',compact('allparameters'));

    } 

    public function getParamIdForDetailedAnalytics(Request $request)
    {
      $parameter_values=DB::table('parameter_values')
        ->where('parameter_id',$request['param_id'])
        ->select('created_at as x','value as y')
        ->orderBy('created_at','DESC')
        ->get()
        ->toArray();
        // \Log::info($parameter_values);
        if($parameter_values =='[]')
        {
          return "nalla aya ha";
        }
        else
        {
          $param_values=[];
          foreach ($parameter_values as $key => $value) 
          {
            $temp=(array)$value;
            $temp['x'] = Carbon::parse($temp['x'])->addHours(5)->format('Y-m-d H:i:s');
            array_push($param_values, $temp);           
          }          
        $sortedArr = collect($param_values)->sortBy('x')->all();
        
        \Log::info($sortedArr);
        $parameter_name= Helper::parameterIdToName($request['param_id']);
        $temp_arr= array('parameter_name'=>$parameter_name,'parameter_values'=> $sortedArr);      
            
          return json_encode($temp_arr);    
        }  
    }
}
