<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Permission;
use App\User;
use App\Model\Department;
use App\Model\DepartmentStatus;
use App\Model\Status;
use DB;
use Hash;
use App\Post;
use Yajra\Datatables\Datatables;
use Response;
use Helper;

class DepartmentStatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

 function __construct()
    {
         $this->middleware('permission:departmentstatus-list|departmentstatus-create|departmentstatus-edit|departmentstatus-delete', ['only' => ['index','store']]);
         
         $this->middleware('permission:departmentstatus-create', ['only' => ['create','store']]);
         $this->middleware('permission:departmentstatus-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:departmentstatus-delete', ['only' => ['destroy']]);
         return view('permission-error');
    }
    public function index(Request $request)
    {

            if($request->ajax())
            {
                $data = DepartmentStatus::latest()->get();

                foreach ($data as $key => $value) 
                {
                    $data[$key]['department_id']= Helper::departmentIdToName($value['department_id']);
                }
                foreach ($data as $key => $value) 
                {
                    $data[$key]['status_id']= Helper::statusIdToDes($value['status_id']);
                }


                return Datatables::of($data)
                  ->addColumn('action', 'datatables.action-button')
                  ->rawColumns(['action'])
                  ->make(true);
            }    
             return view('departmentstatus.index');
        }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $department=Department::all()->pluck('name','id');
        $status=Status::all()->pluck('description','id');
        return view('departmentstatus.create', compact('department','status'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
 {
        $this->validate($request, [
            'department_id' => 'required',
            'status_id' => 'required',
        ]);

        $input = $request->all();
        
        $departmentstatus = DepartmentStatus::create($input);

        return redirect()->route('departmentstatus.index');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
   {
        $departmentstatus = DepartmentStatus::find($id);
        $departmentstatus->department_id = Helper::departmentIdToName($departmentstatus->department_id);
        $departmentstatus->status_id = Helper::statusIdToDes($departmentstatus->status_id);
        return view('departmentstatus.show',compact('departmentstatus'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
   {
        $department=Department::all()->pluck('name','id');
        $status=Status::all()->pluck('description','id');
        $departmentstatus = DepartmentStatus::find($id);
        return view('departmentstatus.edit',compact('departmentstatus','department','status'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
 {
        $this->validate($request, [
            'department_id' => 'required',
            'status_id' => 'required',

        ]);

        $departmentstatus = DepartmentStatus::find($id);
        $departmentstatus->department_id = $request->input('department_id');
        $departmentstatus->status_id = $request->input('status_id');
        $departmentstatus->save();


        return redirect()->route('departmentstatus.index');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
   {
        DB::table("department_status")->where('id',$id)->delete();
    }
}
