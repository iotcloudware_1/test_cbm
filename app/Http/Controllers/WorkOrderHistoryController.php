<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\FormBuilder;
use App\Forms\DepartmentForm;
use App\Http\Controllers\Controller;
//use App\Http\Controllers\Plant;
use App\Model\WorkOrderHistory;
use App\Model\WorkOrder;
use App\Post;
use App\User;
use Auth;
use Yajra\Datatables\Datatables;
use DB;
use Response;
Use Exception;
use Hash;
use Helper;

class WorkOrderHistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:workorder-history-index', ['only' => ['index','store']]);

        return view('permission-error');
    }
    public function index(Request $request)
    {                  
        if($request->ajax())
        {
            $workorderhistory = DB::table('workorder_history')
                ->join('current_workorder', 'workorder_history.current_workorder_id', '=', 'current_workorder.id')
                ->select('workorder_history.*', 'current_workorder.workorder_id','current_workorder.priority','current_workorder.acknowledgement');
            
            $workorder_complete = DB::table('workorders')
                ->joinSub($workorderhistory, 'current_workorder', function ($join) {
                $join->on('workorders.id', '=', 'current_workorder.workorder_id');
            })->get();

                
            $workorder_history = [];

            foreach ($workorder_complete as $key => $data) {
                $w_h = (array)$data;
                $w_h['remarks'] = Helper::workorderIdToRemarks($w_h['current_workorder_id']);
                $w_h['instrument_id'] = Helper::instrumentIdToName($w_h['instrument_id']);
                $w_h['action_taken_by'] = Helper::userIdToName($w_h['action_taken_by']);
                array_push($workorder_history, $w_h);
            }
            return Datatables::of($workorder_history)
                ->setRowClass('@if($priority=="high") 
                    {{ "alert-danger" }} 
                    @elseif($priority=="medium")
                    {{"alert-warning"}}
                    @endif 
                    ')
                ->make(true);
        }    
        return view('workorderhistory.index');            
    }

}
