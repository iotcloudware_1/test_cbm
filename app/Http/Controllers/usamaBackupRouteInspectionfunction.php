public function getInspectionPoints(Request $request)
    {

       ////////////////////addded by usama for route cancell//////////////////////////   

        $route_id = $request['route_id'];
        $operator_id = $request['operator_id'];

        if (RouteHistory::where('route_id',$request['route_id'])
                          ->where('route_state','In Progress')  
                          ->exists())
        {
            $route_history= DB::table('route_history')
              ->where('route_id', $request['route_id'])
              ->where('route_state', 'In Progress')
              ->orderBy('created_at','DESC')
              ->first();  

           if($route_history->operator_id != $request['operator_id'])
           {
              return "Route is already in progress";
           } 

           else
           {
            $route_period=Route::where('id',$request['route_id'])->value('period');
            $start_time1=new DateTime($route_history->start_time);

            $end_time1=new DateTime(Carbon::now());
            $diff1=$end_time1->diff($start_time1);

            $new_time=$diff1->h;

            $hours = $new_time + ($diff1->days*24);

            if($route_period <= $hours)
            {
                RouteHistory::where('route_id',$request['route_id'])->orderBy('created_at','DESC')->first()->update(['instrument_skiped' => '0','route_state' => 'cancelled','end_time' => Carbon::now(),'duration_min'=>'00']); 

                 return "Route cancelled";
            }        

            else
            {
                    $route_history= DB::table('route_history')
                  ->where('route_id', $request['route_id'])
                  ->where('route_state', 'In Progress')
                  ->update(['sessions' =>  DB::raw('sessions + 1')]);
            }            
           }
        }  
        else
          {
              $route_history= new RouteHistory;
              $route_history->route_id= $request['route_id'];
              $route_history->operator_id= $request['operator_id'];
              $route_history->route_state= "In Progress";
              $route_history->start_time= Carbon::now();
              $route_history->save();
          }    

        ////////////////////addded by usama for route cancell/////////////////////;
        // else
        // {
        //   $route_id = $request['route_id'];
        //   $operator_id = $request['operator_id'];
        //   if (RouteHistory::where('route_id',$request['route_id'])
        //                     ->where('route_state','In Progress')  
        //                     ->exists())
        //   {
        //       $route_history= DB::table('route_history')
        //         ->where('route_id', $request['route_id'])
        //         ->where('route_state', 'In Progress')
        //         ->update(['sessions' =>  DB::raw('sessions + 1')]);
        //   }
          
        // }
          

        $inspection_point = RouteInspectionPoint::where('route_id',$route_id)->get();
        if($inspection_point=='[]')
        {
            return '[{"message":"No inspection point found against this route.","route_inspection_point_id":0,"instrument_id":0,"instrument_name":"","instrument_desc":"","route_history_id":0,}]';
        }    
        if (RouteHistory::where('route_id',$request['route_id'])
                          ->where('route_state','In Progress')  
                          ->exists())
        {
        
          $route_history_id= RouteHistory::where('route_id',$request['route_id'])
          ->where('route_state','In Progress')
          ->value('id');
        
        }
        else
        {
          $route_history_id= RouteHistory::orderBy('created_at','desc')->first();
        }
        // \Log::info($route_history_id);
        $all_inspection_points_temp =[];
        $data['message']='';

        foreach ($inspection_point as $key => $point) 
        {
            $instrument_id = $point['instrument_id'];
            $instrument_name = Helper::instrumentIdToName($instrument_id);
            $instrument_desc = Helper::instrumentIdToDes($instrument_id);
            
            $inspection_point_arr = array('message'=>'','route_inspection_point_id'=>$point['id'] , 'instrument_id' => $instrument_id,'instrument_name' => $instrument_name, 'route_history_id'=> $route_history_id,'instrument_desc' => $instrument_desc,);

            array_push($all_inspection_points_temp, $inspection_point_arr);
        }

        $all_inspection_points = array_unique($all_inspection_points_temp,SORT_REGULAR);
        $data= json_encode($all_inspection_points);
        return $data;           
    }