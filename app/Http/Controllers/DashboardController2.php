<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Permission;
use App\User;
use App\Model\Unit;
use App\Model\Alarm;
use App\Model\Department;
use App\Model\Line;
use App\Model\Asset;
use App\Model\Instrument;
use App\Model\Parameter;
use App\Model\Tool;
use App\Model\Route;
use App\Model\WorkOrder;
use App\Model\SkipReason;
use App\Model\CurrentWorkOrder;
use App\Model\AlarmCurrentState;
use App\Model\WorkOrderHistory;
use App\Model\AlarmHistory;
use DB;
use Hash;
use App\Post;
use Yajra\Datatables\Datatables;
use Response;
use App\Charts\DefaultChart;
use App\Providers\ChartsServiceProvider;
use Carbon\Carbon;
use Helper;

class DashboardController2 extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
 function __construct()
    {
        $this->middleware('permission:plant_admin_summary_dashboard-index', ['only' => ['index']]);
        // $this->middleware('permission:plant_admin_alarm_dashboard-index', ['only' => ['alarmDashboard']]);
         
        return view('permission-error');
    }

    public function index()
    {

        $open_workorders= CurrentWorkOrder::where('status','open')->count();
        $close_workorders= CurrentWorkOrder::where('status','closed')->count();
        $labels= ['Open','Closed'];

        $chart = new DefaultChart;
        $chart->labels($labels);
        $chart->dataset('My dataset', 'pie', [$open_workorders,$close_workorders])
        ->backgroundColor(['#4f81bd70','#4bacc699'])
        ->color(['#4f81bd70','#4bacc699']);
        $chart->displayAxes(false);

        $open_high= CurrentWorkOrder::where('status','open')->where('priority','high')->count();
        $open_medium= CurrentWorkOrder::where('status','open')->where('priority','medium')->count();
        $open_low= CurrentWorkOrder::where('status','open')->where('priority','low')->count();
        $bar_labels= ['High','Medium','Low'];
        $bar_chart = new DefaultChart;
        $bar_chart->labels($bar_labels);
        $bar_chart->dataset('Workorder_Open', 'bar', [$open_high,$open_medium,$open_low])
        ->backgroundColor(['#4f81bd70','#4bacc699', '#8064a294'])
        ->color(['#4f81bd70','#4bacc699','#8064a294']); 


        $open_workorders_bar= CurrentWorkOrder::where('status','open')->count();
        $close_workorders_bar= CurrentWorkOrder::where('status','closed')->count();
        $labels_workorder_bar= ['Open','Closed'];

        $bar_chart1 = new DefaultChart;
        $bar_chart1->labels($labels_workorder_bar);
        $bar_chart1->dataset('Status wise', 'bar', [$open_workorders_bar,$close_workorders_bar])
        ->backgroundColor(['#4f81bd70','#4bacc699'])
        ->color(['#4f81bd70','#4bacc699']);

        $workorder_close= CurrentWorkOrder::where('status','closed')->count();
        $workorder_open= CurrentWorkOrder::where('status','open')->count();
        
        $workorder_open_ack= CurrentWorkOrder::where('status','open')->where('acknowledgement','acknowledged')->count();

        $workorder_open_unack= CurrentWorkOrder::where('status','open')->where('acknowledgement','unacknowledged')->count();


         return view('dashboard2.index',compact('chart','bar_chart','bar_chart1','workorder_close','workorder_open','workorder_open_ack','workorder_open_unack'));        
    }

     public function alarmDashboard()
    {

        $alarm= AlarmCurrentState::where('state','alarm')->count();
        $normal= AlarmCurrentState::where('state','normal')->count();
        $labels= ['Alarm','Normal'];

        $chart = new DefaultChart;
        $chart->labels($labels);
        $chart->dataset('My dataset', 'pie', [$alarm,$normal])
        ->backgroundColor(['#4f81bd70','#4bacc699'])
        ->color(['#4f81bd70','#4bacc699']);
        $chart->displayAxes(false);
            

            $alarm_priority_high = DB::table('alarm_current_state')
                ->leftJoin('alarms', 'alarm_current_state.alarm_id', '=', 'alarms.id')
                ->where('alarm_current_state.state','alarm')
                ->where('alarms.priority','high')
                ->count();
            $alarm_priority_medium = DB::table('alarm_current_state')
                ->leftJoin('alarms', 'alarm_current_state.alarm_id', '=', 'alarms.id')
                ->where('alarm_current_state.state','alarm')
                ->where('alarms.priority','medium')
                ->count();
            $alarm_priority_low = DB::table('alarm_current_state')
                ->leftJoin('alarms', 'alarm_current_state.alarm_id', '=', 'alarms.id')
                ->where('alarm_current_state.state','alarm')
                ->where('alarms.priority','low')
                ->count();

        $bar_labels= ['High','Medium','Low'];
        $bar_chart = new DefaultChart;
        $bar_chart->labels($bar_labels);
        $bar_chart->dataset('Alarm Priority', 'bar', [$alarm_priority_high,$alarm_priority_medium,$alarm_priority_low])
        ->backgroundColor(['#4f81bd70','#4bacc699', '#8064a294'])
        ->color(['#4f81bd70','#4bacc699','#8064a294']); 

        $alarm_bar= AlarmCurrentState::where('state','alarm')->count();
        $normal_bar= AlarmCurrentState::where('state','normal')->count();
        $labels_bar= ['Alarm','Normal'];

        $bar_chart1 = new DefaultChart;
        $bar_chart1->labels($labels_bar);
        $bar_chart1->dataset('Alarm Status', 'bar', [$alarm_bar,$normal_bar])
        ->backgroundColor(['#4f81bd70','#4bacc699'])
        ->color(['#4f81bd70','#4bacc699']);

        $current_alarm=DB::table('alarm_current_state')->where('state', 'alarm')->count();
        
        $alarm_history=DB::table('alarm_history')->where('new_state','alarm')->count();


          $alarm_ids= AlarmCurrentState::where('state','alarm')->pluck('id');
          $alarm_ack=0;
          $alarm_unack=0;
          foreach ($alarm_ids as $key => $id) 
          {
            $current_alarm_id=AlarmHistory::where('alarm_current_state_id',$id)->orderBy('created_at','DESC')->first();
            if($current_alarm_id['new_state'] == 'alarm' )
            {
              if ($current_alarm_id['state_ack']=='unacknowledged')
              {
                $alarm_unack++;
              }
              if ($current_alarm_id['state_ack']=='acknowledged')
              {
                $alarm_ack++;
              }

            }  

          }

        // $alarm_ack=DB::table('alarm_history')->where('state_ack', 'acknowledged')
        //             ->where( 'created_at', '>', Carbon::now()->subDays(30))->count();

        // $alarm_unack=DB::table('alarm_history')->where('state_ack', 'unacknowledged')
        //             ->where( 'created_at', '>', Carbon::now()->subDays(30))->count();

        return view('dashboard2.alarmdashboard',compact('chart','bar_chart','bar_chart1','current_alarm','alarm_history','alarm_ack','alarm_unack'));
    }

    public function workOrderDetail()
    {
        return view('dashboard2.workorderdetail');
    }


    public function openDetail()
    { 
        $workorder_open = DB::table('current_workorder')
            ->join('workorders', 'current_workorder.workorder_id', '=', 'workorders.id')
            ->where('current_workorder.status','open')
            ->select('current_workorder.*','workorders.instrument_id')
            ->get();
        $data = json_decode($workorder_open,true);
        foreach ($data as $key => $value)
        {
            $data[$key]['instrument_id']= Helper::instrumentIdToName($value['instrument_id']);  
            $data[$key]['assigned_by']= Helper::userIdToName($value['assigned_by']);
            $data[$key]['assigned_to']= Helper::userIdToName($value['assigned_to']);  
        }
        return view('workorders.opendetail',compact('data'));
    }
    public function closeDetail()
    {   
        $workorder_close = DB::table('current_workorder')
                ->join('workorders', 'current_workorder.workorder_id', '=', 'workorders.id')
                ->where('current_workorder.status','closed')
                ->select('current_workorder.*','workorders.instrument_id')
                ->get();
        $data = json_decode($workorder_close,true);

        foreach ($data as $key => $value)
        {
            $data[$key]['instrument_id']= Helper::instrumentIdToName($value['instrument_id']);
            $data[$key]['assigned_by']= Helper::userIdToName($value['assigned_by']);
            $data[$key]['assigned_to']= Helper::userIdToName($value['assigned_to']);  
        }
        return view('workorders.closedetail',compact('data'));
    }
    public function ackDetail()
    {   $workorder_open_ack = DB::table('current_workorder')
                ->join('workorders', 'current_workorder.workorder_id', '=', 'workorders.id')
                ->where('current_workorder.status','open')->where('acknowledgement','acknowledged')
                ->select('current_workorder.*','workorders.instrument_id')
                ->get();
        $data = json_decode($workorder_open_ack,true);
        foreach ($data as $key => $value)
        {
            $data[$key]['instrument_id']= Helper::instrumentIdToName($value['instrument_id']);
            $data[$key]['assigned_by']= Helper::userIdToName($value['assigned_by']);
            $data[$key]['assigned_to']= Helper::userIdToName($value['assigned_to']);  
        }
        return view('workorders.openack',compact('data'));
    }
    public function unAckDetail()
    {   
        $workorder_open_unack = DB::table('current_workorder')
                ->join('workorders', 'current_workorder.workorder_id', '=', 'workorders.id')
                ->where('current_workorder.status','open')->where('acknowledgement','unacknowledged')
                ->select('current_workorder.*','workorders.instrument_id')
                ->get();
        $data = json_decode($workorder_open_unack,true);
        foreach ($data as $key => $value)
        {
            $data[$key]['instrument_id']= Helper::instrumentIdToName($value['instrument_id']);
            $data[$key]['assigned_by']= Helper::userIdToName($value['assigned_by']);
            $data[$key]['assigned_to']= Helper::userIdToName($value['assigned_to']);  
        }
        return view('workorders.openunack',compact('data'));
    }
}
