<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Permission;
use App\User;
use App\Model\Unit;
use App\Model\UnitStatus;
use App\Model\Status;
use DB;
use Hash;
use App\Post;
use Yajra\Datatables\Datatables;
use Response;
use Helper;


class UnitStatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    function __construct()
    {
         $this->middleware('permission:unitstatus-list|unitstatus-create|unitstatus-edit|unitstatus-delete', ['only' => ['index','store']]);
         
         $this->middleware('permission:unitstatus-create', ['only' => ['create','store']]);
         $this->middleware('permission:unitstatus-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:unitstatus-delete', ['only' => ['destroy']]);
         return view('permission-error');
    }
    public function index(Request $request)
      {

            if($request->ajax())
            {
                $data = UnitStatus::latest()->get();

                foreach ($data as $key => $value) 
                {
                    $data[$key]['unit_id']= Helper::unitIdToName($value['unit_id']);
                }
                foreach ($data as $key => $value) 
                {
                    $data[$key]['status_id']= Helper::statusIdToDes($value['status_id']);
                }


                return Datatables::of($data)
                  ->addColumn('action', 'datatables.action-button')
                  ->rawColumns(['action'])
                  ->make(true);
            }    
             return view('unitstatus.index');
        }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $unit=Unit::all()->pluck('name','id');
        $status=Status::all()->pluck('description','id');
        return view('unitstatus.create', compact('unit','status'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'unit_id' => 'required',
            'status_id' => 'required',
        ]);

        $input = $request->all();
        
        $unitstatus = UnitStatus::create($input);

        return redirect()->route('unitstatus.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $unitstatus = UnitStatus::find($id);
        $unitstatus->unit_id = Helper::unitIdToName($unitstatus->unit_id);
        $unitstatus->status_id = Helper::statusIdToDes($unitstatus->status_id);
        return view('unitstatus.show',compact('unitstatus'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $unit=Unit::all()->pluck('name','id');
        $status=Status::all()->pluck('description','id');
        $unitstatus = UnitStatus::find($id);
        return view('unitstatus.edit',compact('unitstatus','unit','status'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'unit_id' => 'required',
            'status_id' => 'required',

        ]);

        $unitstatus = UnitStatus::find($id);
        $unitstatus->unit_id = $request->input('unit_id');
        $unitstatus->status_id = $request->input('status_id');
        $unitstatus->save();

        return redirect()->route('unitstatus.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table("unit_status")->where('id',$id)->delete();
    }
}
