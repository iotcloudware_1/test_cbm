<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use App\Http\Controllers\Controller;
use App\Http\Controllers\Permission;
use App\User;
use App\Model\Unit;
use App\Model\Department;
use App\Model\Line;
use App\Model\Asset;
use App\Model\Instrument;
use App\Model\Parameter;                                                   
use App\Model\Tool;
use App\Model\Route;
use App\Model\WorkOrder;
use App\Model\CurrentWorkOrder;
use App\Model\WorkOrderHistory; 
use App\Model\SkipReason;
use App\Model\AlarmCurrentState;
use App\Model\AlarmHistory;
use App\Model\RouteHistory;
use DB;
use Hash;
use App\Post;
use Yajra\Datatables\Datatables;
use Response;
use App\Charts\DefaultChart;
use App\Providers\ChartsServiceProvider;
use Carbon\Carbon;
use Helper;

class DashboardController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:plant_admin_summary_dashboard-index');

        return view('permission-error');
    }
  public function index()
  {
/////////////////////////////////////////// DASHBOARD COUNT DATA START/////////////////////////////
        $total_alarms= AlarmHistory::where('new_state','alarm')->count();
        $current_alarms= AlarmCurrentState::where('state','alarm')->count();
          $alarm_ids= AlarmCurrentState::where('state','alarm')->pluck('id');
          $acknowledge_alarms=0;
          $unacknowledge_alarms=0;
          foreach ($alarm_ids as $key => $id) 
          {

            $current_alarm_id=AlarmHistory::where('alarm_current_state_id',$id)->orderBy('created_at','DESC')->first();

            if($current_alarm_id != null)
            {
              if($current_alarm_id['new_state'] == 'alarm' )
              {
                if ($current_alarm_id['state_ack']=='unacknowledged')
                {
                  $unacknowledge_alarms++;
                }
                if ($current_alarm_id['state_ack']=='acknowledged')
                {
                  $acknowledge_alarms++;
                }
              }  
            }
          }          
         //$acknowledge_alarms=DB::table('alarm_history')->where('state_ack', 'acknowledged')
            //         ->where( 'new_state,' 'alarm')->count();

        // $unacknowledge_alarms=DB::table('alarm_history')->where('state_ack', 'unacknowledged')
        //             ->where( 'created_at', '>', Carbon::now()->subDays(30))->count();     

        $in_progress_routes= RouteHistory::where('route_state','in progress')->count();
        $finish_routes= RouteHistory::where('route_state','complete')
        ->count();
        $cancelled_routes= RouteHistory::where('route_state','cancelled')->count();
        $late_routes= RouteHistory::where('route_state','late')->count();

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        $all_time_random_inspection= RouteHistory::where('route_state','In progress')->count();
        $weekly_random_inspection= RouteHistory::where('route_state','complete')
        ->count();
        $monthly_random_inspection= RouteHistory::where('route_state','cancelled')->count();
        $yealry_random_inspection= RouteHistory::where('route_state','late')->count();

 ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////       

        $close_workorders= CurrentWorkOrder::where('status','closed')->where( 'created_at', '>', Carbon::now()->subDays(30))->count();
        $open_workorders= CurrentWorkOrder::where('status','open')->where( 'created_at', '>', Carbon::now()->subDays(30))->count();
        
        $open_acknowledge_workorders= CurrentWorkOrder::where('status','open')->where('acknowledgement','acknowledged')->where( 'created_at', '>', Carbon::now()->subDays(30))->count();

        $open_unacknowledge_workorders= CurrentWorkOrder::where('status','open')->where('acknowledgement','unacknowledged')->where( 'created_at', '>', Carbon::now()->subDays(30))->count();        

//////////// /////////////////////////////DASHBOARD COUNT DATA END//////////////////////////////// 


        /////////////////BAR CHART FOR WORKORDER START///////////////////////////
   
        $months_array=[1,2,3,4,5,6,7,8,9,10,11,12];
        $months_bar_data=[];
        $open=0;
        foreach ($months_array as $key => $month) 
        {
            $workorder_barchart=WorkOrderHistory::whereMonth('created_at', $month)->pluck('current_workorder_id')->toArray();

            if($workorder_barchart==NULL)
            {
                $temp_arr= array('total_records' => 0 ,'open'=>0);
                array_push($months_bar_data, $temp_arr); 
            }
            else
            {
                $workorder_barchart=array_unique($workorder_barchart);
                $total_records=count($workorder_barchart);

                foreach ($workorder_barchart as $key => $data) 
                {
                   $check_open=WorkOrderHistory::where('current_workorder_id',$data)->where('action','closed')->count();
                   if($check_open==0)
                   {
                        $open++;
                   }
                }
                $temp_arr= array('total_records' => $total_records ,'open'=>$open);
                array_push($months_bar_data, $temp_arr);                
            }
        }    

        $workorder_barchart_lables= ['January','Feb','March','April','May','June','July','August','September','October','November','December'];

        $workorder_barchart_open_data=[$months_bar_data[0]['total_records'],$months_bar_data[1]['total_records'],$months_bar_data[2]['total_records'],$months_bar_data[3]['total_records'],$months_bar_data[4]['total_records'],$months_bar_data[5]['total_records'],$months_bar_data[6]['total_records'],$months_bar_data[7]['total_records'],$months_bar_data[8]['total_records'],$months_bar_data[9]['total_records'],$months_bar_data[10]['total_records'],$months_bar_data[11]['total_records']];

        $workorder_barchart_closed_data=[$months_bar_data[0]['open'],$months_bar_data[1]['open'],$months_bar_data[2]['open'],$months_bar_data[3]['open'],$months_bar_data[4]['open'],$months_bar_data[5]['open'],$months_bar_data[6]['open'],$months_bar_data[7]['open'],$months_bar_data[8]['open'],$months_bar_data[9]['open'],$months_bar_data[10]['open'],$months_bar_data[11]['open']]; 

        $workorder_barchart = new DefaultChart;
        $workorder_barchart->labels($workorder_barchart_lables);
        $workorder_barchart->dataset('Total', 'bar', $workorder_barchart_open_data)
        ->backgroundColor(['#c2a8d5ba','#c2a8d5ba','#c2a8d5ba','#c2a8d5ba','#c2a8d5ba','#c2a8d5ba','#c2a8d5ba','#c2a8d5ba','#c2a8d5ba','#c2a8d5ba','#c2a8d5ba','#c2a8d5ba'])
        ->color(['#907c9f','#907c9f','#907c9f','#907c9f','#907c9f','#907c9f','#907c9f','#907c9f','#907c9f','#907c9f','#907c9f','#907c9f']);

        $workorder_barchart->dataset('Open', 'bar', $workorder_barchart_closed_data)
        ->backgroundColor(['#bcbdfdb5','#bcbdfdb5','#bcbdfdb5','#bcbdfdb5','#bcbdfdb5','#bcbdfdb5','#bcbdfdb5','#bcbdfdb5','#bcbdfdb5','#bcbdfdb5','#bcbdfdb5','#bcbdfdb5'])
        ->color(['#7576bd','#7576bd','#7576bd','#7576bd','#7576bd','#7576bd','#7576bd','#7576bd','#7576bd','#7576bd','#7576bd','#7576bd']);
        // ///////////////// BAR CHART FOR WORKORDER END/////////////////////////////


        return view('dashboard.index',compact('total_alarms','current_alarms','acknowledge_alarms','unacknowledge_alarms','in_progress_routes','finish_routes','cancelled_routes','late_routes','open_workorders','close_workorders','open_acknowledge_workorders','open_unacknowledge_workorders','workorder_barchart'));                
           
            
    }
    public function detailInProgressRoute()
    {   
        $in_progress_routes= RouteHistory::where('route_state','in progress')->get();
        foreach ($in_progress_routes as $key => $value)
        {
          $in_progress_routes[$key]['route_id']= Helper::routeIdToDes($value['route_id']);
          $in_progress_routes[$key]['operator_id']= Helper::userIdToName($value['operator_id']);  
        }
        return view('routes.inprogress',compact('in_progress_routes'));
    } 
    public function detailRouteComplete()
    {   
       $finish_routes= RouteHistory::where('route_state','complete')
        ->get();
      
        foreach ($finish_routes as $key => $value)
        {
          $finish_routes[$key]['route_id']= Helper::routeIdToDes($value['route_id']);
          $finish_routes[$key]['operator_id']= Helper::userIdToName($value['operator_id']);  
        }
        return view('routes.complete',compact('finish_routes'));
    } 
    public function detailRouteCancle()
    {   
       $cancelled_routes= RouteHistory::where('route_state','cancelled')->get();
      
        foreach ($cancelled_routes as $key => $value)
        {
          $cancelled_routes[$key]['route_id']= Helper::routeIdToDes($value['route_id']);
          $cancelled_routes[$key]['operator_id']= Helper::userIdToName($value['operator_id']);  
        }
        return view('routes.cancle',compact('cancelled_routes'));
    } 
   public function detailRouteDelayed()
    {   
       $late_routes= RouteHistory::where('route_state','late')->get();
        foreach ($late_routes as $key => $value)
        {
          $late_routes[$key]['route_id']= Helper::routeIdToDes($value['route_id']);
          $late_routes[$key]['operator_id']= Helper::userIdToName($value['operator_id']);  
        }
        return view('routes.delay',compact('late_routes'));
    } 
    public function detailTotalAlarm()
    {   
       $data= AlarmHistory::where('new_state','alarm')->get();
        foreach ($data as $key => $value)
        {
          $data[$key]['alarm_name']= Helper::alarmCurrentStateToAlarmName($value['alarm_current_state_id']) ;
               $data[$key]['alarm_parameter_name']= Helper::alarmCurrentStateToParameterName($value['alarm_current_state_id']) ;
               $data[$key]['alarm_instrument_name']= Helper::alarmCurrentStateToInstrumentName($value['alarm_current_state_id']) ;
               $data[$key]['user_id']= Helper::userIdToName($value['user_id']) ;
            
        }
        return view('alarms.total',compact('data'));
    }
    public function detailCurrentAlarm()
    {   
       $data= AlarmCurrentState::where('state','alarm')->get();
        foreach ($data as $key => $value)
        {
          $data[$key]['alarm_name']= Helper::alarmCurrentStateToAlarmName($value['id']) ;
          $data[$key]['alarm_parameter_name']= Helper::alarmCurrentStateToParameterName($value['id']) ;
          $data[$key]['alarm_instrument_name']= Helper::alarmCurrentStateToInstrumentName($value['id']) ;
            
        }
        return view('alarms.current',compact('data'));
    }
     public function detailAckAlarm()
    {   
       $data=AlarmHistory::where('state_ack', 'acknowledged')
                     ->where( 'new_state', 'alarm')->get();
        foreach ($data as $key => $value)
        {
         $data[$key]['alarm_name']= Helper::alarmCurrentStateToAlarmName($value['alarm_current_state_id']) ;
          $data[$key]['alarm_parameter_name']= Helper::alarmCurrentStateToParameterName($value['alarm_current_state_id']) ;
          $data[$key]['alarm_instrument_name']= Helper::alarmCurrentStateToInstrumentName($value['alarm_current_state_id']) ;
          $data[$key]['user_id']= Helper::userIdToName($value['user_id']) ;
            
        }
        return view('alarms.ackalarm',compact('data'));
    }
     public function detailUnAckAlarm()
    {   
       $data=AlarmHistory::where('state_ack', 'unacknowledged')
                  ->where( 'new_state', 'alarm')->get();
        foreach ($data as $key => $value)
        {
          $data[$key]['alarm_name']= Helper::alarmCurrentStateToAlarmName($value['alarm_current_state_id']) ;
          $data[$key]['alarm_parameter_name']= Helper::alarmCurrentStateToParameterName($value['alarm_current_state_id']) ;
          $data[$key]['alarm_instrument_name']= Helper::alarmCurrentStateToInstrumentName($value['alarm_current_state_id']) ;
            
        }
        return view('alarms.unackalarm',compact('data'));
    }
    public function getAlarmTotal(Request $request)
    {   
      $alarm_total=DB::table('alarm_history')->where('new_state','alarm')->count();

      $current_alarm=DB::table('alarm_current_state')->where('state', 'alarm')->count();

       $alarm_ack=DB::table('alarm_history')->where('state_ack', 'acknowledged')->count();

      $alarm_unack=DB::table('alarm_history')->where('state_ack', 'unacknowledged')->count();

      return $alarm_total;
    }
}
