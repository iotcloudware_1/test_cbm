<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Permission;
use Spatie\Permission\Models\Role;
use DB;
use Hash;
use DataTables;

class PermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
   public function index(Request $request)
    {

        if($request->ajax())
        {
            $data = Permission::latest()->get();
            return Datatables::of($data)
            ->addColumn('action', 'datatables.action-permission')
            ->rawColumns(['action'])
            ->make(true);
        }    
        return view('permissions.index');
        //     ->with('i', ($request->input('page', 1) - 1) * 5);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('permissions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

public function store(Request $request)
    {

        Permission::create(['name' => $request->input('name'), 'guard_name'=> 'web']);
        return redirect()->route('permissions.index')
                        ->with('success','Permission created successfully');
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        Permission::updateOrCreate(['id' => $request->permission_id],
                ['name' => $request->name]);        
   
         return redirect()->route('permissions.index')
                        ->with('success','Permission created successfully');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
 public function destroy($id)
{
        try
        {
        DB::table("permissions")->where('id',$id)->delete();
        return redirect()->route('permissions.index')
        ->with('success','Permission deleted successfully');
        }
        catch(Exception $e)
        {
            $err = json_encode($e);
            $err_array = json_decode($err,true);
            if($err_array['errorInfo'][1] == 1451)
            {
                return Response::json(array('success' => false, 'msg_string' => 'Permission is in use.'), 500);
            }
          
        } 
    }

}


    

