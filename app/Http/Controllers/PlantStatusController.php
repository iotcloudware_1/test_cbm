<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Permission;
use App\User;
use App\Model\Plant;
use App\Model\PlantStatus;
use App\Model\Status;
use DB;
use Hash;
use App\Post;
use Yajra\Datatables\Datatables;
use Response;
use Helper;

class PlantStatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

 function __construct()
    {
         $this->middleware('permission:plantstatus-list|plantstatus-create|plantstatus-edit|plantstatus-delete', ['only' => ['index','store']]);
         
         $this->middleware('permission:plantstatus-create', ['only' => ['create','store']]);
         $this->middleware('permission:plantstatus-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:plantstatus-delete', ['only' => ['destroy']]);
         return view('permission-error');
    }
    
    public function index(Request $request)
           {

            if($request->ajax())
            {
                $data = PlantStatus::latest()->get();

                foreach ($data as $key => $value) 
                {
                    $data[$key]['plant_id']= Helper::plantIdToName($value['plant_id']);
                }
                foreach ($data as $key => $value) 
                {
                    $data[$key]['status_id']= Helper::statusIdToDes($value['status_id']);
                }


                return Datatables::of($data)
                  ->addColumn('action', 'datatables.action-button')
                  ->rawColumns(['action'])
                  ->make(true);
            }    
             return view('plantstatus.index');
        }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $plant=Plant::all()->pluck('name','id');
        $status=Status::all()->pluck('description','id');
        return view('plantstatus.create', compact('plant','status'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
   {
        $this->validate($request, [
            'plant_id' => 'required',
            'status_id' => 'required',
        ]);

        $input = $request->all();
        
        $plantstatus = PlantStatus::create($input);

        return redirect()->route('plantstatus.index')
                        ->with('success','Plant Status created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $plantstatus = PlantStatus::find($id);
        $plantstatus->plant_id = Helper::plantIdToName($plantstatus->plant_id);
        $plantstatus->status_id = Helper::statusIdToDes($plantstatus->status_id);
        return view('plantstatus.show',compact('plantstatus'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $plant=Plant::all()->pluck('name','id');
        $status=Status::all()->pluck('description','id');
        $plantstatus = PlantStatus::find($id);
        return view('plantstatus.edit',compact('plantstatus','plant','status'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'plant_id' => 'required',
            'status_id' => 'required',

        ]);

        $plantstatus = PlantStatus::find($id);
        $plantstatus->plant_id = $request->input('plant_id');
        $plantstatus->status_id = $request->input('status_id');
        $plantstatus->save();


        return redirect()->route('plantstatus.index')
                        ->with('success','Plant Status updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table("plant_status")->where('id',$id)->delete();
    }
}
