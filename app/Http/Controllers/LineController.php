<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\FormBuilder;
use App\Forms\DepartmentForm;
use App\Http\Controllers\Controller;
//use App\Http\Controllers\Plant;
use App\Model\Asset;
use App\Model\Instrument;
use App\Model\Parameter;
use App\Model\AlarmCurrentState;
use App\Model\Department;
use App\Model\Line;
use App\Post;
use App\User;
use Auth;
use Yajra\Datatables\Datatables;
use DB;
use Response;
Use Exception;
use Hash;
use Helper;

class LineController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
         $this->middleware('permission:line-list|line-create|line-edit|line-delete', ['only' => ['index','store']]);
         
         $this->middleware('permission:line-create', ['only' => ['create','store']]);
         $this->middleware('permission:line-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:line-delete', ['only' => ['destroy']]);
         return view('permission-error');
    }
    public function index(Request $request)
    {
         if($request->ajax())
        {
            $data = Line::latest()->get();

            foreach ($data as $key => $value) {
                $data[$key]['department_id']= Helper::departmentIdToName($value['department_id']);
            }


            return Datatables::of($data)
              ->addColumn('action', 'datatables.action-button')
              ->rawColumns(['action'])
              ->make(true);
        }    
            return view('lines.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $department=Department::all()->pluck('name','id');
            //$plant=json_decode($plants, TRUE);
            //dd($plant);
     
        return view('lines.create', compact('department'));
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'department_id' => 'required',
            'name' => 'required',

        ]);
           
            
            $input = $request->all();   

            $input['user_id'] = Auth::user()->id;
            $line_exist = Line::count();
            $line_limit = User::where('id',Auth::user()->id)->value('line_limit');


            if($line_limit > $line_exist)
            {
                Line::create($input);
                return redirect('/lines');
            }
            else
            {
                // return redirect('/departments')->with('msg','Limit reached. Please contact Administrator for adding more department');
                echo("<script>
                        alert('Limit Reached');
                        window.location.href = 'lines'
                    </script>");
            }
            
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
{
        $line = Line::find($id);
        $line->department_id = Helper::departmentIdToName($line->department_id);
        return view('lines.show',compact('line'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $department=Department::all()->pluck('name','id');
        $line = Line::find($id);
        return view('lines.edit',compact('department','line'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'department_id' => 'required',
            'name' => 'required',

        ]);


        $line = Line::find($id);
        $line->department_id = $request->input('department_id');
        $line->name = $request->input('name');
        $line->description = $request->input('description');
        $line->save();


        return redirect()->route('lines.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
       {
            try
            {
                DB::table("lines")->where('id',$id)->delete();
                return redirect()->route('departments.index')
                 ->with('success','Line deleted successfully');
            }
            catch(Exception $e)
            {
                $err = json_encode($e);
                $err_array = json_decode($err,true);
                if($err_array['errorInfo'][1] == 1451)
                {
                    return Response::json(array('success' => false, 'msg_string' => 'Line is in use.'), 500);
                }
              
            } 
        }
    public function getLines()
    {
        $lines = Line::all()->toJson();

        return $lines;
    }    



    public function showDepartmentLine(Request $request)
    {

            $department_name= Helper::departmentIdToName($request['id']);
            $unit_id= Department::where('id',$request['id'])->value('unit_id');
            $unit_name= Helper::unitIdToName($unit_id);  
            $show_department_line= Line::where('department_id',$request['id'])->get();
            $alarms_arr=[];
            $line_id;

            foreach ($show_department_line as $key => $line) 
            {
               $line_id = $line['id'];
               $alarm_count = 0;
               $healthy_count=0; 
                $assets= Line::find($line['id'])->assets; 

                foreach ($assets as $key => $asset) 
                {
                    $instruments= Asset::find($asset->id)->instruments;

                    foreach ($instruments as $instrument) 
                    {
                        $parameters= Instrument::find($instrument->id)->parameters;

                        foreach ($parameters as $parameter) 
                        {
                            $alarms= Parameter::find($parameter->id)->hasAlarm;
                            //dd($alarms);
                            if($alarms != null)
                            {
                                foreach ($alarms as $key => $alarm)
                                {
                                    $current_alarms= AlarmCurrentState::where('alarm_id',$alarm->id)->where('state','alarm')->count();
                                    $healthy_alarms= AlarmCurrentState::where('alarm_id',$alarm->id)->where('state','normal')->count();
                                    $alarm_count = $alarm_count + $current_alarms;
                                    $healthy_count = $healthy_count + $healthy_alarms;
                                }
                            }
                            else
                            {
                                $alarm_count = $alarm_count;
                                $healthy_count = $healthy_count;

                            }    
                        }   
                    }

                }
                    $temp = array('line_id' =>$line_id,'alarm_count'=>$alarm_count,'healthy_count'=>$healthy_count);
                    array_push($alarms_arr, $temp);                                    
            }  

            foreach ($show_department_line as $key => $department) 
            {
                $show_department_line[$key]['alarm_count'] = $alarms_arr[$key]['alarm_count'];
                $show_department_line[$key]['healthy_count'] = $alarms_arr[$key]['healthy_count'];

            }            
            foreach ($show_department_line as $key => $value) 
            {   
                
                $show_department_line[$key]['department_id']= Helper::departmentIdToName($value['department_id']);
            }  
            
        return view('dashboardtree.lines',compact('show_department_line','department_name','unit_name'));
    }
    public function allLines()
    { 
            $department_name='Department';
            $unit_name='Units';
            $alllines = Line::all();
            $alarms_arr=[];
            $line_id;

            foreach ($alllines as $key => $line) 
            {
               $line_id = $line['id'];
               $alarm_count = 0;
               $healthy_count=0; 
                $assets= Line::find($line['id'])->assets; 

                foreach ($assets as $key => $asset) 
                {
                    $instruments= Asset::find($asset->id)->instruments;

                    foreach ($instruments as $instrument) 
                    {
                        $parameters= Instrument::find($instrument->id)->parameters;

                        foreach ($parameters as $parameter) 
                        {
                            $alarms= Parameter::find($parameter->id)->hasAlarm;
                            //dd($alarms);
                            if($alarms != null)
                            {
                                foreach ($alarms as $key => $alarm)
                                {
                                    $current_alarms= AlarmCurrentState::where('alarm_id',$alarm->id)->where('state','alarm')->count();
                                    $healthy_alarms= AlarmCurrentState::where('alarm_id',$alarm->id)->where('state','normal')->count();
                                    $alarm_count = $alarm_count + $current_alarms;
                                    $healthy_count = $healthy_count + $healthy_alarms;
                                }
                            }
                            else
                            {
                                $alarm_count = $alarm_count;
                                $healthy_count = $healthy_count;

                            }    
                        }   
                    }
                }    
                  $temp = array('line_id' =>$line_id,'alarm_count'=>$alarm_count,'healthy_count'=>$healthy_count);
                  array_push($alarms_arr, $temp);                                
            }  

            foreach ($alllines as $key => $department) 
            {
                $alllines[$key]['alarm_count'] = $alarms_arr[$key]['alarm_count'];
                $alllines[$key]['healthy_count'] = $alarms_arr[$key]['healthy_count'];

            }           
            foreach ($alllines as $key => $value) 
              {   
                
                $alllines[$key]['department_id']= Helper::departmentIdToName($value['department_id']);
              } 
        return view('dashboardtree.alllines',compact('alllines','department_name','unit_name'));

    } 
}


