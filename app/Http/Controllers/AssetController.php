<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\FormBuilder;
use App\Forms\DepartmentForm;
use App\Http\Controllers\Controller;
use App\Model\Asset;
use App\Model\Line;
use App\Model\Instrument;
use App\Model\Department;
use App\Model\Parameter;
use App\Model\AlarmCurrentState;
use App\Post;
use App\User;
use Auth;
use Yajra\Datatables\Datatables;
use DB;
use Response;
Use Exception;
use Hash;
use Helper;


class AssetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
         $this->middleware('permission:asset-list|asset-create|asset-edit|asset-delete', ['only' => ['index','store']]);
         
         $this->middleware('permission:asset-create', ['only' => ['create','store']]);
         $this->middleware('permission:asset-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:asset-delete', ['only' => ['destroy']]);
         return view('permission-error');
    }
    public function index(Request $request)
    {   
            if($request->ajax())
            {
                $data = Asset::latest()->get();

                foreach ($data as $key => $value) {
                    $data[$key]['line_id']= Helper::lineIdToName($value['line_id']);
                }


                return Datatables::of($data)
                  ->addColumn('action', 'datatables.action-button')
                  ->rawColumns(['action'])
                  ->make(true);
            }    
             return view('assets.index');

             // $data = dep::latest()->get();
             // dd($data);

            //     ->with('i', ($request->input('page', 1) - 1) * 5);
        }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

       $line=Line::all()->pluck('name','id');
            //$plant=json_decode($plants, TRUE);
            //dd($plant);
     
        return view('assets.create', compact('line'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
   {
        $this->validate($request, [
            'line_id' => 'required',
            'name' => 'required',
        ]);
           
            
            $input = $request->all();   

            $input['user_id'] = Auth::user()->id;
            $asset_exist = Asset::count();
            $asset_limit = User::where('id',Auth::user()->id)->value('asset_limit');


            if($asset_limit > $asset_exist)
            {
                Asset::create($input);
                return redirect('/assets');
            }
            else
            {
                // return redirect('/departments')->with('msg','Limit reached. Please contact Administrator for adding more department');
                echo("<script>
                        alert('Limit Reached');
                        window.location.href = 'assets'
                    </script>");
            }
            
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
   {
        $asset = Asset::find($id);
        $asset->line_id = Helper::lineIdToName($asset->line_id);
        return view('assets.show',compact('asset'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $line=Line::all()->pluck('name','id');
        $asset = Asset::find($id);
        return view('assets.edit',compact('line','asset'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
   {
        $this->validate($request, [
            'line_id' => 'required',
            'name' => 'required',

        ]);


        $asset = Asset::find($id);
        $asset->line_id = $request->input('line_id');
        $asset->name = $request->input('name');
        $asset->description = $request->input('description');
        $asset->save();


        return redirect()->route('assets.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       
        try
        {
            DB::table("assets")->where('id',$id)->delete();
            return redirect()->route('assets.index')
            ->with('success','Assets deleted successfully');
        }
        catch(Exception $e)
        {
            $err = json_encode($e);
            $err1 = json_decode($err,true);
            if($err1['errorInfo'][1] == 1451)
            {
                return Response::json(array('success' => false, 'msg_string' => 'Asset is in use.'), 500);
            }
          
        } 


    }
    public function getAssets()
    {
        $assets = Asset::all()->toJson();

        return $assets;
    } 

    // public function getAssetDashoard(Request $request)
    // {
    //     $show;
    //     if($request['id']== 'all')
    //     {
    //         $show = 'all';
    //         $assets = Asset::all();
    //         $alarms_arr=[];
    //         $asset_id;
    //         foreach ($assets as $key => $asset) 
    //         {
    //             $instruments= Asset::find($asset['id'])->instruments;
    //             $asset_id = $asset['id'];
    //             $alarm_count = 0;
    //             $healthy_count=0;
    //             foreach ($instruments as $instrument) 
    //             {
    //                 $parameters= Instrument::find($instrument->id)->parameters;

    //                 foreach ($parameters as $parameter) 
    //                 {
    //                     $alarms= Parameter::find($parameter->id)->hasAlarm;
    //                     //dd($alarms);
    //                     if($alarms != null)
    //                     {
    //                         foreach ($alarms as $key => $alarm)
    //                         {
    //                             $current_alarms= AlarmCurrentState::where('alarm_id',$alarm->id)->where('state','alarm')->count();
    //                             $healthy_alarms= AlarmCurrentState::where('alarm_id',$alarm->id)->where('state','normal')->count();
    //                             $alarm_count = $alarm_count + $current_alarms;
    //                             $healthy_count = $healthy_count + $healthy_alarms;
    //                         }
    //                     }
    //                     else
    //                     {
    //                         $alarm_count = $alarm_count;
    //                         $healthy_count = $healthy_count;

    //                     }    
    //                 }   
    //             }

    //             $temp = array('asset_id' =>$asset_id,'alarm_count'=>$alarm_count,'healthy_count'=>$healthy_count);
    //             array_push($alarms_arr, $temp);
    //         }  
             
    //         foreach ($assets as $key => $asset) {
    //             $assets[$key]['alarm_count'] = $alarms_arr[$key]['alarm_count'];
    //             $assets[$key]['healthy_count'] = $alarms_arr[$key]['healthy_count'];

    //         }            
    //         return view('dashboardtree.assets',compact('assets','show'));            
    //     }
    //     else
    //     {
    //         $show = 'specific';
    //         $assets = Asset::where('line_id',$request['id'])->get();
    //         $alarms_arr=[];
    //         $asset_id;
    //         foreach ($assets as $key => $asset) 
    //         {
    //             $instruments= Asset::find($asset['id'])->instruments;
    //             $asset_id = $asset['id'];
    //             $alarm_count = 0;
    //             $healthy_count=0;
    //             foreach ($instruments as $instrument) 
    //             {
    //                 $parameters= Instrument::find($instrument->id)->parameters;

    //                 foreach ($parameters as $parameter) 
    //                 {
    //                     $alarms= Parameter::find($parameter->id)->hasAlarm;
    //                     //dd($alarms);
    //                     if($alarms != null)
    //                     {
    //                         foreach ($alarms as $key => $alarm)
    //                         {
    //                             $current_alarms= AlarmCurrentState::where('alarm_id',$alarm->id)->where('state','alarm')->count();
    //                             $healthy_alarms= AlarmCurrentState::where('alarm_id',$alarm->id)->where('state','normal')->count();
    //                             $alarm_count = $alarm_count + $current_alarms;
    //                             $healthy_count = $healthy_count + $healthy_alarms;
    //                         }
    //                     }
    //                     else
    //                     {
    //                         $alarm_count = $alarm_count;
    //                         $healthy_count = $healthy_count;

    //                     }    
    //                 }   
    //             }

    //             $temp = array('asset_id' =>$asset_id,'alarm_count'=>$alarm_count,'healthy_count'=>$healthy_count);
    //             array_push($alarms_arr, $temp);
    //         }  
             
    //         foreach ($assets as $key => $asset) {
    //             $assets[$key]['alarm_count'] = $alarms_arr[$key]['alarm_count'];
    //             $assets[$key]['healthy_count'] = $alarms_arr[$key]['healthy_count'];

    //         }
            
    //         return view('dashboardtree.assets',compact('assets','show'));
    //     }
    // }
    public function showLineAsset(Request $request)
    {       
            $line_name=Helper::lineIdToName($request['id']);

            $department_id= Line::where('id',$request['id'])->value('department_id');
            $department_name= Helper::departmentIdToName($department_id);

            $department_id= Line::where('id',$request['id'])->value('department_id');
            $unit_id= Department::where('id',$department_id)->value('unit_id');
            $unit_name= Helper::unitIdToName($unit_id);      

            $show_line_asset= Asset::where('line_id',$request['id'])->get();
            $alarms_arr=[];
            $asset_id;
            foreach ($show_line_asset as $key => $asset) 
            {
                $instruments= Asset::find($asset['id'])->instruments;
                $asset_id = $asset['id'];
                $alarm_count = 0;
                $healthy_count=0;
                foreach ($instruments as $instrument) 
                {
                    $parameters= Instrument::find($instrument->id)->parameters;

                    foreach ($parameters as $parameter) 
                    {
                        $alarms= Parameter::find($parameter->id)->hasAlarm;
                        //dd($alarms);
                        if($alarms != null)
                        {
                            foreach ($alarms as $key => $alarm)
                            {
                                $current_alarms= AlarmCurrentState::where('alarm_id',$alarm->id)->where('state','alarm')->count();
                                $healthy_alarms= AlarmCurrentState::where('alarm_id',$alarm->id)->where('state','normal')->count();
                                $alarm_count = $alarm_count + $current_alarms;
                                $healthy_count = $healthy_count + $healthy_alarms;
                            }
                        }
                        else
                        {
                            $alarm_count = $alarm_count;
                            $healthy_count = $healthy_count;

                        }    
                    }   
                }

                $temp = array('asset_id' =>$asset_id,'alarm_count'=>$alarm_count,'healthy_count'=>$healthy_count);
                array_push($alarms_arr, $temp);
            }  
             
            foreach ($show_line_asset as $key => $asset) {
                $show_line_asset[$key]['alarm_count'] = $alarms_arr[$key]['alarm_count'];
                $show_line_asset[$key]['healthy_count'] = $alarms_arr[$key]['healthy_count'];

            }
        foreach ($show_line_asset as $key => $value) 
          {   
            
            $show_line_asset[$key]['line_id']= Helper::lineIdToName($value['line_id']);
          }  
        
        return view('dashboardtree.assets',compact('show_line_asset','unit_name','department_name','line_name'));
    } 
   public function allAssets()
    { 
            $allassets = Asset::all();
            $alarms_arr=[];
            $asset_id;
            foreach ($allassets as $key => $asset) 
            {
                $instruments= Asset::find($asset['id'])->instruments;
                $asset_id = $asset['id'];
                $alarm_count = 0;
                $healthy_count=0;
                foreach ($instruments as $instrument) 
                {
                    $parameters= Instrument::find($instrument->id)->parameters;

                    foreach ($parameters as $parameter) 
                    {
                        $alarms= Parameter::find($parameter->id)->hasAlarm;
                        //dd($alarms);
                        if($alarms != null)
                        {
                            foreach ($alarms as $key => $alarm)
                            {
                                $current_alarms= AlarmCurrentState::where('alarm_id',$alarm->id)->where('state','alarm')->count();
                                $healthy_alarms= AlarmCurrentState::where('alarm_id',$alarm->id)->where('state','normal')->count();
                                $alarm_count = $alarm_count + $current_alarms;
                                $healthy_count = $healthy_count + $healthy_alarms;
                            }
                        }
                        else
                        {
                            $alarm_count = $alarm_count;
                            $healthy_count = $healthy_count;

                        }    
                    }   
                }

                $temp = array('asset_id' =>$asset_id,'alarm_count'=>$alarm_count,'healthy_count'=>$healthy_count);
                array_push($alarms_arr, $temp);
            }  
             
            foreach ($allassets as $key => $asset) {
                $allassets[$key]['alarm_count'] = $alarms_arr[$key]['alarm_count'];
                $allassets[$key]['healthy_count'] = $alarms_arr[$key]['healthy_count'];

            }         
        foreach ($allassets as $key => $value) 
          {   
            
            $allassets[$key]['line_id']= Helper::lineIdToName($value['line_id']);
          } 
        return view('dashboardtree.allassets',compact('allassets'));

    }  
}




 
   