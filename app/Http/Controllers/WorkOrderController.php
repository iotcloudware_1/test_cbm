<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\WorkOrder;
use App\Model\InstrumentWorkOrder;
use App\Model\Instrument; 
use App\Model\AlarmHistory; 
use App\Model\CurrentWorkOrder;
use App\Model\WorkOrderHistory; 
use App\Model\Rating;
use App\Model\WorkorderMedia;
use App\Model\ForgetPassword;
use App\Post;
use App\User;
use Auth;
use Yajra\Datatables\Datatables;
use DB;
use Response;
Use Exception;
use Hash;
use Helper;
use App\Charts\DefaultChart;
use App\Providers\ChartsServiceProvider;
use App\Jobs\EmailJob;
use Illuminate\Support\Str;
use Carbon\Carbon;

class WorkOrderController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  function __construct()
  {
      $this->middleware('permission:workorder-list|workorder-create|workorder-edit|workorder-delete', ['only' => ['index','store']]);
       
      $this->middleware('permission:workorder-create', ['only' => ['create','store']]);
      $this->middleware('permission:workorder-edit', ['only' => ['edit','update']]);
      $this->middleware('permission:workorder-delete', ['only' => ['destroy']]);
      
      return view('permission-error');
  }



  public function index(Request $request)
  {
    if($request->ajax())
    {
      $data = WorkOrder::latest()->get();

      foreach ($data as $key => $Workorder) {

         $data[$key]['instrument_id'] = Helper::instrumentIdToName($Workorder['instrument_id']);
        if($Workorder['send_email'] == 0)
        {
            $data[$key]['send_email'] = 'Disable';
        }
        else
        {
            $data[$key]['send_email'] = 'Enable';
        }
        if($Workorder['send_sms'] == 0)
        {
            $data[$key]['send_sms'] = 'Disable';
        }
        else
        {
            $data[$key]['send_sms'] = 'Enable';
        }
      }

      return Datatables::of($data)
      ->addColumn('action', 'datatables.action-button')
      ->rawColumns(['action'])
      ->make(true);
    }

    return view('workorders.index');
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create ()
  {
    $instrument=Instrument::all()->pluck('name','id');
    
    return view('workorders.create',compact('instrument'));    
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
     $this->validate($request, [
        'description' => 'required',
        'type' => 'required',
        'priority' => 'required', 
    ]);

    $input = $request->all();
    
    $WorkOrder = WorkOrder::create($input);

    return redirect()->route('workorders.index')
      ->with('success','Workorder created successfully');    
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $workorder = WorkOrder::find($id);
    $send_sms=$workorder['send_sms'];
    $send_email=$workorder['send_email'];

    if($send_email== 0)
    {
        $workorder['send_email'] = 'Disable';
    }
    else
    {
        $workorder['send_email']= 'Enable';
    }
    if($send_sms== 0)
    {
        $workorder['send_sms'] = 'Disable';
    }
    else
    {
        $workorder['send_sms'] = 'Enable';
    }

    return view('workorders.show',compact('workorder'));
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $instrument=Instrument::all()->pluck('name','id');
    $workorder = WorkOrder::find($id);
    
    return view('workorders.edit',compact('workorder','instrument'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $this->validate($request, [
        'description' => 'required',
        'type' => 'required',
        'priority' => 'required',

    ]);

    $workorder = WorkOrder::find($id);
    $workorder->description = $request->input('description');
    $workorder->type = $request->input('type');
    $workorder->priority = $request->input('priority');
    $workorder->send_email = $request->input('send_email');
    $workorder->send_sms = $request->input('send_sms');
    $workorder->save();

    return redirect()->route('workorders.index')
      ->with('success','Work order updated successfully');
  }
  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    DB::table("workorders")->where('id',$id)->delete();
    
    return redirect()->route('workorder.index')
      ->with('success','Workorder deleted successfully');
  }
  
  public function instrumentWorkOrder()
  {   
    $workorder_id_options = WorkOrder::pluck('name','id')->toArray();
    $instrument_id_options = Instrument::pluck('name','id')->toArray();

    return view('workorders.workorder-instrument', compact('workorder_id_options','instrument_id_options'));
  }

  public function storeinstrumentWorkOrder(Request $request)
  {
    $this->validate($request, [
      'workorder_id' => 'required',
      'instrument_id' => 'required',
      'reported_time' => 'required',
      'priority' => 'required',
      'remarks' => 'required',         
    ]);

    $input = $request->all();
    $input['reported_by'] = Auth::user()->id;
    $workorder = InstrumentWorkOrder::create($input);

    return redirect('/instrumentworkorder');
  }

  public function getWorkOrders(Request $request)
  {
    $workorders = WorkOrder::where('instrument_id',$request['instrument_id'])->get()->toArray();
    if($workorders=='[]')
    {
      return '[{"id":0,"description":"","instrument_id":0,"type":"","priority":"","send_email":0,"send_sms":0,"expected_time":0,"created_at":"","message":"No Workorder Found"}]';
    }  
    $work_orders_data=[];
    foreach ($workorders as $key => $workorder) 
    {
      $work_orders= array('workorder_id'=>$workorder['id'],'workorder_name'=>$workorder['description'],'type'=>$workorder['type'],'priority'=>$workorder['priority'],'message'=>'');
      array_push($work_orders_data, $work_orders);
    }        
    return json_encode($work_orders_data);      
  }

  public function postWorkOrders(Request $request)
  {

    $workorder_id= Workorder::where('id',$request['instrument_id'])->value('id'); 

    $instrumentWorkorder = new CurrentWorkOrder;
    $instrumentWorkorder->workorder_id= $request['workorder_id'];
    $this->validate($request, [
            'file' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'audio' =>'nullable|file|mimes:audio/mpeg,mpga,mp3,wav,aac'
        ]);

////////////////STORE ONE IMAGE SUCCESSFULLY//////////////////////////////////////
    // if($request->hasFile('base_image'))
    // {
    //      \Log::info($request->base_image); 
    //     // $input = $request->all();
    //     $image = $request->file('base_image');
    //     $imageName = time().'.'.$image->extension(); 
    //     $image->move(public_path('workorder-images'), $imageName);
        
    //     $instrumentWorkorder->base_image = '/workorder-images/'.$imageName;

    // } 

      $files = $request->file('base_image');

        if (!empty($files)){
            foreach ($files as $file){

            // $filename = $file->getClientOriginalName();
            $newname =  time().'.'.$file->extension(); 
            $file->move('uploads/workorder_images', $newname);

            WorkorderMedia::insert( [
                'base_image'=> 'uploads/workorder_images/'. $newname,
                'workorder_id'=>$request['workorder_id'],
                'media_type'=>'image',

                //you can put other insertion here
            ]);
            }


        }
      /*Insert your data*/
     if($request->hasFile('audio'))
        {
             \Log::info($request->base_image); 
            // $input = $request->all();
            $audio = $request->file('audio');
            $audioName = time().'.'.$audio->extension(); 
            $audio->move(public_path('workorder-audio'), $audioName);
            WorkorderMedia::insert( [
                    'base_image'=> '/workorder-audio/'.$audioName,
                    'workorder_id'=>$request['workorder_id'],
                    'media_type'=>'audio',

                    //you can put other insertion here
                ]);
            
            // $instrumentWorkorder->base_image = '/workorder-audio/'.$audioName;

        } 
///////////////////TORE ONE IMAGE SUCCESSFULLY/////////////////////////////////////////////////////////      


    $instrumentWorkorder->priority= $request['priority'];
    $instrumentWorkorder->remarks= $request['remarks'];
    $instrumentWorkorder->assigned_by=$request['assigned_by'];
    $instrumentWorkorder->assigned_to= $request['assigned_to'];
    $instrumentWorkorder->status= "open";
    $instrumentWorkorder->acknowledgement='unacknowledged';
    $instrumentWorkorder->save();  

    $current_workorder_id= CurrentWorkOrder::orderBy('created_at','desc')->first();

    $workorder_history= new WorkOrderHistory;
    $workorder_history->current_workorder_id= $current_workorder_id['id'];
    $workorder_history->action= "created";
    $workorder_history->action_taken_by= $request['assigned_by'] ;
    $workorder_history->save();

    $workorder_history= new WorkOrderHistory;
    $workorder_history->current_workorder_id= $current_workorder_id['id'];
    $workorder_history->action= "unacknowledged";
    $workorder_history->action_taken_by= $request['assigned_by'] ;
    $workorder_history->save();       
  }

  // public function test()
  // {
  //   $test=WorkorderMedia::where('id',60)->value('base_image');

  //   return view('workorders.test',compact('test'));
  // }
  public function getOpenWorkOrderLists (Request $request)
  {
    $reported_workorders=[];
    $instrument_id = $request['instrument_id'];
    $workorder_id = DB::table('workorders')
    ->join('current_workorder', function ($join) use ($instrument_id){
        $join->on('workorders.id', '=', 'current_workorder.workorder_id')
        ->where('workorders.instrument_id', $instrument_id)
        ->where('current_workorder.status', 'open');
    })
    ->select('current_workorder.workorder_id','workorders.description',
      'current_workorder.id','current_workorder.priority','current_workorder.remarks')
    ->get();
    if($workorder_id=='[]')
    {
      return '[{"workorder_id":0,"description":"","id":0,"priority":"","remarks":"","message":"No work order found against this instrument"}]';
    }
    $current_workorders=[];
    foreach ($workorder_id as $key => $workorder) 
    { 
      $temp=(array)$workorder;
      $temp['message']='';
      array_push($current_workorders, $temp);
    }

    return json_encode($current_workorders);
  }

  public function postWorkOrderResolved(Request $request)
  {     
    $workorder_resolved = CurrentWorkOrder::where('id',$request['id'])->where('status','open')->update(array('status' => 'closed'));

    $resolved_workorder_id= CurrentWorkOrder::orderBy('created_at','desc')->first();
    $workorder_history= new WorkOrderHistory;
    $workorder_history->current_workorder_id= $resolved_workorder_id['id'];
    $workorder_history->action= "closed";
    $workorder_history->action_taken_by= $request['action_taken_by'];
    $workorder_history->save();    
  }

  public function WorkOrderHistroy(Request $request)
  {
    if($request->ajax())
    {
      $data = WorkOrderHistroy::latest()->get();
      return Datatables::of($data)
        ->addColumn('action', 'datatables.action-button')
        ->rawColumns(['action'])
        ->make(true);
    }    

    return view('workorderhistory.index');
  }

  public function checkAcknowledgement()
  {
    $check_acknowledgement=CurrentWorkOrder::where('status','open')->where('acknowledgement','unacknowledged')->count();
    if($check_acknowledgement==0)
    {
      return 1;
    }
    else
    {
      return 0;
    }
  }

  public function acknowledgeWorkOrder()
  {
    $workorder_resolved = CurrentWorkOrder::where('status','open')->where('acknowledgement','unacknowledged')->update(array('acknowledgement' => 'acknowledged'));

    $resolved_workorder_id= CurrentWorkOrder::orderBy('created_at','desc')->first();
    $workorder_history= new WorkOrderHistory;
    $workorder_history->current_workorder_id= $resolved_workorder_id['id'];
    $workorder_history->action= "acknowledged";
    $workorder_history->action_taken_by= Auth::User()->id;
    $workorder_history->save();   

    return redirect()->route('currentworkorder.index');
  }

  public function ackIndividualWorkorder(Request $request)
  {
    $workorder_resolved = CurrentWorkOrder::where('id',$request['id'])->where('status','open')->where('acknowledgement','unacknowledged')->update(array('acknowledgement' => 'acknowledged'));

    $resolved_workorder_id= CurrentWorkOrder::orderBy('created_at','desc')->first();
    $workorder_history= new WorkOrderHistory;
    $workorder_history->current_workorder_id= $resolved_workorder_id['id'];
    $workorder_history->action= "acknowledged";
    $workorder_history->action_taken_by= Auth::User()->id;
    $workorder_history->save();   

    return redirect()->route('currentworkorder.index');
  }  

  public function getAllWorkOrders()
  {
    return WorkOrder::all()->toJson();
  }

  // public function calculateWorkOrderPercentages()
  // {
  //   $open_ids=DB::table('workorder_history')
  //     ->where('state', 'open')
  //     ->count();
  //   $closed_ids=DB::table('workorder_history')
  //     ->where('state', 'closed')
  //     ->count();
  //   $total_workorders=DB::table('workorder_history')
  //     ->count();      
      
  //   $open_percetange= ($open_ids / $total_workorders) * 100;
  //   $closed_percetange= ($resolved_ids / $total_workorders) * 100;
    
  //   $labels= ['open','closed'];
  //   $chart = new DefaultChart;
  //   $chart->labels($labels);
  //   $chart->dataset('My dataset', 'pie', [50,50]);

  //   return view('dashboard.index',compact('open_percetange','closed_percetange'));             
  // }

  // public function currentWorkOrderList()
  // {
  //   $workOrderlist= ReportWorkOrder::where('state','open')->get();

  //   foreach ($workorderlist as $key => $value) 
  //   {   
  //     $workorderlist[$key]['workorder_id']= Helper::workOrderIdToName($value['workorder_id']);
  //     $workorderlist[$key]['instrument_id']= Helper::instrumentIdToName($value['instrument_id']);
  //   }

  //   return json_encode($workorderlist);
  // }
  // public function getWorkOrderHistory()
  // {
  //   $workorder_history_temp = DB::table('workorder_history')
  //     ->join('reported_workorders', 'workorder_history.reported_workorders_id', '=', 'reported_workorders.id')
  //     ->select('reported_workorders.workorder_id','reported_workorders.instrument_id','reported_workorders.priority','workorder_history.*')
  //     ->get();                  
  //   $workorder_history_array=[];
  //   foreach($workorder_history_temp as $workorder_history)
  //   {
  //       $worked_history_array[] = (array)$workorder_history;
  //   }    
  //   foreach ($workorder_history_array as $key => $row) 
  //   {   
  //     $workorder_history_array[$key]['workorder_id']= Helper::workOrderIdToName($row['workorder_id']);
  //     $workorder_history_array[$key]['instrument_id']= Helper::instrumentIdToName($row['instrument_id']);
  //   }    

  //   return json_encode($workorder_history_array);
  // }
 
  public function getcountdashboard(Request $request)
    {

      $workorder_close= CurrentWorkOrder::where('status','closed')->count();  
      $workorder_open= CurrentWorkOrder::where('status','open')->count(); 
      $workorder_open_ack= CurrentWorkOrder::where('status','open')->where('acknowledgement','acknowledged')->count();
      $workorder_open_unack= CurrentWorkOrder::where('status','open')->where('acknowledgement','unacknowledged')->count();
      $alarm_total=DB::table('alarm_history')->where('new_state','alarm')->count();
      $current_alarm=DB::table('alarm_current_state')->where('state', 'alarm')->count();
      $alarm_ack=DB::table('alarm_history')->where('state_ack', 'acknowledged')->count();
       $alarm_unack=DB::table('alarm_history')->where('state_ack', 'unacknowledged')->count();

      $workorder_count= array('Close Workorders' => $workorder_close ,'Open Workorders'=>$workorder_open, 'Open Workorders Acknowledge' => $workorder_open_ack, 'Open Workorders Unacknowledge' => $workorder_open_unack, 'Total Alarms' => $alarm_total, 'Current Alarms' => $current_alarm, 'Alarms Acknowledge' => $alarm_ack, 'Alarms Unacknowledge' => $alarm_unack );

       return $workorder_count;


    }

  public function WorkOrderHistory()
  {
   // $workorder_history= WorkOrderHistory::all()->toJson();

    $workorder_history = DB::table('workorder_history')
            ->join('current_workorder', 'workorder_history.current_workorder_id', '=', 'current_workorder.id')
            ->select('action','action_taken_by','workorder_id as workorder_name','priority','remarks','assigned_by','assigned_to','status','acknowledgement','workorder_history.created_at')
            ->get();
             $data = json_decode($workorder_history,true);
              foreach ($data as $key => $value)
              {
                  $data[$key]['action_taken_by']= Helper::userIdToName($value['action_taken_by']); 
                  $data[$key]['assigned_by']= Helper::userIdToName($value['assigned_by']);
                  $data[$key]['assigned_to']= Helper::userIdToName($value['assigned_to']);
                 $data[$key]['workorder_name']= Helper::WorkOrderIdToDescription($value['workorder_name']);
                    
              }
    return  $data;
  }

  public function AlarmHistory()
  {
    
    $alarm_history=DB::table('alarm_history')->where( 'alarm_history.created_at', '>', Carbon::now()->subDays(15))
            ->join('alarm_current_state', 'alarm_history.alarm_current_state_id', '=', 'alarm_current_state.id')
            ->join('alarms', 'alarm_current_state.alarm_id', '=', 'alarms.id')
          //    ->join('alarm_current_state', 'alarms.id', '=', 'alarm_current_state.alarm_id')

            ->select('alarm_current_state.alarm_message','old_state','new_state','alarm_current_state.new_value','state_ack','alarms.priority','alarm_history.user_id as acknowledge_by','alarm_history.created_at')
            ->get();
            $data = json_decode($alarm_history,true);
              foreach ($data as $key => $value)
              {
                  $data[$key]['acknowledge_by']= Helper::userIdToName($value['acknowledge_by']);   
              }
    return  $data;
   // return  $alarm_history;
  }


  public function setLocation(Request $request)
  { 
 
    $set_location = Instrument::where('id',$request['id'])->update(array('radius' => $request['radius'],'altitude' => $request['altitude'] , 'latitude' => $request['latitude'], 'longitude' => $request['longitude'] ));

    return $set_location;
  }

  public function getInstrumentDefaultLocation(Request $request)
  {
    $instrument_location=Instrument::where('id',$request['instrument_id'])->first();
    $array = array('altitude' => $instrument_location->altitude,'latitude'=>$instrument_location->latitude,'longitude'=>$instrument_location->longitude);

    return $array;
  }
  public function basicEmail(Request $request)
  { 
    $id = $request['id'];

    $complain_message_body = $request['message'];

    $subject = 'Complain Message';

    $complain_status = 1;
    $email=0;
    //\Log::info($complain_message_body);
    EmailJob::dispatch($complain_message_body,$subject,$complain_status,$email);  
  }

    public function rating(Request $request)
      {   
        $rating= Rating::where('id',$request['user_id'])->value('id');
        $rating  =  new Rating();
          $rating->user_id = $request->input('user_id');
          $rating->rating = $request->input('rating');
          $rating->save();

          $response='Your rating send successfully';

          return $response;
      }
  

  public function forgetPassword(Request $request)  
    {   
      $id= User::where('email', $request['email'])->value('id');
      $email1= User::where('email', $request['email'])->value('email');
      $email= $request['email'];

      $user_not_exist='User not exist';
      $random_token = substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 6)), 0, 6);
      $subject= 'Recovery password code';
      
      if ($email1!=null) 
        {
          $user = new ForgetPassword();
            $user->user_id = $id;
            $user->email = $email;
            $user->token = $random_token;
            $user->save();
            
          $forgetpassword_id= ForgetPassword::where('email' , $request['email'])->latest('id')->value('id');
          $status = 2;

          $message_repose ='Your Email found successfully';
          EmailJob::dispatch($random_token,$subject,$status,$email);  
          $response = array('message' => $message_repose,'forget_password_id' =>$forgetpassword_id,'user_id'=>$id );
          
          return$response;
        }
      else
        {

          $not_exist = array('message' => $user_not_exist, );
          return $not_exist; 
        }      
    }

  public function recoveryAccount(Request $request)  
    {   
       $validate= ForgetPassword::where('id',$request['id'])->value('token');

//       \Login::info($validate);

       $token = $request['token'];


        if ($token == $validate) 
          {
            $successed = "Successfully Verified";
            $temp = array('message' =>$successed );
            return $temp;
          }  
          else
          {
             $not_Successed = "Not Verified";
            $temp = array('message' => $not_Successed);
            return $temp;
          }

    }

    public function updatePassword(Request $request)  
    {   
      $password = Hash::make($request->newpassword);
      $update_passowrd = User::where('id',$request['id'])->update(array('password' => $password));

    }

}
    