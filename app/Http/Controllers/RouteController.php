<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\FormBuilder;
use App\Http\Controllers\Controller;
//use App\Http\Controllers\Plant;
use App\Model\Route;
use App\Post;
use App\User;
use Auth;
use Yajra\Datatables\Datatables;
use DB;
use Response;
Use Exception;
use Hash;
use Helper;
use App\Model\RouteHistory;
use App\Model\RouteInspectionPoint;
use App\Model\RouteInspectionParameter; 
use App\Model\RouteInstrumentSkipped; 
use App\Model\RegisterRoute; 
use App\Model\RouteTools;
use App\Model\Parameter;
use Carbon\carbon;
use DateTime;

class RouteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax())
        {
            $data = Route::latest()->get();

            foreach ($data as $key => $value) {
                $data[$key]['operator_id']= Helper::userIdToName($value['operator_id']);
            }


            return Datatables::of($data)
              ->addColumn('action', 'datatables.route-action-datatable')
              ->rawColumns(['action'])
              ->make(true);
        }    
        return view('routes.index');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('routes.create');
    }    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        \Log::info($request);
        $route = new Route;
        $route->operator_id = $request['operator'];
        $route->description = $request['description'];
        $route->period = $request['period'];
        $route->period = $request['period'];
        $route->save();

        $latest_route = Route::orderBy('created_at','desc')->first();
        //\Log::info($latest_route['id']);
        $inspection_points_data = $request['inspection_points'];

        foreach ($inspection_points_data as $key => $i) {
          $i_p = new RouteInspectionPoint;
          $i_p->route_id = $latest_route['id'];
          $i_p->instrument_id = $i['inspection_point'];
          $i_p->order_no = $i['order_number'];
          $i_p->wrench_time = $i['wrench_time'];
          $i_p->save();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }
    public function getRoutes(Request $request)
    {
        $routes = Route::all(); 
        if($routes=='[]')
        {
          return '[{"message":"NO DATA FOUND","id":0,"operator_id":0,"description":"","period":""}]';
        }
        foreach ($routes as $key => $route) 
        {
          $routes[$key]['message']='';
        }        
        return json_encode($routes);
    }


    public function getRoutesDetails(Request $request)
    {
        $data=[];
        $routes = Route::all(); 
        if($routes=='[]')
        {
          return '[{"message":"NO DATA FOUND","id":0,"operator_id":0,"description":"","period":""}]';
        }
        foreach ($routes as $key => $route) 
        {  

           $route_period = $route->period;
          
           // if ($route->period ==24) {

              // \Log::info("in fazool IF");
            $current_time = Carbon::now();
            $sub_hours = Carbon::parse($current_time)->subHour($route->period);
            \Log::info($sub_hours);

            $get_data= RegisterRoute::where('route_id' , $route['id'])->whereBetween('date',[$sub_hours,$current_time])->orderBy('created_at','desc')->first();
             //\Log::info($get_data);
            $reference_time=$get_data['date'];
           // }
           // elseif ($route->period >24 && $route->period <=48) {
           //   \Log::info("in fazool ELSE IF");
           //      $current_time = Carbon::now();
           //      $sub_hours = Carbon::parse($current_time)->subHour(48);
           //      \Log::info($sub_hours);

           //      $get_data= RegisterRoute::where('route_id' , $route['id'])->whereBetween('date',[$sub_hours,$current_time])->orderBy('created_at','desc')->first();
           //       //\Log::info($get_data);
           //      $reference_time=$get_data['date'];
           // }
           // elseif ($route->period >24 && $route->period <=48) {
           //   // \Log::info("in sahi ELSE IF");
           //      $current_time = Carbon::now();
           //      $sub_hours = Carbon::parse($current_time)->subHour(48);
           //      \Log::info($sub_hours);

           //      $get_data= RegisterRoute::where('route_id' , $route['id'])->whereBetween('date',[$sub_hours,$current_time])->orderBy('created_at','desc')->first();
           //       //\Log::info($get_data);
           //      $reference_time=$get_data['date'];
           // }
           ///////////////////////////////////////////////////////////////////////////////////////

           // elseif ($route->period >48 &&$route->period <=168) {
           //     $current_time = Carbon::now();
           //      $sub_hours = Carbon::parse($current_time)->subHour(336);

           //      $get_data= RegisterRoute::where('route_id' , $route['id'])->whereBetween('date',[$sub_hours,$current_time])->orderBy('created_at','desc')->first();
           //       //\Log::info($get_data);
           //      $reference_time=$get_data['date'];
           // }elseif ($route->period >168 &&$route->period <=360) {
           //    $current_time = Carbon::now();
           //    $sub_hours = Carbon::parse($current_time)->subHour(700);
           //    $get_data= RegisterRoute::where('route_id' , $route['id'])->whereBetween('date',[$sub_hours,$current_time])->orderBy('created_at','desc')->first();
           //       //\Log::info($get_data);
           //      $reference_time=$get_data['date'];
           // }elseif ($route->period >360 &&$route->period <=720) {
           //    $current_time = Carbon::now();
           //    $sub_hours = Carbon::parse($current_time)->subHour(1140);
           //    $get_data= RegisterRoute::where('route_id' , $route['id'])->whereBetween('date',[$sub_hours,$current_time])->orderBy('created_at','desc')->first();
           //       //\Log::info($get_data);
           //    $reference_time=$get_data['date'];
           // }           
            $route_history = RouteHistory::where('route_id' ,$route['id'])->whereBetween('created_at' , [$reference_time,$current_time])->count();  
            \Log::info($route_history);  
            $route_id = $route->id;
        if ($route_history==0) 
        {

          $date1 = new DateTime($reference_time);
          $date2 = new DateTime($current_time);

          $totalDely = $date2->diff($date1);
          $hours = $totalDely->h;
          $hours = $hours + ($totalDely->days*24);
          $routes[$key]['message']='';
          $routes[$key]['Dely']=$hours." hours";
          $routes[$key]['Remaining Time']='';
        }

        elseif ($route_history!=0) 
        {
          $calculate_remaining_time = Carbon::parse($current_time)->addHour($route_period);

          \Log::info("current time :".$calculate_remaining_time);

          $remaing_time = RegisterRoute::where('route_id' ,$route['id'])->whereBetween('date',[$current_time,$calculate_remaining_time])->orderBy('created_at','asc')->first();

           

          $reference_time_remaing=$remaing_time['date'];

           \Log::info("reference_time_remaing :".$reference_time_remaing);
            
            $date1 = new DateTime($reference_time_remaing);
            $date2 = new DateTime($current_time);


           $diff = $date2->diff($date1);
           $hours = $diff->h;
           $hours = $hours + ($diff->days*24);

           // $totalRemainingTime=$totalRemainingTime->h;

           // $totalRemainingTime + ($totalRemainingTime->days*24);


            \Log::info("totalRemainingTime :".$hours);

            $percentage = .30;
            $calculate_percantage = $route_period * $percentage  ;
            $floor=floor($calculate_percantage);

            \Log::info("floor :".$floor);
            $sub_hours_floor = Carbon::parse($reference_time_remaing)->subHour($floor);

            \Log::info("sub_hours_floor :".$sub_hours_floor);

            \Log::info("current_time :".$current_time);

            if  ($current_time < $sub_hours_floor ) 
            {
              $allowed = 'True';
            }
            elseif ($current_time > $sub_hours_floor )
            {
              $allowed = 'False';
            }

          $routes[$key]['message']='';
          $routes[$key]['Dely']='';
          $routes[$key]['Remaining Time']=$hours." hours";
          $routes[$key]['allowed']=$allowed;
          $routes[$key]['activated']=$sub_hours_floor;

          } 
        }
          return json_encode($routes);   
    }
    public function getInspectionPoints(Request $request)
    {

       ////////////////////addded by usama for route cancell//////////////////////////   

        $route_id = $request['route_id'];
        $operator_id = $request['operator_id'];


        if (RouteHistory::where('route_id',$request['route_id'])
                          ->where('route_state','In Progress')
                          ->orderBy('created_at','DESC')
                          ->exists())
        {
          $route_history= DB::table('route_history')
            ->where('route_id', $request['route_id'])
            ->where('route_state', 'In Progress')
            ->orderBy('created_at','DESC')
            ->first();  

          if($route_history->operator_id != $request['operator_id'])
          {
            return "Route is already in progress";
          } 

          else
          {
              $route_period=Route::where('id',$request['route_id'])->value('period');
              $start_time1=new DateTime($route_history->start_time);

              $end_time1=new DateTime(Carbon::now());
              $diff1=$end_time1->diff($start_time1);

              $new_time=$diff1->h;

              $hours = $new_time + ($diff1->days*24);

              if($route_period <= $hours)
              {
                RouteHistory::where('route_id',$request['route_id'])->orderBy('created_at','DESC')->first()->update(['instrument_skiped' => '0','route_state' => 'cancelled','end_time' => Carbon::now(),'duration_min'=>'00']); 

                   // return "Route cancelled";
                $inspection_point = RouteInspectionPoint::where('route_id',$route_id)->get();
                if($inspection_point=='[]')
                {
                    return '[{"message":"No inspection point found against this route.","route_inspection_point_id":0,"instrument_id":0,"instrument_name":"","instrument_desc":"","route_history_id":0,}]';
                }    
                // if (RouteHistory::where('route_id',$request['route_id'])
                //                   ->where('route_state','In Progress')  
                //                   ->exists())
                // {
                
                  $route_history_id= RouteHistory::where('route_id',$request['route_id'])
                  ->where('route_state','In Progress')
                  ->value('id');
                
                // }
                // else
                // {
                //   $route_history_id= RouteHistory::orderBy('created_at','desc')->first();
                // }
                // \Log::info($route_history_id);
                $all_inspection_points_temp =[];
                $data['message']='';

                foreach ($inspection_point as $key => $point) 
                {
                    $instrument_id = $point['instrument_id'];
                    $instrument_name = Helper::instrumentIdToName($instrument_id);
                    $instrument_desc = Helper::instrumentIdToDes($instrument_id);
                    
                    $inspection_point_arr = array('message'=>'Route cancelled due to timeout','route_inspection_point_id'=>$point['id'] , 'instrument_id' => $instrument_id,'instrument_name' => $instrument_name, 'route_history_id'=> $route_history_id,'instrument_desc' => $instrument_desc,);

                    array_push($all_inspection_points_temp, $inspection_point_arr);
                }

                $all_inspection_points = array_unique($all_inspection_points_temp,SORT_REGULAR);
                $data= json_encode($all_inspection_points);
                return $data;                 
              }        

              else
              {
                $route_history= DB::table('route_history')
                ->where('route_id', $request['route_id'])
                ->where('route_state', 'In Progress')
                ->update(['sessions' =>  DB::raw('sessions + 1')]);
                $inspection_point = RouteInspectionPoint::where('route_id',$route_id)->get();
                if($inspection_point=='[]')
                {
                    return '[{"message":"No inspection point found against this route.","route_inspection_point_id":0,"instrument_id":0,"instrument_name":"","instrument_desc":"","route_history_id":0,}]';
                }    
                // if (RouteHistory::where('route_id',$request['route_id'])
                //                   ->where('route_state','In Progress')  
                //                   ->exists())
                // {
                
                  $route_history_id= RouteHistory::where('route_id',$request['route_id'])
                  ->where('route_state','In Progress')
                  ->value('id');
                
                // }
                // else
                // {
                //   $route_history_id= RouteHistory::orderBy('created_at','desc')->first();
                // }
                // \Log::info($route_history_id);
                $all_inspection_points_temp =[];
                $data['message']='';

                foreach ($inspection_point as $key => $point) 
                {
                    $instrument_id = $point['instrument_id'];
                    $instrument_name = Helper::instrumentIdToName($instrument_id);
                    $instrument_desc = Helper::instrumentIdToDes($instrument_id);
                    
                    $inspection_point_arr = array('message'=>'','route_inspection_point_id'=>$point['id'] , 'instrument_id' => $instrument_id,'instrument_name' => $instrument_name, 'route_history_id'=> $route_history_id,'instrument_desc' => $instrument_desc,);

                    array_push($all_inspection_points_temp, $inspection_point_arr);
                }

                $all_inspection_points = array_unique($all_inspection_points_temp,SORT_REGULAR);
                $data= json_encode($all_inspection_points);
                return $data;                  
              }         


          }
        }  
        else
        {
          $route_history= new RouteHistory;
          $route_history->route_id= $request['route_id'];
          $route_history->operator_id= $request['operator_id'];
          $route_history->route_state= "In Progress";
          $route_history->start_time= Carbon::now();
          $route_history->save();
          $inspection_point = RouteInspectionPoint::where('route_id',$route_id)->get();
          if($inspection_point=='[]')
          {
              return '[{"message":"No inspection point found against this route.","route_inspection_point_id":0,"instrument_id":0,"instrument_name":"","instrument_desc":"","route_history_id":0,}]';
          }    
          // if (RouteHistory::where('route_id',$request['route_id'])
          //                   ->where('route_state','In Progress')  
          //                   ->exists())
          // {
          
            $route_history_id= RouteHistory::where('route_id',$request['route_id'])
            ->where('route_state','In Progress')
            ->value('id');
          
          // }
          // else
          // {
          //   $route_history_id= RouteHistory::orderBy('created_at','desc')->first();
          // }
          // \Log::info($route_history_id);
          $all_inspection_points_temp =[];
          $data['message']='';

          foreach ($inspection_point as $key => $point) 
          {
              $instrument_id = $point['instrument_id'];
              $instrument_name = Helper::instrumentIdToName($instrument_id);
              $instrument_desc = Helper::instrumentIdToDes($instrument_id);
              
              $inspection_point_arr = array('message'=>'','route_inspection_point_id'=>$point['id'] , 'instrument_id' => $instrument_id,'instrument_name' => $instrument_name, 'route_history_id'=> $route_history_id,'instrument_desc' => $instrument_desc,);

              array_push($all_inspection_points_temp, $inspection_point_arr);
          }

          $all_inspection_points = array_unique($all_inspection_points_temp,SORT_REGULAR);
          $data= json_encode($all_inspection_points);
          return $data;                
        }    

        ////////////////////addded by usama for route cancell/////////////////////;
        // else
        // {
        //   $route_id = $request['route_id'];
        //   $operator_id = $request['operator_id'];
        //   if (RouteHistory::where('route_id',$request['route_id'])
        //                     ->where('route_state','In Progress')  
        //                     ->exists())
        //   {
        //       $route_history= DB::table('route_history')
        //         ->where('route_id', $request['route_id'])
        //         ->where('route_state', 'In Progress')
        //         ->update(['sessions' =>  DB::raw('sessions + 1')]);
        //   }
          
        // }
                     
    }

    public function postFinishRoutes (Request $request)
    {      
      \Log::info($request);


        $route_history_data= RouteHistory::find($request['route_history_id']);

        $instrument_skipped= $request['instrument_skipped'];
        $skipped_params = json_decode($request['instrument_skipped'],true);
      
        $skipped_parameter_count=(count($skipped_params));

        $start_time=Carbon::parse($route_history_data->start_time);
        $end_time=Carbon::now();
        $diff=$start_time->diffInMinutes($end_time);


       
       ////////////////////addded by usama for route cancell//////////////////////////   
        // $start_time1=new DateTime($route_history_data->start_time);

        // $end_time1=new DateTime(Carbon::now());
        // $diff1=$end_time1->diff($start_time1);

        // $new_time=$diff1->h;

        // $hours = $new_time + ($diff1->days*24);
        // ////////////////////addded by usama for route cancell/////////////////////;
        // if($route_period <= $hours)
        // {
        //     RouteHistory::where('id',$request['route_history_id'])->update(['instrument_skiped' => '0','route_state' => 'cancelled','end_time' => Carbon::now(),'duration_min'=>$diff]); 
        // }
        // else
        // {

          $route_history=DB::table('route_history')
            ->where('id', $request['route_history_id'])
            ->update(['instrument_skiped' => $skipped_parameter_count,'route_state' => $request['status'],'end_time' => Carbon::now(),'duration_min'=>$diff]); 
         // }   

        // foreach ($skipped_params as $key => $skipped_param) 
        // {
        //     $route_instrument_skipped= new RouteInstrumentSkipped;
        //     $route_instrument_skipped->route_history_id= $request['route_history_id'];
        //     $route_instrument_skipped->instrument_id =$skipped_param['instrument_id'];
        //     $route_instrument_skipped->skipped_id =$skipped_param['reason_id'];
        //     $route_instrument_skipped->save();
        // }
    }


    public function getRouteInspectionPointParams(Request $request)
    {


        $route_inspection_point_id = $request['route_inspection_point_id'];
        $parameters = RouteInspectionParameter::where('route_inspection_point_id',$route_inspection_point_id)
            ->pluck('parameter_id');
        if($parameters=='[]')
        {
            return '[{"message":"No parameter found against this route inspection point","id":0,"parameter_name":"","data_type":"","max":"","min":""}]';
        }    
        $paramters_array=[];
        foreach ($parameters as $key => $parameter) {
            $parameter_id = $parameter; 
            $parameter_name = Helper::parameterIdToName($parameter_id);
            $parameter_data_type = Helper::parameterIdToDatatype($parameter_id);
            $parameter_max=Helper::parameterIdToMax($parameter_id);
            $parameter_min=Helper::parameterIdToMin($parameter_id);
            $parameter_desc=Helper::ParameterIdToDescription($parameter_id);
            $parameter_eu=Helper::ParameterIdToEU($parameter_id); 
            if($parameter_data_type != 'Bool')
            {
              $temp_arr = array('message'=>'','parameter_id'=>$parameter_id,'parameter_name'=>$parameter_name." ".'('.$parameter_eu.')', 'data_type'=>$parameter_data_type,'max'=>$parameter_max,'min'=>$parameter_min);
            }
            else
            {
              $temp_arr = array('message'=>'','parameter_id'=>$parameter_id,'parameter_name'=>$parameter_name, 'data_type'=>$parameter_data_type,'max'=>$parameter_max,'min'=>$parameter_min);
            }
            array_push($paramters_array, $temp_arr);
        }
        return json_encode($paramters_array);
    }

    public function getRouteTools(Request $request)
    {
        $route_id = $request['route_id'];
        $route_tools = DB::table('route_tools')
        ->join('tools', function ($join) use ($route_id){
            $join->on('route_tools.tool_id', '=', 'tools.id')
            ->where('route_tools.route_id', $route_id);
        })
        ->select('route_tools.tool_id','tools.name','route_tools.route_id')
        ->get();

        $route_tools_array = [];
        if($route_tools == '[]')
        {
          return '[{"tool_id":0,"name":"","route_id":0,"message":"No tool found against this route"}]';
        }
        else
        {
            foreach ($route_tools as $key => $route_tool) {
               $tool = (array)$route_tool;
               $tool['message'] = '';
               array_push($route_tools_array, $tool);
            }
            return(json_encode($route_tools_array));

            \Log::info($route_toools_array);
        }        
    }

    public function showInspectionPoint(Request $request)
    {
        $show_inspection_point= RouteInspectionPoint::where('route_id',$request['id'])->get();
        $show_inspection_point_wrench_time= RouteInspectionPoint::where('route_id',$request['id'])->get();  
        $route_id = $request['id'];
        foreach ($show_inspection_point as $key => $value) 
        {   
          $id = $value['instrument_id'];
          $show_inspection_point[$key]['route_id']= Helper::routeIdToDes($value['route_id']);
          $show_inspection_point[$key]['instrument_id']= Helper::instrumentIdToName($id);
          $show_inspection_point[$key]['instrument_des']= Helper::instrumentIdToDes($id);

        }  
        return view('showinspectionpoints.index',compact('show_inspection_point','route_id'));
    }

    public function showInspectionParameter(Request $request)
    {
        $show_inspection_parameter= RouteInspectionParameter::where('route_inspection_point_id',$request['id'])->get(); 
          
        foreach ($show_inspection_parameter as $key => $value) 
          {   
            
            $show_inspection_parameter[$key]['parameter_id']= Helper::parameterIdToName($value['parameter_id']);
            $show_inspection_parameter[$key]['instrument_name']= Helper::inspectionPointToInstrumentName($value['route_inspection_point_id']);
            
            $show_inspection_parameter[$key]['route_des']= Helper::inspectionPointToRouteDes($value['route_inspection_point_id']);
          }  
          return view('showinspectionparameter.index',compact('show_inspection_parameter'));
    }

    public function getRouteToolsData()
    {
      $routes=Route::get();
      foreach ($routes as $key => $route) 
      {
        $route_inspection_point_ids=RouteInspectionPoint::where('route_id',$route['id'])->get();
        foreach ($route_inspection_point_ids as $key => $route_inspection_point_id) 
        {
          $parameter_ids= RouteInspectionParameter::where('route_inspection_point_id',$route_inspection_point_id['id'])->get();      
          foreach ($parameter_ids as $key => $parameter_id) 
          {
            $tool=Parameter::where('id',$parameter_id['id'])->value('tool_id');
            //\Log::info($tool." ".$route['id']);
            $route_tools= new RouteTools;
            $route_tools->route_id=$route['id'];        
            $route_tools->tool_id=$tool;
            // $route_tools->save();
          }
        }
      }
    }

    public function addParamCreate(Request $request)
    {
      $id = $request['id'];
      $route_id = RouteInspectionPoint::where('id',$id)->value('route_id');
      $wrench_time = RouteInspectionPoint::where('route_id',$route_id)->value('wrench_time')->get();
      dd($wrench_time);
      return view('routes.add-param-create',compact('id','route_id'));
    }

    public function paramInpectionPointStore(Request $request)
    {
       $delete_row=RouteInspectionParameter::where('route_inspection_point_id',$request['inspection_point_id'])->delete();
       $result = $request['data'];
      foreach ($result as $key => $r) {
        $route_inspection_param = new RouteInspectionParameter;
        $route_inspection_param->route_inspection_point_id = $request['inspection_point_id'];
        $route_inspection_param->parameter_id = $r['parameter'];
        $route_inspection_param->inspection_time_min = $r['inspection_time'];
        $route_inspection_param->save();

        $route_tools = new RouteTools;
        $route_tools->route_id = $request['route_id'];
        $route_tools->tool_id = Helper::parameterIdToToolId($r['parameter']);
        $route_tools->save();
      }
      return response()->json(['success' => 'success', 200]);
    }

    //For Phone DB Sync
    public function getAllRoutes()
    {
      return Route::all()->toJson(); 
    }
    public function getAllRouteTools()
    {
      return RouteTools::all()->toJson();
    }


    public function RouteAlignCreate(Request $request)
    {
      $route_id = $request['id'];
      return view('showinspectionpoints.align',compact('route_id'));  
    }

    public function storeRegisterRoute(Request $request)
    {
      $route_id = $request['route_id'];
      $period = $request['period'];
      $month_array=['January','Feb','March','April','May','June','July','August','September','October','November','December'];

    foreach ($month_array as $key => $value) 
    {
      if($value=='January')
      {
        $day=1;

        $date=2020-01-01;

        while ($day <= 31) 
        {
            //store procedure...........
            $data=new RegisterRoute;
            $data->route_id=$route_id;
            $data->month = $value;
            $data->date=$date;
            $data->save();

            $date = $date->addHour($period);

           // $date=Carbon::addHour($period);
            $day=Carbon::parse($date)->format('d');  //date carbon

            dd($day);
        }
          
       }
      
    }
  }
}
