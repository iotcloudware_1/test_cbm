<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Group;
use Yajra\Datatables\Datatables;
use Kris\LaravelFormBuilder\FormBuilder;
use App\User;
use App\Jobs\EmailJob;
use Auth;
use DB;
use Response;
Use Exception;
use Hash;
use Helper;
use App\Model\UserGroup;
class GroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        // dd('arrive')
        if($request->ajax())
        {
            $data = Group::latest()->get();

            \Log::info($data);
            return Datatables::of($data)
              ->addColumn('action', 'datatables.action-button')
              ->addColumn('action2', 'datatables.action2-button')
              ->rawColumns(['action','action2'])
              ->make(true);
        }    
         return view('groups.index');        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view ('groups.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $this->validate($request, [

            'name' => 'required',
            'description' => 'required',
        ]);

        $input=$request->all();

        $group=Group::create($input);

        return view('groups.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $group=Group::find($id);
        $group_users = UserGroup::where("group_id",$id)->get();
        // dd($g)
        return view('groups.show',compact('group','group_users'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try
        {
            DB::table("groups")->where('id',$id)->delete();
            return redirect()->route('groups.index');
        }
        catch(Exception $e)
        {
            $err = json_encode($e);
            $err_array = json_decode($err,true);
            if($err_array['errorInfo'][1] == 1451)
            {
                return Response::json(array('success' => false, 'msg_string' => 'Group is in use.'), 500);
            }
          
        } 
    }

    public function addUser($id)
    {   
        $input= $id;
        return view('groups.addUser',compact('input'));
        // dd($id);       
    }

}
