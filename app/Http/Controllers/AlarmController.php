<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\FormBuilder;
use App\Forms\AlarmForm;
use App\Http\Controllers\Controller;
use App\Http\Controllers\AlarmController;
//use App\Http\Controllers\Plant;
use App\Model\Alarm;
use App\Model\Parameter;
use App\Model\AlarmCurrentState;
use App\Model\AlarmHistory;
use App\Post;
use App\User;
use Auth;
use Yajra\Datatables\Datatables;
use DB;
use Response;
Use Exception;
use Hash;
use Helper;
use Carbon\Carbon;
//use App\Helper\Helper;
use App\Model\Fcm;

class AlarmController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    function __construct()
    {
         $this->middleware('permission:plant_admin_alarm_dashboard-index', ['only' => ['index','store']]);
         
         $this->middleware('permission:alarm-create', ['only' => ['create','store']]);
         $this->middleware('permission:alarm-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:alarm-delete', ['only' => ['destroy']]);
         return view('permission-error');
    }


    public function index(Request $request)
    {
        if($request->ajax())
        {
          $alarms = DB::table('alarms')
            ->join('parameters', 'alarms.parameter_id', '=', 'parameters.id')
            ->select('alarms.*', 'parameters.instrument_id')->get();
          $data = json_decode($alarms,true);
            
            foreach ($data as $key => $value) 
            {
                $data[$key]['parameter_id'] = Helper::parameterIdToName($value['parameter_id']);
                $data[$key]['instrument_id'] = Helper::instrumentIdToName($value['instrument_id']);
            }
            return Datatables::of($data)
              ->addColumn('action', 'datatables.action-button')
              ->rawColumns(['action'])
              ->make(true);
        }    
        
        return view('alarms.index');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

      // $tokensArray = Fcm::whereIn('user_id', $user->id)->pluck('token')->toArray();
      // Helper::notification($tokensArray, "test title" ,"test body");
      
       $parameter=Parameter::all()->pluck('name','id');
       return view('alarms.create', compact('parameter'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $this->validate($request, [
            'parameter_id' => 'required',
            'condition' => 'required',
            'setpoint' => 'required',
            'priority' => 'required'          
        ]);
        $input=$request->all(); 
        \Log::info($input);  
        $alarm_exist = Alarm::count();
        $alarm_limit = User::where('id',Auth::user()->id)->value('alarm_limit');

        if($alarm_limit > $alarm_exist)
        {
            $alarm_data= Alarm::create($input);
            $current_alarm_data= new AlarmCurrentState;
            $current_alarm_data->alarm_id= $alarm_data->id;
            $current_alarm_data->alarm_message=$alarm_data->alarm_message;
            $current_alarm_data->new_value=0;
            $current_alarm_data->state='normal';
            $current_alarm_data->save();
            return redirect('/alarms');
        }
        else
        {
            // return redirect('/departments')->with('msg','Limit reached. Please contact Administrator for adding more department');
            echo("<script>
                    alert('Limit Reached');
                    window.location.href = 'alarms'
                </script>");
        }     
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $alarm = Alarm::find($id);
        $alarm->parameter_id = Helper::parameterIdToName($alarm->parameter_id);
        return view('alarms.show',compact('alarm'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $parameter=Parameter::all()->pluck('name','id');
        $alarm = Alarm::find($id);
        return view('alarms.edit',compact('parameter','alarm'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'parameter_id' => 'required',
            'alarm_message' => 'required',
            'condition' => 'required',
            'setpoint' => 'required',
            'priority' => 'required',
        ]);


        $alarm = Alarm::find($id);
        $alarm->parameter_id = $request->input('parameter_id');
        $alarm->alarm_message = $request->input('alarm_message');
        $alarm->condition = $request->input('condition');
        $alarm->setpoint = $request->input('setpoint');
        $alarm->priority = $request->input('priority');
        $alarm->save();


        return redirect()->route('alarms.index')
                        ->with('success','Alarms updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try
        {   
            DB::table("alarm_history")->where('alarm_current_state_id',$id)->delete();          
            DB::table("alarm_current_state")->where('alarm_id',$id)->delete();
            DB::table("alarms")->where('id',$id)->delete();
            return redirect()->route('alarms.index')
             ->with('success','Alarm deleted successfully');
        }
        catch(Exception $e)
        {
            $err = json_encode($e);
            $err_array = json_decode($err,true);
            if($err_array['errorInfo'][1] == 1451)
            {
                return Response::json(array('success' => false, 'msg_string' => 'Alarm is in use.'), 500);
            }
          
        } 
    }
    public function currentAlarmState(Request $request)
    {  
     if($request->ajax())
      {
        $current_alarms_state = [];
        $alarms_details = DB::table('alarm_current_state')
          ->join('alarms', 'alarm_current_state.alarm_id', '=', 'alarms.id')
          ->where('alarm_current_state.state','alarm')
          ->select('alarm_current_state.id','alarms.parameter_id','alarm_current_state.alarm_id','alarm_current_state.alarm_message','alarm_current_state.new_value','alarm_current_state.state','alarm_current_state.created_at','alarm_current_state.updated_at')
          ->get();   

        $final_data_array =[];

        foreach ($alarms_details as $key => $alarm_detail) {
          $alarm_detail = (array)$alarm_detail;
          $current_alarm_id = $alarm_detail['id'];
          $latest_row = AlarmHistory::where('alarm_current_state_id',$current_alarm_id)
            ->orderBy('created_at','desc')
            ->first();
          $alarm_detail['instrument_name'] = Helper::instrumentIdToParameter($alarm_detail['parameter_id']);
          $alarm_detail['parameter_id'] = Helper::parameterIdToName($alarm_detail['parameter_id']);
          if($latest_row==NULL)
          {
            $alarm_detail['ack_state'] =' ';
          } 
          if($latest_row['new_state'] == 'normal')
          {
            $alarm_detail['ack_state'] =' ';
          }
          else if($latest_row['new_state'] == 'alarm')
          {
            $alarm_detail['ack_state'] =$latest_row['state_ack'];
          }
          array_push($final_data_array,$alarm_detail);              
        } 
        return Datatables::of($final_data_array)
          ->addColumn('action', 'datatables.current-alarm-button')
          ->rawColumns(['action'])
          ->make(true);
      }    
      
      return view('alarmcurrentstate.index');

    }
    
    public function AlarmHistory(Request $request)
    {
            $data = AlarmHistory::latest()->get();

            foreach ($data as $key => $value) {
               $data[$key]['alarm_name']= Helper::alarmCurrentStateToAlarmName($value['alarm_current_state_id']) ;
               $data[$key]['alarm_parameter_name']= Helper::alarmCurrentStateToParameterName($value['alarm_current_state_id']) ;
               $data[$key]['alarm_instrument_name']= Helper::alarmCurrentStateToInstrumentName($value['alarm_current_state_id']) ;
             
               if($data[$key]['user_id']!=null || $data[$key]['user_id']!="")
               {
                  $data[$key]['user_id']= Helper::userIdToName($value['user_id']);  
               }              
            } 
        if($request->ajax())
          {
            $data = AlarmHistory::latest()->get();
            foreach ($data as $key => $value) {
               $data[$key]['alarm_name']= Helper::alarmCurrentStateToAlarmName($value['alarm_current_state_id']) ;
               $data[$key]['alarm_parameter_name']= Helper::alarmCurrentStateToParameterName($value['alarm_current_state_id']) ;
               $data[$key]['alarm_instrument_name']= Helper::alarmCurrentStateToInstrumentName($value['alarm_current_state_id']) ;
        
               if($data[$key]['user_id']!=null || $data[$key]['user_id']!="")
               {
                  $data[$key]['user_id']= Helper::userIdToName($value['user_id']);  
               }              
            } 
            return Datatables::of($data)
              ->addColumn('action', 'datatables.action-button')
              ->rawColumns(['action'])
              ->make(true);
        }    
          
        return view('alarmhistory.index');
    }
    
    public function getCurrentAlarm()
    {
        $alarm_current= AlarmCurrentState::where('state','ALARM')->pluck('alarm_id');
        $unack=0;
        foreach ($alarm_current as $key => $value) {
            $a_h= AlarmHistory::where('alarm_current_state_id', $value)->orderBy('created_at', 'desc')->first();
            if($a_h['state_ack'] == 'unacknowledged')
            {
                $unack++;
            }

        }
        if($unack==0 || $unack==null )
        {
            return "[]";
        }
        else
        {
            $alarm_current= AlarmCurrentState::where('state','ALARM')->get()->toJson();
            $alarm_current= AlarmCurrentState::where('state','ALARM')->get()->toJson();
            return $alarm_current;
        }
    }

    public function acknowledge()
    {
       $ack= AlarmCurrentState::where('state','alarm')->pluck('alarm_id');

       foreach ($ack as $key => $value) 
       {
              $acknowledge= AlarmHistory::where('alarm_current_state_id', $value)->where('new_state','alarm')->orderBy('created_at','desc')->first()->update(array('state_ack'=>'acknowledged', 'user_id'=> Auth::User()->id));
       }   
        return redirect('/currentalarmstate');

    }
    public function getalarmHistory(Request $request)
    {
      if($request['selected_option']=='daily')
      {
        $date= Carbon::parse($request['selected_period']);
        $alarms=Alarm::where('parameter_id',$request['param_id'])->pluck('id');

        $current_alarm_ids = collect();
        foreach ($alarms as $key => $alarm) {
            $alarm_current_state_id=AlarmCurrentState::where('alarm_id',$alarm)->value('id');
            $current_alarm_ids->push($alarm_current_state_id);
        }
        \Log::info($current_alarm_ids);

        $alarms_count=[];
        foreach ($current_alarm_ids as $key => $current_alarm_id) {
           $count =AlarmHistory::where('alarm_current_state_id',$current_alarm_id)->where('new_state','alarm')->orderBy('created_at','DESC')->count();
           $alarm_name = AlarmCurrentState::find($current_alarm_id)->alarm->alarm_message;
           $temp_array = array('alarm'=>$alarm_name,'count'=>$count);
           array_push($alarms_count, $temp_array);
        }
        
      }

      if($request['selected_option']=='monthly')
      {

        $month=Carbon::parse($request['selected_period']);
          \Log::info($month);
          $month=$month->month;

        $year=Carbon::parse($request['selected_period'])->format('Y');

        $alarms=Alarm::where('parameter_id',$request['param_id'])->pluck('id');

        $current_alarm_ids = collect();
        foreach ($alarms as $key => $alarm) {
            $alarm_current_state_id=AlarmCurrentState::where('alarm_id',$alarm)->value('id');
            $current_alarm_ids->push($alarm_current_state_id);
        }
        \Log::info($current_alarm_ids);

        $alarms_count=[];
        foreach ($current_alarm_ids as $key => $current_alarm_id) {
           $count =AlarmHistory::where('alarm_current_state_id',$current_alarm_id)->where('new_state','alarm')->whereMonth('created_at',$month)->whereYear('created_at',$year)->orderBy('created_at','DESC')->count();
           $alarm_name = AlarmCurrentState::find($current_alarm_id)->alarm->alarm_message;
           $temp_array = array('alarm'=>$alarm_name,'count'=>$count);
           array_push($alarms_count, $temp_array);
        }
        
      }

      if($request['selected_option']=='yearly')
      {
        $year=Carbon::parse($request['selected_period'])->format('Y');
        $alarms=Alarm::where('parameter_id',$request['param_id'])->pluck('id');

        $current_alarm_ids = collect();
        foreach ($alarms as $key => $alarm) {
            $alarm_current_state_id=AlarmCurrentState::where('alarm_id',$alarm)->value('id');
            $current_alarm_ids->push($alarm_current_state_id);
        }
        \Log::info($current_alarm_ids);

        $alarms_count=[];
        foreach ($current_alarm_ids as $key => $current_alarm_id) {
           $count =AlarmHistory::where('alarm_current_state_id',$current_alarm_id)->where('new_state','alarm')->whereYear('created_at',$year)->orderBy('created_at','DESC')->count();
           $alarm_name = AlarmCurrentState::find($current_alarm_id)->alarm->alarm_message;
           $temp_array = array('alarm'=>$alarm_name,'count'=>$count);
           array_push($alarms_count, $temp_array);
        }        
      }      
      return $alarms_count;
    }

    public function getalarmHistoryDatatable(Request $request)
    {
      if($request['selected_option']=='daily')
      {
        $date= Carbon::parse($request['selected_period']);
        $alarms=Alarm::where('parameter_id',$request['param_id'])->pluck('id');

        $current_alarm_ids = collect();
        foreach ($alarms as $key => $alarm) {
            $alarm_current_state_id=AlarmCurrentState::where('alarm_id',$alarm)->value('id');
            $current_alarm_ids->push($alarm_current_state_id);
        }
        \Log::info($current_alarm_ids);

        $alarms_count_datatable=[];
        foreach ($current_alarm_ids as $key => $current_alarm_id) {
           $value =AlarmHistory::where('alarm_current_state_id',$current_alarm_id)->where('new_state','alarm')->whereDate('created_at',$date)->orderBy('created_at','DESC')->pluck('new_value');
              foreach ($value as $key => $v) {
                 $alarm_name = AlarmCurrentState::find($current_alarm_id)->alarm->alarm_message;
                 $temp_array = array('alarm'=>$alarm_name,'value'=>$v);
                 array_push($alarms_count_datatable, $temp_array);
              }
        }
      }

      if($request['selected_option']=='monthly')
      {
        $month=Carbon::parse($request['selected_period']);
          \Log::info($month);
          $month=$month->month;

        $year=Carbon::parse($request['selected_period'])->format('Y');
          // \Log::info($year);
        $alarms=Alarm::where('parameter_id',$request['param_id'])->pluck('id');

        $current_alarm_ids = collect();
        foreach ($alarms as $key => $alarm) {
            $alarm_current_state_id=AlarmCurrentState::where('alarm_id',$alarm)->value('id');
            $current_alarm_ids->push($alarm_current_state_id);
        }
        \Log::info($current_alarm_ids);

        $alarms_count_datatable=[];
        foreach ($current_alarm_ids as $key => $current_alarm_id) {
           $value =AlarmHistory::where('alarm_current_state_id',$current_alarm_id)->where('new_state','alarm')->whereMonth('created_at',$month)->whereYear('created_at',$year)->orderBy('created_at','DESC')->pluck('new_value');
            foreach ($value as $key => $v) 
            {
               $alarm_name = AlarmCurrentState::find($current_alarm_id)->alarm->alarm_message;
               $temp_array = array('alarm'=>$alarm_name,'value'=>$v);
               array_push($alarms_count_datatable, $temp_array);              # code...
            }
        }

      }

      if($request['selected_option']=='yearly')
      {
        $year=Carbon::parse($request['selected_period'])->format('Y');
          // \Log::info($year);
        $alarms=Alarm::where('parameter_id',$request['param_id'])->pluck('id');

        $current_alarm_ids = collect();
        foreach ($alarms as $key => $alarm) {
            $alarm_current_state_id=AlarmCurrentState::where('alarm_id',$alarm)->value('id');
            $current_alarm_ids->push($alarm_current_state_id);
        }
        \Log::info($current_alarm_ids);

        $alarms_count_datatable=[];
        foreach ($current_alarm_ids as $key => $current_alarm_id) {
           $value =AlarmHistory::where('alarm_current_state_id',$current_alarm_id)->where('new_state','alarm')->whereYear('created_at',$year)->orderBy('created_at','DESC')->pluck('new_value');
           foreach ($value as $key => $v) {
               $alarm_name = AlarmCurrentState::find($current_alarm_id)->alarm->alarm_message;
               $temp_array = array('alarm'=>$alarm_name,'value'=>$v);
               array_push($alarms_count_datatable, $temp_array);
            } 
        }

      }      
        return $alarms_count_datatable;

    }    

    public function acknowledgeCurrentAlarms()
    {
      $alarm_ids= AlarmCurrentState::pluck('id');
      foreach ($alarm_ids as $key => $id) 
      {
        $user_id=Auth::id();
        $current_alarm_id=AlarmHistory::where('alarm_current_state_id',$id)->orderBy('created_at','DESC')->first();
        if($current_alarm_id['new_state'] == 'alarm' && $current_alarm_id['state_ack']=='unacknowledged')
        {
          $current_alarm_id->update(array('state_ack' => 'acknowledged','user_id' => $user_id));
        }
   
      }
      return redirect()->route('alarms.currentAlarmState');
    }

    public function checkAcknowledgement()
    {
      $ack_flag = false;
      $alarm_ids= AlarmCurrentState::pluck('id');
      foreach ($alarm_ids as $key => $id) 
      {
       $current_alarm_id=AlarmHistory::where('alarm_current_state_id',$id)->orderBy('created_at','DESC')->first();

        if($current_alarm_id['new_state'] =='alarm' && $current_alarm_id['state_ack']== 'unacknowledged')
        {
          $ack_flag = true;
        }
      }
      if($ack_flag == true)
      {
        return 0;
      }
      else if ($ack_flag == false) 
      {
        return 1;
      }
      
    }

    public function ackIndividualAlarm(Request $request)
    {
        $alarm_id= AlarmCurrentState::where('id',$request['id'])->value('id');
        $user_id=Auth::id();
        $current_alarm_id=AlarmHistory::where('alarm_current_state_id',$alarm_id)->orderBy('created_at','DESC')->first();
        if($current_alarm_id['new_state'] == 'alarm' && $current_alarm_id['state_ack']=='unacknowledged')
        {
          $current_alarm_id->update(array('state_ack' => 'acknowledged','user_id' => $user_id));
        }
        return redirect()->route('alarms.currentAlarmState');
    }

     public function getAlarmTotal(Request $request)
    {   
      $alarm_total=DB::table('alarm_history')->where('new_state','alarm')->count();

      return $alarm_total;
    }
     public function getAlarmCurrent(Request $request)
    {   
      $current_alarm=DB::table('alarm_current_state')->where('state', 'alarm')->count();

      return $current_alarm;
    }
     public function getAlarmAck(Request $request)
    {   
      $alarm_ack=DB::table('alarm_history')->where('state_ack', 'acknowledged')->count();

      return $alarm_ack;
    }
     public function getAlarmUnAck(Request $request)
    {   

      $alarm_unack=DB::table('alarm_history')->where('state_ack', 'unacknowledged')->count();

      return $alarm_unack;
    }
    // public function checkAck()
    // {
    //   $ack_flag = false;
    //   $alarm_ids= AlarmCurrentState::where('id',$request['id'])->value('id');

    //    $current_alarm_id=AlarmHistory::where('alarm_current_state_id',$alarm_ids)->orderBy('created_at','DESC')->first();

    //   if($current_alarm_id['new_state'] =='alarm' && $current_alarm_id['state_ack']== 'unacknowledged')
    //   {
    //     $ack_flag = true;
    //   }
    //   if($ack_flag == true)
    //   {
    //     return 0;
    //   }
    //   else if ($ack_flag == false) 
    //   {
    //     return 1;
    //   }
      
    // }    
}
