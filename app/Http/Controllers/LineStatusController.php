<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Permission;
use App\User;
use App\Model\Line;
use App\Model\LineStatus;
use App\Model\Status;
use DB;
use Hash;
use App\Post;
use Yajra\Datatables\Datatables;
use Response;
use Helper;

class LineStatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
         $this->middleware('permission:linestatus-list|linestatus-create|linestatus-edit|line-delete', ['only' => ['index','store']]);
         
         $this->middleware('permission:linestatus-create', ['only' => ['create','store']]);
         $this->middleware('permission:linestatus-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:linestatus-delete', ['only' => ['destroy']]);
         return view('permission-error');
    }

    public function index(Request $request)
           {

            if($request->ajax())
            {
                $data = LineStatus::latest()->get();

                foreach ($data as $key => $value) 
                {
                    $data[$key]['line_id']= Helper::lineIdToName($value['line_id']);
                }
                foreach ($data as $key => $value) 
                {
                    $data[$key]['status_id']= Helper::statusIdToDes($value['status_id']);
                }


                return Datatables::of($data)
                  ->addColumn('action', 'datatables.action-button')
                  ->rawColumns(['action'])
                  ->make(true);
            }    
             return view('linestatus.index');
        }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $line=Line::all()->pluck('name','id');
        $status=Status::all()->pluck('description','id');
        return view('linestatus.create', compact('line','status'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'line_id' => 'required',
            'status_id' => 'required',
        ]);

        $input = $request->all();
        
        $linestatus = LineStatus::create($input);

        return redirect()->route('linestatus.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
{
        $linestatus = LineStatus::find($id);
        $linestatus->line_id = Helper::lineIdToName($linestatus->line_id);
        $linestatus->status_id = Helper::statusIdToDes($linestatus->status_id);
        return view('linestatus.show',compact('linestatus'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $line=Line::all()->pluck('name','id');
        $status=Status::all()->pluck('description','id');
        $linestatus = LineStatus::find($id);
        return view('linestatus.edit',compact('linestatus','line','status'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'line_id' => 'required',
            'status_id' => 'required',

        ]);

        $linestatus = LineStatus::find($id);
        $linestatus->line_id = $request->input('line_id');
        $linestatus->status_id = $request->input('status_id');
        $linestatus->save();


        return redirect()->route('linestatus.index');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         DB::table("line_status")->where('id',$id)->delete();
    }
}
