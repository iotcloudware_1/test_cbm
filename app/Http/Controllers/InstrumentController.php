<?php    
 
namespace App\Http\Controllers;

use Illuminate\Http\Request; 
use App\Http\Controllers\Controller;
use App\User;
use App\Model\Line;
use App\Model\Department;
use App\Model\Parameter;
use App\Model\AlarmCurrentState;
use App\Model\Instrument;
use DB;
use Hash;
use App\Post;
use Yajra\Datatables\Datatables;
use Response;
use Kris\LaravelFormBuilder\FormBuilder;
use App\Forms\InstrumentForm;
use Auth;
Use Exception;
// use App\Model\AlarmCurrentState; 
use App\Model\Asset;
use Helper;


class InstrumentController extends Controller
{
    /** 
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    function __construct()
    {
         $this->middleware('permission:instrument-list|instrument-create|instrument-edit|instrument-delete', ['only' => ['index','store']]);
         
         $this->middleware('permission:instrument-create', ['only' => ['create','store']]);
         $this->middleware('permission:instrument-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:instrument-delete', ['only' => ['destroy']]);
         return view('permission-error');
    }


 
    public function index(Request $request)
    {

        if($request->ajax())
        {
            $data = Instrument::latest()->get();

            foreach ($data as $key => $value) 
            {
                $data[$key]['asset_id'] = Helper::assetIdToName($value['asset_id']);
            }
            return Datatables::of($data)
           
            ->addColumn('action', 'datatables.action-button')
            ->rawColumns(['action'])
            ->make(true);
        }    
        return view('instruments.index');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $asset=Asset::all()->pluck('name','id');
     
        return view('instruments.create', compact('asset'));   
    }
  
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'asset_id' => 'required',    
            'name' => 'required',
            'description' => 'required',
            'type' => 'required',
            'location' => 'required',

        ]);

        $input = $request->all();   
        $input['user_id'] = Auth::user()->id;
       
        $instrument_exist = Instrument::count();
        $instrument_limit = User::where('id',Auth::user()->id)->value('instrument_limit');

        if($instrument_limit > $instrument_exist)
        {
            Instrument::create($input);
            $instrument= Instrument::latest()->first();
            $instrument_id= $instrument['id'];
            $qr_code = $instrument_id;

            Instrument::latest()->first()->update(array('qr_code' =>$qr_code)); 
            return redirect('/instruments');
        }
        else
        {
            // return redirect('/departments')->with('msg','Limit reached. Please contact Administrator for adding more department');
            echo("<script>
                    alert('Limit Reached');
                    window.location.href = 'instruments'
                </script>");
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) 
    {
        $instrument = Instrument::find($id);

        $instrument->asset_id = Helper::assetIdToName( $instrument->asset_id);
        $parameterList = Parameter::where('instrument_id',$id)->select('id','name')->get();


        $qrCode = array('code'=> $instrument['qr_code']);   

        $qrCodeJson = json_encode($qrCode);
        
        $modified_json = '['.$qrCodeJson.']';

        return view('instruments.show',compact('instrument','modified_json'));
    }
  
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $asset=Asset::all()->pluck('name','id');
        $instrument = Instrument::find($id);

        return view('instruments.edit',compact('instrument','asset'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'asset_id' => 'required',    
            'name' => 'required',
            'description' => 'required',
            'type' => 'required',
            'location' => 'required',

        ]);


        $instrument = Instrument::find($id);
        $instrument->asset_id = $request->input('asset_id');
        
        $instrument->name = $request->input('name');
        $instrument->description = $request->input('description');
        $instrument->type = $request->input('type');
        $instrument->location = $request->input('location');
        $instrument->save();


        return redirect()->route('instruments.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       
        try
        {
            DB::table("instruments")->where('id',$id)->delete();
            return redirect()->route('instruments.index')
                ->with('success','Instrument deleted successfully');
        }
        catch(Exception $e)
        {
            $err = json_encode($e);
            $err1 = json_decode($err,true);
            if($err1['errorInfo'][1] == 1451)
            {
                return Response::json(array('success' => false, 'msg_string' => 'Instrument is in use.'), 500);
            }
        } 
    }

    public function getinstrument(Request $request)
    {

        $instrument = Instrument::all()->toJson();
        return $instrument;
    }

    public function selectInstrument(Request $request)    
    {
        if($request->ajax())
        {
            $data = Instrument::latest()->get();
            return Datatables::of($data)
           
            ->addColumn('action', 'datatables.select-instrument-action-button')
            ->rawColumns(['action'])
            ->make(true);
        }

        return view('qrcodes.select');
    }

    // {
    //     $instruments = Instrument::get();
    //     return view('qrcodes.select',compact('instruments'));
    // }
    
    public function allQr()
    {

        $instruments = Instrument::all();
        $allQrCodes=[];

        foreach ($instruments as $key => $instrument)
        {
            $parameterList = Parameter::where('instrument_id',$instrument->id)
                             ->select('id','name')
                             ->get();

            $qrCode = array('instrument_id' => $instrument->id, 'instrument_name' => $instrument->name, 'instrument_parameters' =>$parameterList  ); 
            $qrCodeJson = json_encode($qrCode); 
            array_push($allQrCodes, $qrCodeJson);
            
        }
 
        return view('qrcodes.show',compact('allQrCodes'));
        // $qrcode = Instrument::find($id);
        // return view('qrcodes.show',compact('qrcode'));
    }

    public function getInstruments()
    {
        $instruments = Instrument::all()->toJson();

        return $instruments;
    }


    // public function getInstrumentDashoard(Request $request)
    // {   
    //     $show;
    //     if($request['id']== 'all')
    //     {
    //         $show = 'all';
    //         $instruments = Instrument::all();
    //         $alarms_arr=[];
    //         $instrument_id;
    //             foreach ($instruments as $instrument) 
    //             {
    //                 $parameters= Instrument::find($instrument['id'])->parameters;
    //                 $instrument_id = $instrument['id'];
    //                 $alarm_count = 0;
    //                 $healthy_count=0;    
    //                 foreach ($parameters as $parameter) 
    //                 {
    //                     $alarms= Parameter::find($parameter->id)->hasAlarm;
                        
    //                     if($alarms != null)
    //                     {
    //                         foreach ($alarms as $key => $alarm)
    //                         {
    //                             $current_alarms= AlarmCurrentState::where('alarm_id',$alarm->id)->where('state','alarm')->count();
    //                             $healthy_alarms= AlarmCurrentState::where('alarm_id',$alarm->id)->where('state','normal')->count();
    //                             $alarm_count = $alarm_count + $current_alarms;
    //                             $healthy_count = $healthy_count + $healthy_alarms;
    //                         }
    //                     }
    //                     else
    //                     {
    //                         $alarm_count = $alarm_count;
    //                         $healthy_count = $healthy_count;
    //                     }    
    //                 } 
    //              $temp = array('instrument_id' =>$instrument_id,'alarm_count'=>$alarm_count,'healthy_count'=>$healthy_count);
    //             array_push($alarms_arr, $temp);  
    //             } 
             
    //         foreach ($instruments as $key => $instrument) {
    //             $instruments[$key]['alarm_count'] = $alarms_arr[$key]['alarm_count'];
    //             $instruments[$key]['healthy_count'] = $alarms_arr[$key]['healthy_count'];

    //         }            
    //         return view('dashboardtree.instruments',compact('instruments','show'));            
    //     }
    //     else
    //     {   
            
    //         $show = 'specific';
    //         $instruments = Instrument::where('asset_id',$request['id'])->get();
    //         $alarms_arr=[];
    //         $instrument_id;
    //             foreach ($instruments as $instrument) 
    //             {
    //                 $parameters= Instrument::find($instrument['id'])->parameters;
    //                 $instrument_id = $instrument['id'];
    //                 $alarm_count = 0;
    //                 $healthy_count=0;    
    //                 foreach ($parameters as $parameter) 
    //                 {
    //                     $alarms= Parameter::find($parameter->id)->hasAlarm;
                        
    //                     if($alarms != null)
    //                     {
    //                         foreach ($alarms as $key => $alarm)
    //                         {
    //                             $current_alarms= AlarmCurrentState::where('alarm_id',$alarm->id)->where('state','alarm')->count();
    //                             $healthy_alarms= AlarmCurrentState::where('alarm_id',$alarm->id)->where('state','normal')->count();
    //                             $alarm_count = $alarm_count + $current_alarms;
    //                             $healthy_count = $healthy_count + $healthy_alarms;
    //                         }
    //                     }
    //                     else
    //                     {
    //                         $alarm_count = $alarm_count;
    //                         $healthy_count = $healthy_count;
    //                     }    
    //                 } 
    //              $temp = array('instrument_id' =>$instrument_id,'alarm_count'=>$alarm_count,'healthy_count'=>$healthy_count);
    //             array_push($alarms_arr, $temp);  
    //             } 
             
    //         foreach ($instruments as $key => $instrument) {
    //             $instruments[$key]['alarm_count'] = $alarms_arr[$key]['alarm_count'];
    //             $instruments[$key]['healthy_count'] = $alarms_arr[$key]['healthy_count'];

    //         }
            
    //         return view('dashboardtree.instruments',compact('instruments','show'));
    //     }        
    // }
    public function showAssetInstrument(Request $request)
    {
            $asset_name=Helper::assetIdToName($request['id']);

            $line_id= Asset::where('id',$request['id'])->value('line_id');
            $line_name=Helper::lineIdToName($line_id);

            
            $line_id= Asset::where('id',$request['id'])->pluck('line_id');
            $department_id= Line::where('id',$line_id)->value('department_id');
            $department_name= Helper::departmentIdToName($department_id);
            
            $line_id= Asset::where('id',$request['id'])->pluck('line_id');
            $department_id= Line::where('id',$line_id)->pluck('department_id');
            $unit_id= Department::where('id',$department_id)->value('unit_id');
            $unit_name= Helper::unitIdToName($unit_id); 
            // dd($unit_name);

            $show_asset_instrument= Instrument::where('asset_id',$request['id'])->get();
            $alarms_arr=[];
            $instrument_id;
                foreach ($show_asset_instrument as $instrument) 
                {
                    $parameters= Instrument::find($instrument['id'])->parameters;
                    $instrument_id = $instrument['id'];
                    $alarm_count = 0;
                    $healthy_count=0;    
                    foreach ($parameters as $parameter) 
                    {
                        $alarms= Parameter::find($parameter->id)->hasAlarm;
                        
                        if($alarms != null)
                        {
                            foreach ($alarms as $key => $alarm)
                            {
                                $current_alarms= AlarmCurrentState::where('alarm_id',$alarm->id)->where('state','alarm')->count();
                                $healthy_alarms= AlarmCurrentState::where('alarm_id',$alarm->id)->where('state','normal')->count();
                                $alarm_count = $alarm_count + $current_alarms;
                                $healthy_count = $healthy_count + $healthy_alarms;
                            }
                        }
                        else
                        {
                            $alarm_count = $alarm_count;
                            $healthy_count = $healthy_count;
                        }    
                    } 
                 $temp = array('instrument_id' =>$instrument_id,'alarm_count'=>$alarm_count,'healthy_count'=>$healthy_count);
                array_push($alarms_arr, $temp);  
                } 
             
            foreach ($show_asset_instrument as $key => $instrument) {
                $show_asset_instrument[$key]['alarm_count'] = $alarms_arr[$key]['alarm_count'];
                $show_asset_instrument[$key]['healthy_count'] = $alarms_arr[$key]['healthy_count'];

            }         

        foreach ($show_asset_instrument as $key => $value) 
          {   
            
            $show_asset_instrument[$key]['asset_id']= Helper::assetIdToName($value['asset_id']);
          }  
        
        return view('dashboardtree.instruments',compact('show_asset_instrument','unit_name','department_name','line_name','asset_name'));
    }  
     public function allInstruments()
    { 
        $allinstruments = Instrument::all();
            $alarms_arr=[];
            $instrument_id;
                foreach ($allinstruments as $instrument) 
                {
                    $parameters= Instrument::find($instrument['id'])->parameters;
                    $instrument_id = $instrument['id'];
                    $alarm_count = 0;
                    $healthy_count=0;    
                    foreach ($parameters as $parameter) 
                    {
                        $alarms= Parameter::find($parameter->id)->hasAlarm;
                        
                        if($alarms != null)
                        {
                            foreach ($alarms as $key => $alarm)
                            {
                                $current_alarms= AlarmCurrentState::where('alarm_id',$alarm->id)->where('state','alarm')->count();
                                $healthy_alarms= AlarmCurrentState::where('alarm_id',$alarm->id)->where('state','normal')->count();
                                $alarm_count = $alarm_count + $current_alarms;
                                $healthy_count = $healthy_count + $healthy_alarms;
                            }
                        }
                        else
                        {
                            $alarm_count = $alarm_count;
                            $healthy_count = $healthy_count;
                        }    
                    } 
                 $temp = array('instrument_id' =>$instrument_id,'alarm_count'=>$alarm_count,'healthy_count'=>$healthy_count);
                array_push($alarms_arr, $temp);  
                } 
             
            foreach ($allinstruments as $key => $instrument) {
                $allinstruments[$key]['alarm_count'] = $alarms_arr[$key]['alarm_count'];
                $allinstruments[$key]['healthy_count'] = $alarms_arr[$key]['healthy_count'];

            }          
      foreach ($allinstruments as $key => $value) 
          {   
            
            $allinstruments[$key]['asset_id']= Helper::assetIdToName($value['asset_id']);
          }  
          
        return view('dashboardtree.allinstruments',compact('allinstruments'));

    }  

}



    