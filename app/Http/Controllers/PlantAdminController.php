<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Permission;
use App\User;
use App\Model\Plant;
use Kris\LaravelFormBuilder\FormBuilder;
use Illuminate\Support\Str;
use DB;
use Auth;
use Hash;
use App\Post;
use Yajra\Datatables\Datatables;
use Response;
use Helper;

class PlantAdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     function __construct()
    {
         $this->middleware('permission:plant-admin-list|plant-admin-create|plant-admin-edit|plant-admin-delete', ['only' => ['index','store']]);
         
         $this->middleware('permission:plant-admin-create', ['only' => ['create','store']]);
         $this->middleware('permission:plant-admin-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:plant-admin-delete', ['only' => ['destroy']]);
         return view('permission-error');
    }

    public function index(Request $request)
      {

        if($request->ajax())
        {
            $data = User::latest()->where('role','plant_admin')->get();

            foreach ($data as $key => $value) {
               $data[$key]['plant_id']= Helper::plantIdToName($value['plant_id']) ;
            } 
            return Datatables::of($data)
              ->addColumn('action', 'datatables.action-button')
              ->rawColumns(['action'])
              ->make(true);
        }   
        return view('plantsadmin.index');

    //     ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //$ = Plant::all()->pluck('id')
        $plant_id_options = Plant::all()->pluck('name','id')->toArray();
        return view('plantsadmin.create', compact('plant_id_options'));
    }



    /*
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $this->validate($request, [
            'plant_id' => 'required',
            'name' => 'required',
            'email' => 'required',  
            'dept_limit' => 'required',
            'instrument_limit' => 'required',
            'parameter_limit' => 'required',
            'user_limit' => 'required',
            'alarm_limit' => 'required',
            'confirm-password' => 'required',
           

        ]);
    
        $input = $request->all();
        $input['created_by'] = Auth::user()->name;
        $input['password'] = Hash::make($input['password']);
        $input['api_token'] = Str::random(60);
        $input['role'] = 'plant_admin';
        
        $plantAdmin = User::create($input);
        $plantAdmin->assignRole(3);


        return redirect('/plantadmin');

    }
     /*
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $plantadmin = User::find($id);
        return view('plantsadmin.show',compact('plantadmin'));
    }


    /*
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $plant_id_options = Plant::all()->pluck('name','id')->toArray();
      

        $plantadmin = User::find($id);
        return view('plantsadmin.edit',compact('plantadmin','plant_id_options'));
    }


    // *
    //  * Update the specified resource in storage.
    //  *
    //  * @param  \Illuminate\Http\Request  $request
    //  * @param  int  $id
    //  * @return \Illuminate\Http\Response
     
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'plant_id' => 'required',
            'name' => 'required',
            'email' => 'required', 
            'dept_limit' => 'required',
            'instrument_limit' => 'required',
            'parameter_limit' => 'required',
            'user_limit' => 'required',
            'alarm_limit' => 'required',
            'routes_limit' => 'required',
            'created_by' => 'required',

        ]);


    $plantadmin = User::find($id);
    $plantadmin->plant_id = $request->input('plant_id');
    $plantadmin->name = $request->input('name'); 
    $plantadmin->email = $request->input('email');
    $plantadmin->address = $request->input('address');
    $plantadmin->phone1 = $request->input('phone1');
    $plantadmin->phone2 = $request->input('phone2');
    $plantadmin->dept_limit = $request->input('dept_limit');
    $plantadmin->instrument_limit = $request->input('instrument_limit');
    $plantadmin->parameter_limit = $request->input('parameter_limit');
    $plantadmin->user_limit = $request->input('user_limit');
    $plantadmin->alarm_limit = $request->input('alarm_limit');
    $plantadmin->routes_limit = $request->input('routes_limit');
    $plantadmin->created_by = $request->input('created_by');

        
    $plantadmin->save();


    return redirect()->route('plantadmin.index');
    }
    /*
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
   {
       
        try
        {
            DB::table("users")->where('id',$id)->delete();
            return redirect()->route('plantadmin.index')
            ->with('success','Plant admin deleted successfully');
        }
        catch(Exception $e)
        {
            $err = json_encode($e);
            $err1 = json_decode($err,true);
            if($err1['errorInfo'][1] == 1451)
            {
                return Response::json(array('success' => false, 'msg_string' => 'Plant Admin is in use.'), 500);
            }
          
        } 


    }

}