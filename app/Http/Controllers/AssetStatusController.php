<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Permission;
use App\User;
use App\Model\Asset;
use App\Model\AssetStatus;
use App\Model\Status;
use DB;
use Hash;
use App\Post;
use Yajra\Datatables\Datatables;
use Response;
use Helper;

class AssetStatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
         $this->middleware('permission:assetstatus-list|assetstatus-create|assetstatus-edit|assetstatus-delete', ['only' => ['index','store']]);
         
         $this->middleware('permission:assetstatus-create', ['only' => ['create','store']]);
         $this->middleware('permission:assetstatus-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:assetstatus-delete', ['only' => ['destroy']]);
         return view('permission-error');
    }
    public function index(Request $request)
           {

            if($request->ajax())
            {
                $data = AssetStatus::latest()->get();

                foreach ($data as $key => $value) 
                {
                    $data[$key]['asset_id']= Helper::assetIdToName($value['asset_id']);
                }
                foreach ($data as $key => $value) 
                {
                    $data[$key]['status_id']= Helper::statusIdToDes($value['status_id']);
                }


                return Datatables::of($data)
                  ->addColumn('action', 'datatables.action-button')
                  ->rawColumns(['action'])
                  ->make(true);
            }    
             return view('assetstatus.index');  
        }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
     {
        $asset=Asset::all()->pluck('name','id');
        $status=Status::all()->pluck('description','id');
        return view('assetstatus.create', compact('asset','status'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'asset_id' => 'required',
            'status_id' => 'required',
        ]);

        $input = $request->all();

        // dd ($input);
        
        $assetstatus = AssetStatus::create($input);

        return redirect()->route('assetstatus.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
   {
        $assetstatus = AssetStatus::find($id);
        $assetstatus->asset_id = Helper::assetIdToName($assetstatus->asset_id);
        $assetstatus->status_id = Helper::statusIdToDes($assetstatus->status_id);
        return view('assetstatus.show',compact('assetstatus'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
  {
        $asset=Asset::all()->pluck('name','id');
        $status=Status::all()->pluck('description','id');
        $assetstatus = AssetStatus::find($id);
        return view('assetstatus.edit',compact('assetstatus','asset','status'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'asset_id' => 'required',
            'status_id' => 'required',

        ]);

        $assetstatus = AssetStatus::find($id);
        $assetstatus->asset_id = $request->input('asset_id');
        $assetstatus->status_id = $request->input('status_id');
        $assetstatus->save();


        return redirect()->route('assetstatus.index');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
           DB::table("plant_status")->where('id',$id)->delete();
    }
}
