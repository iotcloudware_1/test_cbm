<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\FormBuilder;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Plant;
use App\Model\Deviation;
use App\Post;
use App\User;
use Auth;
use Yajra\Datatables\Datatables;
use DB;
use Response;
Use Exception;
use Hash;


class DeviationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     function __construct()
    {
         $this->middleware('permission:deviation-list|deviation-show||deviation-delete', ['only' => ['index','store']]);
         
         $this->middleware('permission:deviation-create', ['only' => ['create','store']]);
         $this->middleware('permission:deviation-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:deviation-delete', ['only' => ['destroy']]);
         return view('permission-error');
    }


    public function index(Request $request)
    {

        if($request->ajax())
        {
            $data = Deviation::latest()->get();
            return Datatables::of($data)
            ->addColumn('action', 'datatables.deviation-action-button')
            ->rawColumns(['action'])
            ->make(true);
        }    
        return view('deviations.index');

        //     ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $deviation = Deviation::find($id);
        return view('deviations.show',compact('deviation'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try
        {
            DB::table("deviations")->where('id',$id)->delete();
            return redirect()->route('deviations.index')
             ->with('success','Deviations deleted successfully');
        }
        catch(Exception $e)
        {
            $err = json_encode($e);
            $err_array = json_decode($err,true);
            if($err_array['errorInfo'][1] == 1451)
            {
                return Response::json(array('success' => false, 'msg_string' => 'Deviations is in use.'), 500);
            }
          
        } 
    }
}
