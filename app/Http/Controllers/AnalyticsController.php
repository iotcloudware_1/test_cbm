<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Charts\DefaultChart;
use App\Model\ParameterValues;
use App\Providers\ChartsServiceProvider;
use Auth;
use Carbon\Carbon;
use Yajra\Datatables\Datatables;
use DB;

class AnalyticsController extends Controller
{
    public function singleParameter()
    {
		$chart_value= ParameterValues::pluck('value');
		$labels= ParameterValues::pluck('created_at');
		foreach ($labels as $key => $label) 
		{
			$labels[$key]= Carbon::parse($label)->format('Y-m-d H:i:s');
		}
			

		// echo($chart_value);	

		$chart = new DefaultChart;
		$chart->labels($labels);
		$chart->dataset('My dataset', 'line', $chart_value)
		->fill(false)
		->color('purple')
		->lineTension(1);
		// $chart->backgroundColor('blue');
		// $chart->dataset('My dataset 2', 'line', [4, 3, 2, 1]);



    	return view('analytics.single-analytics', compact('chart'));
    }
}
