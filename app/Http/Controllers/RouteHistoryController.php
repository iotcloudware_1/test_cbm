<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\WorkOrder;
use App\Model\InstrumentWorkOrder;
use App\Model\Instrument; 
use App\Model\RouteHistory;
use App\Model\ReportWorkOrder;
use App\Model\WorkOrderHistory;
use App\Model\RouteWorkOrder;
use App\Model\Route;
use App\Model\RouteInstrumentSkipped; 
use App\Post;
use App\User;
use Auth;
use Yajra\Datatables\Datatables;
use DB;
use Response;
Use Exception;
use Hash;
use Helper;


class RouteHistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax())
        {
            $data = RouteHistory::latest()->get();

            foreach ($data as $key => $value) {
                $data[$key]['operator_id']= Helper::userIdToName($value['operator_id']);
                $data[$key]['route_id']= Helper::routeIdToDes($value['route_id']);
            }
            return Datatables::of($data)
              ->addColumn('action', 'datatables.route-history-action-button-datatable')
              ->rawColumns(['action'])
              ->make(true);
        }    
        return view('routehistory.index');
    }

    public function getSkippedInstrument(Request $request)
    {
        $skipped_instrument= RouteInstrumentSkipped::where('route_history_id',$request['id'])->get(); 

        foreach ($skipped_instrument as $key => $value) 
        {   
            $skipped_instrument[$key]['route_history_id']= Helper::routeHisIdToDes($value['route_history_id']);
            $skipped_instrument[$key]['instrument_id']= Helper::instrumentIdToName($value['instrument_id']);
            $skipped_instrument[$key]['skipped_id']= Helper::skipReasonToName($value['skipped_id']);

        }     



        return view('skipreasons.skipped-instrument',compact('skipped_instrument'));
    }
}
